u"""
SIPPY: Stellar Interpolation Package for PYthon
Author: Lyra Cao
A simple software package to interpolate freely between stellar parameters.
3/9/2018
"""

from __future__ import absolute_import
import interpolator
import calculator
