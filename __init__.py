from __future__ import absolute_import
__version__ = u"0.0.1"

__all__ = [u"interpolator", u"calculator"]
from sippy import interpolator
from sippy import calculator

# Debug:
#from sippy import example
