"""
sippy.interpolator.BCAHInterpolator:
Interpolator for the BCAH isochrones.

Author: Lyra Cao
4/12/2018
"""

import numpy as np
from astropy import units as u
import re, os
from io import StringIO, BytesIO
from sippy.interpolator.BaseInterpolator import *
import scipy.optimize
import random

class BCAH98IsochroneInterpolator(BaseIsochroneInterpolator):
    """Performs 2D interpolation on BCAH"""
    filename = "BCAH/BCAH98_iso.3_extend"
    default_units = \
    {"logage": u.dimensionless_unscaled,
    #"logage": u.dex(u.yr),
     "mass": u.Msun,
     #"logT": u.dex(u.K),
     #"logL": u.dex(u.Lsun)
     "logT": u.dimensionless_unscaled,
     "logL": u.dimensionless_unscaled}
    # The following column headers are from load_data, ordered.
    column_headers = ["logage", "mass", "logT", "logL"]
    comments = "-"*80
    @classmethod
    def load_data(self):
        """Loads and returns BCAH98.
        Currently, BCAH98 tracks correspond to 994 points in isochrone space."""
        filename = os.path.dirname(os.path.abspath(__file__))\
                 + "/Data/" + self.filename
        with open(filename, "r") as f:
            isotxt = f.read()
            isotxt = isotxt.split("log t (yr) =  ")[1:]
            points = [] #Age (yr), Mass (Msun), logT, logL
        for idx, power_age in enumerate(isotxt):
            curr_logage = float(power_age.split("\n")[0])
            raw_points =\
            np.loadtxt(isotxt[idx].splitlines(),
                          comments=self.comments, usecols=(0,1,3),
                          skiprows=3).tolist()
            for point in raw_points:
                points.append([curr_logage, point[0], np.log10(point[1]), point[2]]) #Get in logT format
        return np.array(points)

class BCAH15IsochroneInterpolator(BaseIsochroneInterpolator):
    """Performs 2D interpolation on BCAH"""
    filename = "BCAH/BHAC15_tracks+structure"
    default_units = \
    {"logage": u.dimensionless_unscaled,
    #"logage": u.dex(u.yr),
     "mass": u.Msun,
     #"logT": u.dex(u.K),
     #"logL": u.dex(u.Lsun)
     "logT": u.dimensionless_unscaled,
     "logL": u.dimensionless_unscaled}
    # The following column headers are from load_data, ordered.
    column_headers = ["logage", "mass", "logT", "logL"]
    base_column_headers = ["mass", "logage", "Teff", "logL", "g", "radius", "log_Lithium", "log_central_Temp", "log_central_rho", "M_rad", "R_rad", "k2conv", "k2rad"]
    comments = "!"
    max_logage = 8.
    tol = 1.5e-1
    @classmethod
    def change_max_logage(self, logage):
        self.max_logage = logage
    @classmethod
    def load_data(self):
        """Loads and returns BCAH15."""
        filename = os.path.dirname(os.path.abspath(__file__))\
                 + "/Data/" + self.filename
        points = [] #Age (yr), Mass (Msun), logT, logL
        raw_points =\
        np.loadtxt(filename, comments=self.comments, skiprows = 44)
        for point in raw_points:
            if point[self.base_column_headers.index("logage")] < self.max_logage + self.tol:
                point_tuple =\
                [point[self.base_column_headers.index("logage")],
                point[self.base_column_headers.index("mass")],
                np.log10(point[self.base_column_headers.index("Teff")]),
                point[self.base_column_headers.index("logL")]]
                # print point_tuple[0], point_tuple[1], np.logical_not(np.isnan(point_tuple))
                if np.all(np.logical_not(np.isnan(point_tuple))):
                    points.append(point_tuple) #Get in logT format
        return np.array(points)
