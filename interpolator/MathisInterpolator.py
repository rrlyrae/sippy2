"""
Mathis (1990) Extinction Law Interpolator

Provides A_lambda/A_V calculations between 0.002 microns to 250 microns.

Default input unit: micron
Default output unit: A_lambda/A_V (ratio)
"""

from __future__ import division
from __future__ import absolute_import
from sippy.interpolator.BaseInterpolator import *
import os
import numpy as np
import scipy
import scipy.interpolate
import matplotlib.pyplot as plt
from astropy import units as u
from itertools import izip

class MathisInterpolator(BaseInterpolator):
    """MathisInterpolator.py: Interpolator for the Mathis (1990)
    Extinction Law, provided by `extinction.mathis.dat`

    interpolation_kind can be 'linear', 'nearest', 'slinear', 'quadratic',
    'cubic', from the scipy documentation."""
    _default_input_unit = u.micron
    _default_output_unit = u.dimensionless_unscaled
    def __init__(self, interpolation_kind = "linear",
                 output_unit = u.dimensionless_unscaled):
        #Set the units to output in
        self.output_unit = output_unit

        # Load in the data
        filename = os.path.dirname(os.path.abspath(__file__))\
                 + "/Data/" + "extinction.mathis.dat"
        load_data = np.loadtxt(filename, comments = "#")
        # Load the wavelength values from the file, in microns
        wavelength = load_data.T[0]
        # Load the extinction values, A_lambda/A_J
        A_lambda_div_A_J = load_data.T[1]
        # Convert from A_lambda/A_J to A_lambda/A_V
        A_lambda_div_A_V = A_lambda_div_A_J/3.55
        # Formalize inputs and outputs
        input_interp = wavelength
        output_interp = A_lambda_div_A_V
        ### Making sure to only expose the non-NaN values.
        # We obtain the indices at which neither the input or output interp
        # are NaN by performing an AND on the valid entries --- rejecting
        # an index if either the input or output interpolations are NaN.
        valid_indices =\
        np.logical_and(np.logical_not(np.isnan(input_interp)),
                       np.logical_not(np.isnan(output_interp)))
        # Sort interpolation arrays prior to interpolation:
        sorted_output_interp_trimmed =\
        [output_int for input_int, output_int in\
         sorted(izip(input_interp[valid_indices],
                    output_interp[valid_indices]),
                key = lambda x: x[0])]
        sorted_input_interp_trimmed =\
        sorted(input_interp[valid_indices])
        # Remove duplicates from the target input_interp:
        tiny = 1e-15
        sorted_input_interp_trimmed_no_duplicates = []
        sorted_output_interp_trimmed_no_duplicates = []
        for index in xrange(1, len(sorted_input_interp_trimmed)):
            if abs(sorted_input_interp_trimmed[index] -\
                   sorted_input_interp_trimmed[index-1]) > tiny:
                sorted_input_interp_trimmed_no_duplicates.\
                append(sorted_input_interp_trimmed[index])
                sorted_output_interp_trimmed_no_duplicates.\
                append(sorted_output_interp_trimmed[index])
        # Expose the interpolation evaluation method.
        self._evaluate =\
        scipy.interpolate.interp1d(sorted_input_interp_trimmed_no_duplicates,
                                   sorted_output_interp_trimmed_no_duplicates,
                                   kind = interpolation_kind,
                                   bounds_error = False,
                                   fill_value = np.nan, assume_sorted = True)
