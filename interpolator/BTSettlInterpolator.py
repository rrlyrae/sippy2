"""
BT-Settl Color Table Interpolator [Allard (2014), Baraffe (2015)]
"""

from __future__ import absolute_import
from sippy.interpolator.BaseInterpolator import *
import os
import numpy as np
import scipy
import scipy.interpolate
import matplotlib.pyplot as plt
from astropy import units as u
import itertools as it
from itertools import izip

class BTSettlBaseInterpolator(BaseInterpolator):
    """The abstract base interpolator for the BTSettl (2013)
    Color Tables.

    Do not instantiate this class --- instantiate the child BTSettl classes
    instead.
    """
    def load_data(self, filename):
        raise NotImplementedError
    def __init__(self, input, output, logg = 4.43812,
                 interpolation_kind = "linear",
                 output_unit = None):
        """Returns a BT Settl interpolator with the defined values.
        Note: logg needs to be set. (Usually defined between 2.5-5.5.)
        The color is interpolated to the provided version of logg.

        A solar model is the default, at 4.43812, calculated by Eric Mamajek."""
        # Set the units to output in
        self.output_unit = output_unit

        # Load in the data
        filename = os.path.dirname(os.path.abspath(__file__))\
                 + "/Data/" + self.filename
        preload_data = self.load_data(filename)
        column_headers = self.column_headers

        ### We need to select/interpolate all colors for a given logg.
        load_data = []
        load_dictionary = {}
        for row in preload_data:
            # Find the indices of the columns corresponding to "Teff" and "logg"
            Teff_index = column_headers.index("Teff")
            Logg_index = column_headers.index("Logg")
            # If the Teff value is not in the dictionary,
            # add the row under a list.
            if row[Teff_index] not in load_dictionary:
                load_dictionary[row[Teff_index]] = [row]
            else:
                #It is in the dictionary, so append it to the existing list.
                load_dictionary[row[Teff_index]].append(row)

        # Iterate over all distinct temperatures:
        for Teff in load_dictionary:
            # Initialize a row.
            # Recall, the desired value of logg is stored as logg from arguments
            new_row = np.zeros(len(column_headers)) + np.nan
            new_row[Teff_index] = Teff
            new_row[Logg_index] = logg
            # Iterate over all the column entries:
            for index in xrange(len(column_headers)):
                # Skip the columns corresponding to Teff and logg:
                if index == Teff_index or index == Logg_index:
                    pass
                else:
                    # Iterate over all the rows in this part of the dictionary.
                    # Create an interpolator and evaluate it with the
                    # requested logg.
                    index_values = np.array(\
                    [temp_row[index] for temp_row in load_dictionary[Teff]])
                    logg_values = np.array(\
                    [temp_row[Logg_index] for temp_row in load_dictionary[Teff]\
                     ])

                    # Check to see if there are enough points to interpolate.
                    necessary_points = {
                            "linear": 1,
                            "cubic": 4,
                            "quadratic": 3,
                            "nearest": 0,
                            "zero": 0,
                            "slinear": 1
                        }

                    if len(index_values) > \
                    necessary_points.get(interpolation_kind, interpolation_kind):
                        ### Making sure to only expose the non-NaN values.
                        # Get the indices at which neither the input/output
                        # are NaN by performing an AND on valid entries ---
                        # rejecting an index if either the input or output
                        # interpolations are NaN.
                        valid_indices =\
                        np.logical_and(np.logical_not(np.isnan(logg_values)),
                                       np.logical_not(np.isnan(index_values)))
                        # Sort the logg_values and index_values prior to
                        # interpolation.
                        sorted_index_values_trimmed =\
                        [index_value for logg_value, index_value in\
                         sorted(izip(logg_values[valid_indices],
                                    index_values[valid_indices]),
                                key = lambda x: x[0])]
                        sorted_logg_values_trimmed =\
                        sorted(logg_values[valid_indices])
                        result =\
                        scipy.interpolate.interp1d(sorted_logg_values_trimmed,
                                                   sorted_index_values_trimmed,
                                                   kind = interpolation_kind,
                                                   bounds_error = False,
                                                   fill_value = np.nan,
                                                   assume_sorted = True)(logg)
                    else:
                        # Can't interpolate with just one point.
                        result = np.NaN
                    new_row[index] = result
            # Append this new row to the empty data that we are constructing.
            load_data.append(new_row)

        # We need to explicitly calculate colors from the photometry.

        # If the input is a color:
        input_color = input.split("-")
        if len(input_color) > 1:
            # The desired input is a color. Check if it is a valid color.
            assert len(input_color) == 2, "Wrong length for input color."
            assert input_color[0] in self.bands\
            and input_color[1] in self.bands,\
            "Colors with invalid photometric bands. Valid bands: "+\
            repr(self.bands)
            # Use the default units for the chosen input color.
            # Since the operation is subtraction, assume they are the same
            # unit:
            self._default_input_unit = self.default_units[input_color[0]]
            ###Now, set the input interpolator.
            # First, find the index of each of the photometric bands.
            input_color0_index = column_headers.index(input_color[0])
            input_color1_index = column_headers.index(input_color[1])
            # Next, simply subtract the values for each row, evaluating
            # Band1 - Band2 in each.
            input_interp =\
            np.array([aa[input_color0_index] - aa[input_color1_index] for \
                      aa in load_data])
        else:
            # Input is not a color.
            assert input in column_headers,\
            "Unsupported column headers chosen for input: " + repr(input) + " \n"+\
            "Valid options: "+repr(column_headers)
            # Use the default units for the chosen input column.
            self._default_input_unit = self.default_units[input]
            # Find the indices of the columns that correspond to input.
            input_index = column_headers.index(input)
            # Set the input interpolator.
            input_interp = np.array([aa[input_index] for aa in load_data])

        # If the output is a color:
        output_color = output.split("-")
        if len(output_color) > 1:
            # The desired output is a color. Check if it is a valid color.
            assert len(output_color) == 2, "Wrong length for output color."
            assert output_color[0] in self.bands\
            and output_color[1] in self.bands,\
            "Colors with invalid photometric bands. Valid bands: "+\
            repr(self.bands)
            # Use the default units for the chosen output color.
            # Since the operation is subtraction, assume they are the same
            # unit:
            self._default_output_unit = self.default_units[output_color[0]]
            ###Now, set the output interpolator.
            # First, find the index of each of the photometric bands.
            output_color0_index = column_headers.index(output_color[0])
            output_color1_index = column_headers.index(output_color[1])
            # Next, simply subtract the values for each row, evaluating
            # Band1 - Band2 in each.
            output_interp =\
            np.array([aa[output_color0_index] - aa[output_color1_index] for \
                      aa in load_data])
        else:
            # output is not a color.
            assert output in column_headers,\
            "Unsupported column header chosen for output. Choice: "+output+\
            " Valid options: "+repr(column_headers)
            # Use the default units for the chosen output column.
            self._default_output_unit = self.default_units[output]
            # Find the indices of the columns that correspond to output.
            output_index = column_headers.index(output)
            # Set the output interpolator.
            output_interp = np.array([aa[output_index] for aa in load_data])

        ## If the output unit is unset, then assume that the default units
        ## are what's requested.

        if output_unit is None:
            self.output_unit = self._default_output_unit

        ### Making sure to only expose the non-NaN values.
        # We obtain the indices at which neither the input or output interp
        # are NaN by performing an AND on the valid entries --- rejecting
        # an index if either the input or output interpolations are NaN.
        valid_indices =\
        np.logical_and(np.logical_not(np.isnan(input_interp)),
                       np.logical_not(np.isnan(output_interp)))
        # Sort interpolation arrays prior to interpolation:
        sorted_output_interp_trimmed =\
        [output_int for input_int, output_int in\
         sorted(izip(input_interp[valid_indices],
                    output_interp[valid_indices]),
                key = lambda x: x[0])]
        sorted_input_interp_trimmed =\
        sorted(input_interp[valid_indices])
        # Expose the interpolation evaluation method.
        self._evaluate =\
        scipy.interpolate.interp1d(sorted_input_interp_trimmed,
                                   sorted_output_interp_trimmed,
                                   kind = interpolation_kind,
                                   bounds_error = False,
                                   fill_value = np.nan, assume_sorted = True)

class BTSettlSDSSInterpolator(BTSettlBaseInterpolator):
    """Sloan (ugriz) interpolator for the BT Settl theoretical colors.

    "input" and "output" can be one of the following strings:

    Teff, Logg, [M/H], [a/H], u, g, r, i, z

    Or any color combination between the photometric bands:

    u,g,r,i,z

    interpolation_kind can be 'linear', 'nearest', 'slinear', 'quadratic',
    'cubic', from the scipy documentation."""
    # Define the default units of each column.
    default_units = \
    {"Teff": u.K,
     "Logg": u.dimensionless_unscaled,
     "[M/H]": u.dimensionless_unscaled,
     "[a/H]": u.dimensionless_unscaled,
     "u": u.mag,
     "g": u.mag,
     "r": u.mag,
     "i": u.mag,
     "z": u.mag}
    filename = "colmag.BT-Settl.server.SDSS.Vega"
    # The following column headers are directly from the file, and ordered:
    column_headers = ["Teff", "Logg", "[M/H]", "[a/H]", "u", "g", "r", "i",
    "z", "u1", "u2", "u3", "u4", "u5", "u6", "g1", "g2", "g3", "g4", "g5",
    "g6", "r1", "r2", "r3", "r4", "r5", "r6", "i1", "i2", "i3", "i4", "i5",
    "i6", "z1", "z2", "z3", "z4", "z5", "z6"]
    # Denote the explicit photometric bands here:
    bands = ["u", "g", "r", "i", "z"]
    available_colors = ["-".join(x) for x in it.combinations(bands, 2)]
    # No need to deal explicitly with missing values or filling values
    # by default
    missing_values = None
    filling_values = None
    # Skip 0 lines before reading in data:
    skip_header = 0
    # Read the entire file in:
    max_rows = None
    # Use default whitespace to delimit entries:
    delimiter = None
    # Comments are '!'
    comments = "!"
    @classmethod
    def load_data(self, filename):
        """Return the loaded data, according to the datatypes, missing values,
        and other parameters defined in the class, and parsed by row."""
        return np.genfromtxt(filename, comments=self.comments,
                             delimiter=self.delimiter,
                             skip_header=self.skip_header,
                             max_rows=self.max_rows,
                             missing_values=self.missing_values,
                             filling_values=self.filling_values)

class BTSettl2MASSInterpolator(BTSettlBaseInterpolator):
    """2MASS (JHK) interpolator for the BT Settl theoretical colors.

    "input" and "output" can be one of the following strings:

    Teff, Logg, [M/H], [a/H], J, H, K

    Or any color combination between the photometric bands:

    J, H, K

    interpolation_kind can be 'linear', 'nearest', 'slinear', 'quadratic',
    'cubic', from the scipy documentation."""
    # Define the default units of each column.
    default_units = \
    {"Teff": u.K,
     "Logg": u.dimensionless_unscaled,
     "[M/H]": u.dimensionless_unscaled,
     "[a/H]": u.dimensionless_unscaled,
     "J": u.mag,
     "H": u.mag,
     "Ks": u.mag}
    filename = "colmag.BT-Settl.server.2MASS.Vega"
    # The following column headers are directly from the file, and ordered:
    column_headers = ["Teff", "Logg", "[M/H]", "[a/H]", "J", "H", "Ks", "J-H", "J-Ks",
                      "H-K"]
    # Denote the explicit photometric bands here:
    bands = ["J", "H", "Ks"]
    available_colors = ["-".join(x) for x in it.combinations(bands, 2)]
    # No need to deal explicitly with missing values or filling values
    # by default
    missing_values = None
    filling_values = None
    # Skip 0 lines before reading in data:
    skip_header = 0
    # Read the entire file in:
    max_rows = None
    # Use default whitespace to delimit entries:
    delimiter = None
    # Comments are '!'
    comments = "!"
    @classmethod
    def load_data(self, filename):
        """Return the loaded data, according to the datatypes, missing values,
        and other parameters defined in the class, and parsed by row."""
        return np.genfromtxt(filename, comments=self.comments,
                             delimiter=self.delimiter,
                             skip_header=self.skip_header,
                             max_rows=self.max_rows,
                             missing_values=self.missing_values,
                             filling_values=self.filling_values)

class BTSettlJohnsonInterpolator(BTSettlBaseInterpolator):
    """Johnson-Cousins (UBV Rc Ic) interpolator for the BT Settl theoretical colors.

    "input" and "output" can be one of the following strings:

    Teff, Logg, [M/H], [a/H], U, B, V, Rc, Ic

    Or any color combination between the photometric bands:

    U, B, V, Rc, Ic

    interpolation_kind can be 'linear', 'nearest', 'slinear', 'quadratic',
    'cubic', from the scipy documentation."""
    # Define the default units of each column.
    default_units = \
    {"Teff": u.K,
     "Logg": u.dimensionless_unscaled,
     "[M/H]": u.dimensionless_unscaled,
     "[a/H]": u.dimensionless_unscaled,
     "U": u.mag,
     "B": u.mag,
     "V": u.mag,
     "R": u.mag,
     "I": u.mag,
     "BCV": u.mag}
    filename = "colmag.BT-Settl.server.JOHNSON.Vega"
    # The following column headers are directly from the file, and ordered:
    # Note: the column header i was changed to I.
    column_headers = ["Teff", "Logg", "[M/H]", "[a/H]", "U", "B", "V", "R", "I",
                      "(R-I)", "(I-J)", "(J-K)", "(K-L)", "(K-LL)", "(K-M)", "(K-N)",
                      "?", "BCV"]
    # Denote the explicit photometric bands here:
    bands = ["U", "B", "V", "R", "I"]
    available_colors = ["-".join(x) for x in it.combinations(bands, 2)]
    # No need to deal explicitly with missing values or filling values
    # by default
    missing_values = None
    filling_values = None
    # Skip 0 lines before reading in data:
    skip_header = 0
    # Read the entire file in:
    max_rows = None
    # Use default whitespace to delimit entries:
    delimiter = None
    # Comments are '!'
    comments = "!"
    @classmethod
    def load_data(self, filename):
        """Return the loaded data, according to the datatypes, missing values,
        and other parameters defined in the class, and parsed by row."""
        return np.genfromtxt(filename, comments=self.comments,
                             delimiter=self.delimiter,
                             skip_header=self.skip_header,
                             max_rows=self.max_rows,
                             missing_values=self.missing_values,
                             filling_values=self.filling_values)

class BTSettlLandoltInterpolator(BTSettlBaseInterpolator):
    """Landolt (UBV Rc Ic) interpolator for the BT Settl theoretical colors.

    "input" and "output" can be one of the following strings:

    Teff, Logg, [M/H], [a/H], U, B, V, Rc, Ic

    Or any color combination between the photometric bands:

    U, B, V, Rc, Ic

    interpolation_kind can be 'linear', 'nearest', 'slinear', 'quadratic',
    'cubic', from the scipy documentation."""
    # Define the default units of each column.
    default_units = \
    {"Teff": u.K,
     "Logg": u.dimensionless_unscaled,
     "[M/H]": u.dimensionless_unscaled,
     "[a/H]": u.dimensionless_unscaled,
     "U": u.mag,
     "B": u.mag,
     "V": u.mag,
     "Rc": u.mag,
     "Ic": u.mag}
    filename = "colmag.BT-Settl.server.LANDOLT.Vega"
    # The following column headers are directly from the file, and ordered:
    # Note: the column header i was changed to I.
    column_headers = ["Teff", "Logg", "[M/H]", "[a/H]", "U", "B", "V", "Rc",
                      "Ic"]
    # Denote the explicit photometric bands here:
    bands = ["U", "B", "V", "Rc", "Ic"]
    available_colors = ["-".join(x) for x in it.combinations(bands, 2)]
    # No need to deal explicitly with missing values or filling values
    # by default
    missing_values = None
    filling_values = None
    # Skip 0 lines before reading in data:
    skip_header = 0
    # Read the entire file in:
    max_rows = None
    # Use default whitespace to delimit entries:
    delimiter = None
    # Comments are '!'
    comments = "!"
    @classmethod
    def load_data(self, filename):
        """Return the loaded data, according to the datatypes, missing values,
        and other parameters defined in the class, and parsed by row."""
        return np.genfromtxt(filename, comments=self.comments,
                             delimiter=self.delimiter,
                             skip_header=self.skip_header,
                             max_rows=self.max_rows,
                             missing_values=self.missing_values,
                             filling_values=self.filling_values)

class BTSettlInterpolator(BTSettlBaseInterpolator):
    """BT Settl Interpolator including all possible BT Settl colors.
    Merges all BT Settl interpolators, and presents the combination.

    Note: since this is a OOP pattern, filename is a dummy variable."""
    filename = ""
    BTSettlInterpolators = [BTSettlSDSSInterpolator,
                            BTSettl2MASSInterpolator,
                            BTSettlJohnsonInterpolator,
                            BTSettlLandoltInterpolator]
    @classmethod
    def preinit(self):
        """Run this method to pre-initialize BTSettlInterpolator,
        in case methods require column headers and bands."""
        self.column_headers = []
        self.bands = []
        self.default_units = {}

        for iter_num, BTSettlInterp in enumerate(self.BTSettlInterpolators):
            # Copy default units:
            for default_unit in BTSettlInterp.default_units:
                self.default_units[default_unit] =\
                BTSettlInterp.default_units[default_unit]

            self.column_headers += BTSettlInterp.column_headers
            # Denote the explicit photometric bands here:
            self.bands += BTSettlInterp.bands

            # Load in the data
            filename = os.path.dirname(os.path.abspath(__file__))\
                     + "/Data/" + BTSettlInterp.filename

            if iter_num == 0:
                self.data = np.array(BTSettlInterp.load_data(filename))
            else:
                self.data =\
                np.concatenate((self.data,
                np.array(BTSettlInterp.load_data(filename))),
                axis = 1)

        # Recompute total available colors:
        self.available_colors =\
        ["-".join(x) for x in it.combinations(list(set(self.bands)), 2)]

        ### Assign each a default unit:
        for available_color in self.available_colors:
            self.default_units[available_color] = u.mag
    def __init__(self, input, output, logg = 4.43812,
                 interpolation_kind = "linear",
                 output_unit = None):
        self.preinit()

        return \
        super(BTSettlInterpolator, self).\
        __init__(input, output,
                 logg = logg,
                 interpolation_kind = interpolation_kind,
                 output_unit = output_unit)

    def load_data(self, filename):
        """Return the combination of all BT Settl color tables."""
        return self.data


class BTSettlBCVInterpolator(BaseInterpolator):
    BCVs = None
    teffs = None
    filename = "colmag.BT-Settl.server.COUSINS.Vega.bolcor"
    # Define the default units of each column.
    default_units = \
    {"Teff": u.K,
     "Logg": u.dimensionless_unscaled,
     "[M/H]": u.dimensionless_unscaled,
     "[a/H]": u.dimensionless_unscaled,
     "B": u.mag,
     "V": u.mag,
     "R": u.mag,
     "I": u.mag,
     "BCV": u.mag}
    # The following column headers are directly from the file, and ordered:
    column_headers = ["Teff", "Logg", "[M/H]", "[a/H]", "B", "V", "R", "I"]
    # Denote the explicit photometric bands here:
    bands = ["B", "V", "R", "I"]
    available_colors = ["BCV"]
    # No need to deal explicitly with missing values or filling values
    # by default
    missing_values = None
    filling_values = None
    # Skip 0 lines before reading in data:
    skip_header = 0
    # Read the entire file in:
    max_rows = None
    # Use default whitespace to delimit entries:
    delimiter = None
    # Comments are '!'
    comments = "!"
    def preinit(self, logg = 4.43812, FeH = 0., tiny = 1e-3, num_points = 1000, interpolation_kind = "linear"):
        """Run this method to pre-initialize BTSettlBCVInterpolator with a specific logg and FeH."""
        loggs = np.array([-0.5,0.,0.5,1.,1.5,2.,2.5,3.,3.5,4.,4.5,5.,5.5,6.])
        fehs = np.array([-2.5, -2., -1.5, -1., -.5, 0., 0.5])

        # if np.min(np.abs(loggs - logg)) < tiny:
        #     logg_val = loggs[np.argmin(np.abs(loggs - logg))]

        if np.any(logg < loggs) and np.any(logg > loggs) and np.any(FeH < fehs) and np.any(FeH > fehs):

            logg_val_left = np.max(loggs[loggs < logg])
            logg_val_right = np.min(loggs[loggs > logg])
            ### Assuming they are not the same value, these are the bounds.

            feh_val_left = np.max(fehs[fehs < FeH])
            feh_val_right = np.min(fehs[fehs > FeH])

            ### Find the arrays corresponding to the corners of the interpolation box.
            # Set the units to output in
            # self.output_unit = output_unit

            # Load in the data
            filename = os.path.dirname(os.path.abspath(__file__))\
                     + "/Data/" + self.filename
            preload_data = np.loadtxt(filename, comments = self.comments, skiprows = self.skip_header)

            column_headers = self.column_headers

            ### We need to select/interpolate BCV for a given logg, FeH.

            Teff_index = column_headers.index("Teff")
            Logg_index = column_headers.index("Logg")
            feh_index = column_headers.index("[M/H]")
            bcv_index = column_headers.index("V")

            teff_data = preload_data.T[Teff_index]
            logg_data = preload_data.T[Logg_index]
            feh_data = preload_data.T[feh_index]
            BCVs = -10.*np.log10(teff_data/5777)+(preload_data.T[bcv_index])

            resample_teff = np.logspace(np.log10(np.min(teff_data)), np.log10(np.max(teff_data)), num_points)
            # print "resample_teff", resample_teff

            teffs_low_logg_low_feh =\
            teff_data[np.logical_and(\
            np.abs(logg_data - logg_val_left) < tiny,\
            np.abs(feh_data - feh_val_left) < tiny)]

            BCVs_low_logg_low_feh =\
            BCVs[np.logical_and(\
            np.abs(logg_data - logg_val_left) < tiny,\
            np.abs(feh_data - feh_val_left) < tiny)]

            teffs_low_logg_high_feh =\
            teff_data[np.logical_and(\
            np.abs(logg_data - logg_val_left) < tiny,\
            np.abs(feh_data - feh_val_right) < tiny)]

            BCVs_low_logg_high_feh =\
            BCVs[np.logical_and(\
            np.abs(logg_data - logg_val_left) < tiny,\
            np.abs(feh_data - feh_val_right) < tiny)]

            teffs_high_logg_low_feh =\
            teff_data[np.logical_and(\
            np.abs(logg_data - logg_val_right) < tiny,\
            np.abs(feh_data - feh_val_left) < tiny)]

            BCVs_high_logg_low_feh =\
            BCVs[np.logical_and(\
            np.abs(logg_data - logg_val_right) < tiny,\
            np.abs(feh_data - feh_val_left) < tiny)]

            teffs_high_logg_high_feh =\
            teff_data[np.logical_and(\
            np.abs(logg_data - logg_val_right) < tiny,\
            np.abs(feh_data - feh_val_right) < tiny)]

            BCVs_high_logg_high_feh =\
            BCVs[np.logical_and(\
            np.abs(logg_data - logg_val_right) < tiny,\
            np.abs(feh_data - feh_val_right) < tiny)]

            points = []
            values = []

            points_dof = 0

            if len(teffs_low_logg_low_feh) > 0:
                BCVs_low_logg_low_feh_resampled =\
                scipy.interpolate.interp1d(teffs_low_logg_low_feh, BCVs_low_logg_low_feh, kind = interpolation_kind, bounds_error = False)(resample_teff)
                points += zip(resample_teff, \
                np.zeros_like(resample_teff) + logg_val_left, \
                np.zeros_like(resample_teff) + feh_val_left)

                values += BCVs_low_logg_low_feh_resampled.tolist()
                points_dof += 1

            if len(teffs_low_logg_high_feh) > 0:
                BCVs_low_logg_high_feh_resampled =\
                scipy.interpolate.interp1d(teffs_low_logg_high_feh, BCVs_low_logg_high_feh, kind = interpolation_kind, bounds_error = False)(resample_teff)

                points += zip(resample_teff, \
                np.zeros_like(resample_teff) + logg_val_left, \
                np.zeros_like(resample_teff) + feh_val_right)

                values += BCVs_low_logg_high_feh_resampled.tolist()
                points_dof += 1

            if len(teffs_high_logg_low_feh) > 0:
                BCVs_high_logg_low_feh_resampled =\
                scipy.interpolate.interp1d(teffs_high_logg_low_feh, BCVs_high_logg_low_feh, kind = interpolation_kind, bounds_error = False)(resample_teff)

                points += zip(resample_teff, \
                np.zeros_like(resample_teff) + logg_val_right, \
                np.zeros_like(resample_teff) + feh_val_left)

                values += BCVs_high_logg_low_feh_resampled.tolist()
                points_dof += 1

            if len(teffs_high_logg_high_feh) > 0:
                BCVs_high_logg_high_feh_resampled =\
                scipy.interpolate.interp1d(teffs_high_logg_high_feh, BCVs_high_logg_high_feh, kind = interpolation_kind, bounds_error = False)(resample_teff)

                points += zip(resample_teff, \
                np.zeros_like(resample_teff) + logg_val_right, \
                np.zeros_like(resample_teff) + feh_val_right)

                values += BCVs_high_logg_high_feh_resampled.tolist()
                points_dof += 1

            # print "zip(BCVs_low_logg_low_feh_resampled, BCVs_low_logg_high_feh_resampled, BCVs_high_logg_low_feh_resampled, BCVs_high_logg_high_feh_resampled)", zip(BCVs_low_logg_low_feh_resampled, BCVs_low_logg_high_feh_resampled, BCVs_high_logg_low_feh_resampled, BCVs_high_logg_high_feh_resampled)
            #
            # ### If any of the four grid points is NaN, we still have a triangle. So --- calculate the interpolants of the four possible triangles.
            # # Interpolate along the long side. There are two possibilities --- low_logg_low_feh to high_logg_high_feh or low_logg_high_feh to high_logg_low_feh
            #
            # # Following is applicable for interpolations where NaN is highlow or lowhigh
            # # First we need to find which triangle the interpolation is in.
            #
            # BCVs_side_sym =\
            # BCVS_low_logg_low_feh_resampled + ()/ * (BCVs_high_logg_high_feh_resampled) #lowlow to highhigh
            #
            # BCVs_low_logg =\
            # BCVs_low_logg_low_feh_resampled + (FeH - feh_val_left)/feh_val_right * (BCVs_low_logg_high_feh_resampled - BCVs_low_logg_low_feh_resampled)
            #
            # BCVs_high_logg =\
            # BCVs_high_logg_low_feh_resampled + (FeH - feh_val_left)/feh_val_right * (BCVs_high_logg_high_feh_resampled - BCVs_high_logg_low_feh_resampled)
            #
            # BCVs_calculated =\
            # BCVs_low_logg + (logg - logg_val_left)/logg_val_right * (BCVs_high_logg - BCVs_low_logg)

            if points_dof > 2:
                BCVs_calculated = scipy.interpolate.griddata(points, values, (resample_teff, np.zeros_like(resample_teff) + logg, np.zeros_like(resample_teff) + FeH))

                self.BCVs = np.array(BCVs_calculated)
                self.teffs = np.array(resample_teff)
            else:
                self.teffs = np.array([-1e10,-1e10+1])
                self.BCVs = np.array([0.,0.])
        else:
            self.teffs = np.array([-1e10,-1e10+1])
            self.BCVs = np.array([0.,0.])
    def __init__(self, input, output, logg = 4.43812, FeH = 0., interpolation_kind = "linear", output_unit = None):
        self.output_unit = output_unit
        self._default_input_unit = self.default_units[input]
        self._default_output_unit = self.default_units[output]
        if output_unit is None:
            self.output_unit = self._default_output_unit
        assert input in ["BCV", "Teff"], "Input type not BCV or Teff"
        assert output in ["BCV", "Teff"], "Output type not BCV or Teff"
        if self.BCVs is not None:
            pass
        else:
            self.preinit(logg = logg, FeH = FeH, interpolation_kind = interpolation_kind)

        if input == "BCV":
            input_interp = self.BCVs
        elif input == "Teff":
            input_interp = self.teffs

        if output == "BCV":
            output_interp = self.BCVs
        elif output == "Teff":
            output_interp = self.teffs

        ### Use only for our conditionals non-nan elements.

        cond_valid = np.logical_and(np.logical_not(np.isnan(input_interp)), np.logical_not(np.isnan(output_interp)))

        # print zip(input_interp[cond_valid], output_interp[cond_valid])

        sorted_input_interp_trimmed = sorted(input_interp[cond_valid])
        sorted_output_interp_trimmed =\
        [y[1] for y in sorted(zip(input_interp[cond_valid], output_interp[cond_valid]), key = lambda x: x[0])]
        if len(sorted_input_interp_trimmed) > 0:
            self._evaluate =\
            scipy.interpolate.interp1d(sorted_input_interp_trimmed,
                                       sorted_output_interp_trimmed,
                                       kind = interpolation_kind,
                                       bounds_error = False,
                                       fill_value = np.nan, assume_sorted = True)
        else:
            self._evaluate =\
            scipy.interpolate.interp1d([-1e10,1e10+1],
                                       [0.,0.],
                                       kind = interpolation_kind,
                                       bounds_error = False,
                                       fill_value = np.nan, assume_sorted = True)
