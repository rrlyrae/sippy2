import numpy as np
import scipy
import scipy.interpolate

def PanSTARRS(Fe_H_val, logg_val, folder_prefix = "Data/C3K/PanSTARRS"):
    """Interpolates photometries to a given value for FeH and logg."""
    Fe_Hs =\
    [-4.00, -3.50, -3.00, -2.75, -2.50, -2.25, -2.00, -1.75, -1.50, -1.25,
    -1.00, -0.75, -0.50, -0.25, 0.00, 0.25, 0.50, 0.75]
    Loggs =\
    [-4.0, -3.0, -2.0, -1.5, -1.0, -0.5, 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0,
    3.5, 4.0, 4.5, 5.0, 5.5, 6.0, 6.5, 7.0, 7.5, 8.0, 8.5, 9.0, 9.5]
    Teffs = None
    column_headers = ["Teff","Logg","[Fe/H]","Av","Rv","BCg","BCr","BCi",
    "BCz","BCy","BCw","BCopen"]
    available_bolometric_corrections = ["BCg","BCr","BCi",
    "BCz","BCy","BCw","BCopen"]

    grid_BCs = {}

    BC_values = {}

    for bol_corr_text in available_bolometric_corrections:
        grid_BCs[bol_corr_text] = None

    for Fe_H in Fe_Hs:
        if Fe_H < 0:
            string_sgn = "m"
        else:
            string_sgn = "p"
        filename = "feh" + string_sgn + "%03i" % (np.abs(Fe_H)*100) + ".PanSTARRS"
        table = np.genfromtxt(folder_prefix + "/" + filename)
        if Teffs is None:
            Teffs = sorted(np.unique(table.T[column_headers.index("Teff")]))

        for bol_corr_text in available_bolometric_corrections:
            if grid_BCs[bol_corr_text] is None:
                grid_BCs[bol_corr_text] = table.T[column_headers.index(bol_corr_text)]
            else:
                grid_BCs[bol_corr_text] = np.concatenate((grid_BCs[bol_corr_text], table.T[column_headers.index(bol_corr_text)]))

    load_data = np.zeros((len(Teffs), len(column_headers)))
    for Teff_index in range(len(Teffs)):
        load_data[Teff_index][column_headers.index("Teff")] = Teffs[Teff_index]
        load_data[Teff_index][column_headers.index("Logg")] = logg_val
        load_data[Teff_index][column_headers.index("[Fe/H]")] = Fe_H_val
        load_data[Teff_index][column_headers.index("Rv")] = 3.1

    for bol_corr_text in available_bolometric_corrections:
        print grid_BCs[bol_corr_text].shape
        # points = np.array([Fe_Hs, Teffs, Loggs])
        # grid_BCs[bol_corr_text] =\
        # np.swapaxes(\
        # grid_BCs[bol_corr_text].reshape((len(Fe_Hs), len(Teffs), len(Loggs))),\
        # 1,2)
        #Fe/H, Logg, Teff

        points = np.array([Fe_Hs, Loggs])
        grid_BCs[bol_corr_text] =\
        np.swapaxes(\
        grid_BCs[bol_corr_text].reshape((len(Fe_Hs), len(Teffs), len(Loggs))),\
        0,1)

        #Teff, Fe/H, Logg

        for Teff_index in range(len(Teffs)):
            load_data[Teff_index][column_headers.index(bol_corr_text)] =\
            scipy.interpolate.RegularGridInterpolator(points,
            grid_BCs[bol_corr_text][Teff_index], bounds_error = False,
            fill_value = np.NaN)\
            ((Fe_H_val, logg_val))

    return load_data

    # for filename in files:
    #     log_rhos = np.zeros(len_rho) + np.NaN
    #     with open(folder_prefix + "/" + filename, "r") as open_f:
    #         file_data = open_f.read()
    #         for block_index, density_and_table in \
    #         enumerate(file_data.split("density(g/cc)=")[1:]):
    #             density = float(density_and_table.split("U=MB*cc/gm")[0])
    #             table_StringIO = io.StringIO(density_and_table)
    #             table = np.genfromtxt(table_StringIO, skip_header = 3, skip_footer = 1)
    #             num_entries = len(T6)
    #             table_T6 = table.T[0]
    #             table_log_T = np.flip(np.log10(T6) + 6.)
    #             table_log_P = np.zeros(num_entries) + np.NaN
    #             log_rhos[block_index] = np.log10(density)
    #             table_log_rho = np.zeros(num_entries) + log_rhos[block_index]
    #             for table_index in range(len(table_T6)):
    #                 index_T6 =\
    #                 np.argmin((table_T6[table_index] - T6)*\
    #                 (table_T6[table_index] - T6))
    #
    #                 # Take the log of this later for efficiency.
    #                 table_log_P[index_T6] = table.T[3][table_index]
    #
    #             # Because temperature has to be sorted (simply, flipped),
    #             # Pressure has to be flipped
    #             table_log_P = np.flip(np.log10(table_log_P))
    #
    #             text_X_Z = filename.split("_")[1]
    #             text_X = text_X_Z.split("z")[1].split("x")[0]
    #             if len(text_X) < 2:
    #                 val_X = float("0." + text_X)
    #             else:
    #                 val_X = float(text_X[0] + "." + text_X[1])
    #             val_Z = float("0." + text_X_Z.split("z")[0])
    #             table_X = np.zeros(num_entries) + val_X
    #             table_Z = np.zeros(num_entries) + val_Z
    #
    #             if log_T is None:
    #                 log_T = table_log_T
    #             else:
    #                 log_T = np.concatenate((log_T, table_log_T))
    #
    #             if log_P is None:
    #                 log_P = table_log_P
    #             else:
    #                 log_P = np.concatenate((log_P, table_log_P))
    #
    #             if log_rho is None:
    #                 log_rho = table_log_rho
    #             else:
    #                 log_rho = np.concatenate((log_rho, table_log_rho))
    #
    #             if X is None:
    #                 X = table_X
    #             else:
    #                 X = np.concatenate((X, table_X))
    #
    #             if Z is None:
    #                 Z = table_Z
    #             else:
    #                 Z = np.concatenate((Z, table_Z))
    #
    # points = np.array([Xs, Zs, log_rhos, table_log_T])
    # log_P = log_P.reshape((len(Xs), len(Zs), len_rho, len(table_log_T)))
    #
    # np.savez_compressed(folder_prefix + "/" + savefile,
    # points = points,
    # log_P = log_P)
    # return True
