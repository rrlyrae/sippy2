"""
Pleiades-Calibrated Color Table Interpolator
"""

from __future__ import absolute_import
from sippy.interpolator.BaseInterpolator import *
import os
import numpy as np
import scipy
import scipy.interpolate
import matplotlib.pyplot as plt
from astropy import units as u
from itertools import izip

class PleiadesCalibratedInterpolator(SpTBaseInterpolator):
    """Interpolator for the Pleiades-Calibrated Color Tables.

    "input" and "output" are case-sensitive strings each corresponding to one of the
    following quantities:

    SpT,Teff,Ks-g,Ks-i,Ks-H,Ks-J,Ks-r,Ks-z

    The "output_unit" parameter is an astropy unit corresponding to the
    dimension that the output data is desired. For dimensionless quantities,
    units.dimensionless_unscaled will suffice.

    For Teff, the output unit will be assumed to be "K" unless set to a
    compatible temperature unit.

    For SpT, the output unit can be set to both units.dimensionless_unscaled,
    or the string "SpT", to output string SpT values without a luminosity class.

    interpolation_kind can be 'linear', 'nearest', 'slinear', 'quadratic',
    'cubic', from the scipy documentation."""
    # Define the default units of each column.
    default_units = \
    {"SpT": u.dimensionless_unscaled,
     "Teff": u.K,
     "Ks-B": u.mag,
     "Ks-g": u.mag,
     "Ks-i": u.mag,
     "Ks-H": u.mag,
     "Ks-J": u.mag,
     "Ks-r": u.mag,
     "Ks-V": u.mag,
     "Ks-Ic": u.mag,
     "Ks-z": u.mag,
     "BCV": u.mag}
    filename = "Pleiades_calibrated_color_table.dat"
    # The following column headers are directly from the file, and ordered:
    column_headers = ["SpT","Teff", "Ks-B", "Ks-g", "Ks-i", "Ks-H", "Ks-J", "Ks-r", "Ks-V", "Ks-Ic", "Ks-z", "BCV"]
    available_colors = ["Ks-B", "Ks-g", "Ks-i", "Ks-H", "Ks-J", "Ks-r", "Ks-V", "Ks-Ic", "Ks-z"]
    # No need to specify a special dtype.
    # No need to deal explicitly with missing values or filling values
    # by default
    missing_values = None
    filling_values = None
    # Don't skip lines
    skip_header = 0
    # Read the entire file in:
    max_rows = None
    # The input file uses whitespace to delimit entries, so denote that:
    delimiter = None
    def load_data(self, filename):
        """Return the loaded data, according to the datatypes, missing values,
        and other parameters defined in the class, and parsed by row."""
        return np.genfromtxt(filename, comments = "#",
                             delimiter=self.delimiter,
                             skip_header=self.skip_header,
                             max_rows=self.max_rows,
                             missing_values=self.missing_values,
                             filling_values=self.filling_values)
    def __init__(self, input, output, interpolation_kind = "linear",
                 output_unit = None):
        # Set the units to output in
        self.output_unit = output_unit

        # Set up the base class requirements, though these can be run
        # with different values after setting up the class!
        self.set_spectral_types()
        self.set_SpT_zero_point()

        # Load in the data
        filename = os.path.dirname(os.path.abspath(__file__))\
                 + "/Data/" + self.filename
        load_data = self.load_data(filename)
        column_headers = self.column_headers
        # Throw an error if input and output are not valid choices.
        assert input in column_headers and output in column_headers,\
        "Unsupported column headers chosen for input and output."+\
        "Valid options: "+repr(column_headers)
        ### Set the default inputs and outputs according to the default unit
        ### of each column datatype.
        # Use the default units for the chosen input and output columns.
        self._default_input_unit = self.default_units[input]
        self._default_output_unit = self.default_units[output]

        ## If the output unit is unset, then assume that the default units
        ## are what's requested.

        if output_unit is None:
            self.output_unit = self._default_output_unit

        # Find the indices of the columns that correspond to input and output.
        input_index = column_headers.index(input)
        output_index = column_headers.index(output)

        input_interp = np.array([aa[input_index] for aa in load_data])
        output_interp = np.array([bb[output_index] for bb in load_data])

        # If the input is SpT, wrap "evaluate" with the ability to
        # read in lists of strings, numpy arrays with dimensionless_unscaled,
        # or lists of numbers, or something in between.
        ## Also, since the SpT column input is in string SpT's, we need
        ## to convert them to numerical SpT's for interpolation.
        if input == "SpT":
            self.evaluate = self.evaluate___SpT_input
            ### Also, since the input is SpT, coerce it to well-behaved strings:
            #input_interp =\
            #[input_interp[x].decode("utf-8")\
             #for x in xrange(len(input_interp))]
            ### Then convert it to numerical SpTs:
            #input_interp = self.get_num_SpTs(input_interp)
        if output == "SpT":
            self.evaluate = self.evaluate___SpT_output
            # Also, since the output is SpT, coerce it to well-behaved strings:
            output_interp =\
            [output_interp[x].decode("utf-8")\
             for x in xrange(len(output_interp))]
            # Then convert it to numerical SpTs:
            output_interp = self.get_num_SpTs(output_interp)

        ### Making sure to only expose the non-NaN values.
        # We obtain the indices at which neither the input or output interp
        # are NaN by performing an AND on the valid entries --- rejecting
        # an index if either the input or output interpolations are NaN.
        valid_indices =\
        np.logical_and(np.logical_not(np.isnan(input_interp)),
                       np.logical_not(np.isnan(output_interp)))
        # Sort interpolation arrays prior to interpolation:
        sorted_output_interp_trimmed =\
        [output_int for input_int, output_int in\
         sorted(izip(input_interp[valid_indices],
                    output_interp[valid_indices]),
                key = lambda x: x[0])]
        sorted_input_interp_trimmed =\
        sorted(input_interp[valid_indices])
        # Remove duplicates from the target input_interp:
        tiny = 1e-15
        sorted_input_interp_trimmed_no_duplicates = []
        sorted_output_interp_trimmed_no_duplicates = []
        for index in xrange(1, len(sorted_input_interp_trimmed)):
            if abs(sorted_input_interp_trimmed[index] -\
                   sorted_input_interp_trimmed[index-1]) > tiny:
                sorted_input_interp_trimmed_no_duplicates.\
                append(sorted_input_interp_trimmed[index])
                sorted_output_interp_trimmed_no_duplicates.\
                append(sorted_output_interp_trimmed[index])
        # Expose the interpolation evaluation method.
        self._evaluate =\
        scipy.interpolate.interp1d(sorted_input_interp_trimmed_no_duplicates,
                                   sorted_output_interp_trimmed_no_duplicates,
                                   kind = interpolation_kind,
                                   bounds_error = False,
                                   fill_value = np.nan, assume_sorted = True)
