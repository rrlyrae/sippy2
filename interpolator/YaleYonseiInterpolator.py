"""
Y2 Interpolator
"""

from __future__ import absolute_import
from sippy.interpolator.BaseInterpolator import *
import os
import numpy as np
import scipy
import scipy.interpolate
import matplotlib.pyplot as plt
from astropy import units as u
import itertools as it
from itertools import izip

class YaleYonseiBaseInterpolator(BaseInterpolator):
    def basic_preinit(self, FeH = 0., interpolation_kind = "linear", alpha = 1.):
        file_folder = "sippy/interpolator/Data/Y2/fs255_grid/"
        Z_sol = 0.0169

        columns = ["Mass","Teff","logL","logg","Mv","U-B","B-V","V-I","V-K","R-I","I-K","J-H","H-K"]
        directories = os.listdir(file_folder)

        #Sort by Z, and sort by logage.
        directory_Z_list = []
        Z_list = []

        FeH_list = []

        file_logage_list = []
        logage_list = []

        for directory in directories:
            if directory[0] == "X":
                X_str = directory[1:8].replace("p", ".")
                Z_str = directory[10:17].replace("p", ".")
                A_str = directory[19:24].replace("p", ".")
                X = float(X_str)
                Z = float(Z_str)
                A = float(A_str)
                FeH_val = np.log10(Z/Z_sol)
                if np.square(A - alpha) < 1e-6:
                    files = os.listdir(file_folder+directory)
                    directory_Z_list.append(directory)
                    Z_list.append(Z)
                    FeH_list.append(FeH_val)
                    for file in files:
                        if file[len(file) - 4:] == "iso2":
                            file_source = file_folder+directory+"/"+file
                            age_str = file[0:6].replace("Gyr", "e9").replace("Myr", "e6")
                            age = float(age_str)
                            logage = np.log10(age)
                            if file[0:6] not in file_logage_list:
                                file_logage_list.append(file[0:6])
                                logage_list.append(logage)

        sorted_directories = [x[0] for x in sorted(zip(directory_Z_list, Z_list), key = lambda y: y[1])]
        sorted_files_prefix = [x[0] for x in sorted(zip(file_logage_list, logage_list), key = lambda y: y[1])]

        mass_range = np.logspace(np.log10(0.09), np.log10(2.4), 100)

        logage_range = np.linspace(6.6,10.,24)

        left_logg_interp_array = np.zeros((len(logage_list), len(mass_range))) + np.NaN
        left_BCVs_interp_array = np.zeros((len(logage_list), len(mass_range))) + np.NaN
        left_log_Teffs_interp_array = np.zeros((len(logage_list), len(mass_range))) + np.NaN
        left_masses_interp_array = np.zeros((len(logage_list), len(mass_range))) + np.NaN
        left_log_radius_interp_array = np.zeros((len(logage_list), len(mass_range))) + np.NaN
        left_logages_interp_array = np.zeros((len(logage_list), len(mass_range))) + np.NaN

        left_logages = np.zeros(len(logage_list)) + np.NaN

        right_logg_interp_array = np.zeros((len(logage_list), len(mass_range))) + np.NaN
        right_BCVs_interp_array = np.zeros((len(logage_list), len(mass_range))) + np.NaN
        right_log_Teffs_interp_array = np.zeros((len(logage_list), len(mass_range))) + np.NaN
        right_masses_interp_array = np.zeros((len(logage_list), len(mass_range))) + np.NaN
        right_log_radius_interp_array = np.zeros((len(logage_list), len(mass_range))) + np.NaN
        right_logages_interp_array = np.zeros((len(logage_list), len(mass_range))) + np.NaN

        right_logages = np.zeros(len(logage_list)) + np.NaN

        FeHs = sorted(FeH_list)
        if (not np.all(FeH > np.array(FeHs))) and (not np.all(FeH <= np.array(FeHs))):

            #For the choice of any specific FeH, we then can lookup the nearest two to interpolate between.

            FeH_left = np.max(np.array(FeHs)[FeH > np.array(FeHs)])
            ind_FeH_left = np.argmin(np.abs(FeH_left - np.array(FeHs)))
            FeH_right = np.min(np.array(FeHs)[FeH <= np.array(FeHs)])
            ind_FeH_right = np.argmin(np.abs(FeH_right - np.array(FeHs)))

            directory = sorted_directories[ind_FeH_left]
            X_str = directory[1:8].replace("p", ".")
            Z_str = directory[10:17].replace("p", ".")
            X = float(X_str)
            Z = float(Z_str)
            FeH_val = np.log10(Z/Z_sol)
            for index_idx, file_prefix in enumerate(sorted_files_prefix):
                file = file_prefix + "_"+directory+".iso2"
                file_source = file_folder+directory+"/"+file
                # print file_source
                # print file
                age_str = file[0:6].replace("Gyr", "e9").replace("Myr", "e6")
                age = float(age_str)
                logage = np.log10(age)
                # print logage
                data = np.genfromtxt(file_source, missing_values = "-999.00000")
                shape = data.shape
                temp_data = data.flatten()
                temp_data[temp_data < -998.] = np.NaN
                data = temp_data.reshape(shape)
                # columns = ["Mass","Teff","logL","logg","Mv","U-B","B-V","V-I","V-K","R-I","I-K","J-H","H-K"]

                left_logg_interp_array[index_idx] =\
                scipy.interpolate.interp1d(data.T[columns.index("Mass")], data.T[columns.index("logg")],
                kind = interpolation_kind, bounds_error = False)(mass_range)

                left_BCVs_interp_array[index_idx] =\
                scipy.interpolate.interp1d(data.T[columns.index("Mass")], 4.74 - data.T[columns.index("Mv")] - 2.5*data.T[columns.index("logL")],
                kind = interpolation_kind, bounds_error = False)(mass_range)

                left_log_Teffs_interp_array[index_idx] =\
                scipy.interpolate.interp1d(data.T[columns.index("Mass")], np.log10(data.T[columns.index("Teff")]),
                kind = interpolation_kind, bounds_error = False)(mass_range)

                left_masses_interp_array[index_idx] =\
                mass_range

                left_log_radius_interp_array[index_idx] =\
                0.5*(scipy.interpolate.interp1d(data.T[columns.index("Mass")],
                data.T[columns.index("logL")],
                kind = interpolation_kind, bounds_error = False)(mass_range))\
                -2.*scipy.interpolate.interp1d(data.T[columns.index("Mass")],
                np.log10(data.T[columns.index("Teff")]),
                kind = interpolation_kind, bounds_error = False)(mass_range)\
                +2.*np.log10(5777.)

                left_logages_interp_array[index_idx] = np.zeros_like(mass_range) + logage

                left_logages[index_idx] = logage

            directory = sorted_directories[ind_FeH_right]
            X_str = directory[1:8].replace("p", ".")
            Z_str = directory[10:17].replace("p", ".")
            X = float(X_str)
            Z = float(Z_str)
            FeH_val = np.log10(Z/Z_sol)
            for index_idx, file_prefix in enumerate(sorted_files_prefix):
                file = file_prefix + "_"+directory+".iso2"
                file_source = file_folder+directory+"/"+file
                # print file_source
                # print file
                age_str = file[0:6].replace("Gyr", "e9").replace("Myr", "e6")
                age = float(age_str)
                logage = np.log10(age)
                # print logage
                data = np.genfromtxt(file_source, missing_values = "-999.00000")
                shape = data.shape
                temp_data = data.flatten()
                temp_data[temp_data < -998.] = np.NaN
                data = temp_data.reshape(shape)
                # columns = ["Mass","Teff","logL","logg","Mv","U-B","B-V","V-I","V-K","R-I","I-K","J-H","H-K"]

                right_logg_interp_array[index_idx] =\
                scipy.interpolate.interp1d(data.T[columns.index("Mass")], data.T[columns.index("logg")],
                kind = interpolation_kind, bounds_error = False)(mass_range)

                right_BCVs_interp_array[index_idx] =\
                scipy.interpolate.interp1d(data.T[columns.index("Mass")], 4.74 - data.T[columns.index("Mv")] - 2.5*data.T[columns.index("logL")],
                kind = interpolation_kind, bounds_error = False)(mass_range)

                right_log_Teffs_interp_array[index_idx] =\
                scipy.interpolate.interp1d(data.T[columns.index("Mass")], np.log10(data.T[columns.index("Teff")]),
                kind = interpolation_kind, bounds_error = False)(mass_range)

                right_masses_interp_array[index_idx] =\
                mass_range

                right_log_radius_interp_array[index_idx] =\
                0.5*(scipy.interpolate.interp1d(data.T[columns.index("Mass")],
                data.T[columns.index("logL")],
                kind = interpolation_kind, bounds_error = False)(mass_range))\
                -2.*scipy.interpolate.interp1d(data.T[columns.index("Mass")],
                np.log10(data.T[columns.index("Teff")]),
                kind = interpolation_kind, bounds_error = False)(mass_range)\
                +2.*np.log10(5777.)

                right_logages_interp_array[index_idx] = np.zeros_like(mass_range) + logage

                right_logages[index_idx] = logage

            l_logages_interp_array = np.zeros((len(logage_range), len(mass_range))) + np.NaN
            l_logg_interp_array = np.zeros((len(logage_range), len(mass_range))) + np.NaN
            l_log_Teffs_interp_array = np.zeros((len(logage_range), len(mass_range))) + np.NaN
            l_masses_interp_array = np.zeros((len(logage_range), len(mass_range))) + np.NaN
            l_log_radius_interp_array = np.zeros((len(logage_range), len(mass_range))) + np.NaN
            l_BCVs_interp_array = np.zeros((len(logage_range), len(mass_range))) + np.NaN

            r_logages_interp_array = np.zeros((len(logage_range), len(mass_range))) + np.NaN
            r_logg_interp_array = np.zeros((len(logage_range), len(mass_range))) + np.NaN
            r_log_Teffs_interp_array = np.zeros((len(logage_range), len(mass_range))) + np.NaN
            r_masses_interp_array = np.zeros((len(logage_range), len(mass_range))) + np.NaN
            r_log_radius_interp_array = np.zeros((len(logage_range), len(mass_range))) + np.NaN
            r_BCVs_interp_array = np.zeros((len(logage_range), len(mass_range))) + np.NaN

            for index_idx_mass in range(len(mass_range)):
            	if np.sum(np.logical_not(np.isnan(left_logages_interp_array.T[index_idx_mass]))) > 1:
            		l_logages_interp_array.T[index_idx_mass] = scipy.interpolate.interp1d(left_logages[np.logical_not(np.isnan(left_logages_interp_array.T[index_idx_mass]))],
            		left_logages_interp_array.T[index_idx_mass][np.logical_not(np.isnan(left_logages_interp_array.T[index_idx_mass]))],
            		kind = interpolation_kind, bounds_error = False)(logage_range)
            	else:
            		l_logages_interp_array.T[index_idx_mass] = np.NaN

            	if np.sum(np.logical_not(np.isnan(left_logg_interp_array.T[index_idx_mass]))) > 1:
            		l_logg_interp_array.T[index_idx_mass] = scipy.interpolate.interp1d(left_logages[np.logical_not(np.isnan(left_logg_interp_array.T[index_idx_mass]))],
            		left_logg_interp_array.T[index_idx_mass][np.logical_not(np.isnan(left_logg_interp_array.T[index_idx_mass]))],
            		kind = interpolation_kind, bounds_error = False)(logage_range)
            	else:
            		l_logg_interp_array.T[index_idx_mass] = np.NaN

            	if np.sum(np.logical_not(np.isnan(left_log_Teffs_interp_array.T[index_idx_mass]))) > 1:
            		l_log_Teffs_interp_array.T[index_idx_mass] = scipy.interpolate.interp1d(left_logages[np.logical_not(np.isnan(left_log_Teffs_interp_array.T[index_idx_mass]))],
            		left_log_Teffs_interp_array.T[index_idx_mass][np.logical_not(np.isnan(left_log_Teffs_interp_array.T[index_idx_mass]))],
            		kind = interpolation_kind, bounds_error = False)(logage_range)
            	else:
            		l_log_Teffs_interp_array.T[index_idx_mass] = np.NaN

            	if np.sum(np.logical_not(np.isnan(right_logages_interp_array.T[index_idx_mass]))) > 1:
            		r_logages_interp_array.T[index_idx_mass] = scipy.interpolate.interp1d(right_logages[np.logical_not(np.isnan(right_logages_interp_array.T[index_idx_mass]))],
            		right_logages_interp_array.T[index_idx_mass][np.logical_not(np.isnan(right_logages_interp_array.T[index_idx_mass]))],
            		kind = interpolation_kind, bounds_error = False)(logage_range)
            	else:
            		r_logages_interp_array.T[index_idx_mass] = np.NaN

            	if np.sum(np.logical_not(np.isnan(right_logg_interp_array.T[index_idx_mass]))) > 1:
            		r_logg_interp_array.T[index_idx_mass] = scipy.interpolate.interp1d(right_logages[np.logical_not(np.isnan(right_logg_interp_array.T[index_idx_mass]))],
            		right_logg_interp_array.T[index_idx_mass][np.logical_not(np.isnan(right_logg_interp_array.T[index_idx_mass]))],
            		kind = interpolation_kind, bounds_error = False)(logage_range)
            	else:
            		r_logg_interp_array.T[index_idx_mass] = np.NaN

            	if np.sum(np.logical_not(np.isnan(right_log_Teffs_interp_array.T[index_idx_mass]))) > 1:
            		r_log_Teffs_interp_array.T[index_idx_mass] = scipy.interpolate.interp1d(right_logages[np.logical_not(np.isnan(right_log_Teffs_interp_array.T[index_idx_mass]))],
            		right_log_Teffs_interp_array.T[index_idx_mass][np.logical_not(np.isnan(right_log_Teffs_interp_array.T[index_idx_mass]))],
            		kind = interpolation_kind, bounds_error = False)(logage_range)
            	else:
            		r_log_Teffs_interp_array.T[index_idx_mass] = np.NaN

            	#

            	if np.sum(np.logical_not(np.isnan(left_masses_interp_array.T[index_idx_mass]))) > 1:
            		l_masses_interp_array.T[index_idx_mass] = scipy.interpolate.interp1d(left_logages[np.logical_not(np.isnan(left_masses_interp_array.T[index_idx_mass]))],
            		left_masses_interp_array.T[index_idx_mass][np.logical_not(np.isnan(left_masses_interp_array.T[index_idx_mass]))],
            		kind = interpolation_kind, bounds_error = False)(logage_range)
            	else:
            		l_masses_interp_array.T[index_idx_mass] = np.NaN

            	if np.sum(np.logical_not(np.isnan(left_log_radius_interp_array.T[index_idx_mass]))) > 1:
            		l_log_radius_interp_array.T[index_idx_mass] = scipy.interpolate.interp1d(left_logages[np.logical_not(np.isnan(left_log_radius_interp_array.T[index_idx_mass]))],
            		left_log_radius_interp_array.T[index_idx_mass][np.logical_not(np.isnan(left_log_radius_interp_array.T[index_idx_mass]))],
            		kind = interpolation_kind, bounds_error = False)(logage_range)
            	else:
            		l_log_radius_interp_array.T[index_idx_mass] = np.NaN

            	if np.sum(np.logical_not(np.isnan(right_masses_interp_array.T[index_idx_mass]))) > 1:
            		r_masses_interp_array.T[index_idx_mass] = scipy.interpolate.interp1d(right_logages[np.logical_not(np.isnan(right_masses_interp_array.T[index_idx_mass]))],
            		right_masses_interp_array.T[index_idx_mass][np.logical_not(np.isnan(right_masses_interp_array.T[index_idx_mass]))],
            		kind = interpolation_kind, bounds_error = False)(logage_range)
            	else:
            		r_masses_interp_array.T[index_idx_mass] = np.NaN

            	if np.sum(np.logical_not(np.isnan(right_log_radius_interp_array.T[index_idx_mass]))) > 1:
            		r_log_radius_interp_array.T[index_idx_mass] = scipy.interpolate.interp1d(right_logages[np.logical_not(np.isnan(right_log_radius_interp_array.T[index_idx_mass]))],
            		right_log_radius_interp_array.T[index_idx_mass][np.logical_not(np.isnan(right_log_radius_interp_array.T[index_idx_mass]))],
            		kind = interpolation_kind, bounds_error = False)(logage_range)
            	else:
            		r_log_radius_interp_array.T[index_idx_mass] = np.NaN

            	if np.sum(np.logical_not(np.isnan(left_BCVs_interp_array.T[index_idx_mass]))) > 1:
            		l_BCVs_interp_array.T[index_idx_mass] = scipy.interpolate.interp1d(left_logages[np.logical_not(np.isnan(left_BCVs_interp_array.T[index_idx_mass]))],
            		left_BCVs_interp_array.T[index_idx_mass][np.logical_not(np.isnan(left_BCVs_interp_array.T[index_idx_mass]))],
            		kind = interpolation_kind, bounds_error = False)(logage_range)
            	else:
            		l_BCVs_interp_array.T[index_idx_mass] = np.NaN

            	if np.sum(np.logical_not(np.isnan(right_BCVs_interp_array.T[index_idx_mass]))) > 1:
            		r_BCVs_interp_array.T[index_idx_mass] = scipy.interpolate.interp1d(right_logages[np.logical_not(np.isnan(right_BCVs_interp_array.T[index_idx_mass]))],
            		right_BCVs_interp_array.T[index_idx_mass][np.logical_not(np.isnan(right_BCVs_interp_array.T[index_idx_mass]))],
            		kind = interpolation_kind, bounds_error = False)(logage_range)
            	else:
            		r_BCVs_interp_array.T[index_idx_mass] = np.NaN

            logg_interp_array = l_logg_interp_array + (r_logg_interp_array - l_logg_interp_array) * (FeH - FeH_left)/(FeH_right - FeH_left)
            BCVs_interp_array = l_BCVs_interp_array + (r_BCVs_interp_array - l_BCVs_interp_array) * (FeH - FeH_left)/(FeH_right - FeH_left)
            log_Teffs_interp_array = l_log_Teffs_interp_array + (r_log_Teffs_interp_array - l_log_Teffs_interp_array) * (FeH - FeH_left)/(FeH_right - FeH_left)

            masses_interp_array = l_masses_interp_array + (r_masses_interp_array - l_masses_interp_array) * (FeH - FeH_left)/(FeH_right - FeH_left)
            log_radius_interp_array = l_log_radius_interp_array + (r_log_radius_interp_array - l_log_radius_interp_array) * (FeH - FeH_left)/(FeH_right - FeH_left)

            logages_interp_array = l_logages_interp_array + (r_logages_interp_array - l_logages_interp_array) * (FeH - FeH_left)/(FeH_right - FeH_left)

            return {"logg_interp_array": logg_interp_array,
            "BCVs_interp_array": BCVs_interp_array,
            "log_Teffs_interp_array": log_Teffs_interp_array,
            "masses_interp_array": masses_interp_array,
            "log_radius_interp_array": log_radius_interp_array,
            "logages_interp_array": logages_interp_array,
            "mass_range": mass_range}


        return {"logg_interp_array": np.zeros_like(mass_range) + np.NaN,
        "BCVs_interp_array": np.zeros_like(mass_range) + np.NaN,
        "log_Teffs_interp_array": np.zeros_like(mass_range) + np.NaN,
        "masses_interp_array": np.zeros_like(mass_range) + np.NaN,
        "log_radius_interp_array": np.zeros_like(mass_range) + np.NaN,
        "logages_interp_array": np.zeros_like(mass_range) + np.NaN,
        "mass_range": mass_range}

class YaleYonseiBCVInterpolator(YaleYonseiBaseInterpolator):
    # Define the default units of each column.
    default_units = \
    {"Teff": u.K,
     "Logg": u.dimensionless_unscaled,
     "[M/H]": u.dimensionless_unscaled,
     "[a/H]": u.dimensionless_unscaled,
     "B": u.mag,
     "V": u.mag,
     "R": u.mag,
     "I": u.mag,
     "BCV": u.mag}
    # Denote the explicit photometric bands here:
    bands = ["U", "B", "V", "R", "I", "J", "H", "Ks"]
    available_colors = ["BCV"]
    # No need to deal explicitly with missing values or filling values
    # by default
    missing_values = None
    filling_values = None
    # Skip 0 lines before reading in data:
    skip_header = 0
    # Read the entire file in:
    max_rows = None
    # Use default whitespace to delimit entries:
    delimiter = None
    # Comments are '#'
    comments = "#"
    def preinit(self, logg = 4.43812, FeH = 0., interpolation_kind = "linear", alpha = 1.):
        """Run this method to pre-initialize YaleYonseiBCVInterpolator with a specific FeH."""
        dict_init = self.basic_preinit(FeH = FeH, interpolation_kind = interpolation_kind, alpha = alpha)

        mass_range = dict_init["mass_range"]
        loggs_interp_array = dict_init["logg_interp_array"]
        BCVs_interp_array = dict_init["BCVs_interp_array"]
        log_Teffs_interp_array = dict_init["log_Teffs_interp_array"]

        self.BCVs = np.zeros_like(mass_range) + np.NaN
        self.teffs = np.zeros_like(mass_range) + np.NaN

        if not np.all(np.isnan(loggs_interp_array)):
            # Now, for each mass, we interpolate along logg.
            for mass_idx in range(len(mass_range)):
                self.BCVs[mass_idx] = scipy.interpolate.interp1d(loggs_interp_array.T[mass_idx], BCVs_interp_array.T[mass_idx], kind = interpolation_kind, bounds_error = False)(logg)
                self.teffs[mass_idx] = np.power(10.,scipy.interpolate.interp1d(loggs_interp_array.T[mass_idx], log_Teffs_interp_array.T[mass_idx], kind = interpolation_kind, bounds_error = False)(logg))

    def __init__(self, input, output, logg = 4.43812, FeH = 0., interpolation_kind = "linear", output_unit = None):
        self.output_unit = output_unit
        self._default_input_unit = self.default_units[input]
        self._default_output_unit = self.default_units[output]
        if output_unit is None:
            self.output_unit = self._default_output_unit
        assert input in ["BCV", "Teff"], "Input type not BCV or Teff"
        assert output in ["BCV", "Teff"], "Output type not BCV or Teff"
        if hasattr(self, "BCVs"):
            pass
        else:
            self.preinit(logg = logg, FeH = FeH, interpolation_kind = interpolation_kind)

        if input == "BCV":
            input_interp = self.BCVs
        elif input == "Teff":
            input_interp = self.teffs

        if output == "BCV":
            output_interp = self.BCVs
        elif output == "Teff":
            output_interp = self.teffs

        ### Use only for our conditionals non-nan elements.
        if len(input_interp > 0):
            cond_valid = np.logical_and(np.logical_not(np.isnan(input_interp)), np.logical_not(np.isnan(output_interp)))

            # print zip(input_interp[cond_valid], output_interp[cond_valid])

            sorted_input_interp_trimmed = sorted(input_interp[cond_valid])
            sorted_output_interp_trimmed =\
            [y[1] for y in sorted(zip(input_interp[cond_valid], output_interp[cond_valid]), key = lambda x: x[0])]
            if len(sorted_input_interp_trimmed) > 2:
                self._evaluate =\
                scipy.interpolate.interp1d(sorted_input_interp_trimmed,
                                           sorted_output_interp_trimmed,
                                           kind = interpolation_kind,
                                           bounds_error = False,
                                           fill_value = np.nan, assume_sorted = True)
            else:
                self._evaluate =\
                scipy.interpolate.interp1d([-1e10,-1e10+1],
                                           [0.,0.],
                                           kind = interpolation_kind,
                                           bounds_error = False,
                                           fill_value = np.nan, assume_sorted = True)
        else:
            self._evaluate =\
            scipy.interpolate.interp1d([-1e10,-1e10+1],
                                       [0.,0.],
                                       kind = interpolation_kind,
                                       bounds_error = False,
                                       fill_value = np.nan, assume_sorted = True)

class YaleYonseiMassRadiusInterpolator(YaleYonseiBaseInterpolator):
    # Define the default units of each column.
    default_units = \
    {"Teff": u.K,
     "Logg": u.dimensionless_unscaled,
     "[M/H]": u.dimensionless_unscaled,
     "[a/H]": u.dimensionless_unscaled,
     "B": u.mag,
     "V": u.mag,
     "R": u.mag,
     "I": u.mag,
     "BCV": u.mag,
     "mass": u.Msun,
     "radius": u.Rsun}
    # Denote the explicit photometric bands here:
    bands = ["U", "B", "V", "R", "I", "J", "H", "Ks"]
    available_colors = ["mass", "radius"]
    # No need to deal explicitly with missing values or filling values
    # by default
    missing_values = None
    filling_values = None
    # Skip 0 lines before reading in data:
    skip_header = 0
    # Read the entire file in:
    max_rows = None
    # Use default whitespace to delimit entries:
    delimiter = None
    # Comments are '#'
    comments = "#"
    def preinit(self, logg = 4.43812, FeH = 0., interpolation_kind = "linear", alpha = 1.):
        """Run this method to pre-initialize YaleYonseiBCVInterpolator with a specific FeH."""
        dict_init = self.basic_preinit(FeH = FeH, interpolation_kind = interpolation_kind, alpha = alpha)

        mass_range = dict_init["mass_range"]
        loggs_interp_array = dict_init["logg_interp_array"]
        masses_interp_array = dict_init["masses_interp_array"]
        log_radius_interp_array = dict_init["log_radius_interp_array"]

        self.mass = np.zeros_like(mass_range) + np.NaN
        self.radius = np.zeros_like(mass_range) + np.NaN

        if not np.all(np.isnan(loggs_interp_array)):
            # Now, for each mass, we interpolate along logg.
            for mass_idx in range(len(mass_range)):
                self.mass[mass_idx] = scipy.interpolate.interp1d(loggs_interp_array.T[mass_idx], masses_interp_array.T[mass_idx], kind = interpolation_kind, bounds_error = False)(logg)
                self.radius[mass_idx] = np.power(10.,scipy.interpolate.interp1d(loggs_interp_array.T[mass_idx], log_radius_interp_array.T[mass_idx], kind = interpolation_kind, bounds_error = False)(logg))

    def __init__(self, input, output, logg = 4.43812, FeH = 0., interpolation_kind = "linear", output_unit = None):
        self.output_unit = output_unit
        self._default_input_unit = self.default_units[input]
        self._default_output_unit = self.default_units[output]
        if output_unit is None:
            self.output_unit = self._default_output_unit
        assert input in ["mass", "radius"], "Input type not mass or radius"
        assert output in ["mass", "radius"], "Output type not mass or radius"
        if hasattr(self, "mass"):
            pass
        else:
            self.preinit(logg = logg, FeH = FeH, interpolation_kind = interpolation_kind)

        if input == "mass":
            input_interp = self.mass
        elif input == "radius":
            input_interp = self.radius

        if output == "mass":
            output_interp = self.mass
        elif output == "radius":
            output_interp = self.radius

        ### Use only for our conditionals non-nan elements.

        cond_valid = np.logical_and(np.logical_not(np.isnan(input_interp)), np.logical_not(np.isnan(output_interp)))

        # print zip(input_interp[cond_valid], output_interp[cond_valid])

        sorted_input_interp_trimmed = sorted(input_interp[cond_valid])
        sorted_output_interp_trimmed =\
        [y[1] for y in sorted(zip(input_interp[cond_valid], output_interp[cond_valid]), key = lambda x: x[0])]
        if len(sorted_input_interp_trimmed) > 2:
            self._evaluate =\
            scipy.interpolate.interp1d(sorted_input_interp_trimmed,
                                       sorted_output_interp_trimmed,
                                       kind = interpolation_kind,
                                       bounds_error = False,
                                       fill_value = np.nan, assume_sorted = True)
        else:
            self._evaluate =\
            scipy.interpolate.interp1d([-1e10,-1e10+1],
                                       [0.,0.],
                                       kind = interpolation_kind,
                                       bounds_error = False,
                                       fill_value = np.nan, assume_sorted = True)

class YaleYonseiGravityAgeInterpolator(YaleYonseiBaseInterpolator):
    # Define the default units of each column.
    default_units = \
    {"Teff": u.K,
     "Logg": u.dimensionless_unscaled,
     "[M/H]": u.dimensionless_unscaled,
     "[a/H]": u.dimensionless_unscaled,
     "B": u.mag,
     "V": u.mag,
     "R": u.mag,
     "I": u.mag,
     "logg": u.dimensionless_unscaled,
     "mass": u.Msun}
    # Denote the explicit photometric bands here:
    bands = ["U", "B", "V", "R", "I", "J", "H", "Ks"]
    available_colors = ["logg"]
    # No need to deal explicitly with missing values or filling values
    # by default
    missing_values = None
    filling_values = None
    # Skip 0 lines before reading in data:
    skip_header = 0
    # Read the entire file in:
    max_rows = None
    # Use default whitespace to delimit entries:
    delimiter = None
    # Comments are '#'
    comments = "#"
    def preinit(self, logage = 9.69897, FeH = 0., interpolation_kind = "linear", alpha = 1.):
        """Run this method to pre-initialize YaleYonseiBCVInterpolator with a specific FeH."""
        dict_init = self.basic_preinit(FeH = FeH, interpolation_kind = interpolation_kind, alpha = alpha)

        mass_range = dict_init["mass_range"]
        logages_interp_array = dict_init["logages_interp_array"]
        logg_interp_array = dict_init["logg_interp_array"]
        log_Teffs_interp_array = dict_init["log_Teffs_interp_array"]

        self.logg = np.zeros_like(mass_range) + np.NaN
        self.teffs = np.zeros_like(mass_range) + np.NaN

        if not np.all(np.isnan(logages_interp_array)):
            # Now, for each mass, we interpolate along logg.
            for mass_idx in range(len(mass_range)):
                self.logg[mass_idx] = scipy.interpolate.interp1d(logages_interp_array.T[mass_idx], logg_interp_array.T[mass_idx], kind = interpolation_kind, bounds_error = False)(logage)
                self.teffs[mass_idx] = np.power(10.,scipy.interpolate.interp1d(logages_interp_array.T[mass_idx], log_Teffs_interp_array.T[mass_idx], kind = interpolation_kind, bounds_error = False)(logage))

    def __init__(self, input, output, logage = 9.69897, FeH = 0., interpolation_kind = "linear", output_unit = None):
        self.output_unit = output_unit
        self._default_input_unit = self.default_units[input]
        self._default_output_unit = self.default_units[output]
        if output_unit is None:
            self.output_unit = self._default_output_unit
        assert input in ["logg", "Teff"], "Input type not logg or Teff"
        assert output in ["logg", "Teff"], "Output type not logg or Teff"
        if hasattr(self, "logg"):
            pass
        else:
            self.preinit(logage = logage, FeH = FeH, interpolation_kind = interpolation_kind)

        if input == "logg":
            input_interp = self.logg
        elif input == "Teff":
            input_interp = self.teffs

        if output == "logg":
            output_interp = self.logg
        elif output == "Teff":
            output_interp = self.teffs

        ### Use only for our conditionals non-nan elements.
        if len(input_interp > 0):
            cond_valid = np.logical_and(np.logical_not(np.isnan(input_interp)), np.logical_not(np.isnan(output_interp)))

            # print zip(input_interp[cond_valid], output_interp[cond_valid])

            sorted_input_interp_trimmed = sorted(input_interp[cond_valid])
            sorted_output_interp_trimmed =\
            [y[1] for y in sorted(zip(input_interp[cond_valid], output_interp[cond_valid]), key = lambda x: x[0])]
            if len(sorted_input_interp_trimmed) > 2:
                self._evaluate =\
                scipy.interpolate.interp1d(sorted_input_interp_trimmed,
                                           sorted_output_interp_trimmed,
                                           kind = interpolation_kind,
                                           bounds_error = False,
                                           fill_value = np.nan, assume_sorted = True)
            else:
                self._evaluate =\
                scipy.interpolate.interp1d([-1e10,-1e10+1],
                                           [0.,0.],
                                           kind = interpolation_kind,
                                           bounds_error = False,
                                           fill_value = np.nan, assume_sorted = True)
        else:
            self._evaluate =\
            scipy.interpolate.interp1d([-1e10,-1e10+1],
                                       [0.,0.],
                                       kind = interpolation_kind,
                                       bounds_error = False,
                                       fill_value = np.nan, assume_sorted = True)
