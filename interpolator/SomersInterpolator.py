"""
sippy.interpolator.SomersInterpolator:
Interpolator for the Somers 2018 spotted isochrones.

Author: Lyra Cao
5/5/2018
"""

import numpy as np
from astropy import units as u
import re, os
from io import StringIO, BytesIO
from sippy.interpolator.BaseInterpolator import *
import scipy.optimize
import random

class SomersIsochroneInterpolator(BaseTripleIsochroneInterpolator):
    """Performs 2D interpolation on Somers & Pinsonneault, 2018 tracks"""
    directory = "SP/"
    default_units = \
    {"logage": u.dimensionless_unscaled,
    #"logage": u.dex(u.yr),
     "mass": u.Msun,
     #"logT": u.dex(u.K),
     #"logL": u.dex(u.Lsun)
     "logT": u.dimensionless_unscaled,
     "logL": u.dimensionless_unscaled,
     "spot_alpha": u.dimensionless_unscaled}
    # The following column headers are from load_data, ordered.
    column_headers = ["logage", "mass", "logT", "logL", "spot_alpha"]
    @classmethod
    def load_data(self):
        """Load Somers & Pinsonneault spotted tracks."""
        directory = os.path.dirname(os.path.abspath(__file__))\
        + "/Data/" + self.directory
        points = None
        files = [f for f in os.listdir(directory) if\
                 os.path.isfile(directory+f)]
        for filename in files:
            if points is None:
                points = np.loadtxt(directory+filename)
            points = np.concatenate((points,np.loadtxt(directory+filename)))
        points.T[0] = np.log10(points.T[0]) #log age!
        return np.array(points)
