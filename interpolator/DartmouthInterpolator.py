"""
sippy.interpolator.DartmouthInterpolator:
Interpolator for the Dartmouth 2014 isochrones.

Author: Lyra Cao
4/12/2018
"""

import numpy as np
from astropy import units as u
import re, os
from io import StringIO, BytesIO
from sippy.interpolator.BaseInterpolator import *
import scipy.optimize
import random

class DartmouthIsochroneInterpolator(BaseIsochroneInterpolator):
    """Performs 2D interpolation on Dartmouth"""
    directory = "dartmouth_tracks/"
    default_units = \
    {"logage": u.dimensionless_unscaled,
    #"logage": u.dex(u.yr),
     "mass": u.Msun,
     #"logT": u.dex(u.K),
     #"logL": u.dex(u.Lsun)
     "logT": u.dimensionless_unscaled,
     "logL": u.dimensionless_unscaled}
    # The following column headers are from load_data, ordered.
    column_headers = ["logage", "mass", "logT", "logL"]
    @classmethod
    def load_data(self):
        """Dartmouth tracks correspond to 375,286 points in isochrone space."""
        directory = os.path.dirname(os.path.abspath(__file__))\
        + "/Data/" + self.directory
        points = []
        files = [f for f in os.listdir(directory) if\
                 os.path.isfile(directory+f)]
        for filename in files:
            with open(directory+filename, "r") as f:
                isotxt = f.read()
                if len(isotxt) > 0:
                    mass_string = filename[1:].split("_")[0]
                    curr_mass = float(mass_string[0:1]+"."+mass_string[1:])
                    #print (curr_mass)
                    raw_points = np.genfromtxt(directory+filename,
                                               usecols = (0,1,3)).tolist()
                    for point_idx, point in enumerate(raw_points):
                        points.append([np.log10(point[0]),
                                       curr_mass,
                                       point[1],
                                       point[2]])
        return np.array(points)

class DartmouthBCVInterpolator(BaseInterpolator):
    # Define the default units of each column.
    default_units = \
    {"Teff": u.K,
     "Logg": u.dimensionless_unscaled,
     "[M/H]": u.dimensionless_unscaled,
     "[a/H]": u.dimensionless_unscaled,
     "B": u.mag,
     "V": u.mag,
     "R": u.mag,
     "I": u.mag,
     "BCV": u.mag}
    # The following column headers are directly from the file, and ordered:
    columns = ["logage", "Mini", "mass", "log_Teff", "logg", "log_L", "FeH", "Mv"]
    # Denote the explicit photometric bands here:
    bands = ["U", "B", "V", "R", "I", "J", "H", "Ks"]
    available_colors = ["BCV"]
    # No need to deal explicitly with missing values or filling values
    # by default
    missing_values = None
    filling_values = None
    # Skip 0 lines before reading in data:
    skip_header = 0
    # Read the entire file in:
    max_rows = None
    # Use default whitespace to delimit entries:
    delimiter = None
    # Comments are '#'
    comments = "#"
    def preinit(self, logg = 4.43812, FeH = 0., tiny = 1e-3, num_points = 1000, interpolation_kind = "linear"):
        """Run this method to pre-initialize DartmouthBCVInterpolator with a specific FeH."""
        j=np.loadtxt("sippy/interpolator/Data/DSEP/DSEP_calc")

        #Read through and demarcate individual logg, metallicity bins
        #For a chosen metallicity, find two bounding metallicities.
        indices = [[0]]
        FeHs = [j[0][self.columns.index("FeH")]]
        last_age = j[0][self.columns.index("logage")]
        for row_idx, row in enumerate(j[1:]):
            FeH_val = row[self.columns.index("FeH")]
            if np.nanmin(np.abs(FeH_val - np.array(FeHs))) > 1e-2:
                FeHs.append(FeH_val)
                indices.append([])
                indices[-1].append(row_idx)
                last_age = row[self.columns.index("logage")]
            elif np.abs(row[self.columns.index("logage")] - last_age) > 1e-1:
                indices[-1].append(row_idx)
                last_age = row[self.columns.index("logage")]

        # print "indices", indices
        # print "FeHs", FeHs

        if (not np.all(FeH > np.array(FeHs))) and (not np.all(FeH <= np.array(FeHs))):

            #For the choice of any specific FeH, we then can lookup the nearest two to interpolate between.

            FeH_left = np.max(np.array(FeHs)[FeH > np.array(FeHs)])
            ind_FeH_left = np.argmin(np.abs(FeH_left - np.array(FeHs)))
            FeH_right = np.min(np.array(FeHs)[FeH <= np.array(FeHs)])
            ind_FeH_right = np.argmin(np.abs(FeH_right - np.array(FeHs)))

            #Then for this bound in metallicity, we can then interpolate in logg.
            #First make sure we interpolate along an appropriate mass scale.
            #Note: we are not interested in RGB stars, where Teff is double-valued, so we truncate at 2.4 Msun.
            mass_range = np.logspace(np.log10(0.09), np.log10(2.4), 100)

            flat_indices = [item for sublist in indices for item in sublist]
            flat_indices.append(-1)

            logage_range = np.linspace(6.6,10.,24)

            #Interpolate along in 1D for this range. Get the mass-logg-BCV relationship.
            left_loggs_interp_array = np.zeros((len(indices[ind_FeH_left]), len(mass_range))) + np.NaN
            left_BCVs_interp_array = np.zeros((len(indices[ind_FeH_left]), len(mass_range))) + np.NaN
            left_log_Teffs_interp_array = np.zeros((len(indices[ind_FeH_left]), len(mass_range))) + np.NaN
            left_logages = np.zeros(len(indices[ind_FeH_left])) + np.NaN

            for index_idx, index_in_age in enumerate(indices[ind_FeH_left]):
                next_index_in_age = flat_indices[np.argmin(np.abs(index_in_age - np.array(flat_indices)))+1]

                left_loggs_interp_array[index_idx] =\
                scipy.interpolate.interp1d(j[index_in_age:next_index_in_age,self.columns.index("Mini")],
                j[index_in_age:next_index_in_age,self.columns.index("logg")],
                kind = interpolation_kind, bounds_error = False)(mass_range)

                left_BCVs_interp_array[index_idx] =\
                scipy.interpolate.interp1d(j[index_in_age:next_index_in_age,self.columns.index("Mini")],
                4.74 - j[index_in_age:next_index_in_age,self.columns.index("Mv")] - 2.5*j[index_in_age:next_index_in_age,self.columns.index("log_L")],
                kind = interpolation_kind, bounds_error = False)(mass_range)

                left_log_Teffs_interp_array[index_idx] =\
                scipy.interpolate.interp1d(j[index_in_age:next_index_in_age,self.columns.index("Mini")],
                j[index_in_age:next_index_in_age,self.columns.index("log_Teff")],
                kind = interpolation_kind, bounds_error = False)(mass_range)

                left_logages[index_idx] = j[index_in_age, self.columns.index("logage")]

            # print "left_logages", left_logages
            #
            # print "left_BCVs_interp_array", left_BCVs_interp_array
            # print "left_loggs_interp_array", left_loggs_interp_array
            # print "left_log_Teffs_interp_array", left_log_Teffs_interp_array
            #
            # print "left_logages[np.logical_not(np.isnan(left_BCVs_interp_array.T[index_idx]))]", left_logages[np.logical_not(np.isnan(left_BCVs_interp_array.T[index_idx]))]
            # print "left_BCVs_interp_array.T[index_idx][np.logical_not(np.isnan(left_BCVs_interp_array.T[index_idx]))]", left_BCVs_interp_array.T[index_idx][np.logical_not(np.isnan(left_BCVs_interp_array.T[index_idx]))]

            l_loggs_interp_array = np.zeros((len(logage_range), len(mass_range))) + np.NaN
            l_BCVs_interp_array = np.zeros((len(logage_range), len(mass_range))) + np.NaN
            l_log_Teffs_interp_array = np.zeros((len(logage_range), len(mass_range))) + np.NaN
            for index_idx in range(len(mass_range)):
                if np.sum(np.logical_not(np.isnan(left_loggs_interp_array.T[index_idx]))) > 1:
                    l_loggs_interp_array.T[index_idx] = scipy.interpolate.interp1d(left_logages[np.logical_not(np.isnan(left_loggs_interp_array.T[index_idx]))],
                    left_loggs_interp_array.T[index_idx][np.logical_not(np.isnan(left_loggs_interp_array.T[index_idx]))],
                    kind = interpolation_kind, bounds_error = False)(logage_range)
                else:
                    l_loggs_interp_array.T[index_idx] = np.NaN

                if np.sum(np.logical_not(np.isnan(left_BCVs_interp_array.T[index_idx]))) > 1:
                    l_BCVs_interp_array.T[index_idx] = scipy.interpolate.interp1d(left_logages[np.logical_not(np.isnan(left_BCVs_interp_array.T[index_idx]))],
                    left_BCVs_interp_array.T[index_idx][np.logical_not(np.isnan(left_BCVs_interp_array.T[index_idx]))],
                    kind = interpolation_kind, bounds_error = False)(logage_range)
                else:
                    l_BCVs_interp_array.T[index_idx] = np.NaN

                if np.sum(np.logical_not(np.isnan(left_log_Teffs_interp_array.T[index_idx]))) > 1:
                    l_log_Teffs_interp_array.T[index_idx] = scipy.interpolate.interp1d(left_logages[np.logical_not(np.isnan(left_log_Teffs_interp_array.T[index_idx]))],
                    left_log_Teffs_interp_array.T[index_idx][np.logical_not(np.isnan(left_log_Teffs_interp_array.T[index_idx]))],
                    kind = interpolation_kind, bounds_error = False)(logage_range)
                else:
                    l_log_Teffs_interp_array.T[index_idx] = np.NaN

            right_loggs_interp_array = np.zeros((len(indices[ind_FeH_right]), len(mass_range))) + np.NaN
            right_BCVs_interp_array = np.zeros((len(indices[ind_FeH_right]), len(mass_range))) + np.NaN
            right_log_Teffs_interp_array = np.zeros((len(indices[ind_FeH_right]), len(mass_range))) + np.NaN
            right_logages = np.zeros(len(indices[ind_FeH_right])) + np.NaN

            for index_idx, index_in_age in enumerate(indices[ind_FeH_right]):
                next_index_in_age = flat_indices[np.argmin(np.abs(index_in_age - np.array(flat_indices)))+1]

                right_loggs_interp_array[index_idx] =\
                scipy.interpolate.interp1d(j[index_in_age:next_index_in_age,self.columns.index("Mini")],
                j[index_in_age:next_index_in_age,self.columns.index("logg")],
                kind = interpolation_kind, bounds_error = False)(mass_range)

                right_BCVs_interp_array[index_idx] =\
                scipy.interpolate.interp1d(j[index_in_age:next_index_in_age,self.columns.index("Mini")],
                4.74 - j[index_in_age:next_index_in_age,self.columns.index("Mv")] - 2.5*j[index_in_age:next_index_in_age,self.columns.index("log_L")],
                kind = interpolation_kind, bounds_error = False)(mass_range)

                right_log_Teffs_interp_array[index_idx] =\
                scipy.interpolate.interp1d(j[index_in_age:next_index_in_age,self.columns.index("Mini")],
                j[index_in_age:next_index_in_age,self.columns.index("log_Teff")],
                kind = interpolation_kind, bounds_error = False)(mass_range)

                right_logages[index_idx] = j[index_in_age, self.columns.index("logage")]

            r_loggs_interp_array = np.zeros((len(logage_range), len(mass_range))) + np.NaN
            r_BCVs_interp_array = np.zeros((len(logage_range), len(mass_range))) + np.NaN
            r_log_Teffs_interp_array = np.zeros((len(logage_range), len(mass_range))) + np.NaN
            for index_idx in range(len(mass_range)):
                if np.sum(np.logical_not(np.isnan(right_loggs_interp_array.T[index_idx]))) > 1:
                    r_loggs_interp_array.T[index_idx] = scipy.interpolate.interp1d(right_logages[np.logical_not(np.isnan(right_loggs_interp_array.T[index_idx]))],
                    right_loggs_interp_array.T[index_idx][np.logical_not(np.isnan(right_loggs_interp_array.T[index_idx]))],
                    kind = interpolation_kind, bounds_error = False)(logage_range)
                else:
                    r_loggs_interp_array.T[index_idx] = np.NaN

                if np.sum(np.logical_not(np.isnan(right_BCVs_interp_array.T[index_idx]))) > 1:
                    r_BCVs_interp_array.T[index_idx] = scipy.interpolate.interp1d(right_logages[np.logical_not(np.isnan(right_BCVs_interp_array.T[index_idx]))],
                    right_BCVs_interp_array.T[index_idx][np.logical_not(np.isnan(right_BCVs_interp_array.T[index_idx]))],
                    kind = interpolation_kind, bounds_error = False)(logage_range)
                else:
                    r_BCVs_interp_array.T[index_idx] = np.NaN

                if np.sum(np.logical_not(np.isnan(right_log_Teffs_interp_array.T[index_idx]))) > 1:
                    r_log_Teffs_interp_array.T[index_idx] = scipy.interpolate.interp1d(right_logages[np.logical_not(np.isnan(right_log_Teffs_interp_array.T[index_idx]))],
                    right_log_Teffs_interp_array.T[index_idx][np.logical_not(np.isnan(right_log_Teffs_interp_array.T[index_idx]))],
                    kind = interpolation_kind, bounds_error = False)(logage_range)
                else:
                    r_log_Teffs_interp_array.T[index_idx] = np.NaN

            # print "l_BCVs_interp_array", l_BCVs_interp_array
            # print "l_loggs_interp_array", l_loggs_interp_array
            # print "l_log_Teffs_interp_array", l_log_Teffs_interp_array

            loggs_interp_array = l_loggs_interp_array + (r_loggs_interp_array - l_loggs_interp_array) * (FeH - FeH_left)/(FeH_right - FeH_left)
            BCVs_interp_array = l_BCVs_interp_array + (r_BCVs_interp_array - l_BCVs_interp_array) * (FeH - FeH_left)/(FeH_right - FeH_left)
            log_Teffs_interp_array = l_log_Teffs_interp_array + (r_log_Teffs_interp_array - l_log_Teffs_interp_array) * (FeH - FeH_left)/(FeH_right - FeH_left)


            # print "BCVs_interp_array", BCVs_interp_array
            # print "loggs_interp_array", loggs_interp_array
            # print "log_Teffs_interp_array", log_Teffs_interp_array

            self.BCVs = np.zeros_like(mass_range) + np.NaN
            self.teffs = np.zeros_like(mass_range) + np.NaN

            # Now, for each mass, we interpolate along logg.
            for mass_idx in range(len(mass_range)):
                self.BCVs[mass_idx] = scipy.interpolate.interp1d(loggs_interp_array.T[mass_idx], BCVs_interp_array.T[mass_idx], kind = interpolation_kind, bounds_error = False)(logg)
                self.teffs[mass_idx] = np.power(10.,scipy.interpolate.interp1d(loggs_interp_array.T[mass_idx], log_Teffs_interp_array.T[mass_idx], kind = interpolation_kind, bounds_error = False)(logg))

        else:
            mass_range = np.logspace(np.log10(0.09), np.log10(2.4), 100)
            self.BCVs = np.zeros_like(mass_range) + np.NaN
            self.teffs = np.zeros_like(mass_range) + np.NaN

    def __init__(self, input, output, logg = 4.43812, FeH = 0., interpolation_kind = "linear", output_unit = None):
        self.output_unit = output_unit
        self._default_input_unit = self.default_units[input]
        self._default_output_unit = self.default_units[output]
        if output_unit is None:
            self.output_unit = self._default_output_unit
        assert input in ["BCV", "Teff"], "Input type not BCV or Teff"
        assert output in ["BCV", "Teff"], "Output type not BCV or Teff"
        if hasattr(self, "BCVs"):
            pass
        else:
            self.preinit(logg = logg, FeH = FeH, interpolation_kind = interpolation_kind)

        if input == "BCV":
            input_interp = self.BCVs
        elif input == "Teff":
            input_interp = self.teffs

        if output == "BCV":
            output_interp = self.BCVs
        elif output == "Teff":
            output_interp = self.teffs

        ### Use only for our conditionals non-nan elements.
        if len(input_interp > 0):
            cond_valid = np.logical_and(np.logical_not(np.isnan(input_interp)), np.logical_not(np.isnan(output_interp)))

            # print zip(input_interp[cond_valid], output_interp[cond_valid])

            sorted_input_interp_trimmed = sorted(input_interp[cond_valid])
            sorted_output_interp_trimmed =\
            [y[1] for y in sorted(zip(input_interp[cond_valid], output_interp[cond_valid]), key = lambda x: x[0])]
            if len(sorted_input_interp_trimmed) > 1:
                self._evaluate =\
                scipy.interpolate.interp1d(sorted_input_interp_trimmed,
                                           sorted_output_interp_trimmed,
                                           kind = interpolation_kind,
                                           bounds_error = False,
                                           fill_value = np.nan, assume_sorted = True)
            else:
                self._evaluate =\
                scipy.interpolate.interp1d([-1e10,-1e10+1],
                                           [0.,0.],
                                           kind = interpolation_kind,
                                           bounds_error = False,
                                           fill_value = np.nan, assume_sorted = True)
        else:
            self._evaluate =\
            scipy.interpolate.interp1d([-1e10,-1e10+1],
                                       [0.,0.],
                                       kind = interpolation_kind,
                                       bounds_error = False,
                                       fill_value = np.nan, assume_sorted = True)

class DartmouthMassRadiusInterpolator(BaseInterpolator):
    # Define the default units of each column.
    default_units = \
    {"Teff": u.K,
     "Logg": u.dimensionless_unscaled,
     "[M/H]": u.dimensionless_unscaled,
     "[a/H]": u.dimensionless_unscaled,
     "B": u.mag,
     "V": u.mag,
     "R": u.mag,
     "I": u.mag,
     "BCV": u.mag,
     "mass": u.Msun,
     "radius": u.Rsun}
    # The following column headers are directly from the file, and ordered:
    columns = ["logage", "Mini", "mass", "log_Teff", "logg", "log_L", "FeH", "Mv"]
    # Denote the explicit photometric bands here:
    bands = ["U", "B", "V", "R", "I", "J", "H", "Ks"]
    available_colors = ["mass", "radius"]
    # No need to deal explicitly with missing values or filling values
    # by default
    missing_values = None
    filling_values = None
    # Skip 0 lines before reading in data:
    skip_header = 0
    # Read the entire file in:
    max_rows = None
    # Use default whitespace to delimit entries:
    delimiter = None
    # Comments are '#'
    comments = "#"
    def preinit(self, logg = 4.43812, FeH = 0., tiny = 1e-3, num_points = 1000, interpolation_kind = "linear"):
        """Run this method to pre-initialize DartmouthBCVInterpolator with a specific FeH."""
        j=np.loadtxt("sippy/interpolator/Data/DSEP/DSEP_calc")

        #Read through and demarcate individual logg, metallicity bins
        #For a chosen metallicity, find two bounding metallicities.
        indices = [[0]]
        FeHs = [j[0][self.columns.index("FeH")]]
        last_age = j[0][self.columns.index("logage")]
        for row_idx, row in enumerate(j[1:]):
            FeH_val = row[self.columns.index("FeH")]
            if np.nanmin(np.abs(FeH_val - np.array(FeHs))) > 1e-1:
                FeHs.append(FeH_val)
                indices.append([])
                indices[-1].append(row_idx)
                last_age = row[self.columns.index("logage")]
            elif np.abs(row[self.columns.index("logage")] - last_age) > 1e-1:
                indices[-1].append(row_idx)
                last_age = row[self.columns.index("logage")]

        if (not np.all(FeH > np.array(FeHs))) and (not np.all(FeH <= np.array(FeHs))):

            #For the choice of any specific FeH, we then can lookup the nearest two to interpolate between.

            FeH_left = np.max(np.array(FeHs)[FeH > np.array(FeHs)])
            ind_FeH_left = np.argmin(np.abs(FeH_left - np.array(FeHs)))
            FeH_right = np.min(np.array(FeHs)[FeH <= np.array(FeHs)])
            ind_FeH_right = np.argmin(np.abs(FeH_right - np.array(FeHs)))

            #Then for this bound in metallicity, we can then interpolate in logg.
            #First make sure we interpolate along an appropriate mass scale.
            #Note: we are not interested in RGB stars, where Teff is double-valued, so we truncate at 2.4 Msun.
            mass_range = np.logspace(np.log10(0.09), np.log10(2.4), 100)

            flat_indices = [item for sublist in indices for item in sublist]
            flat_indices.append(-1)
            logage_range = np.linspace(6.6,10.,24)

            #Interpolate along in 1D for this range. Get the mass-logg-BCV relationship.
            left_loggs_interp_array = np.zeros((len(indices[ind_FeH_left]), len(mass_range))) + np.NaN
            left_masses_interp_array = np.zeros((len(indices[ind_FeH_left]), len(mass_range))) + np.NaN
            left_log_radius_interp_array = np.zeros((len(indices[ind_FeH_left]), len(mass_range))) + np.NaN
            left_logages = np.zeros(len(indices[ind_FeH_left])) + np.NaN

            for index_idx, index_in_age in enumerate(indices[ind_FeH_left]):
                next_index_in_age = flat_indices[np.argmin(np.abs(index_in_age - np.array(flat_indices)))+1]

                left_loggs_interp_array[index_idx] =\
                scipy.interpolate.interp1d(j[index_in_age:next_index_in_age,self.columns.index("Mini")],
                j[index_in_age:next_index_in_age,self.columns.index("logg")],
                kind = interpolation_kind, bounds_error = False)(mass_range)

                left_masses_interp_array[index_idx] =\
                scipy.interpolate.interp1d(j[index_in_age:next_index_in_age,self.columns.index("Mini")],
                j[index_in_age:next_index_in_age,self.columns.index("mass")],
                kind = interpolation_kind, bounds_error = False)(mass_range)

                left_log_radius_interp_array[index_idx] =\
                0.5*(scipy.interpolate.interp1d(j[index_in_age:next_index_in_age,self.columns.index("Mini")],
                j[index_in_age:next_index_in_age,self.columns.index("log_L")],
                kind = interpolation_kind, bounds_error = False)(mass_range))\
                -2.*scipy.interpolate.interp1d(j[index_in_age:next_index_in_age,self.columns.index("Mini")],
                j[index_in_age:next_index_in_age,self.columns.index("log_Teff")],
                kind = interpolation_kind, bounds_error = False)(mass_range)\
                +2.*np.log10(5777.)

                left_logages[index_idx] = j[index_in_age, self.columns.index("logage")]

            l_loggs_interp_array = np.zeros((len(logage_range), len(mass_range))) + np.NaN
            l_masses_interp_array = np.zeros((len(logage_range), len(mass_range))) + np.NaN
            l_log_radius_interp_array = np.zeros((len(logage_range), len(mass_range))) + np.NaN

            for index_idx in range(len(mass_range)):
                if np.sum(np.logical_not(np.isnan(left_loggs_interp_array.T[index_idx]))) > 1:
                    l_loggs_interp_array.T[index_idx] = scipy.interpolate.interp1d(left_logages[np.logical_not(np.isnan(left_loggs_interp_array.T[index_idx]))],
                    left_loggs_interp_array.T[index_idx][np.logical_not(np.isnan(left_loggs_interp_array.T[index_idx]))],
                    kind = interpolation_kind, bounds_error = False)(logage_range)
                else:
                    l_loggs_interp_array.T[index_idx] = np.NaN

                if np.sum(np.logical_not(np.isnan(left_masses_interp_array.T[index_idx]))) > 1:
                    l_masses_interp_array.T[index_idx] = scipy.interpolate.interp1d(left_logages[np.logical_not(np.isnan(left_masses_interp_array.T[index_idx]))],
                    left_masses_interp_array.T[index_idx][np.logical_not(np.isnan(left_masses_interp_array.T[index_idx]))],
                    kind = interpolation_kind, bounds_error = False)(logage_range)
                else:
                    l_masses_interp_array.T[index_idx] = np.NaN

                if np.sum(np.logical_not(np.isnan(left_log_radius_interp_array.T[index_idx]))) > 1:
                    l_log_radius_interp_array.T[index_idx] = scipy.interpolate.interp1d(left_logages[np.logical_not(np.isnan(left_log_radius_interp_array.T[index_idx]))],
                    left_log_radius_interp_array.T[index_idx][np.logical_not(np.isnan(left_log_radius_interp_array.T[index_idx]))],
                    kind = interpolation_kind, bounds_error = False)(logage_range)
                else:
                    l_log_radius_interp_array.T[index_idx] = np.NaN

            right_loggs_interp_array = np.zeros((len(indices[ind_FeH_right]), len(mass_range))) + np.NaN
            right_masses_interp_array = np.zeros((len(indices[ind_FeH_right]), len(mass_range))) + np.NaN
            right_log_radius_interp_array = np.zeros((len(indices[ind_FeH_right]), len(mass_range))) + np.NaN
            right_logages = np.zeros(len(indices[ind_FeH_right])) + np.NaN

            for index_idx, index_in_age in enumerate(indices[ind_FeH_right]):
                next_index_in_age = flat_indices[np.argmin(np.abs(index_in_age - np.array(flat_indices)))+1]

                right_loggs_interp_array[index_idx] =\
                scipy.interpolate.interp1d(j[index_in_age:next_index_in_age,self.columns.index("Mini")],
                j[index_in_age:next_index_in_age,self.columns.index("logg")],
                kind = interpolation_kind, bounds_error = False)(mass_range)

                right_masses_interp_array[index_idx] =\
                scipy.interpolate.interp1d(j[index_in_age:next_index_in_age,self.columns.index("Mini")],
                j[index_in_age:next_index_in_age,self.columns.index("mass")],
                kind = interpolation_kind, bounds_error = False)(mass_range)

                right_log_radius_interp_array[index_idx] =\
                0.5*(scipy.interpolate.interp1d(j[index_in_age:next_index_in_age,self.columns.index("Mini")],
                j[index_in_age:next_index_in_age,self.columns.index("log_L")],
                kind = interpolation_kind, bounds_error = False)(mass_range))\
                -2.*scipy.interpolate.interp1d(j[index_in_age:next_index_in_age,self.columns.index("Mini")],
                j[index_in_age:next_index_in_age,self.columns.index("log_Teff")],
                kind = interpolation_kind, bounds_error = False)(mass_range)\
                +2.*np.log10(5777.)

                right_logages[index_idx] = j[index_in_age, self.columns.index("logage")]

            r_loggs_interp_array = np.zeros((len(logage_range), len(mass_range))) + np.NaN
            r_masses_interp_array = np.zeros((len(logage_range), len(mass_range))) + np.NaN
            r_log_radius_interp_array = np.zeros((len(logage_range), len(mass_range))) + np.NaN

            for index_idx in range(len(mass_range)):

                if np.sum(np.logical_not(np.isnan(right_loggs_interp_array.T[index_idx]))) > 1:
                    r_loggs_interp_array.T[index_idx] = scipy.interpolate.interp1d(right_logages[np.logical_not(np.isnan(right_loggs_interp_array.T[index_idx]))],
                    right_loggs_interp_array.T[index_idx][np.logical_not(np.isnan(right_loggs_interp_array.T[index_idx]))],
                    kind = interpolation_kind, bounds_error = False)(logage_range)
                else:
                    r_loggs_interp_array.T[index_idx] = np.NaN

                if np.sum(np.logical_not(np.isnan(right_masses_interp_array.T[index_idx]))) > 1:
                    r_masses_interp_array.T[index_idx] = scipy.interpolate.interp1d(right_logages[np.logical_not(np.isnan(right_masses_interp_array.T[index_idx]))],
                    right_masses_interp_array.T[index_idx][np.logical_not(np.isnan(right_masses_interp_array.T[index_idx]))],
                    kind = interpolation_kind, bounds_error = False)(logage_range)
                else:
                    r_masses_interp_array.T[index_idx] = np.NaN

                if np.sum(np.logical_not(np.isnan(right_log_radius_interp_array.T[index_idx]))) > 1:
                    r_log_radius_interp_array.T[index_idx] = scipy.interpolate.interp1d(right_logages[np.logical_not(np.isnan(right_log_radius_interp_array.T[index_idx]))],
                    right_log_radius_interp_array.T[index_idx][np.logical_not(np.isnan(right_log_radius_interp_array.T[index_idx]))],
                    kind = interpolation_kind, bounds_error = False)(logage_range)
                else:
                    r_log_radius_interp_array.T[index_idx] = np.NaN

            loggs_interp_array = l_loggs_interp_array + (r_loggs_interp_array - l_loggs_interp_array) * (FeH - FeH_left)/(FeH_right - FeH_left)
            masses_interp_array = l_masses_interp_array + (r_masses_interp_array - l_masses_interp_array) * (FeH - FeH_left)/(FeH_right - FeH_left)
            log_radius_interp_array = l_log_radius_interp_array + (r_log_radius_interp_array - l_log_radius_interp_array) * (FeH - FeH_left)/(FeH_right - FeH_left)

            self.mass = np.zeros_like(mass_range) + np.NaN
            self.radius = np.zeros_like(mass_range) + np.NaN

            # Now, for each mass, we interpolate along logg.
            for mass_idx in range(len(mass_range)):
                self.mass[mass_idx] = scipy.interpolate.interp1d(loggs_interp_array.T[mass_idx], masses_interp_array.T[mass_idx], kind = interpolation_kind, bounds_error = False)(logg)
                self.radius[mass_idx] = np.power(10.,scipy.interpolate.interp1d(loggs_interp_array.T[mass_idx], log_radius_interp_array.T[mass_idx], kind = interpolation_kind, bounds_error = False)(logg))

        else:
            mass_range = np.logspace(np.log10(0.09), np.log10(2.4), 100)
            self.mass = np.zeros_like(mass_range) + np.NaN
            self.radius = np.zeros_like(mass_range) + np.NaN
    def __init__(self, input, output, logg = 4.43812, FeH = 0., interpolation_kind = "linear", output_unit = None):
        self.output_unit = output_unit
        self._default_input_unit = self.default_units[input]
        self._default_output_unit = self.default_units[output]
        if output_unit is None:
            self.output_unit = self._default_output_unit
        assert input in ["mass", "radius"], "Input type not mass or radius"
        assert output in ["mass", "radius"], "Output type not mass or radius"
        if hasattr(self, "mass"):
            pass
        else:
            self.preinit(logg = logg, FeH = FeH, interpolation_kind = interpolation_kind)

        if input == "mass":
            input_interp = self.mass
        elif input == "radius":
            input_interp = self.radius

        if output == "mass":
            output_interp = self.mass
        elif output == "radius":
            output_interp = self.radius

        ### Use only for our conditionals non-nan elements.

        cond_valid = np.logical_and(np.logical_not(np.isnan(input_interp)), np.logical_not(np.isnan(output_interp)))

        # print zip(input_interp[cond_valid], output_interp[cond_valid])

        sorted_input_interp_trimmed = sorted(input_interp[cond_valid])
        sorted_output_interp_trimmed =\
        [y[1] for y in sorted(zip(input_interp[cond_valid], output_interp[cond_valid]), key = lambda x: x[0])]
        if len(sorted_input_interp_trimmed) > 1:
            self._evaluate =\
            scipy.interpolate.interp1d(sorted_input_interp_trimmed,
                                       sorted_output_interp_trimmed,
                                       kind = interpolation_kind,
                                       bounds_error = False,
                                       fill_value = np.nan, assume_sorted = True)
        else:
            self._evaluate =\
            scipy.interpolate.interp1d([-1e10,-1e10+1],
                                       [0.,0.],
                                       kind = interpolation_kind,
                                       bounds_error = False,
                                       fill_value = np.nan, assume_sorted = True)

class DartmouthGravityAgeInterpolator(BaseInterpolator):
    # Define the default units of each column.
    default_units = \
    {"Teff": u.K,
     "Logg": u.dimensionless_unscaled,
     "[M/H]": u.dimensionless_unscaled,
     "[a/H]": u.dimensionless_unscaled,
     "B": u.mag,
     "V": u.mag,
     "R": u.mag,
     "I": u.mag,
     "logg": u.dimensionless_unscaled,
     "mass": u.Msun}
    # The following column headers are directly from the file, and ordered:
    columns = ["logage", "Mini", "mass", "log_Teff", "logg", "log_L", "FeH", "Mv"]
    # Denote the explicit photometric bands here:
    bands = ["U", "B", "V", "R", "I", "J", "H", "Ks"]
    available_colors = ["logg"]
    # No need to deal explicitly with missing values or filling values
    # by default
    missing_values = None
    filling_values = None
    # Skip 0 lines before reading in data:
    skip_header = 0
    # Read the entire file in:
    max_rows = None
    # Use default whitespace to delimit entries:
    delimiter = None
    # Comments are '#'
    comments = "#"
    def preinit(self, logage = 9.69897, FeH = 0., tiny = 1e-3, num_points = 1000, interpolation_kind = "linear"):
        """Run this method to pre-initialize DartmouthBCVInterpolator with a specific FeH."""
        j=np.loadtxt("sippy/interpolator/Data/DSEP/DSEP_calc")

        #Read through and demarcate individual logage, metallicity bins
        #For a chosen metallicity, find two bounding metallicities.
        indices = [[0]]
        FeHs = [j[0][self.columns.index("FeH")]]
        last_age = j[0][self.columns.index("logage")]
        for row_idx, row in enumerate(j[1:]):
            FeH_val = row[self.columns.index("FeH")]
            if np.nanmin(np.abs(FeH_val - np.array(FeHs))) > 1e-1:
                FeHs.append(FeH_val)
                indices.append([])
                indices[-1].append(row_idx)
                last_age = row[self.columns.index("logage")]
            elif np.abs(row[self.columns.index("logage")] - last_age) > 1e-1:
                indices[-1].append(row_idx)
                last_age = row[self.columns.index("logage")]

        # print "FeH", FeH
        # print "FeH > np.array(FeHs)", FeH > np.array(FeHs), (not np.all(FeH > np.array(FeHs)))
        # print "FeH <= np.array(FeHs)", FeH <= np.array(FeHs), (not np.all(FeH <= np.array(FeHs)))

        if (not np.all(FeH > np.array(FeHs))) and (not np.all(FeH <= np.array(FeHs))):

            #For the choice of any specific FeH, we then can lookup the nearest two to interpolate between.

            FeH_left = np.max(np.array(FeHs)[FeH > np.array(FeHs)])
            ind_FeH_left = np.argmin(np.abs(FeH_left - np.array(FeHs)))
            FeH_right = np.min(np.array(FeHs)[FeH <= np.array(FeHs)])
            ind_FeH_right = np.argmin(np.abs(FeH_right - np.array(FeHs)))

            #Then for this bound in metallicity, we can then interpolate in logage.
            #First make sure we interpolate along an appropriate mass scale.
            #Note: we are not interested in RGB stars, where Teff is double-valued, so we truncate at 2.4 Msun.
            mass_range = np.logspace(np.log10(0.09), np.log10(2.4), 100)

            flat_indices = [item for sublist in indices for item in sublist]
            flat_indices.append(-1)
            left_logages = np.zeros(len(indices[ind_FeH_left])) + np.NaN

            logage_range = np.linspace(6.6,10.,24)

            #Interpolate along in 1D for this range. Get the mass-logage-BCV relationship.
            left_logages_interp_array = np.zeros((len(indices[ind_FeH_left]), len(mass_range))) + np.NaN
            left_logg_interp_array = np.zeros((len(indices[ind_FeH_left]), len(mass_range))) + np.NaN
            left_log_Teffs_interp_array = np.zeros((len(indices[ind_FeH_left]), len(mass_range))) + np.NaN

            for index_idx, index_in_age in enumerate(indices[ind_FeH_left]):
                next_index_in_age = flat_indices[np.argmin(np.abs(index_in_age - np.array(flat_indices)))+1]

                left_logages_interp_array[index_idx] =\
                scipy.interpolate.interp1d(j[index_in_age:next_index_in_age,self.columns.index("Mini")],
                j[index_in_age:next_index_in_age,self.columns.index("logage")],
                kind = interpolation_kind, bounds_error = False)(mass_range)

                left_logg_interp_array[index_idx] =\
                scipy.interpolate.interp1d(j[index_in_age:next_index_in_age,self.columns.index("Mini")],
                j[index_in_age:next_index_in_age,self.columns.index("logg")],
                kind = interpolation_kind, bounds_error = False)(mass_range)

                left_log_Teffs_interp_array[index_idx] =\
                scipy.interpolate.interp1d(j[index_in_age:next_index_in_age,self.columns.index("Mini")],
                j[index_in_age:next_index_in_age,self.columns.index("log_Teff")],
                kind = interpolation_kind, bounds_error = False)(mass_range)

                left_logages[index_idx] = j[index_in_age, self.columns.index("logage")]

            l_logages_interp_array = np.zeros((len(logage_range), len(mass_range))) + np.NaN
            l_logg_interp_array = np.zeros((len(logage_range), len(mass_range))) + np.NaN
            l_log_Teffs_interp_array = np.zeros((len(logage_range), len(mass_range))) + np.NaN

            for index_idx in range(len(mass_range)):
                if np.sum(np.logical_not(np.isnan(left_logages_interp_array.T[index_idx]))) > 1:
                    l_logages_interp_array.T[index_idx] = scipy.interpolate.interp1d(left_logages[np.logical_not(np.isnan(left_logages_interp_array.T[index_idx]))],
                    left_logages_interp_array.T[index_idx][np.logical_not(np.isnan(left_logages_interp_array.T[index_idx]))],
                    kind = interpolation_kind, bounds_error = False)(logage_range)
                else:
                    l_logages_interp_array.T[index_idx] = np.NaN

                if np.sum(np.logical_not(np.isnan(left_logg_interp_array.T[index_idx]))) > 1:
                    l_logg_interp_array.T[index_idx] = scipy.interpolate.interp1d(left_logages[np.logical_not(np.isnan(left_logg_interp_array.T[index_idx]))],
                    left_logg_interp_array.T[index_idx][np.logical_not(np.isnan(left_logg_interp_array.T[index_idx]))],
                    kind = interpolation_kind, bounds_error = False)(logage_range)
                else:
                    l_logg_interp_array.T[index_idx] = np.NaN

                if np.sum(np.logical_not(np.isnan(left_log_Teffs_interp_array.T[index_idx]))) > 1:
                    l_log_Teffs_interp_array.T[index_idx] = scipy.interpolate.interp1d(left_logages[np.logical_not(np.isnan(left_log_Teffs_interp_array.T[index_idx]))],
                    left_log_Teffs_interp_array.T[index_idx][np.logical_not(np.isnan(left_log_Teffs_interp_array.T[index_idx]))],
                    kind = interpolation_kind, bounds_error = False)(logage_range)
                else:
                    l_log_Teffs_interp_array.T[index_idx] = np.NaN

            right_logages = np.zeros(len(indices[ind_FeH_right])) + np.NaN
            right_logages_interp_array = np.zeros((len(indices[ind_FeH_right]), len(mass_range))) + np.NaN
            right_logg_interp_array = np.zeros((len(indices[ind_FeH_right]), len(mass_range))) + np.NaN
            right_log_Teffs_interp_array = np.zeros((len(indices[ind_FeH_right]), len(mass_range))) + np.NaN

            for index_idx, index_in_age in enumerate(indices[ind_FeH_right]):
                next_index_in_age = flat_indices[np.argmin(np.abs(index_in_age - np.array(flat_indices)))+1]

                right_logages_interp_array[index_idx] =\
                scipy.interpolate.interp1d(j[index_in_age:next_index_in_age,self.columns.index("Mini")],
                j[index_in_age:next_index_in_age,self.columns.index("logage")],
                kind = interpolation_kind, bounds_error = False)(mass_range)

                right_logg_interp_array[index_idx] =\
                scipy.interpolate.interp1d(j[index_in_age:next_index_in_age,self.columns.index("Mini")],
                j[index_in_age:next_index_in_age,self.columns.index("logg")],
                kind = interpolation_kind, bounds_error = False)(mass_range)

                right_log_Teffs_interp_array[index_idx] =\
                scipy.interpolate.interp1d(j[index_in_age:next_index_in_age,self.columns.index("Mini")],
                j[index_in_age:next_index_in_age,self.columns.index("log_Teff")],
                kind = interpolation_kind, bounds_error = False)(mass_range)

                right_logages[index_idx] = j[index_in_age, self.columns.index("logage")]

            r_logages_interp_array = np.zeros((len(logage_range), len(mass_range))) + np.NaN
            r_logg_interp_array = np.zeros((len(logage_range), len(mass_range))) + np.NaN
            r_log_Teffs_interp_array = np.zeros((len(logage_range), len(mass_range))) + np.NaN

            for index_idx in range(len(mass_range)):
                if np.sum(np.logical_not(np.isnan(right_logages_interp_array.T[index_idx]))) > 1:
                    r_logages_interp_array.T[index_idx] = scipy.interpolate.interp1d(right_logages[np.logical_not(np.isnan(right_logages_interp_array.T[index_idx]))],
                    right_logages_interp_array.T[index_idx][np.logical_not(np.isnan(right_logages_interp_array.T[index_idx]))],
                    kind = interpolation_kind, bounds_error = False)(logage_range)
                else:
                    r_logages_interp_array.T[index_idx] = np.NaN

                if np.sum(np.logical_not(np.isnan(right_logg_interp_array.T[index_idx]))) > 1:
                    r_logg_interp_array.T[index_idx] = scipy.interpolate.interp1d(right_logages[np.logical_not(np.isnan(right_logg_interp_array.T[index_idx]))],
                    right_logg_interp_array.T[index_idx][np.logical_not(np.isnan(right_logg_interp_array.T[index_idx]))],
                    kind = interpolation_kind, bounds_error = False)(logage_range)
                else:
                    r_logg_interp_array.T[index_idx] = np.NaN

                if np.sum(np.logical_not(np.isnan(right_log_Teffs_interp_array.T[index_idx]))) > 1:
                    r_log_Teffs_interp_array.T[index_idx] = scipy.interpolate.interp1d(right_logages[np.logical_not(np.isnan(right_log_Teffs_interp_array.T[index_idx]))],
                    right_log_Teffs_interp_array.T[index_idx][np.logical_not(np.isnan(right_log_Teffs_interp_array.T[index_idx]))],
                    kind = interpolation_kind, bounds_error = False)(logage_range)
                else:
                    r_log_Teffs_interp_array.T[index_idx] = np.NaN

            logages_interp_array = l_logages_interp_array + (r_logages_interp_array - l_logages_interp_array) * (FeH - FeH_left)/(FeH_right - FeH_left)
            logg_interp_array = l_logg_interp_array + (r_logg_interp_array - l_logg_interp_array) * (FeH - FeH_left)/(FeH_right - FeH_left)
            log_Teffs_interp_array = l_log_Teffs_interp_array + (r_log_Teffs_interp_array - l_log_Teffs_interp_array) * (FeH - FeH_left)/(FeH_right - FeH_left)

            self.logg = np.zeros_like(mass_range) + np.NaN
            self.teffs = np.zeros_like(mass_range) + np.NaN

            # Now, for each mass, we interpolate along logg.
            for mass_idx in range(len(mass_range)):
                self.logg[mass_idx] = scipy.interpolate.interp1d(logages_interp_array.T[mass_idx], logg_interp_array.T[mass_idx], kind = interpolation_kind, bounds_error = False)(logage)
                self.teffs[mass_idx] = np.power(10.,scipy.interpolate.interp1d(logages_interp_array.T[mass_idx], log_Teffs_interp_array.T[mass_idx], kind = interpolation_kind, bounds_error = False)(logage))
        else:
            mass_range = np.logspace(np.log10(0.09), np.log10(2.4), 100)
            self.logg = np.zeros_like(mass_range) + np.NaN
            self.teffs = np.zeros_like(mass_range) + np.NaN

    def __init__(self, input, output, logage = 9.69897, FeH = 0., interpolation_kind = "linear", output_unit = None):
        self.output_unit = output_unit
        self._default_input_unit = self.default_units[input]
        self._default_output_unit = self.default_units[output]
        if output_unit is None:
            self.output_unit = self._default_output_unit
        assert input in ["logg", "Teff"], "Input type not logg or Teff"
        assert output in ["logg", "Teff"], "Output type not logg or Teff"
        if hasattr(self, "logg"):
            pass
        else:
            self.preinit(logage = logage, FeH = FeH, interpolation_kind = interpolation_kind)

        if input == "logg":
            input_interp = self.logg
        elif input == "Teff":
            input_interp = self.teffs

        if output == "logg":
            output_interp = self.logg
        elif output == "Teff":
            output_interp = self.teffs

        ### Use only for our conditionals non-nan elements.
        if len(input_interp > 0):
            cond_valid = np.logical_and(np.logical_not(np.isnan(input_interp)), np.logical_not(np.isnan(output_interp)))

            # print zip(input_interp[cond_valid], output_interp[cond_valid])

            sorted_input_interp_trimmed = sorted(input_interp[cond_valid])
            sorted_output_interp_trimmed =\
            [y[1] for y in sorted(zip(input_interp[cond_valid], output_interp[cond_valid]), key = lambda x: x[0])]
            if len(sorted_input_interp_trimmed) > 1:
                self._evaluate =\
                scipy.interpolate.interp1d(sorted_input_interp_trimmed,
                                           sorted_output_interp_trimmed,
                                           kind = interpolation_kind,
                                           bounds_error = False,
                                           fill_value = np.nan, assume_sorted = True)
            else:
                self._evaluate =\
                scipy.interpolate.interp1d([-1e10,-1e10+1],
                                           [0.,0.],
                                           kind = interpolation_kind,
                                           bounds_error = False,
                                           fill_value = np.nan, assume_sorted = True)
        else:
            self._evaluate =\
            scipy.interpolate.interp1d([-1e10,-1e10+1],
                                       [0.,0.],
                                       kind = interpolation_kind,
                                       bounds_error = False,
                                       fill_value = np.nan, assume_sorted = True)
