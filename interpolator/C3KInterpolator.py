"""
C3K Color Table Interpolator
"""

from __future__ import absolute_import
from sippy.interpolator.BaseInterpolator import *
import os
import numpy as np
import scipy
import scipy.interpolate
import matplotlib.pyplot as plt
from astropy import units as u
import itertools as it
from itertools import izip

class C3KBaseInterpolator(BaseInterpolator):
    """The abstract base interpolator for the C3K
    Color Tables.

    Data files are prepared by truncating nonzero Av data files.

    The interpolation needs to be done on two axes:
    1. logg
    2. [Fe/H]

    Do not instantiate this class --- instantiate the child classes
    instead.
    """
    def load_data(self, filename):
        raise NotImplementedError
    def preprocess(Fe_H_val, logg_val):
        raise NotImplementedError
    def __init__(self, input, output, logg = 4.43812, FeH = 0.0,
                 interpolation_kind = "linear",
                 output_unit = None):
        """Returns a C3K interpolator with the defined values.
        Note: logg needs to be set. (Usually defined between 2.5-5.5.)
        The color is interpolated to the provided version of logg.
        [Fe/H] also needs to be set.

        A solar model is the default, at 4.43812, calculated by Eric Mamajek."""
        # Set the units to output in
        self.output_unit = output_unit

        # Load in the data
        load_data = self.load_data(FeH, logg)
        column_headers = self.column_headers

        # We need to explicitly calculate colors from the photometry.

        # If the input is a color:
        input_color = input.split("-")
        if len(input_color) > 1:
            # The desired input is a color. Check if it is a valid color.
            assert len(input_color) == 2, "Wrong length for input color."
            assert input_color[0] in self.bands\
            and input_color[1] in self.bands,\
            "Colors with invalid photometric bands. Valid bands: "+\
            repr(self.bands)
            # Use the default units for the chosen input color.
            # Since the operation is subtraction, assume they are the same
            # unit:
            self._default_input_unit = self.default_units["BC" + input_color[0]]
            ###Now, set the input interpolator.
            # First, find the index of each of the photometric bands.
            input_bc0_index = column_headers.index("BC" + input_color[0])
            input_bc1_index = column_headers.index("BC" + input_color[1])
            # Next, simply subtract the values for each row, evaluating
            # Band1 - Band2 in each.
            input_interp =\
            np.array([aa[input_bc1_index] - aa[input_bc0_index] for \
                      aa in load_data])
        else:
            # Input is not a color.
            assert input in column_headers,\
            "Unsupported column headers chosen for input."+\
            "Valid options: "+repr(column_headers)
            # Use the default units for the chosen input column.
            self._default_input_unit = self.default_units[input]
            # Find the indices of the columns that correspond to input.
            input_index = column_headers.index(input)
            # Set the input interpolator.
            input_interp = np.array([aa[input_index] for aa in load_data])

        # If the output is a color:
        output_color = output.split("-")
        if len(output_color) > 1:
            # The desired output is a color. Check if it is a valid color.
            assert len(output_color) == 2, "Wrong length for output color."
            assert output_color[0] in self.bands\
            and output_color[1] in self.bands,\
            "Colors with invalid photometric bands. Valid bands: "+\
            repr(self.bands)
            # Use the default units for the chosen output color.
            # Since the operation is subtraction, assume they are the same
            # unit:
            self._default_output_unit = self.default_units[output_color[0]]
            ###Now, set the output interpolator.
            # First, find the index of each of the photometric bands.
            output_bc0_index = column_headers.index("BC" + output_color[0])
            output_bc1_index = column_headers.index("BC" + output_color[1])
            # Next, simply subtract the values for each row, evaluating
            # Band1 - Band2 in each.
            output_interp =\
            np.array([aa[output_bc1_index] - aa[output_bc0_index] for \
                      aa in load_data])
        else:
            # output is not a color.
            assert output in column_headers,\
            "Unsupported column header chosen for output. Choice: "+output+\
            " Valid options: "+repr(column_headers)
            # Use the default units for the chosen output column.
            self._default_output_unit = self.default_units[output]
            # Find the indices of the columns that correspond to output.
            output_index = column_headers.index(output)
            # Set the output interpolator.
            output_interp = np.array([aa[output_index] for aa in load_data])

        ## If the output unit is unset, then assume that the default units
        ## are what's requested.

        if output_unit is None:
            self.output_unit = self._default_output_unit

        ### Making sure to only expose the non-NaN values.
        # We obtain the indices at which neither the input or output interp
        # are NaN by performing an AND on the valid entries --- rejecting
        # an index if either the input or output interpolations are NaN.
        valid_indices =\
        np.logical_and(np.logical_not(np.isnan(input_interp)),
                       np.logical_not(np.isnan(output_interp)))
        # Sort interpolation arrays prior to interpolation:
        sorted_output_interp_trimmed =\
        [output_int for input_int, output_int in\
         sorted(izip(input_interp[valid_indices],
                    output_interp[valid_indices]),
                key = lambda x: x[0])]
        sorted_input_interp_trimmed =\
        sorted(input_interp[valid_indices])
        # Expose the interpolation evaluation method.
        # print input, output
        self._evaluate =\
        scipy.interpolate.interp1d(sorted_input_interp_trimmed,
                                   sorted_output_interp_trimmed,
                                   kind = interpolation_kind,
                                   bounds_error = False,
                                   fill_value = np.nan, assume_sorted = True)

class C3KSDSSInterpolator(C3KBaseInterpolator):
    """Pan-STARRS (ugriz) interpolator for the C3K calculated atmosphere.

    "input" and "output" can be one of the following strings:

    Teff, Logg, [Fe/H], BCg, BCr, BCi, BCz, BCy, BCw, BCopen

    Or any color combination between the photometric bands:

    g,r,i,z,y,w,open

    interpolation_kind can be 'linear', 'nearest', 'slinear', 'quadratic',
    'cubic', from the scipy documentation."""
    # Define the default units of each column.
    default_units = \
    {"Teff": u.K,
     "Logg": u.dimensionless_unscaled,
     "[Fe/H]": u.dimensionless_unscaled,
     "Av": u.mag,
     "Rv": u.dimensionless_unscaled,
     "g": u.mag,
     "r": u.mag,
     "i": u.mag,
     "z": u.mag,
     "y": u.mag,
     "w": u.mag,
     "open": u.mag,
     "BCg": u.mag,
     "BCr": u.mag,
     "BCi": u.mag,
     "BCz": u.mag,
     "BCy": u.mag,
     "BCw": u.mag,
     "BCopen": u.mag}
    # The following column headers are directly from the file, and ordered:
    column_headers = ["Teff","Logg","[Fe/H]","Av","Rv","BCg","BCr","BCi",
    "BCz","BCy","BCw","BCopen"]
    available_bolometric_corrections = ["BCg","BCr","BCi",
    "BCz","BCy","BCw","BCopen"]
    # Denote the explicit photometric bands here:
    bands = [bolometric_correction[2:] for bolometric_correction \
    in available_bolometric_corrections]
    available_colors = ["-".join(x) for x in it.combinations(bands, 2)]
    @staticmethod
    def load_data(Fe_H_val, logg_val, cache = True):
        """Interpolates photometries to a given value for FeH and logg."""
        folder_cached = "/Data/C3K/Cached"
        if os.path.isfile(os.path.dirname(os.path.abspath(__file__))\
        + folder_cached + "/FeH_%.03f_logg_%.03f.npy" % (Fe_H_val, logg_val)):
            print "loading data", os.path.dirname(os.path.abspath(__file__))\
            + folder_cached + "/FeH_%.03f_logg_%.03f.npy" % (Fe_H_val, logg_val)
            load_data = np.load(os.path.dirname(os.path.abspath(__file__))\
            + folder_cached + "/FeH_%.03f_logg_%.03f.npy" % (Fe_H_val, logg_val))
            return load_data
        else:
            print "cannot find data", os.path.dirname(os.path.abspath(__file__))\
            + folder_cached + "/FeH_%.03f_logg_%.03f.npy" % (Fe_H_val, logg_val)
        column_headers = ["Teff","Logg","[Fe/H]","Av","Rv","BCg","BCr","BCi",
        "BCz","BCy","BCw","BCopen"]
        available_bolometric_corrections = ["BCg","BCr","BCi",
        "BCz","BCy","BCw","BCopen"]
        folder_prefix = "/Data/C3K/PanSTARRS"
        Fe_Hs =\
        [-4.00, -3.50, -3.00, -2.75, -2.50, -2.25, -2.00, -1.75, -1.50, -1.25,
        -1.00, -0.75, -0.50, -0.25, 0.00, 0.25, 0.50, 0.75]
        Loggs =\
        [-4.0, -3.0, -2.0, -1.5, -1.0, -0.5, 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0,
        3.5, 4.0, 4.5, 5.0, 5.5, 6.0, 6.5, 7.0, 7.5, 8.0, 8.5, 9.0, 9.5]
        Teffs = None

        grid_BCs = {}

        BC_values = {}

        for bol_corr_text in available_bolometric_corrections:
            grid_BCs[bol_corr_text] = None

        for Fe_H in Fe_Hs:
            if Fe_H < 0:
                string_sgn = "m"
            else:
                string_sgn = "p"
            filename = "feh" + string_sgn + "%03i" % (np.abs(Fe_H)*100) + ".PanSTARRS"
            table = np.genfromtxt(os.path.dirname(os.path.abspath(__file__))\
            + folder_prefix + "/" + filename)
            if Teffs is None:
                Teffs = sorted(np.unique(table.T[column_headers.index("Teff")]))

            for bol_corr_text in available_bolometric_corrections:
                if grid_BCs[bol_corr_text] is None:
                    grid_BCs[bol_corr_text] = table.T[column_headers.index(bol_corr_text)]
                else:
                    grid_BCs[bol_corr_text] = np.concatenate((grid_BCs[bol_corr_text], table.T[column_headers.index(bol_corr_text)]))

        load_data = np.zeros((len(Teffs), len(column_headers)))
        for Teff_index in range(len(Teffs)):
            load_data[Teff_index][column_headers.index("Teff")] = Teffs[Teff_index]
            load_data[Teff_index][column_headers.index("Logg")] = logg_val
            load_data[Teff_index][column_headers.index("[Fe/H]")] = Fe_H_val
            load_data[Teff_index][column_headers.index("Rv")] = 3.1

        for bol_corr_text in available_bolometric_corrections:
            # points = np.array([Fe_Hs, Teffs, Loggs])
            # grid_BCs[bol_corr_text] =\
            # np.swapaxes(\
            # grid_BCs[bol_corr_text].reshape((len(Fe_Hs), len(Teffs), len(Loggs))),\
            # 1,2)
            #Fe/H, Logg, Teff

            points = np.array([Fe_Hs, Loggs])
            grid_BCs[bol_corr_text] =\
            np.swapaxes(\
            grid_BCs[bol_corr_text].reshape((len(Fe_Hs), len(Teffs), len(Loggs))),\
            0,1)

            #Teff, Fe/H, Logg

            for Teff_index in range(len(Teffs)):
                load_data[Teff_index][column_headers.index(bol_corr_text)] =\
                scipy.interpolate.RegularGridInterpolator(points,
                grid_BCs[bol_corr_text][Teff_index], bounds_error = False,
                fill_value = np.NaN)\
                ((Fe_H_val, logg_val))

        if cache:
            np.save(os.path.dirname(os.path.abspath(__file__))\
            + folder_cached + "/FeH_%.03f_logg_%.03f" %\
            (Fe_H_val, logg_val),
            load_data)

        return load_data

class C3KUBVRIplusInterpolator(C3KBaseInterpolator):
    """Pan-STARRS (ugriz) interpolator for the C3K calculated atmosphere.

    "input" and "output" can be one of the following strings:

    Teff, Logg, [M/H], [a/H], u, g, r, i, z

    Or any color combination between the photometric bands:

    U,B,V,Rc,Ic,J,H,Ks,Kp,D51,Hp,B_Tycho,V_Tycho,Gaia_G,Gaia_BP,Gaia_RP,
    TESS

    interpolation_kind can be 'linear', 'nearest', 'slinear', 'quadratic',
    'cubic', from the scipy documentation."""
    # Define the default units of each column.
    default_units = \
    {"Teff": u.K,
     "Logg": u.dimensionless_unscaled,
     "[Fe/H]": u.dimensionless_unscaled,
     "[a/H]": u.dimensionless_unscaled,
     "Av": u.mag,
     "Rv": u.dimensionless_unscaled,
     "U": u.mag,
     "B": u.mag,
     "V": u.mag,
     "Rc": u.mag,
     "Ic": u.mag,
     "J": u.mag,
     "H": u.mag,
     "Ks": u.mag,
     "Kp": u.mag,
     "D51": u.mag,
     "HP": u.mag,
     "BT": u.mag,
     "VT": u.mag,
     "Gaia_G": u.mag,
     "Gaia_BP": u.mag,
     "Gaia_RP": u.mag,
     "TESS": u.mag,
     "BCU": u.mag,
     "BCB": u.mag,
     "BCV": u.mag,
     "BCRc": u.mag,
     "BCIc": u.mag,
     "BCJ": u.mag,
     "BCH": u.mag,
     "BCKs": u.mag,
     "BCKp": u.mag,
     "BCD51": u.mag,
     "BCHP": u.mag,
     "BCBT": u.mag,
     "BCVT": u.mag,
     "BCGaia_G": u.mag,
     "BCGaia_BP": u.mag,
     "BCGaia_RP": u.mag,
     "BCTESS": u.mag}
    # The following column headers are directly from the file, and ordered:
    column_headers = ["Teff","Logg","[Fe/H]","Av","Rv","BCU","BCB","BCV","BCRc","BCIc",
    "BCJ","BCH","BCKs","BCKp","BCD51","BCHP","BCBT","BCVT","BCGaia_G","BCGaia_BP","BCGaia_RP","BCTESS"]
    available_bolometric_corrections = ["BCU","BCB","BCV","BCRc","BCIc",
    "BCJ","BCH","BCKs","BCKp","BCD51","BCHP","BCBT","BCVT","BCGaia_G","BCGaia_BP","BCGaia_RP","BCTESS"]
    # Denote the explicit photometric bands here:
    bands = [bolometric_correction[2:] for bolometric_correction \
    in available_bolometric_corrections]
    available_colors = ["-".join(x) for x in it.combinations(bands, 2)]
    @staticmethod
    def load_data(Fe_H_val, logg_val, cache = True):
        """Interpolates photometries to a given value for FeH and logg."""
        folder_cached = "/Data/C3K/Cached"
        if os.path.isfile(os.path.dirname(os.path.abspath(__file__))\
        +folder_cached + "/FeH_%.03f_logg_%.03f.npy" % (Fe_H_val, logg_val)):
            print "load data", os.path.dirname(os.path.abspath(__file__))\
            + folder_cached + "/FeH_%.03f_logg_%.03f.npy" % (Fe_H_val, logg_val)
            load_data = np.load(os.path.dirname(os.path.abspath(__file__))\
            + folder_cached + "/FeH_%.03f_logg_%.03f.npy" % (Fe_H_val, logg_val))
            return load_data
        else:
            print "cannot find data", os.path.dirname(os.path.abspath(__file__))\
            + folder_cached + "/FeH_%.03f_logg_%.03f.npy" % (Fe_H_val, logg_val)
        column_headers = ["Teff","Logg","[Fe/H]","Av","Rv","BCU","BCB","BCV","BCRc","BCIc",
        "BCJ","BCH","BCKs","BCKp","BCD51","BCHP","BCBT","BCVT","BCGaia_G","BCGaia_BP","BCGaia_RP","BCTESS"]
        available_bolometric_corrections = ["BCU","BCB","BCV","BCRc","BCIc",
        "BCJ","BCH","BCKs","BCKp","BCD51","BCHP","BCBT","BCVT","BCGaia_G","BCGaia_BP","BCGaia_RP","BCTESS"]
        folder_prefix = "/Data/C3K/UBVRIplus"
        Fe_Hs =\
        [-4.00, -3.50, -3.00, -2.75, -2.50, -2.25, -2.00, -1.75, -1.50, -1.25,
        -1.00, -0.75, -0.50, -0.25, 0.00, 0.25, 0.50, 0.75]
        Loggs =\
        [-4.0, -3.0, -2.0, -1.5, -1.0, -0.5, 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0,
        3.5, 4.0, 4.5, 5.0, 5.5, 6.0, 6.5, 7.0, 7.5, 8.0, 8.5, 9.0, 9.5]
        Teffs = None

        grid_BCs = {}

        BC_values = {}

        for bol_corr_text in available_bolometric_corrections:
            grid_BCs[bol_corr_text] = None

        for Fe_H in Fe_Hs:
            if Fe_H < 0:
                string_sgn = "m"
            else:
                string_sgn = "p"
            filename = "feh" + string_sgn + "%03i" % (np.abs(Fe_H)*100) + ".UBVRIplus"
            table = np.genfromtxt(os.path.dirname(os.path.abspath(__file__))\
            + folder_prefix + "/" + filename)
            if Teffs is None:
                Teffs = sorted(np.unique(table.T[column_headers.index("Teff")]))

            for bol_corr_text in available_bolometric_corrections:
                if grid_BCs[bol_corr_text] is None:
                    grid_BCs[bol_corr_text] = table.T[column_headers.index(bol_corr_text)]
                else:
                    grid_BCs[bol_corr_text] = np.concatenate((grid_BCs[bol_corr_text], table.T[column_headers.index(bol_corr_text)]))

        load_data = np.zeros((len(Teffs), len(column_headers)))
        for Teff_index in range(len(Teffs)):
            load_data[Teff_index][column_headers.index("Teff")] = Teffs[Teff_index]
            load_data[Teff_index][column_headers.index("Logg")] = logg_val
            load_data[Teff_index][column_headers.index("[Fe/H]")] = Fe_H_val
            load_data[Teff_index][column_headers.index("Rv")] = 3.1

        for bol_corr_text in available_bolometric_corrections:
            # points = np.array([Fe_Hs, Teffs, Loggs])
            # grid_BCs[bol_corr_text] =\
            # np.swapaxes(\
            # grid_BCs[bol_corr_text].reshape((len(Fe_Hs), len(Teffs), len(Loggs))),\
            # 1,2)
            #Fe/H, Logg, Teff

            points = np.array([Fe_Hs, Loggs])
            grid_BCs[bol_corr_text] =\
            np.swapaxes(\
            grid_BCs[bol_corr_text].reshape((len(Fe_Hs), len(Teffs), len(Loggs))),\
            0,1)

            #Teff, Fe/H, Logg

            for Teff_index in range(len(Teffs)):
                load_data[Teff_index][column_headers.index(bol_corr_text)] =\
                scipy.interpolate.RegularGridInterpolator(points,
                grid_BCs[bol_corr_text][Teff_index], bounds_error = False,
                fill_value = np.NaN)\
                ((Fe_H_val, logg_val))

        if cache:
            np.save(os.path.dirname(os.path.abspath(__file__))\
            + folder_cached + "/FeH_%.03f_logg_%.03f" %\
            (Fe_H_val, logg_val),
            load_data)

        return load_data

class C3KInterpolator(C3KBaseInterpolator):
    """C3K Interpolator including all possible C3K colors.
    Merges all C3K interpolators, and presents the combination.

    Note: since this is a OOP pattern, filename is a dummy variable."""
    C3KInterpolators = [C3KSDSSInterpolator, C3KUBVRIplusInterpolator]
    logg = 4.4
    FeH = 0.
    @classmethod
    def preinit(self, logg = None, FeH = None):
        """Run this method to pre-initialize C3KInterpolator,
        in case methods require column headers and bands."""
        self.column_headers = []
        self.bands = []
        self.available_bolometric_corrections = []
        self.default_units = {}

        if logg is not None:
            self.logg = logg
        if FeH is not None:
            self.FeH = FeH

        for iter_num, C3KInterp in enumerate(self.C3KInterpolators):
            # Copy default units:
            for default_unit in C3KInterp.default_units:
                self.default_units[default_unit] =\
                C3KInterp.default_units[default_unit]

            self.column_headers += C3KInterp.column_headers
            self.available_bolometric_corrections +=\
            C3KInterp.available_bolometric_corrections
            # Denote the explicit photometric bands here:
            self.bands += C3KInterp.bands

            if iter_num == 0:
                self.data = np.array(C3KInterp.load_data(self.FeH, self.logg))
            else:
                self.data =\
                np.concatenate((self.data,
                np.array(C3KInterp.load_data(self.FeH, self.logg))),
                axis = 1)

        # Recompute total available colors:
        self.available_colors =\
        ["-".join(x) for x in it.combinations(list(set(self.bands)), 2)]

        ### Assign each a default unit:
        for available_color in self.available_colors:
            self.default_units[available_color] = u.mag
    def __init__(self, input, output,
                 interpolation_kind = "linear",
                 output_unit = None,
                 logg = None, FeH = None):
        return \
        super(C3KInterpolator, self).\
        __init__(input, output,
                 logg = self.logg,
                 FeH = self.FeH,
                 interpolation_kind = interpolation_kind,
                 output_unit = output_unit)

    def load_data(self, Fe_H_val, logg_val):
        """Return the combination of all C3K Settl color tables."""
        return self.data
