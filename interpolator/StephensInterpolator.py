"""
Stephens+ (2009) Color Table Interpolator
"""

from __future__ import absolute_import
from sippy.interpolator.BaseInterpolator import *
import os
import numpy as np
import scipy
import scipy.interpolate
import matplotlib.pyplot as plt
from astropy import units as u
from itertools import izip

class StephensInterpolator(SpTBaseInterpolator):
    """The SpT interpolator for the Stephens (2009)
    SpT relation for late M to late T spectral types.
    """
    default_units = \
    {"SpT": u.dimensionless_unscaled,
     "Teff": u.K}
    # The following column headers are directly from the file, and ordered:
    column_headers = ["SpT","Teff"]
    available_colors = []
    def load_data(self, filename):
        raise NotImplementedError
    def __init__(self, input, output, interpolation_kind = "linear",
                 output_unit = None):
        # Set the units to output in
        self.output_unit = output_unit

        # Set up the base class requirements, though these can be run
        # with different values after setting up the class!
        self.set_spectral_types()
        self.set_SpT_zero_point()

        column_headers = self.column_headers

        assert input in column_headers and output in column_headers,\
        "Unsupported column headers chosen for input and output."+\
        "Valid options: "+repr(column_headers)
        ### Set the default inputs and outputs according to the default unit
        ### of each column datatype.
        # Use the default units for the chosen input and output columns.
        self._default_input_unit = self.default_units[input]
        self._default_output_unit = self.default_units[output]

        ## If the output unit is unset, then assume that the default units
        ## are what's requested.

        if output_unit is None:
            self.output_unit = self._default_output_unit

        # Find the indices of the columns that correspond to input and output.
        input_index = column_headers.index(input)
        output_index = column_headers.index(output)

        # If the input is SpT, wrap "evaluate" with the ability to
        # read in lists of strings, numpy arrays with dimensionless_unscaled,
        # or lists of numbers, or something in between.
        ## Also, since the SpT column input is in string SpT's, we need
        ## to convert them to numerical SpT's for interpolation.
        if input == "SpT":
            self.evaluate = self.evaluate___SpT_input
            #M6 is 6, T8 is 28
            input_interp = np.linspace(6., 28., 1000)
            output_interp =\
            2265.9+347.82*input_interp-60.558*input_interp*input_interp\
            +3.151*input_interp*input_interp*input_interp\
            -0.060481*input_interp*input_interp*input_interp*input_interp\
            +0.00024506*input_interp*input_interp*input_interp*input_interp*input_interp
            #We calculated output_interp, but we need to
            #move the zero point of the input_interp:
            input_interp = input_interp + 10.*(6. + self.zero_point)
        elif output == "SpT":
            self.evaluate = self.evaluate___SpT_output
            #M6 is 6, T8 is 28
            output_interp = np.linspace(6., 28., 1000)
            input_interp =\
            2265.9+347.82*output_interp-60.558*output_interp*output_interp\
            +3.151*output_interp*output_interp*output_interp\
            -0.060481*output_interp*output_interp*output_interp*output_interp\
            +0.00024506*output_interp*output_interp*output_interp*output_interp*output_interp
            #We calculated input_interp, but we need to
            #move the zero point of the output_interp:
            output_interp = output_interp + 10.*(6. + self.zero_point)
        elif (input == "Teff") and (output == "Teff"):
            #For the special case of identity:
            input_interp = np.linspace(6., 28., 1000)
            output_interp = np.linspace(6., 28., 1000)

        ### Making sure to only expose the non-NaN values.
        # We obtain the indices at which neither the input or output interp
        # are NaN by performing an AND on the valid entries --- rejecting
        # an index if either the input or output interpolations are NaN.
        valid_indices =\
        np.logical_and(np.logical_not(np.isnan(input_interp)),
                       np.logical_not(np.isnan(output_interp)))
        # Sort interpolation arrays prior to interpolation:
        sorted_output_interp_trimmed =\
        [output_int for input_int, output_int in\
         sorted(izip(input_interp[valid_indices],
                    output_interp[valid_indices]),
                key = lambda x: x[0])]
        sorted_input_interp_trimmed =\
        sorted(input_interp[valid_indices])
        # Remove duplicates from the target input_interp:
        tiny = 1e-15
        sorted_input_interp_trimmed_no_duplicates = []
        sorted_output_interp_trimmed_no_duplicates = []
        for index in xrange(1, len(sorted_input_interp_trimmed)):
            if abs(sorted_input_interp_trimmed[index] -\
                   sorted_input_interp_trimmed[index-1]) > tiny:
                sorted_input_interp_trimmed_no_duplicates.\
                append(sorted_input_interp_trimmed[index])
                sorted_output_interp_trimmed_no_duplicates.\
                append(sorted_output_interp_trimmed[index])
        # Expose the interpolation evaluation method.
        self._evaluate =\
        scipy.interpolate.interp1d(sorted_input_interp_trimmed_no_duplicates,
                                   sorted_output_interp_trimmed_no_duplicates,
                                   kind = interpolation_kind,
                                   bounds_error = False,
                                   fill_value = np.nan, assume_sorted = True)
