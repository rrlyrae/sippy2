import os
import numpy as np
import matplotlib.pyplot as plt
### Imposes an age cut and a cut in the logage.
directory = os.path.dirname(os.path.abspath(__file__))\
+ "/SP/"
read_directory = os.path.dirname(os.path.abspath(__file__))\
+ "/../../../sp/"

points = []
files = [f for f in os.listdir(read_directory) if\
		 os.path.isfile(read_directory+f)]
logage_tol = 1e-2
logage_max = 9

for filename in files:
	#star_age 	 star_mass 	 logL 	 logT spot_alpha

	#ppI_idx = cols.index("ppI_lum")
	#ppII_idx = cols.index("ppII_lum")
	#ppIII_idx = cols.index("ppIII_lum")
	#CNO_idx = cols.index("CNO_lum")
	#triple_alpha_idx = cols.index("3-alpha_lum")
	#He_C_idx = cols.index("He-C_lum")

	#spot_text = filename.split("spo")[1].split(".")[0]
	#spot_alpha = 1.-float(spot_text[0] + "." + spot_text[1:])

	print (filename)
	raw_points = np.loadtxt(read_directory+filename)
	if len(raw_points) > 0:
		with open(read_directory+filename, 'r') as open_f:
			cols = open_f.readline().replace("#", " ").split()

		star_age_idx = cols.index("Age(Gyr)")
		mass_idx = cols.index("Mass")
		log_L_idx = cols.index("log(L/Lsun)")
		log_Teff_idx = cols.index("log(Teff)")
		F_spot_idx = cols.index("Fspot")
		X_spot_idx = cols.index("Xspot")

		#print(raw_points)
		spot_alpha = 1.-raw_points.T[F_spot_idx]*(1.-np.power(raw_points.T[X_spot_idx], 4.))
		print 'sa',spot_alpha[0]
		points = [[raw_points[0][star_age_idx]*1e9, raw_points[0][mass_idx], raw_points[0][log_Teff_idx], raw_points[0][log_L_idx], spot_alpha[0]]]
		tol = 1e-8
		for idx in range(1,len(raw_points)):
			#print (raw_points[idx])
			if np.log10(raw_points[idx][star_age_idx]*1e9) -\
			np.log10(points[-1][0]) >\
			logage_tol and np.log10(raw_points[idx][star_age_idx]*1e9) <\
			logage_max \
			and raw_points[idx][log_Teff_idx] > 0.1:
				points.append([raw_points[idx][star_age_idx]*1e9,
				raw_points[idx][mass_idx], raw_points[idx][log_Teff_idx],
				raw_points[idx][log_L_idx], spot_alpha[idx]])
		#if (np.abs(spot_alpha - 0.7) < 1e-9):
		if True:
			np.savetxt(directory+filename,points,header="#Somers 2018 reprocessed starspot track  \n#star_age    mass 	 logTeff 	 logL   spot_alpha",comments="")
		print ("Saved", filename)
	else:
		print ("Omitted", filename)
