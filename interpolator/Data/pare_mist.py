import os
import numpy as np
import matplotlib.pyplot as plt
### Imposes an age cut and a cut in the logage.
directory = os.path.dirname(os.path.abspath(__file__))\
+ "/MIST_preMS/"
read_directory = os.path.dirname(os.path.abspath(__file__))\
+ "/../../../../MIST_v1.1_feh_p0.00_afe_p0.0_vvcrit0.0_EEPS/"

points = []
files = [f for f in os.listdir(read_directory) if\
         os.path.isfile(read_directory+f)]
logage_tol = 1e-2
logage_max = 10
for filename in files:
    #star_age 	 star_mass 	 logL 	 logT
    star_age_idx = 0
    star_mass_idx = 1
    log_L_idx = 6
    log_Teff_idx = 11
    phase_idx = 76
    print (filename)
    raw_points = np.genfromtxt(read_directory+filename,usecols=(star_age_idx,
                                                           star_mass_idx,
                                                           log_L_idx,
                                                           log_Teff_idx,
                                                           phase_idx))
    points = []
    tol = 1e-8

    ### Calculate for a given logT, whether it doubles back.
    # falling_down = np.array([raw_points[idx][3] for idx in range(len(raw_points))])
    # plt.plot(np.array([raw_points[idx][3] for idx in range(len(raw_points)) if True]), np.array([raw_points[idx][2] for idx in range(len(raw_points)) if True]))
    # plt.gca().invert_xaxis()
    # plt.show()
    # plt.plot(np.array([raw_points[idx][3] for idx in range(len(raw_points)) if True]))
    # plt.show()
    for idx in range(0,len(raw_points)):
        if np.abs(raw_points[idx][-1] + 1) < tol:# and falling_down[idx]:# or np.abs(raw_points[idx][-1] + 0) < tol: #-1=PMS, 0=MS, 2=RGB, 3=CHeB, 4=EAGB, 5=TPAGB, 6=postAGB, 9=WR
            points.append(raw_points[idx][0:4])
    np.savetxt(directory+filename,points,header="#MIST v1.1 [Fe/H] = 0, v_init = 0, reprocessed track  \n#star_age 	 star_mass 	 logL 	 logT",comments="")
    print ("Saved", filename)

# points = []
# files = [f for f in os.listdir(directory) if\
#          os.path.isfile(directory+f)]
# logage_tol = 1e-2
# logage_max = 10
# for filename in files:
#     with open(directory+filename, "r") as f:
#         isotxt = f.readlines()
#         raw_points = np.genfromtxt(directory+filename)
#         points = [raw_points[0]]
#         age_idx = 0 #index of age column
#         #plt.plot((np.log10(raw_points.T[age_idx]) - np.log10(np.roll(raw_points.T[age_idx], 1)))[1:])
#         #plt.semilogy()
#         #plt.show()
#         #plt.clf()
#         for idx in range(1,len(raw_points)):
#             if np.log10(raw_points[idx][age_idx]) - np.log10(points[-1][age_idx]) > logage_tol and np.log10(raw_points[idx][age_idx]) < logage_max:
#                 points.append(raw_points[idx])
#         if len(isotxt) > 3:
#             np.savetxt(directory+filename,points,header="".join([isotxt[0], isotxt[1][:-1]]),comments="")
#             print ("Saved", filename)
#         else:
#             print ("Failed", filename)
