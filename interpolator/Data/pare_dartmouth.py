import os
import numpy as np
import matplotlib.pyplot as plt
### Imposes an age cut and a cut in the logage.
directory = os.path.dirname(os.path.abspath(__file__))\
+ "/dartmouth_tracks/"
read_directory = os.path.dirname(os.path.abspath(__file__))\
+ "/../../../../dartmouth_tracks/"

points = []
files = [f for f in os.listdir(read_directory) if\
         os.path.isfile(read_directory+f)]
logage_tol = 1e-2
logage_max = 10
for filename in files:
    #star_age 	 star_mass 	 logL 	 logT
    star_age_idx = 0
    log_L_idx = 3
    log_Teff_idx = 1
    L_H_idx = 9
    print (filename)
    raw_points = np.genfromtxt(read_directory+filename, usecols = list(range(11)))
    #print(raw_points)
    points = [[raw_points[0][star_age_idx], raw_points[0][log_Teff_idx], raw_points[0][L_H_idx], raw_points[0][log_L_idx]]]
    tol = 1e-8
    for idx in range(1,len(raw_points)):
        #print (raw_points[idx])
        if np.log10(raw_points[idx][star_age_idx]) - np.log10(points[-1][0]) >\
        logage_tol and np.log10(raw_points[idx][star_age_idx]) < logage_max and\
        raw_points[idx][L_H_idx]/np.power(10.,raw_points[idx][log_L_idx]) < 0.1\
        and raw_points[idx][log_Teff_idx] > 0.1:
            points.append([raw_points[idx][star_age_idx], raw_points[idx][log_Teff_idx], raw_points[idx][L_H_idx], raw_points[idx][log_L_idx]])
    np.savetxt(directory+filename,points,header="#Dartmouth reprocessed track  \n#star_age 	 logT 	 L_H 	 logL",comments="")
    print ("Saved", filename)
