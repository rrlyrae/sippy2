"""
Pecaut & Mamajek (2013) Color Table Interpolator
"""

from __future__ import absolute_import
from sippy.interpolator.BaseInterpolator import *
import os
import numpy as np
import scipy
import scipy.interpolate
import matplotlib.pyplot as plt
from astropy import units as u
from itertools import izip

class PecautMamajekBaseInterpolator(SpTBaseInterpolator):
    """The abstract base interpolator for the Pecaut and Mamajek (2013)
    Color Tables.

    Do not instantiate this class --- instantiate the child PM13 classes
    instead, like

    * PecautMamajekDwarfInterpolator
    * PecautMamajekYoungInterpolator
    * PecautMamajekExtendedDwarfInterpolator
    """
    def load_data(self, filename):
        raise NotImplementedError
    @staticmethod
    def wrap_helper(fn, bound_left, bound_right):
        def bound_helper(xs):
            xs = np.array(xs)
            if hasattr(xs, "__len__"):
                cond_left = xs >= bound_left
                cond_right = xs <= bound_right
                cond_bound = np.logical_and(cond_left, cond_right)
                ys = np.zeros(len(xs)) + np.nan
                ys[cond_bound] = fn(xs[cond_bound])
            else:
                if (xs >= bound_left) and (xs <= bound_right):
                    ys = fn(xs)
                else:
                    ys = np.nan
            return ys
        return bound_helper
    def __init__(self, input, output, interpolation_kind = "linear",
                 output_unit = None):
        # Set the units to output in
        self.output_unit = output_unit

        # Set up the base class requirements, though these can be run
        # with different values after setting up the class!
        self.set_spectral_types()
        self.set_SpT_zero_point()

        # Load in the data
        filename = os.path.dirname(os.path.abspath(__file__))\
                 + "/Data/" + self.filename
        load_data = self.load_data(filename)
        column_headers = self.column_headers
        # Throw an error if input and output are not valid choices.
        assert input in column_headers and output in column_headers,\
        "Unsupported column headers chosen for input and output."+\
        "Valid options: "+repr(column_headers)
        ### Set the default inputs and outputs according to the default unit
        ### of each column datatype.
        # Use the default units for the chosen input and output columns.
        self._default_input_unit = self.default_units[input]
        self._default_output_unit = self.default_units[output]

        ## If the output unit is unset, then assume that the default units
        ## are what's requested.

        if output_unit is None:
            self.output_unit = self._default_output_unit

        # Find the indices of the columns that correspond to input and output.
        input_index = column_headers.index(input)
        output_index = column_headers.index(output)

        input_interp = np.array([aa[input_index] for aa in load_data])
        output_interp = np.array([bb[output_index] for bb in load_data])

        # If the input is SpT, wrap "evaluate" with the ability to
        # read in lists of strings, numpy arrays with dimensionless_unscaled,
        # or lists of numbers, or something in between.
        ## Also, since the SpT column input is in string SpT's, we need
        ## to convert them to numerical SpT's for interpolation.
        if input == "SpT":
            self.evaluate = self.evaluate___SpT_input
            # Also, since the input is SpT, coerce it to well-behaved strings:
            input_interp =\
            [input_interp[x].decode("utf-8")\
             for x in xrange(len(input_interp))]
            # Then convert it to numerical SpTs:
            input_interp = self.get_num_SpTs(input_interp)
        if output == "SpT":
            self.evaluate = self.evaluate___SpT_output
            # Also, since the output is SpT, coerce it to well-behaved strings:
            output_interp =\
            [output_interp[x].decode("utf-8")\
             for x in xrange(len(output_interp))]
            # Then convert it to numerical SpTs:
            output_interp = self.get_num_SpTs(output_interp)


        ### Making sure to only expose the non-NaN values.
        # We obtain the indices at which neither the input or output interp
        # are NaN by performing an AND on the valid entries --- rejecting
        # an index if either the input or output interpolations are NaN.
        valid_indices =\
        np.logical_and(np.logical_not(np.isnan(input_interp)),
                       np.logical_not(np.isnan(output_interp)))
        # Sort interpolation arrays prior to interpolation:
        sorted_output_interp_trimmed =\
        [output_int for input_int, output_int in\
         sorted(izip(input_interp[valid_indices],
                    output_interp[valid_indices]),
                key = lambda x: x[0])]
        sorted_input_interp_trimmed =\
        sorted(input_interp[valid_indices])
        # Remove duplicates from the target input_interp:
        tiny = 1e-15
        sorted_input_interp_trimmed_no_duplicates = []
        sorted_output_interp_trimmed_no_duplicates = []
        for index in xrange(1, len(sorted_input_interp_trimmed)):
            if abs(sorted_input_interp_trimmed[index] -\
                   sorted_input_interp_trimmed[index-1]) > tiny:
                sorted_input_interp_trimmed_no_duplicates.\
                append(sorted_input_interp_trimmed[index])
                sorted_output_interp_trimmed_no_duplicates.\
                append(sorted_output_interp_trimmed[index])
        # Expose the interpolation evaluation method.
        self._evaluate =\
        self.wrap_helper(
        scipy.interpolate.UnivariateSpline(\
        sorted_input_interp_trimmed_no_duplicates,
        sorted_output_interp_trimmed_no_duplicates,
        k=5, s=1) \
        , np.nanmin(sorted_input_interp_trimmed_no_duplicates), np.nanmax(sorted_input_interp_trimmed_no_duplicates))
        # scipy.interpolate.UnivariateSpline(sorted(input_interp),
        # [y[1] for y in sorted(zip(input_interp, output_interp), key = lambda x: x[0])],
        # k=5, s=1)
        # self._evaluate =\
        # scipy.interpolate.interp1d(sorted_input_interp_trimmed_no_duplicates,
        #                            sorted_output_interp_trimmed_no_duplicates,
        #                            kind = "linear",
        #                            bounds_error = False,
        #                            fill_value = np.nan, assume_sorted = True)

class PecautMamajekDwarfInterpolator(PecautMamajekBaseInterpolator):
    """Interpolator for the Pecaut & Mamajek (2013)
    Color Tables, for O9-M9 Dwarfs.

    "input" and "output" are case-sensitive strings each corresponding to one of the
    following quantities:

    SpT,Teff,BCV,U-B,B-V,V-Rc,V-Ic,V-J,V-H,V-Ks,K-W1,K-W2,K-W3,K-W4

    The "output_unit" parameter is an astropy unit corresponding to the
    dimension that the output data is desired. For dimensionless quantities,
    units.dimensionless_unscaled will suffice.

    For Teff, the output unit will be assumed to be "K" unless set to a
    compatible temperature unit.

    For SpT, the output unit can be set to both units.dimensionless_unscaled,
    or the string "SpT", to output string SpT values without a luminosity class.

    interpolation_kind can be 'linear', 'nearest', 'slinear', 'quadratic',
    'cubic', from the scipy documentation."""
    # Define the default units of each column.
    default_units = \
    {"SpT": u.dimensionless_unscaled,
     "Teff": u.K,
     "BCV": u.mag,
     "U-B": u.mag,
     "B-V": u.mag,
     "V-Rc": u.mag,
     "V-Ic": u.mag,
     "V-J": u.mag,
     "V-H": u.mag,
     "V-Ks": u.mag,
     "K-W1": u.mag,
     "K-W2": u.mag,
     "K-W3": u.mag,
     "K-W4": u.mag}
    filename = "PecautMamajek2013.table5.tsv"
    # The following column headers are directly from the file, and ordered:
    column_headers = ["SpT","Teff","BCV","U-B","B-V","V-Rc","V-Ic","V-J",
    "V-H","V-Ks","K-W1","K-W2","K-W3","K-W4"]
    available_colors = ["U-B","B-V","V-Rc","V-Ic","V-J",
    "V-H","V-Ks","K-W1","K-W2","K-W3","K-W4"]
    # Define the datatypes:
    dtype = ["|S10",float,float,float,float,float,float,float,
    float,float,float,float,float,float]
    # No need to deal explicitly with missing values or filling values
    # by default
    missing_values = None
    filling_values = None
    # Skip 44 lines before reading in data:
    skip_header = 44
    # Read the entire file in:
    max_rows = None
    # The input file uses ";" to delimit entries, so denote that:
    delimiter = ";"
    def load_data(self, filename):
        """Return the loaded data, according to the datatypes, missing values,
        and other parameters defined in the class, and parsed by row."""
        return np.genfromtxt(filename, comments = "#",
                             delimiter=self.delimiter,
                             skip_header=self.skip_header,
                             max_rows=self.max_rows,
                             dtype=self.dtype,
                             missing_values=self.missing_values,
                             filling_values=self.filling_values)

class PecautMamajekYoungInterpolator(PecautMamajekBaseInterpolator):
    """Interpolator for the Pecaut & Mamajek (2013)
    Color Tables, for 5-30 Myr Stars.

    "input" and "output" are case-sensitive strings each corresponding to one of the
    following quantities:

    SpT,Teff,B-V,V-Ic,V-Ks,J-H,H-Ks,Ks-W1,Ks-W2,Ks-W3,Ks-W4,BCV,BCJ

    The "output_unit" parameter is an astropy unit corresponding to the
    dimension that the output data is desired. For dimensionless quantities,
    units.dimensionless_unscaled will suffice.

    For Teff, the output unit will be assumed to be "K" unless set to a
    compatible temperature unit.

    For SpT, the output unit can be set to both units.dimensionless_unscaled,
    or the string "SpT", to output string SpT values without a luminosity class.

    interpolation_kind can be 'linear', 'nearest', 'slinear', 'quadratic',
    'cubic', from the scipy documentation."""
    # Define the default units of each column.
    default_units = \
    {"SpT": u.dimensionless_unscaled,
     "Teff": u.K,
     "B-V": u.mag,
     "V-Ic": u.mag,
     "V-Ks": u.mag,
     "J-H": u.mag,
     "H-Ks": u.mag,
     "Ks-W1": u.mag,
     "Ks-W2": u.mag,
     "Ks-W3": u.mag,
     "Ks-W4": u.mag,
     "BCV": u.mag,
     "BCJ": u.mag}
    filename = "PecautMamajek2013.table6.tsv"
    # The following column headers are directly from the file, and ordered:
    column_headers = ["SpT","Teff","B-V","V-Ic","V-Ks","J-H","H-Ks","Ks-W1",
                      "Ks-W2","Ks-W3","Ks-W4","BCV","BCJ"]
    available_colors = ["B-V","V-Ic","V-Ks","J-H","H-Ks","Ks-W1",
                      "Ks-W2","Ks-W3","Ks-W4"]
    # No need to specify a special dtype.
    dtype = ["|S10",float,float,float,float,float,float,float,
                      float,float,float,float,float]
    # No need to deal explicitly with missing values or filling values
    # by default
    missing_values = None
    filling_values = None
    # Skip 43 lines before reading in data:
    skip_header = 43
    # Read the entire file in:
    max_rows = None
    # The input file uses ";" to delimit entries, so denote that:
    delimiter = ";"
    def load_data(self, filename):
        """Return the loaded data, according to the datatypes, missing values,
        and other parameters defined in the class, and parsed by row."""
        return np.genfromtxt(filename, comments = "#",
                             delimiter=self.delimiter,
                             skip_header=self.skip_header,
                             max_rows=self.max_rows,
                             dtype=self.dtype,
                             missing_values=self.missing_values,
                             filling_values=self.filling_values)

class PecautMamajekExtendedDwarfInterpolator(PecautMamajekBaseInterpolator):
    """Interpolator for the Pecaut & Mamajek (2013)
    Color Tables, for O3-Y0.

    "input" and "output" are case-sensitive strings each corresponding to one of the
    following quantities:

    SpT,Teff,logT,BCv,Mv,logL,B-V,Bt-Vt,U-B,V-Rc,V-Ic,V-Ks,J-H,H-K,Ks-W1,Msun,
    logAge,b-y,SpTcopy,M_J,M_Ks,Mbol,i-z,z-Y,W1-W2

    The "output_unit" parameter is an astropy unit corresponding to the
    dimension that the output data is desired. For dimensionless quantities,
    units.dimensionless_unscaled will suffice.

    For Teff, the output unit will be assumed to be "K" unless set to a
    compatible temperature unit.

    For SpT, the output unit can be set to both units.dimensionless_unscaled,
    or the string "SpT", to output string SpT values without a luminosity class.

    interpolation_kind can be 'linear', 'nearest', 'slinear', 'quadratic',
    'cubic', from the scipy documentation."""
    # Define the default units of each column.
    default_units = \
    {"SpT": u.dimensionless_unscaled,
     "Teff": u.K,
     "logT": u.dimensionless_unscaled,
     "BCV": u.mag,
     "Mv": u.mag,
     "logL": u.dimensionless_unscaled,
     "B-V": u.mag,
     "Bt-Vt": u.mag,
     "V-Rc": u.mag,
     "V-Ic": u.mag,
     "V-Ks": u.mag,
     "J-H": u.mag,
     "H-Ks": u.mag,
     "Ks-W1": u.mag,
     "Msun": u.Msun,
     "logAge": u.dimensionless_unscaled,
     "b-y": u.mag,
     "SpTcopy": u.dimensionless_unscaled,
     "M_J": u.mag,
     "M_Ks": u.mag,
     "Mbol": u.mag,
     "i-z": u.mag,
     "z-Y": u.mag,
     "W1-W2": u.mag,
     "R_Rsun": u.dimensionless_unscaled}
    filename = "EEM_dwarf_UBVIJHK_colors_Teff.dat"
    # The following column headers are directly from the file, and ordered:
    column_headers = ["SpT", "Teff", "logT", "BCV", "Mv", "logL", "B-V",
                      "Bt-Vt", "G-V", "U-B", "V-Rc", "V-Ic", "V-Ks", "J-H", "H-Ks",
                      "Ks-W1", "W1-W2", "W1-W3", "W1-W4", "Msun", "logAge", "b-y", "M_J",
                      "M_Ks", "Mbol", "i-z", "z-Y", "R_Rsun"]
    available_colors = ["B-V", "Bt-Vt", "U-B", "V-Rc", "V-Ic", "V-Ks", "J-H",
                        "H-Ks", "Ks-W1", "b-y", "i-z", "z-Y", "W1-W2"]
    dtype = ["|S10", float, float, float, float, float, float,
             float, float, float, float, float, float, float, float,
             float, float, float, float, float, float, float, float,
             float, float, float, float, float]
    ### For missing values, coerce to np.NaN:
    missing_values = ["...", ".....", ",.."]
    filling_values = np.NaN
    # Skip 21 lines before reading in data:
    skip_header = 21
    # 107 total data entries
    max_rows = 107
    # The input file uses whitespace to delimit entries, so denote that:
    delimiter = None
    def load_data(self, filename):
        """Return the loaded data, according to the datatypes, missing values,
        and other parameters defined in the class, and parsed by row."""
        return np.genfromtxt(filename, comments = "#",
                             delimiter=self.delimiter,
                             skip_header=self.skip_header,
                             max_rows=self.max_rows,
                             dtype=self.dtype,
                             missing_values=self.missing_values,
                             filling_values=self.filling_values)
