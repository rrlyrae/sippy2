"""Davenport et al. (2014) Stellar locus color interpolator.
"""

from __future__ import absolute_import
from sippy.interpolator.BaseInterpolator import *
import os
import numpy as np
import scipy
import scipy.interpolate
import matplotlib.pyplot as plt
from astropy import units as u
from itertools import izip

class DavenportInterpolator(BaseInterpolator):
    filename = "Davenport2014.tsv"
    # The following column headers are directly from the file, and ordered:
    column_headers = ["g-i","Nstar","u-g","e_u-g","g-r","e_g-r","r-i","e_r-i",
                      "i-z","e_i-z","z-J","e_z-J","J-H","e_J-H","H-Ks","e_H-Ks",
                      "Nstars","Ks-W1","e_Ks-W1","W1-W2","e_W1-W2","NStarsW3",
                      "W2-W3","e_W2-W3"]
    # Define the default units of each column.
    default_units =\
    {"g-i": u.mag,
     "Nstar": u.dimensionless_unscaled,
     "u-g": u.mag,
     "e_u-g": u.mag,
     "g-r": u.mag,
     "e_g-r": u.mag,
     "r-i": u.mag,
     "e_r-i": u.mag,
     "i-z": u.mag,
     "e_i-z": u.mag,
     "z-J": u.mag,
     "e_z-J": u.mag,
     "J-H": u.mag,
     "e_J-H": u.mag,
     "H-Ks": u.mag,
     "e_H-Ks": u.mag,
     "Nstars": u.dimensionless_unscaled,
     "Ks-W1": u.mag,
     "e_Ks-W1": u.mag,
     "W1-W2": u.mag,
     "e_W1-W2": u.mag,
     "NStarsW3": u.dimensionless_unscaled,
     "W2-W3": u.mag,
     "e_W2-W3": u.mag}
    # Define the available colors:
    available_colors = ["g-i", "u-g", "g-r", "r-i", "i-z", "z-J", "J-H", "H-Ks",
                        "Ks-W1", "W1-W2", "W2-W3"]
    # Define the datatypes:
    dtype = [float,float,float,float,float,float,float,float,float,float,float,
             float,float,float,float,float,float,float,float,float,float,float,
             float,float]
    # No need to deal explicitly with missing values or filling values
    # by default
    missing_values = None
    filling_values = None
    # Skip 44 lines before reading in data:
    skip_header = 44
    # Read the entire file in:
    max_rows = None
    # The input file uses ";" to delimit entries:
    delimiter = ";"
    def __init__(self, input, output, interpolation_kind = "linear",
                 output_unit = None):
        # Set the units to output in
        self.output_unit = output_unit

        ### Set the default inputs and outputs according to the default unit
        ### of each column datatype.
        # Use the default units for the chosen input and output columns.
        self._default_input_unit = self.default_units[input]
        self._default_output_unit = self.default_units[output]

        ## If the output unit is unset, then assume that the default units
        ## are what's requested.

        if output_unit is None:
            self.output_unit = self._default_output_unit

        # Load in the data
        filename = os.path.dirname(os.path.abspath(__file__))\
                 + "/Data/" + self.filename
        load_data = self.load_data(filename)
        column_headers = self.column_headers
        ## If the output unit is unset, then assume that the default units
        ## are what's requested.

        if output_unit is None:
            self.output_unit = self._default_output_unit

        # Find the indices of the columns that correspond to input and output.
        input_index = column_headers.index(input)
        output_index = column_headers.index(output)

        input_interp = np.array([aa[input_index] for aa in load_data])
        output_interp = np.array([bb[output_index] for bb in load_data])

        ### Making sure to only expose the non-NaN values.
        # We obtain the indices at which neither the input or output interp
        # are NaN by performing an AND on the valid entries --- rejecting
        # an index if either the input or output interpolations are NaN.
        valid_indices =\
        np.logical_and(np.logical_not(np.isnan(input_interp)),
                       np.logical_not(np.isnan(output_interp)))
        # Sort interpolation arrays prior to interpolation:
        sorted_output_interp_trimmed =\
        [output_int for input_int, output_int in\
         sorted(izip(input_interp[valid_indices],
                    output_interp[valid_indices]),
                key = lambda x: x[0])]
        sorted_input_interp_trimmed =\
        sorted(input_interp[valid_indices])
        # Remove duplicates from the target input_interp:
        tiny = 1e-15
        sorted_input_interp_trimmed_no_duplicates = []
        sorted_output_interp_trimmed_no_duplicates = []
        for index in xrange(1, len(sorted_input_interp_trimmed)):
            if abs(sorted_input_interp_trimmed[index] -\
                   sorted_input_interp_trimmed[index-1]) > tiny:
                sorted_input_interp_trimmed_no_duplicates.\
                append(sorted_input_interp_trimmed[index])
                sorted_output_interp_trimmed_no_duplicates.\
                append(sorted_output_interp_trimmed[index])
        # Expose the interpolation evaluation method.
        self._evaluate =\
        scipy.interpolate.interp1d(sorted_input_interp_trimmed_no_duplicates,
                                   sorted_output_interp_trimmed_no_duplicates,
                                   kind = interpolation_kind,
                                   bounds_error = False,
                                   fill_value = np.nan, assume_sorted = True)
    def load_data(self, filename):
        """Return the loaded data, according to the datatypes, missing values,
        and other parameters defined in the class, and parsed by row."""
        return np.genfromtxt(filename, comments = "#",
                             delimiter=self.delimiter,
                             skip_header=self.skip_header,
                             max_rows=self.max_rows,
                             dtype=self.dtype,
                             missing_values=self.missing_values,
                             filling_values=self.filling_values)
