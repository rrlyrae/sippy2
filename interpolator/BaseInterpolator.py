"""
sippy.interpolator.BaseInterpolator:
Base python class and methods for creating interpolators between
stellar parameters.
Author: Lyra Cao
3/9/2018
"""

from __future__ import division
from __future__ import absolute_import
import numpy as np
from astropy import units as u
import re
import itertools as it
import random
import scipy.interpolate

class BaseInterpolator(object):
	"""Base class for interpolators."""
	def __init__(self):
		raise NotImplementedError
	def _evaluate(self, values):
		raise NotImplementedError
	def evaluate(self, values, *args, **kwargs):
		"""Evaluate and interpolate a given array or value."""
		if isinstance(values, u.Quantity):
			# If so, set it.
			values = values.to(self._default_input_unit).value
		### Polymorphism:
		### Check if additional arguments are given. If so, we need
		### to convert each input value systematically.
		if len(args) > 0:
			for index in xrange(len(args)):
				# Check to see if the values passed have a dimensional component
				if isinstance(args[index], u.Quantity):
					#Set the dimensional component for each value.
					args[index] =\
					args[index].to(self._default_input_unit[index+1]).value
			if len(kwargs.keys()) > 0:
				# Check whether kwargs is used as a key. (TODO: replace)
				if "kwargs" in kwargs.keys():
					output = self._evaluate(values, *args)
				else:
					output = self._evaluate(values, *args, **kwargs)
			else:
				output = self._evaluate(values, *args)
		else:
			if len(kwargs.keys()) > 0:
				# Check whether kwargs is used as a key. (TODO: replace)
				if "kwargs" in kwargs.keys():
					output = self._evaluate(values)
				else:
					output = self._evaluate(values, **kwargs)
			else:
				output = self._evaluate(values)
		# Convert from the default output unit to the specified output unit
		output *= (self.output_unit/self._default_output_unit).\
		in_units(u.dimensionless_unscaled)
		## Another possibility, though it will not error if there is a
		## problem with units:
		#output *= (self.output_unit/self._default_output_unit).\
		#decompose().scale
		return output
	def __call__(self, values, *args, **kwargs):
		"""Allows calling the class-level object with evaluate's arguments.
		Returns the values with proper units attached.
		To avoid returning the units as well, use evaluate()."""
		return self.evaluate(values, *args, **kwargs) * self.output_unit
	def __add__(self, other):
		"""Allows adding interpolators and numbers.
		"""
		return AddInterpolator(self, other)
	def __sub__(self, other):
		"""Allows subtracting interpolators and numbers.
		"""
		return SubInterpolator(self, other)
	def __mul__(self, other):
		"""Allows multiplying interpolators and numbers.
		"""
		return MulInterpolator(self, other)
	def __div__(self, other):
		"""Allows dividing interpolators and numbers.
		"""
		return DivInterpolator(self, other)

class AddInterpolator(BaseInterpolator):
	def __init__(self, interp1, interp2):
		self.interp1 = interp1
		self.interp2 = interp2
		# Assume units are the same --- it is addition, after all.
		self.output_unit = interp1.output_unit
		self._default_input_unit = interp1._default_input_unit
		self._default_output_unit = interp1._default_output_unit
	def evaluate(self, values, *args, **kwargs):
		#Check if interp2 is a BaseInterpolator:
		if isinstance(self.interp2, BaseInterpolator):
			return_val =\
			self.interp1.evaluate(values, *args, **kwargs) +\
			self.interp2.evaluate(values, *args, **kwargs)
		else:
			return_val =\
			self.interp1.evaluate(values, *args, **kwargs) +\
			self.interp2
		return return_val

class SubInterpolator(BaseInterpolator):
	def __init__(self, interp1, interp2):
		self.interp1 = interp1
		self.interp2 = interp2
		# Assume units are the same --- it is subtraction, after all.
		self.output_unit = interp1.output_unit
		self._default_input_unit = interp1._default_input_unit
		self._default_output_unit = interp1._default_output_unit
	def evaluate(self, values, *args, **kwargs):
		#Check if interp2 is a BaseInterpolator:
		if isinstance(self.interp2, BaseInterpolator):
			return_val =\
			self.interp1.evaluate(values, *args, **kwargs) -\
			self.interp2.evaluate(values, *args, **kwargs)
		else:
			return_val =\
			self.interp1.evaluate(values, *args, **kwargs) -\
			self.interp2
		return return_val

class MulInterpolator(BaseInterpolator):
	def __init__(self, interp1, interp2):
		self.interp1 = interp1
		self.interp2 = interp2
		self._default_input_unit = interp1._default_input_unit
		# Units are multiplied. But we have to check whether interp2 has an
		# output unit.
		if hasattr(interp2, "output_unit"):
			self.output_unit = interp1.output_unit*interp2.output_unit
			self._default_output_unit =\
			interp1._default_output_unit*\
			interp2._default_output_unit
		# If it doesn't, check if it has a unit:
		elif hasattr(interp2, "unit"):
			self.output_unit = interp1.output_unit*interp2.unit
			self._default_output_unit =\
			interp1._default_output_unit*\
			interp2.unit
		# Otherwise, just assume it's a scale factor:
		else:
			self.output_unit = interp1.output_unit
			self._default_output_unit =\
			interp1._default_output_unit
	def evaluate(self, values, *args, **kwargs):
		#Check if interp2 is a BaseInterpolator:
		if isinstance(self.interp2, BaseInterpolator):
			return_val =\
			self.interp1.evaluate(values, *args, **kwargs) *\
			self.interp2.evaluate(values, *args, **kwargs)
		else:
			if hasattr(self.interp2, "unit"):
				# It has a unit, so we have to get only its value here.
				interp2 = self.interp2.value
			else:
				# It doesn't have a unit. Scale factor?
				interp2 = self.interp2
			return_val =\
			self.interp1.evaluate(values, *args, **kwargs) *\
			interp2
		return return_val

class DivInterpolator(BaseInterpolator):
	def __init__(self, interp1, interp2):
		self.interp1 = interp1
		self.interp2 = interp2
		self._default_input_unit = interp1._default_input_unit
		# Units are multiplied. But we have to check whether interp2 has an
		# output unit.
		if hasattr(interp2, "output_unit"):
			self.output_unit = interp1.output_unit/interp2.output_unit
			self._default_output_unit =\
			interp1._default_output_unit/\
			interp2._default_output_unit
		# If it doesn't, check if it has a unit:
		elif hasattr(interp2, "unit"):
			self.output_unit = interp1.output_unit/interp2.unit
			self._default_output_unit =\
			interp1._default_output_unit/\
			interp2.unit
		# Otherwise, just assume it's a scale factor:
		else:
			self.output_unit = interp1.output_unit
			self._default_output_unit =\
			interp1._default_output_unit
	def evaluate(self, values, *args, **kwargs):
		#Check if interp2 is a BaseInterpolator:
		if isinstance(self.interp2, BaseInterpolator):
			return_val =\
			self.interp1.evaluate(values, *args, **kwargs) /\
			self.interp2.evaluate(values, *args, **kwargs)
		else:
			if hasattr(self.interp2, "unit"):
				# It has a unit, so we have to get only its value here.
				interp2 = self.interp2.value
			else:
				# It doesn't have a unit. Scale factor?
				interp2 = self.interp2
			return_val =\
			self.interp1.evaluate(values, *args, **kwargs) /\
			interp2
		return return_val

class NegativeInterpolator(BaseInterpolator):
	def __init__(self, interp):
		self.interp = interp
		# Units are the same.
		self.output_unit = interp.output_unit
		self._default_input_unit = interp._default_input_unit
		self._default_output_unit = interp._default_output_unit
	def evaluate(self, values, *args, **kwargs):
		# The return value is simply the negative evaluation.
		return_val =\
		-self.interp.evaluate(values, *args, **kwargs)
		return return_val

class SpTBaseInterpolator(BaseInterpolator):
	"""Base class for interpolators, with additional SpT helper functions.

	Note: evaluate___SpT_input currently
	does not support polymorphism like the BaseInterpolator's evaluate.

	There hasn't really been a need for this in the interpolation context,
	but if a function ever needs to pass in an argument list that has a
	SpT in it, then evaluate___SpT_input needs to be rewritten."""
	decimals = 3 # Significant figures to round to in output string SpT's
	spectral_types=["O", "B", "A", "F", "G", "K", "M", "L", "T", "Y"]
	zero_point=0
	@classmethod
	def set_spectral_types(self, spectral_types=["O", "B", "A", "F", "G",
												 "K", "M", "L", "T", "Y"]):
		"""Sets the ordered list of spectral types that are used,
		starting with O.

		The first spectral type used corresponds to the zero_point
		(nominally "O")"""
		self.spectral_types = spectral_types
	@classmethod
	def set_SpT_zero_point(self, zero_point=0):
		"""Sets the SpT zero_point consistently for the routines:
		SpT_to_num and num_to_SpT.

		By default, the "O" class corresponds to 0, ie: O7V corresponding to 7,
		but it can be changed to "O" corresponding to 10, and O7V corresponding
		to 17, by setting the zero_point to 1.

		Note: if set_spectral_types is invoked with a list with a different
		first item, that is what this zero point will refer to instead of "O".
		"""
		self.zero_point = zero_point
	@classmethod
	def SpT_to_num(self, SpT):
		"""Helper function for conversion of a single SpT to a single
		numerical value. Ignores luminosity class.

		Input: string SpT
		Output: float SpT

		Returns np.NaN in other cases!"""
		if isinstance(SpT, basestring):
			# Strip SpT of whitespace:
			SpT = SpT.strip()
			### Use regex to match the SpT literal:
			SpT_class = re.match("[A-Za-z]+", SpT).group(0)
			# Find the index corresponding to that SpT in the spectral types
			# list, set in set_spectral_types.
			SpT_index = self.spectral_types.index(SpT_class)
			# Find the spectral type with the correct zero point set by
			# set_SpT_zero_point.
			SpT_index = SpT_index + self.zero_point

			### Use regex to match the numerical literal:
			num_SpT_subclass = float(re.search("[0-9.]+", SpT).group(0))

			### The end SpT numerical value is:
			num = SpT_index*10. + num_SpT_subclass
			return num
		else: return np.NaN
	@classmethod
	def num_to_SpT(self, num):
		"""Helper function for conversion of numerical values to SpT. Does not
		return luminosity class.

		Input: float SpT
		Output: string SpT

		Returns "nan" if a string is passed, or the SpT is a nonsensical value.
		"""
		if isinstance(num, unicode):
			return "nan"
		else:
			if np.isfinite(num):
				### Find the SpT class:
				SpT_index = int(num/10)
				# Correct with the zero point set by set_SpT_zero_point
				SpT_index = SpT_index - self.zero_point
				# Find the SpT class:
				SpT_class = self.spectral_types[SpT_index]

				### Add the additional part:
				num_SpT_subclass = np.around(num - int(num/10)*10,
											 decimals = self.decimals)
				SpT_subclass = unicode(num_SpT_subclass)

				### Create the SpT:
				SpT = SpT_class + SpT_subclass

				return SpT
			else: return "nan"
	@classmethod
	def get_num_SpTs(self, SpTs):
		"""Gets a numpy array of numerical SpTs given any supported input:

		* String SpT (i.e., A7.5V)
		* Astropy unit (i.e., 27.5*u.dimensionless_unscaled)
		* Float (i.e., 27.5)
		"""
		### Currently have to check if input is lists/np arrays or just a value:
		if hasattr(SpTs, "__len__") and (not isinstance(SpTs, basestring)):
			# Yes, this is a list/np array.
			# Find the total number of SpT's passed to us.
			return np.array([self.get_num_SpTs(SpT) for SpT in SpTs])
		else:
			# No, this is a single item, like a string or float.
			SpT = SpTs
			if isinstance(SpT, u.Quantity):
				num_SpT =\
				SpT.to(self._default_input_unit).value
			# If this is a float, just copy it over:
			elif isinstance(SpT, float) or isinstance(SpT, int):
				num_SpT = SpT
			# Otherwise, consider it a string to be converted:
			else:
				num_SpT = self.SpT_to_num(SpT)
			return num_SpT
	def get_SpTs(self, num_SpTs):
		"""Takes a list of numerical SpT's and returns a list of string SpTs"""
		# Check if the numerical SpTs passed are a list/np array or a
		# scalar float.
		# Coerce to a numpy array.
		if hasattr(num_SpTs, "__len__"):
			# Yes, this is a list of values:
			return [self.get_SpTs(num_SpT) for num_SpT in num_SpTs]
		else:
			# No, this is a single value.
			num_SpT = num_SpTs
			SpT = self.num_to_SpT(num_SpT)
			return SpT
	def evaluate___SpT_input(self,  values, *args, **kwargs):
		"""The evaluate method, modified for SpT inputs."""
		# Convert from SpT input:
		values = self.get_num_SpTs(values)
		# Evaluate interpolator with these numerical SpTs:
		output = self._evaluate(values)
		# Convert from the default output unit to the specified output unit
		# If the output unit is the string "SpT", then we convert back:
		if self.output_unit == "SpT":
			# Convert to a string SpT:
			output = self.get_SpTs(output)
		else:
			output *= (self.output_unit/self._default_output_unit).\
			in_units(u.dimensionless_unscaled)
		return output
	def evaluate___SpT_output(self, values, *args, **kwargs):
		"""The evaluate method, modified for SpT outputs."""
		if isinstance(values, u.Quantity):
			# If so, set it.
			values = values.to(self._default_input_unit).value
		### Polymorphism:
		### Check if additional arguments are given. If so, we need
		### to convert each input value systematically.
		if len(args) > 0:
			for index in xrange(len(args)):
				# Check to see if the values passed have a dimensional component
				if isinstance(args[index], u.Quantity):
					#Set the dimensional component for each value.
					args[index] =\
					args[index].to(self._default_input_unit[index+1]).value
			output = self._evaluate(values, *args)
		else:
			output = self._evaluate(values)
		# Convert from the default output unit to the specified output unit
		# If the output unit is the string "SpT", then we convert back:
		if self.output_unit == "SpT":
			# Convert to a string SpT:
			output = self.get_SpTs(output)
		else:
			output *= (self.output_unit/self._default_output_unit).\
			in_units(u.dimensionless_unscaled)
		return output

class MergedFallbackInstantiatedInterpolator(BaseInterpolator):
	"""Provides "fallback interpolation" for an ordered list of similar
	instantiated interpolators that have the same input and output types.

	Instantiated means that the input and output of the interpolators has
	already been chosen.

	Fallback interpolation is defined as using the first interpolator in the
	list, and then replacing all NaN values with the second interpolator,
	and so on."""
	def __init__(self,interpolators,**kwargs):
		self.interpolators = interpolators
		# Assume the first interpolator is representative of all the
		# interpolators. Set the default inputs and outputs to that
		# interpolator.
		if hasattr(self.interpolators[0], "default_unit"):
			self.default_units = self.interpolators[0].default_units
		if hasattr(self.interpolators[0], "_default_input_unit"):
			self._default_input_unit = self.interpolators[0]._default_input_unit
		if hasattr(self.interpolators[0], "_default_output_unit"):
			self._default_output_unit = self.interpolators[0]._default_output_unit
		if hasattr(self.interpolators[0], "available_colors"):
			self.available_colors = self.interpolators[0].available_colors
		if hasattr(self.interpolators[0], "available_output_colors"):
			self.available_output_colors = self.interpolators[0].available_output_colors
		self.kwargs = kwargs
	def add_interpolator_last(self, interpolator):
		"""Adds an interpolator to the end of the ordered list
		of interpolators"""
		self.interpolators = self.interpolators + [interpolator]
	def add_interpolator_first(self, interpolator):
		"""Adds an interpolator to the start of the ordered list
		of interpolators"""
		self.interpolators = [interpolator] + self.interpolators
	def evaluate(self, values, *args, **kwargs):
		"""Iterates over all child interpolators using the fallback scheme"""
		# Save all the argument calls for child interpolators.
		saved_args = locals().copy()
		### Add own kwargs from initiation.
		saved_args.update(self.kwargs)
		# Remove "self" argument from the saved arguments so we can
		# Use it to evaluate our interpolators later in their own context.
		saved_args.pop("self", None)
		# Make sure the input array for values is a numpy array:
		values = np.array(values)
		### Iterate over all the interpolators, replacing NaN values with
		### data from the next interpolator.
		N = len(self.interpolators) # Number of interpolators
		# Get the evaluation of the first interpolator:
		output = self.interpolators[0].evaluate(**saved_args)
		# Iterate over all the rest
		for i in xrange(1, N):
			# Find the NaN values of the output
			nan_locations = np.isnan(output)
			# Amend the arguments to only use the
			# values corresponding to the NaN's of the previous iterator:
			saved_args["values"] = values[nan_locations]
			# Get the evaluation of the interpolator
			temp_output = self.interpolators[i].evaluate(**saved_args)
			# Amend output with this additional temp_output:
			output[nan_locations] = temp_output
		return output
	def __call__(self, values, *args, **kwargs):
		"""Allows calling the class-level object with evaluate's arguments.
		Returns the values with proper units attached.
		To avoid returning the units as well, use evaluate().

		Since MergedFallbackInterpolator is a list of interpolators with similar
		behavior, we use the output unit of the first interpolator in the chain.
		"""
		return self.evaluate(values, *args, **kwargs) *\
		self.interpolators[0].output_unit

class MergedFallbackInterpolator(BaseInterpolator):
	"""Provides "fallback interpolation" for an ordered list of similar
	uninstantiated interpolators that have the same input and output types.

	The method "on" constructs an interpolator on a specified input and output.

	Fallback interpolation is defined as using the first interpolator in the
	list, and then replacing all NaN values with the second interpolator,
	and so on.

	If uninstantiated color interpolators are added, available_colors,
	as well as available_input_colors, and available_output_colors,
	are defined."""
	def __init__(self,interpolators):
		self.interpolators = interpolators
		# Since the interpolators are not instantiated, the units are
		# ambiguous.
		# After using the key "on", input and output units can be defined.
		self.available_input_colors = []
		self.available_output_colors = []
		self.refresh()
	def add_interpolator_last(self, interpolator):
		"""Adds an interpolator to the end of the ordered list
		of interpolators"""
		self.interpolators = self.interpolators + [interpolator]
		self.refresh()
	def add_interpolator_first(self, interpolator):
		"""Adds an interpolator to the start of the ordered list
		of interpolators"""
		self.interpolators = [interpolator] + self.interpolators
		self.refresh()
	def refresh(self):
		### Set the available colors for automated querying of this method.
		#Iterate over each interpolator:
		self.default_units = {}

		for interpolator in self.interpolators:
			for attrib in interpolator.default_units:
				self.default_units[attrib] = interpolator.default_units[attrib]
			if hasattr(interpolator, "available_colors"):
				for available_color in interpolator.available_colors:
					if available_color not in self.available_input_colors:
						# If there is a color that the interpolator supports
						# that is not recorded in input, add it:
						self.available_input_colors.append(available_color)
					if available_color not in self.available_output_colors:
						# If there is a color that the interpolator supports
						# that is not recorded in output, add it:
						self.available_output_colors.append(available_color)
			if hasattr(interpolator, "available_input_colors"):
				for available_color in interpolator.available_input_colors:
					if available_color not in self.available_input_colors:
						# If there is a color that the interpolator supports
						# that is not recorded in input, add it:
						self.available_input_colors.append(available_color)
			if hasattr(interpolator, "available_output_colors"):
				for available_color in interpolator.available_output_colors:
					if available_color not in self.available_output_colors:
						# If there is a color that the interpolator supports
						# that is not recorded in output, add it:
						self.available_output_colors.append(available_color)
	def on(self, input, output, **kwargs):
		interpolator_list = []
		for interpolator in self.interpolators:
			# Instantiate each interpolator, and where an interpolator has
			# the behavior, "on", use that to instantiate it.
			if hasattr(interpolator, "on"):
				instantiated_interpolator =\
				interpolator.on(input, output, **kwargs)
			else:
				instantiated_interpolator =\
				interpolator(input, output, **kwargs)
			# Append the newly instantiated interpolator to the list
			interpolator_list.append(instantiated_interpolator)
		#Create a new MergedFallbackInstantiatedInterpolator and give it
		#these instantiated interpolators.
		return MergedFallbackInstantiatedInterpolator(interpolator_list)

class GeneralizedSpTInterpolator(object):
	"""Takes a color interpolator at a specific SpT and creates an interpolator
	that takes a wavelength and returns a color with a consistent second band,
	such as U-V, B-V, Ic-V, Ks-V ...

	Which can be turned into a spectrum by adding a zero-point photometry."""
	def __init__(self, SpT, ColorInterpolators = []):
		pass

class LinkedInstantiatedInterpolator(BaseInterpolator):
	"""A simple interpolator class that runs through each instantiated
	interpolator and feeds the output of the preceding one to the current one
	in sequence."""
	def _evaluate(self, values, *args, **kwargs):
		output = values
		# Iterating over each interpolator, feed the output of one to the next
		# one in the list.
		for interpolator in self.interpolators:
			output = interpolator.evaluate(output, *args, **kwargs)
		return output
	def __init__(self, interpolators, output_unit = None):
		self.interpolators = interpolators
		# Set default inputs and outputs.
		self._default_input_unit = self.interpolators[0]._default_input_unit
		self._default_output_unit = self.interpolators[-1]._default_output_unit
		if output_unit is None:
			self.output_unit = self._default_output_unit
		else:
			self.output_unit = output_unit

class LinkedInterpolator(BaseInterpolator):
	"""Returns an interpolator upon instantiation that links interpolators,
	by taking the output of the first interpolator as the input for the second
	interpolator, and so on.

	Must send these parameters to LinkedInterpolator.set().

	Interpolators is a list of uninstantiated interpolators.
	Links is a list of "link" parameters, which denotes the name of the column
	or input/output value that is shared between the interpolators, in sequence.
	There must be 1 less link than interpolators.

	For example, a value for Links of ["A", "B", "C"] would give the following:
	Interpolator 1: Input -> A
	Interpolator 2: A -> B
	Interpolator 3: B -> C
	Interpolator 4: C -> Output

	Where Input and Output is determined at runtime.

	arg_list is a list of dictionaries which correspond to the **kwargs to
	initialize each interpolator, in sequence.

	Usage:
	from sippy.interpolator import *
	BTSettl_with_SpT =\
	LinkedInterpolator([PecautMamajekYoungInterpolator,
						BTSettlInterpolator],
						["Teff"],
						arg_list = [{"interpolation_kind": "linear"},
									{"interpolation_kind": "linear",
									 "logg": 4.4}])

	BTSettl_gz = BTSettl_with_SpT.on("SpT", "g-z")
	BTSettl_gz("G0V")

	Note: since this code takes uninstantiated interpolators,
	and Python doesn't support multiple constructors, it must be instantiated
	with ``on''.

	Under the hood, this evaluates:
	PecautMamajekYoungInterpolator("G0V") to get a Teff,
	and BTSettlSDSSInterpolator with that Teff.
	"""
	def on(self, input, output, interpolation_kind = "linear",
				 output_unit = None):
		"""Creates and returns a LinkedInstantiatedInterpolator on the
		specified input and output.

		If interpolation_kind is already specified in arg_list,
		that is the value that is used."""
		# print "self.links", self.links
		interpolators = []
		# The full links object has `input` as its first element
		# and `output` as its last element.
		full_links = [input] + self.links + [output]
		# print "full_links", full_links
		# Create input and output argument pairs:
		for index in xrange(len(full_links) - 1):
			argument_pairs = (full_links[index], full_links[index+1])
			# print "base index", index, argument_pairs, self.arg_list[index]
			#Which is now a list full of tuples from
			#Input to X, X to Y, Y to Output. (To arbitrary length.)
			if hasattr(self.interpolators[index], "on"):
				interpolators.append(self.interpolators[index]\
									 .on(*argument_pairs,**self.arg_list[index]))
			else:
				interpolators.append(self.interpolators[index]\
									 (*argument_pairs,**self.arg_list[index]))

		return LinkedInstantiatedInterpolator(interpolators)
	def __init__(self, interpolators, links, arg_list = None, color_input = False):
		"""Instantiate a linked interpolator."""
		#assert isinstance(links, str),\
		#"The parameter for the links must be a list/tuple. "+\
		#"(It can be a list with one element.)"
		if isinstance(links, unicode):
			links = [links]
		assert len(links) == len(interpolators) - 1, "Wrong number of links"\
		+" for the number of interpolators passed."
		if arg_list is not None:
			assert len(arg_list) == len(interpolators), "Wrong number of"\
			+" argument dictionaries for the number of interpolators passed."
		else:
			# No keyword args to pass, so the arg_list is just an empty
			# dictionary
			arg_list = [dict() for x in xrange(len(interpolators))]
		self.interpolators = interpolators
		self.links = links
		self.arg_list = arg_list

		self.default_units = {}

		for interpolator in interpolators:
			for attrib in interpolator.default_units:
				self.default_units[attrib] = interpolator.default_units[attrib]

		### We choose available_colors manually now with the argument "color_input = True / False".

		if color_input:
			if hasattr(self.interpolators[0], "available_colors"):
				self.available_colors =\
				self.interpolators[0].available_colors
				self.available_input_colors =\
				self.interpolators[0].available_colors
			if hasattr(self.interpolators[0], "available_input_colors"):
				self.available_colors =\
				self.interpolators[0].available_input_colors
				self.available_input_colors =\
				self.interpolators[0].available_input_colors
		else:
			if hasattr(self.interpolators[-1], "available_colors"):
				self.available_colors =\
				self.interpolators[-1].available_colors
				self.available_output_colors =\
				self.interpolators[-1].available_colors
			if hasattr(self.interpolators[-1], "available_input_colors"):
				self.available_colors =\
				self.interpolators[-1].available_input_colors
				self.available_output_colors =\
				self.interpolators[-1].available_input_colors

		# # If the first interpolator is a ColorInterpolator, then set
		# # available_colors:
		# if hasattr(self.interpolators[0], "available_colors"):
		# 	self.available_input_colors =\
		# 	 self.interpolators[0].available_colors
		#
		# # If the last interpolator is a ColorInterpolator, then set
		# # available_colors:
		# if hasattr(self.interpolators[-1], "available_colors"):
		# 	self.available_output_colors =\
		# 	 self.interpolators[-1].available_colors
		#
		# # If the first interpolator is a subclass that extends ColorInterpolator
		# # set available_colors:
		# if hasattr(self.interpolators[0], "available_input_colors"):
		# 	self.available_input_colors =\
		# 	self.interpolators[0].available_input_colors
		#
		# # If the last interpolator is a subclass that extends ColorInterpolator
		# # set available_colors:
		# if hasattr(self.interpolators[-1], "available_output_colors"):
		# 	self.available_output_colors =\
		# 	self.interpolators[-1].available_output_colors

class ExtendColorInterpolator(SpTBaseInterpolator):
	"""Returns a ColorInterpolator that routes colors to its child
	ColorInterpolators.

	Accepts an ordered list of ColorInterpolators upon initialization. When
	ColorInterpolators have repeats of available colors, only the first instance
	of the color will be used.

	Only novel colors will be added. For instance, if U-B and B-V are already
	defined, a new addition of a U-V color will be ignored, because U-V can be
	obtained with the existing colors.

	When initializing "on", only one of input or output can be a color.
	If both are colors, a better class to use might be LinkedInterpolator.

	Note: since this code takes uninstantiated interpolators,
	and Python doesn't support multiple constructors, it must be instantiated
	with ``on''."""
	def __init__(self, ColorInterpolators):
		# Polymorphism: even if a single ColorInterpolator is passed, this
		# will work.
		if not hasattr(ColorInterpolators, "__len__"):
			ColorInterpolators = [ColorInterpolators]
		self.interpolators = ColorInterpolators
		self.refresh()
	def refresh(self):
		"""Refreshes the ColorInterpolator map."""
		self.route_input = {} #Routes for interpolators that input colors.
		self.route_output = {} #Routes for interpolators that output colors.
		# Iterate over each color in each ColorInterpolator,
		# Starting with the first ColorInterpolator's colors.
		# With these colors, create a routing table that points to
		# Each ColorInterpolator.
		for ColorInterpolator in self.interpolators:
			if hasattr(ColorInterpolator, "available_colors"):
				for available_color in ColorInterpolator.available_colors:
					# Check whether the color is a duplicate (or can be obtained
					# by existing colors).
					# First create a list of tuples corresponding to each color:
					route_input_keys_tuple =\
					[x.split("-") for x in self.route_input.keys()]
					route_output_keys_tuple =\
					[x.split("-") for x in self.route_output.keys()]
					# Create a graph corresponding to these tuples.
					graph_input =\
					self.color_list_to_graphs(route_input_keys_tuple)
					graph_output =\
					self.color_list_to_graphs(route_output_keys_tuple)
					# If there is no path between the color in question, add it
					if self.find_shortest_path(graph_input,
											   available_color.split("-")[0],
											   available_color.split("-")[1])\
					is None:
						# If the color is not in the route, we add it.
						self.route_input[available_color] = ColorInterpolator
					if self.find_shortest_path(graph_output,
											   available_color.split("-")[0],
											   available_color.split("-")[1])\
					is None:
						self.route_output[available_color] = ColorInterpolator
			if hasattr(ColorInterpolator, "available_input_colors"):
				for available_color in ColorInterpolator.available_input_colors:
					# Check whether the color is a duplicate (or can be obtained
					# by existing colors).
					# First create a list of tuples corresponding to each color:
					route_input_keys_tuple =\
					[x.split("-") for x in self.route_input.keys()]
					# Create a graph corresponding to these tuples.
					graph_input =\
					self.color_list_to_graphs(route_input_keys_tuple)
					# If there is no path between the color in question, add it
					if self.find_shortest_path(graph_input,
											   available_color.split("-")[0],
											   available_color.split("-")[1])\
					is None:
						# If the color is not in the route, we add it.
						self.route_input[available_color] = ColorInterpolator
			if hasattr(ColorInterpolator, "available_output_colors"):
				for available_color in ColorInterpolator.available_output_colors:
					# Check whether the color is a duplicate (or can be obtained
					# by existing colors).
					# First create a list of tuples corresponding to each color:
					route_output_keys_tuple =\
					[x.split("-") for x in self.route_output.keys()]
					# Create a graph corresponding to these tuples.
					graph_output =\
					self.color_list_to_graphs(route_output_keys_tuple)
					# If there is no path between the color in question, add it
					if self.find_shortest_path(graph_output,
											   available_color.split("-")[0],
											   available_color.split("-")[1])\
					is None:
						# If the color is not in the route, we add it.
						self.route_output[available_color] = ColorInterpolator

		# Expose the available colors.
		self.available_input_colors = self.route_input.keys()
		self.available_output_colors = self.route_output.keys()
		# Expose the updated default unit dictionary by iterating on children:
		self.default_units = {}
		for ColorInterpolator in self.interpolators:
			for unit_key in ColorInterpolator.default_units:
				self.default_units[unit_key] =\
				ColorInterpolator.default_units[unit_key]
	def on(self, input, output, **kwargs):
		assert len(input.split("-")) < 2 or len(output.split("-")) < 2,\
		"Input and output cannot both be colors."
		if len(input.split("-")) == 2:
			# The input is a color, so choose that color in the routing table.
			interpolator = self.route_input[input]
			# If the interpolator has the key "on", use that:
			if hasattr(interpolator, "on"):
				return interpolator.on(input, output, **kwargs)
			return interpolator(input, output, **kwargs)
		elif len(output.split("-")) == 2:
			# The output is a color, so choose that color in the routing table.
			interpolator = self.route_output[output]
			# If the interpolator has the key "on", use that:
			if hasattr(interpolator, "on"):
				return interpolator.on(input, output, **kwargs)
			return interpolator(input, output, **kwargs)
		# print "Warning: neither input or output are colors."
		### By default, return the first interpolator's value.
		if hasattr(self.interpolators[0], "on"):
			return self.interpolators[0].on(input, output, **kwargs)
		else:
			return self.interpolators[0](input, output, **kwargs)
	def add_interpolator_first(self, interpolator):
		self.interpolators = [intepolator] + self.interpolators
		self.refresh()
	def add_interpolator_last(self, interpolator):
		self.interpolators = self.interpolators + [intepolator]
		self.refresh()
	def find_shortest_path(self, graph, start, end, path=[]):
		"""Code from https://www.python.org/doc/essays/graphs/
		Returns the shortest path between nodes in a supplied graph.
		3/19/2018"""
		path = path + [start]
		if start == end:
			return path
		if not start in graph:
			return None
		shortest = None
		for node in graph[start]:
			if node not in path:
				newpath = self.find_shortest_path(graph, node, end, path)
				if newpath:
					if not shortest or len(newpath) < len(shortest):
						shortest = newpath
		return shortest
	@staticmethod
	def color_list_to_graphs(color_list):
		"""Generates a dictionary of graphs from a list of colors.
		Returns a bidirectional graph under the key "bidirectional",
		A forward-traversing graph under the key "forward",
		A backward-traversing graph under the key "backward".

		Accepts a list of color tuples like ("B", "V").
		Assumes tuple is a directed edge from the first color to
		the second color."""
		#Create the graph dictionaries:
		bidirectional_graph = {}
		forward_facing_graph = {}
		backward_facing_graph = {}
		#Traverse forward from each band to the final band.
		for initial_band, final_band in color_list:
			if not initial_band in forward_facing_graph:
				bidirectional_graph[initial_band] = [final_band]
				forward_facing_graph[initial_band] = [final_band]
			else:
				bidirectional_graph[initial_band].append(final_band)
				forward_facing_graph[initial_band].append(final_band)
		#Create a graph, going in reverse.
		for final_band, initial_band in color_list:
			if not initial_band in backward_facing_graph:
				backward_facing_graph[initial_band] = [final_band]
			else:
				backward_facing_graph[initial_band].append(final_band)
			if not initial_band in bidirectional_graph:
				bidirectional_graph[initial_band] = [final_band]
			else:
				bidirectional_graph[initial_band].append(final_band)
		#Return a dictionary of color lists.
		return {"forward":forward_facing_graph,
				"backward":backward_facing_graph,
				"bidirectional":bidirectional_graph}

# class ModifyColorInterpolator(BaseInterpolator):
	# """Returns a the same uninstantiated ColorInterpolator, except with
	# modifications to particular bands.
#
	# Note: since this code takes uninstantiated interpolators,
	# and Python doesn't support multiple constructors, it must be instantiated
	# with ``on''."""
	# def __init__(self, ColorInterpolator):
		## Polymorphism: even if a single ColorInterpolator is passed, this
		## will work
		# self.interpolator = ColorInterpolator
		# self.available_input_colors = self.interpolator.available_input_colors
		# self.available_output_colors = self.interpolator.available_output_colors
		# self.modifier_log_teffs = {}
		# self.modifier_band_deltas = {}
	# def add_condition(self, band, log_teffs, delta_band):
		# """Adds a modifier to a band."""
		# self.modifier_log_teffs[band] = log_teffs
		# self.modifier_band_deltas[band] = delta_band
	# @staticmethod
	# def wrapped_modifier_input(modify_teffs, modify_band_delta, interpolator, negative = False):
		# """A wrapped modifier that applies the specified modification to inputs"""
		# def inner_wrapper_input(input, output, **kwargs):
			# return interpolator(input, output, **kwargs)
		# return inner_wrapper_input
	# def wrapped_modifier_output(modify_teffs, modify_band_delta, interpolator, negative = False):
		# """A wrapped modifier that applies the specified modification to outputs"""
		# def inner_wrapper_output(input, output, **kwargs):
			# return interpolator(input, output, **kwargs)
		# return inner_wrapper_output
	# def on(self, input, output, **kwargs):
		# """Apply modifiers to the resultant interpolator --- wrapping _evaluate."""
		# input_split = input.split("-")
		# output_split = output.split("-")
		# if len(input_split) == 2:
			## The input is a color, so choose that color in the routing table.
			# interpolator = self.route_input[input]
			## If the interpolator has the key "on", use that:
			# if hasattr(interpolator, "on"):
				# interpolator = self.wrapped_modifier_input(np.power(10., ), , interpolator.on(input, output, **kwargs))
			# return interpolator(input, output, **kwargs)
		# if len(output_split) == 2:
			# out_band_1, out_band_2 = output_split
			## If the interpolator has the key "on", use that:
			# if hasattr(interpolator, "on"):
				##First band is positive, second band is negative.
				#
				# interpolator =\
				# self.wrapped_modifier_output(\
				# np.power(10., self.modifier_log_teffs[out_band_1]),
				# self.modifier_band_deltas[out_band_1],
				# interpolator.on(input, output, **kwargs),
				# negative = False)
#
				# interpolator =\
				# self.wrapped_modifier_output(\
				# np.power(10., self.modifier_log_teffs[out_band_2]),
				# self.modifier_band_deltas[out_band_2],
				# interpolator.on(input, output, **kwargs),
				# negative = True)
			# return interpolator(input, output, **kwargs)

class SDSSBaseColorInterpolator(BaseInterpolator):
	"""Creates a SDSS-Color Interpolator from a Johnsons-Cousins
	Color Interpolator."""
	available_colors = ["g-V","r-i","r-z","r-Rc","u-g",
	"g-B","g-r","i-Ic"]
	default_units =\
	{"g-V": u.mag,
	 "r-i": u.mag,
	 "r-z": u.mag,
	 "r-Rc": u.mag,
	 "u-g": u.mag,
	 "g-B": u.mag,
	 "g-r": u.mag,
	 "i-Ic": u.mag}
	SDSSTransformRelations =\
	{"a1": 0.630,
	"b1": -0.124,
	"a2": 1.007,
	"b2": -0.236,
	"a3": 1.584,
	"b3": -0.386,
	"a4_a": 0.267,
	"b4_a": 0.088,
	"a4_b": 0.77,
	"b4_b": -0.37,
	"a5": 0.750,
	"b5": 0.770,
	"c5": 0.720,
	"a6": -0.370,
	"b6": -0.124,
	"a7": 1.646,
	"b7": -0.139,
	"a8": 0.247,
	"b8": 0.329}
	def __init__(self, ColorInterpolator,
	output_unit = u.mag):
		self.output_unit = output_unit
		#Instantiate a SDSSTransformRelations object and use it to calculate
		#The actual transforms:
		S =\
		self.SDSSTransformRelations

		self.transform_table =\
		{"g-V": {"colors": ["B-V"],
		"function": lambda x: np.array(S["a1"]*x+S["b1"])},
		 "r-i": {"colors": ["V-Rc", "V-Ic"],
		 "function": lambda x, y: np.array(S["a2"]*(y-x)+S["b2"])},
		 "r-z": {"colors": ["V-Rc", "V-Ic"],
		 "function": lambda x, y: np.array(S["a3"]*(y-x)+S["b3"])},
		 "r-Rc": {"colors": ["V-Rc"],
		 "function": lambda x:\
		 np.where(x > 0.93, S["a4_b"]*x+S["b4_b"], S["a4_a"]*x+S["b4_a"])},
		 "u-g": {"colors": ["U-B", "B-V"],
		 "function":\
		 lambda x, y: np.array(S["a5"]*x+S["b5"]*y+S["c5"])},
		 "g-B": {"colors": ["B-V"],
		 "function": lambda x: np.array(S["a6"]*x+S["b6"])},
		 "g-r": {"colors": ["V-Rc"],
		 "function": lambda x: np.array(S["a7"]*x+S["b7"])},
		 "i-Ic": {"colors": ["V-Rc", "V-Ic"],
		 "function": lambda x, y: np.array(S["a8"]*(y-x)+S["b8"])}}

		self.ColorInterpolator = ColorInterpolator

		self.column_headers = []
		if hasattr(ColorInterpolator, "column_headers"):
			column_headers = ColorInterpolator.column_headers
		elif hasattr(ColorInterpolator, "available_input_colors"):
			column_headers = ColorInterpolator.available_input_colors
		for column_header in column_headers:
			if len(column_header.split("-")) > 1:
				pass
			else:
				self.column_headers.append(column_header)
				self.default_units[column_header] =\
				ColorInterpolator.default_units[column_header]
	def on(self, input, output, interpolation_kind = "linear", output_unit = None):
		#### Create a copy of the interpolator, and return it
		if output_unit is None:
			output_unit = self.output_unit
		CopyInterpolator = SDSSBaseColorInterpolator(self.ColorInterpolator,
		output_unit = output_unit)
		CopyInterpolator._default_input_unit =\
		self.ColorInterpolator.default_units[input]
		CopyInterpolator._default_output_unit =\
		CopyInterpolator.default_units[output]
		if input in CopyInterpolator.transform_table:
			assert False,\
			"Input as color not well-defined yet for SDSSBaseColorInterpolator"
		if output in CopyInterpolator.transform_table:
			CopyInterpolator.output = output
			if hasattr(CopyInterpolator.ColorInterpolator, "on"):
				CopyInterpolator.color_interpolators =\
				[CopyInterpolator.ColorInterpolator.on(input,\
				CopyInterpolator.transform_table[output]["colors"][color_idx])\
				for color_idx in \
				range(len(CopyInterpolator.transform_table[output]["colors"]))]
			else:
				CopyInterpolator.color_interpolators =\
				[CopyInterpolator.ColorInterpolator(input,\
				CopyInterpolator.transform_table[output]["colors"][color_idx])\
				for color_idx in \
				range(len(CopyInterpolator.transform_table[output]["colors"]))]
		return CopyInterpolator
	def _evaluate(self, vals):
		return \
		self.transform_table[self.output]["function"](\
		*[color_interpolator.evaluate(vals) for color_interpolator in\
		self.color_interpolators])

class SDSSAddedColorInterpolator(BaseInterpolator):
	"""Add SDSS colors to a color interpolator that has U,B,V,Rc,Ic defined.
	Does this by defining a SDSSBaseColorInterpolator and appending it
	to the color interpolator."""
	pass

### Kron system:
# coeffs for (V-R)k ... [-0.05269324  0.53111231  0.90875872 -0.27256252]
# range for (V-R)c: 0.113 2.0305625
# coeffs for (R-I)k ... [-0.11446847  1.1366631  -0.34691909  0.10843744]
# range for (R-I)c: 0.102 2.29544
# coeffs for (V-I)k ... [-0.24594173  1.07473192 -0.03411036  0.00928926]
# range for (V-I)c: 0.227 4.3083875

class KronBaseColorInterpolator(BaseInterpolator):
	"""Creates a Kron Color Interpolator from a Cousins
	Color Interpolator."""
	available_colors = ["V-Rk", "Rk-Ik", "V-Ik"]
	default_units =\
	{"V-Rk": u.mag,
	 "Rk-Ik": u.mag,
	 "V-Ik": u.mag}
	KronTransformRelations =\
	{"a1": -0.05269324,
	 "b1": 0.53111231,
	 "c1": 0.90875872,
	 "d1": -0.27256252,
	 "a2": -0.11446847,
 	 "b2": 1.1366631,
 	 "c2": -0.34691909,
 	 "d2": 0.10843744,
	 "a3": -0.24594173,
 	 "b3": 1.07473192,
 	 "c3": -0.03411036,
 	 "d3": 0.00928926}
	KronWindows =\
	{"V-Rk": [0.113, 2.0305625],
	 "Rk-Ik": [0.102, 2.29544],
	 "V-Ik": [0.227, 4.3083875]}
	def __init__(self, ColorInterpolator,
	output_unit = u.mag):
		self.output_unit = output_unit
		#Instantiate a KronTransformRelations object and use it to calculate
		#The actual transforms:
		S =\
		self.KronTransformRelations

		W =\
		self.KronWindows

		self.transform_table =\
		{"V-Rk": {"colors": ["V-Rc"],
		 "function": lambda x:\
		 np.where(np.logical_and(\
		 np.array(S["a1"]+S["b1"]*x+S["c1"]*x*x+S["d1"]*x*x*x) > W["V-Rk"][0],
		 np.array(S["a1"]+S["b1"]*x+S["c1"]*x*x+S["d1"]*x*x*x) < W["V-Rk"][1]),
		 np.array(S["a1"]+S["b1"]*x+S["c1"]*x*x+S["d1"]*x*x*x),
		 np.zeros_like(x) + np.NaN)},
		 "Rk-Ik": {"colors": ["Rc-Ic"],
		 "function": lambda x:\
		 np.where(np.logical_and(\
		 np.array(S["a2"]+S["b2"]*x+S["c2"]*x*x+S["d2"]*x*x*x) > W["Rk-Ik"][0],
		 np.array(S["a2"]+S["b2"]*x+S["c2"]*x*x+S["d2"]*x*x*x) < W["Rk-Ik"][1]),
		 np.array(S["a2"]+S["b2"]*x+S["c2"]*x*x+S["d2"]*x*x*x),
		 np.zeros_like(x) + np.NaN)},
		 "V-Ik": {"colors": ["V-Ic"],
		 "function": lambda x:\
		 np.where(np.logical_and(\
		 np.array(S["a3"]+S["b3"]*x+S["c3"]*x*x+S["d3"]*x*x*x) > W["V-Ik"][0],
		 np.array(S["a3"]+S["b3"]*x+S["c3"]*x*x+S["d3"]*x*x*x) < W["V-Ik"][1]),
		 np.array(S["a3"]+S["b3"]*x+S["c3"]*x*x+S["d3"]*x*x*x),
		 np.zeros_like(x) + np.NaN)}}

		self.ColorInterpolator = ColorInterpolator

		self.column_headers = []
		if hasattr(ColorInterpolator, "column_headers"):
			column_headers = ColorInterpolator.column_headers
		elif hasattr(ColorInterpolator, "available_input_colors"):
			column_headers = ColorInterpolator.available_input_colors
		for column_header in column_headers:
			if len(column_header.split("-")) > 1:
				pass
			else:
				self.column_headers.append(column_header)
				self.default_units[column_header] =\
				ColorInterpolator.default_units[column_header]
	def on(self, input, output, interpolation_kind = "linear", output_unit = None):
		#### Create a copy of the interpolator, and return it
		if output_unit is None:
			output_unit = self.output_unit
		CopyInterpolator = KronBaseColorInterpolator(self.ColorInterpolator,
		output_unit = output_unit)
		CopyInterpolator._default_input_unit =\
		self.ColorInterpolator.default_units[input]
		CopyInterpolator._default_output_unit =\
		CopyInterpolator.default_units[output]
		if input in CopyInterpolator.transform_table:
			assert False,\
			"Input as color not well-defined yet for SDSSBaseColorInterpolator"
		if output in CopyInterpolator.transform_table:
			CopyInterpolator.output = output
			if hasattr(CopyInterpolator.ColorInterpolator, "on"):
				CopyInterpolator.color_interpolators =\
				[CopyInterpolator.ColorInterpolator.on(input,\
				CopyInterpolator.transform_table[output]["colors"][color_idx])\
				for color_idx in \
				range(len(CopyInterpolator.transform_table[output]["colors"]))]
			else:
				CopyInterpolator.color_interpolators =\
				[CopyInterpolator.ColorInterpolator(input,\
				CopyInterpolator.transform_table[output]["colors"][color_idx])\
				for color_idx in \
				range(len(CopyInterpolator.transform_table[output]["colors"]))]
		return CopyInterpolator
	def _evaluate(self, vals):
		return \
		self.transform_table[self.output]["function"](\
		*[color_interpolator.evaluate(vals) for color_interpolator in\
		self.color_interpolators])

class BaseIsochroneInterpolator(object):
	"""Abstract base class for isochronal calculators."""
	def load_data(self):
		raise NotImplementedError
	def __init__(self, from_tuple, to_tuple, subsample_N = None, tol = 1e-6,
				 output_unit = (None, None), scheme = "linear",
				 resolution_factor = 2, filename = None, save = False):
		"""Initializes an isochronal interpolator on the tuples provided as
		from_tuple to to_tuple.
		IE: from: ("logT", "logL") to ("logage", "mass")
		Available values are found in self.column_headers.

		Creates interpolants by gridding onto a regular grid of from_tuple,
		then exposing this as an interpolant.

		subsample_N is an optional parameter specifying the number of points of
		the initial guess 2d array. Leaving it as None will use the entire grid
		to guess with a simple nearest neighbor method.

		tol refers to the tolerance for gradient estimation in
		scipy.interpolate.

		output_unit is a tuple of units corresponding to the output to_tuple.

		scheme denotes the kind of interpolation to use. Acceptable values
		are: linear, cubic, and nearest.

		resolution_factor corresponds to the density multiplication in the
		number of points used for the grid.

		filename is a string that corresponds to a file to load or save as the
		parameters grid_out_v1 and grid_out_v2.

		save is a boolean --- if set to False, it will try to load the filename
		and if set to True, it will try to save grid_out_v1, grid_out_v2 to the
		filename."""
		# Set the units to output in
		self.output_unit = output_unit

		### Set the default inputs and outputs according to the default unit
		### of each column datatype.
		# Use the default units for the chosen input and output columns.
		self._default_input_unit =\
		(self.default_units[from_tuple[0]], self.default_units[from_tuple[1]])
		self._default_output_unit =\
		(self.default_units[to_tuple[0]], self.default_units[to_tuple[1]])

		## If the output unit is unset, then assume that the default units
		## are what's requested.

		if output_unit[0] is None:
			_output_unit_0 = self._default_output_unit[0]
		else:
			_output_unit_0 = output_unit[0]
		if output_unit[1] is None:
			_output_unit_1 = self._default_output_unit[1]
		else:
			_output_unit_1 = output_unit[1]
		self.output_unit = (_output_unit_0, _output_unit_1)

		for provided_column_name in from_tuple + to_tuple:
			assert provided_column_name in self.column_headers,\
			"Column not found. Available columns are: "+\
			repr(self.column_headers)
		self.points = self.load_data()
		# Create a sub-sampled population that provides the initial guess
		# efficiently, for scipy.optimize.fmin to use prior to
		# performing the full minimization.
		self.sample_points = self.subsample(self.points, N = subsample_N)
		# Save the tuples that correspond to from and to.
		self.from_tuple = from_tuple
		self.to_tuple = to_tuple

		# Find the indices corresponding to the desired input and output
		# In the interpolation.
		input_index_val1 = self.column_headers.index(self.from_tuple[0])
		input_index_val2 = self.column_headers.index(self.from_tuple[1])
		output_index_val1 = self.column_headers.index(self.to_tuple[0])
		output_index_val2 = self.column_headers.index(self.to_tuple[1])

		if filename is not None and not save:
			### Load data:
			with np.load(filename) as data:
				grid_out_v1 = data["grid_out_v1"]
				grid_out_v2 = data["grid_out_v2"]
				val1_regular = data["val1_regular"]
				val2_regular = data["val2_regular"]
		else:

			###Create the interpolants:
			if scheme == "linear":
				interpolator1 =\
				scipy.interpolate.\
				LinearNDInterpolator(list(zip(self.points.T[input_index_val1],
											  self.points.T[input_index_val2])),
									 self.points.T[output_index_val1])
				interpolator2 =\
				scipy.interpolate.\
				LinearNDInterpolator(list(zip(self.points.T[input_index_val1],
											  self.points.T[input_index_val2])),
									 self.points.T[output_index_val2])
			if scheme == "nearest":
				interpolator1 =\
				scipy.interpolate.\
				NearestNDInterpolator(list(zip(self.points.T[input_index_val1],
											   self.points.T[input_index_val2])),
									  self.points.T[output_index_val1])
				interpolator2 =\
				scipy.interpolate.\
				NearestNDInterpolator(list(zip(self.points.T[input_index_val1],
											   self.points.T[input_index_val2])),
									  self.points.T[output_index_val2])
			if scheme == "cubic":
				interpolator1 =\
				scipy.interpolate.\
				CloughTocher2DInterpolator(list(zip(self.points.T[input_index_val1],
													self.points.T[input_index_val2])),
										   self.points.T[output_index_val1],
										   tol = tol)
				interpolator2 =\
				scipy.interpolate.\
				CloughTocher2DInterpolator(list(zip(self.points.T[input_index_val1],
													self.points.T[input_index_val2])),
										   self.points.T[output_index_val2],
										   tol = tol)

			### Grid onto a regular plane of from_tuple.
			### The number of grid points
			min_val1 = np.nanmin(self.points.T[input_index_val1])
			max_val1 = np.nanmax(self.points.T[input_index_val1])
			min_val2 = np.nanmin(self.points.T[input_index_val2])
			max_val2 = np.nanmax(self.points.T[input_index_val2])
			num_points = len(self.points)
			side_grid_points = np.ceil(np.sqrt(resolution_factor * num_points))
			val1_regular = np.linspace(min_val1, max_val1, side_grid_points)
			val2_regular = np.linspace(min_val2, max_val2, side_grid_points)
			### If the parameter used is mass, make a special exception
			### and grid it in log mass steps. Otherwise, we don't cover
			### the low-mass regime.
			if self.from_tuple[0] == "mass":
				val1_regular = np.logspace(np.log10(min_val1),
										   np.log10(max_val1),
										   side_grid_points)
			if self.from_tuple[1] == "mass":
				val2_regular = np.logspace(np.log10(min_val2),
										   np.log10(max_val2),
										   side_grid_points)

			# Will run into a MemoryError if the entire grid is passed in
			# at once, so we need to iterate over val1:
			#val1s = val1_regular[:,None]
			#val2s = val2_regular[None,:]
			param_array = np.zeros(len(val1_regular))
			grid_out_v1 = []
			grid_out_v2 = []
			for val1 in val1_regular:
				param_array.fill(val1)
				grid_out_v1.append(
				interpolator1(param_array, val2_regular))
				grid_out_v2.append(
				interpolator2(param_array, val2_regular))
			if filename is not None:
				np.savez(filename,
						 grid_out_v1 = grid_out_v1,
						 grid_out_v2 = grid_out_v2,
						 val1_regular = val1_regular,
						 val2_regular = val2_regular)

		### Note: we need to get the original MIST tracks and clip out all wolf-rayet and post-MS evolution!!!
		### I suspect that's what's contaminating our sample, looking like pre-MS stars.
		### We do an age clip, but that won't stop higher-mass stellar tracks from showing up.
		#import matplotlib.pyplot as plt
		#plt.imshow(grid_out_v1,extent=(val1_regular[0],val1_regular[-1],val2_regular[0],val2_regular[-1]), aspect="auto")
		#plt.colorbar()
		#plt.show()

		self.interpolator1 =\
		self.wrap_fn(\
					 scipy.interpolate.RegularGridInterpolator((val1_regular,
												   val2_regular),
												  grid_out_v1,
												  bounds_error=False,
												  method="linear",
												  fill_value=np.NaN)\
					 )

		self.interpolator2 =\
		self.wrap_fn(\
					 scipy.interpolate.RegularGridInterpolator((val1_regular,
												   val2_regular),
												  grid_out_v2,
												  bounds_error=False,
												  method="linear",
												  fill_value=np.NaN)\
					 )

	@classmethod
	def subsample(self, points, N = None):
		"""Creates a random subsample of points of size N"""
		if N is None:
			N = len(points)
		elif N > len(points):
			N = len(points)
		sorting_list = list(range(len(points)))
		random.shuffle(sorting_list)
		return points[sorting_list[0:N]]
	@staticmethod
	def wrap_fn(fn):
		def helper_fn(*args):
			return fn(args)
		return helper_fn
	def get_guess(self, value1, value2,
				  value1_prior = lambda points: 1.,
				  value2_prior = lambda points: 1.):
		"""Gets an initial guess to populate the solver"""
		if not hasattr(value1, "__len__"):
			len_value1 = 0
		else:
			len_value1 = len(value1)
		if not hasattr(value2, "__len__"):
			len_value2 = 0
		else:
			len_value2 = len(value2)
		assert len_value1 == len_value2,\
		"Arrays or values passed not of same dimension!"

		# Find the indices corresponding to the desired input and output
		# In the interpolation.
		input_index_val1 = self.column_headers.index(self.from_tuple[0])
		input_index_val2 = self.column_headers.index(self.from_tuple[1])
		output_index_val1 = self.column_headers.index(self.to_tuple[0])
		output_index_val2 = self.column_headers.index(self.to_tuple[1])
		### Obtain a fast initial guess for each value1, value2.

		#Polymorphism support: if value1 and value2 by extension are scalars:
		if len_value1 < 1:
			chisq_wo_sigma =\
			np.square((value1 - self.sample_points.T[input_index_val1])) +\
			np.square((value2 - self.sample_points.T[input_index_val2]))
			# Obtain the log likelihood:
			with np.errstate(divide='ignore'):
				ln_likelihood =\
				np.log(value1_prior(self.sample_points))+\
				np.log(value2_prior(self.sample_points))+\
				(-.5*chisq_wo_sigma)
				# This method produces infinities, so turn these into NaN's
				ln_likelihood = np.where(np.isfinite(ln_likelihood),
										 ln_likelihood,
										 np.zeros(len(ln_likelihood)) + np.NaN)
			#Find the highest likelihood:
			guess_output_val1 =\
			self.sample_points[np.nanargmax(ln_likelihood)][output_index_val1]
			guess_output_val2 =\
			self.sample_points[np.nanargmax(ln_likelihood)][output_index_val2]
			output_vals = (guess_output_val1, guess_output_val2)
		else:
			guess_output_val1 = np.zeros(len_value1) + np.nan
			guess_output_val2 = np.zeros(len_value2) + np.nan
			for val_index in range(len_value1):
				chisq_wo_sigma =\
				np.square((value1[val_index] -\
						   self.sample_points.T[input_index_val1])) +\
				np.square((value2[val_index] -\
						   self.sample_points.T[input_index_val2]))
				# Obtain the log likelihood:
				with np.errstate(divide='ignore'):
					ln_likelihood =\
					np.log(value1_prior(self.sample_points))+\
					np.log(value2_prior(self.sample_points))+\
					(-.5*chisq_wo_sigma)
					# This method produces infinities, so turn these into NaN's
					ln_likelihood = np.where(np.isfinite(ln_likelihood),
											 ln_likelihood,
											 np.zeros(len(ln_likelihood)) +\
											 np.NaN)
				#Find the highest likelihood:
				guess_output_val1[val_index] =\
				self.sample_points[np.nanargmax(ln_likelihood)][output_index_val1]
				guess_output_val2[val_index] =\
				self.sample_points[np.nanargmax(ln_likelihood)][output_index_val2]
			output_vals = (guess_output_val1, guess_output_val2)
		return output_vals
	def _evaluate(self, value1, value2,
				  debug = True):
		"""Evalaute the 2D interpolated method"""
		### Polymorphism check.
		# First thing we check for is if a single error has been given
		# for each of the parameters:
		if not hasattr(value1, "__len__"):
			len_value1 = 0
		else:
			len_value1 = len(value1)
		if not hasattr(value2, "__len__"):
			len_value2 = 0
		else:
			len_value2 = len(value2)
		assert len_value1 == len_value2,\
		"Arrays or values passed not of same dimension!"

		# Find the indices corresponding to the desired input and output
		# In the interpolation.
		input_index_val1 = self.column_headers.index(self.from_tuple[0])
		input_index_val2 = self.column_headers.index(self.from_tuple[1])
		output_index_val1 = self.column_headers.index(self.to_tuple[0])
		output_index_val2 = self.column_headers.index(self.to_tuple[1])
		### Obtain a fast initial guess for each value1, value2.
		#initial_guess = self.get_guess(value1, value2,
		#                               value1_prior = value1_prior,
		#                               value2_prior = value2_prior)

		output_value =\
		(self.interpolator1(value1, value2),
		 self.interpolator2(value1, value2))

		return output_value
	def evaluate(self, value1, value2, *args, **kwargs):
		"""Evaluate and interpolate a given array or value.
		Modified to accept two values for 2D interpolation purposes."""
		if isinstance(value1, u.Quantity):
			# If so, set it.
			value1 = value1.to(self._default_input_unit[0]).value
		if isinstance(value2, u.Quantity):
			# If so, set it.
			value2 = value2.to(self._default_input_unit[1]).value
		### Polymorphism:
		### Check if additional arguments are given. If so, we need
		### to convert each input value systematically.
		if len(args) > 0:
			for index in range(len(args)):
				# Check to see if the values passed have a dimensional component
				if isinstance(args[index], u.Quantity):
					#Set the dimensional component for each value.
					args[index] =\
					args[index].to(self._default_input_unit[index+1]).value
			if len(kwargs.keys()) > 0:
				# Check whether kwargs is used as a key. (TODO: replace)
				if "kwargs" in kwargs.keys():
					output = self._evaluate(value1, value2, *args)
				else:
					output = self._evaluate(value1, value2, *args, **kwargs)
			else:
				output = self._evaluate(value1, value2, *args)
		else:
			if len(kwargs.keys()) > 0:
				# Check whether kwargs is used as a key. (TODO: replace)
				if "kwargs" in kwargs.keys():
					output = self._evaluate(value1, value2)
				else:
					output = self._evaluate(value1, value2, **kwargs)
			else:
				output = self._evaluate(value1, value2)
		# Convert from the default output unit to the specified output unit
		output_tuple =\
		((self.output_unit[0]/self._default_output_unit[0]).\
		in_units(u.dimensionless_unscaled)*output[0],
		(self.output_unit[1]/self._default_output_unit[1]).\
		in_units(u.dimensionless_unscaled)*output[1])
		## Another possibility, though it will not error if there is a
		## problem with units:
		#output *= (self.output_unit/self._default_output_unit).\
		#decompose().scale
		return output_tuple
	def __call__(self, value1, value2, *args, **kwargs):
		"""Allows calling the class-level object with evaluate's arguments.
		Returns the values with proper units attached.
		To avoid returning the units as well, use evaluate()."""
		evaluated_value = self.evaluate(value1, value2, *args, **kwargs)
		output_tuple =\
		(self.output_unit[0]*evaluated_value[0],
		 self.output_unit[1]*evaluated_value[1])
		return output_tuple

class MergedIsochroneInterpolatorOnMass(BaseIsochroneInterpolator):
	"""Create a merged isochrone interpolator on the mass coordinate.
	Use the points from the first interpolator, and pick isochronal points
	that are smaller."""
	@classmethod
	def load_isochrones(self, IsochroneInterpolators):
		self.IsochroneInterpolators = IsochroneInterpolators
		self.default_units = self.IsochroneInterpolators[0].default_units
		self.column_headers = self.IsochroneInterpolators[0].column_headers
	def load_data(self):
		points = self.IsochroneInterpolators[0].load_data().tolist()
		for iso_idx in range(len(self.IsochroneInterpolators) - 1):
			points_a = self.IsochroneInterpolators[iso_idx].load_data()
			masses_a = [x[1] for x in points_a]
			min_mass_a = np.min(masses_a)
			points_b = self.IsochroneInterpolators[iso_idx + 1].load_data()
			masses_b = [x[1] for x in points_b]
			cond_b = masses_b < min_mass_a
			for cond_idx, cond_val in enumerate(cond_b):
				if cond_val:
					points.append(points_b[cond_idx])
		return np.array(points)

class BaseTripleIsochroneInterpolator(object):
	"""Abstract base class for isochronal calculators."""
	def load_data(self):
		raise NotImplementedError
	def __init__(self, from_tuple, to_tuple, subsample_N = None, tol = 1e-6,
				 output_unit = (None, None), scheme = "linear",
				 resolution_factor = 2, filename = None, save = False,
				 side_grid_points_spot_alpha = 10):
		"""Initializes an isochronal interpolator on the tuples provided as
		from_tuple to to_tuple.
		IE: from: ("logT", "logL", "alpha") to ("logage", "mass")
		Available values are found in self.column_headers.

		Creates interpolants by gridding onto a regular grid of from_tuple,
		then exposing this as an interpolant.

		subsample_N is an optional parameter specifying the number of points of
		the initial guess 2d array. Leaving it as None will use the entire grid
		to guess with a simple nearest neighbor method.

		tol refers to the tolerance for gradient estimation in
		scipy.interpolate.

		output_unit is a tuple of units corresponding to the output to_tuple.

		scheme denotes the kind of interpolation to use. Acceptable values
		are: linear, cubic, and nearest.

		resolution_factor corresponds to the density multiplication in the
		number of points used for the grid.

		filename is a string that corresponds to a file to load or save as the
		parameters grid_out_v1 and grid_out_v2.

		save is a boolean --- if set to False, it will try to load the filename
		and if set to True, it will try to save grid_out_v1, grid_out_v2 to the
		filename.

		side_grid_points_spot_alpha corresponds to the number of points in
		the resolution for spot_alpha."""
		# Set the units to output in
		self.output_unit = output_unit

		### Set the default inputs and outputs according to the default unit
		### of each column datatype.
		# Use the default units for the chosen input and output columns.
		self._default_input_unit =\
		(self.default_units[from_tuple[0]], self.default_units[from_tuple[1]], self.default_units[from_tuple[2]])
		self._default_output_unit =\
		(self.default_units[to_tuple[0]], self.default_units[to_tuple[1]])

		## If the output unit is unset, then assume that the default units
		## are what's requested.

		if output_unit[0] is None:
			_output_unit_0 = self._default_output_unit[0]
		else:
			_output_unit_0 = output_unit[0]
		if output_unit[1] is None:
			_output_unit_1 = self._default_output_unit[1]
		else:
			_output_unit_1 = output_unit[1]
		self.output_unit = (_output_unit_0, _output_unit_1)

		for provided_column_name in from_tuple + to_tuple:
			assert provided_column_name in self.column_headers,\
			"Column not found. Available columns are: "+\
			repr(self.column_headers)
		self.points = self.load_data()
		# Create a sub-sampled population that provides the initial guess
		# efficiently, for scipy.optimize.fmin to use prior to
		# performing the full minimization.
		self.sample_points = self.subsample(self.points, N = subsample_N)
		# Save the tuples that correspond to from and to.
		self.from_tuple = from_tuple
		self.to_tuple = to_tuple

		# Find the indices corresponding to the desired input and output
		# In the interpolation.
		input_index_val1 = self.column_headers.index(self.from_tuple[0])
		input_index_val2 = self.column_headers.index(self.from_tuple[1])
		input_index_val3 = self.column_headers.index(self.from_tuple[2])
		output_index_val1 = self.column_headers.index(self.to_tuple[0])
		output_index_val2 = self.column_headers.index(self.to_tuple[1])

		if filename is not None and not save:
			### Load data:
			with np.load(filename) as data:
				grid_out_v1 = data["grid_out_v1"]
				grid_out_v2 = data["grid_out_v2"]
				val1_regular = data["val1_regular"]
				val2_regular = data["val2_regular"]
				val3_regular = data["val3_regular"]
		else:

			###Create the interpolants:
			if scheme == "linear":
				interpolator1 =\
				scipy.interpolate.\
				LinearNDInterpolator(list(zip(self.points.T[input_index_val1],
											  self.points.T[input_index_val2],
											  self.points.T[input_index_val3])),
									 self.points.T[output_index_val1])
				interpolator2 =\
				scipy.interpolate.\
				LinearNDInterpolator(list(zip(self.points.T[input_index_val1],
											  self.points.T[input_index_val2],
											  self.points.T[input_index_val3])),
									 self.points.T[output_index_val2])
			if scheme == "nearest":
				interpolator1 =\
				scipy.interpolate.\
				NearestNDInterpolator(list(zip(self.points.T[input_index_val1],
											   self.points.T[input_index_val2],
											   self.points.T[input_index_val3])),
									  self.points.T[output_index_val1])
				interpolator2 =\
				scipy.interpolate.\
				NearestNDInterpolator(list(zip(self.points.T[input_index_val1],
											   self.points.T[input_index_val2],
											   self.points.T[input_index_val3])),
									  self.points.T[output_index_val2])

			### Grid onto a regular plane of from_tuple.
			### The number of grid points
			min_val1 = np.nanmin(self.points.T[input_index_val1])
			max_val1 = np.nanmax(self.points.T[input_index_val1])
			min_val2 = np.nanmin(self.points.T[input_index_val2])
			max_val2 = np.nanmax(self.points.T[input_index_val2])
			min_val3 = np.nanmin(self.points.T[input_index_val3])
			max_val3 = np.nanmax(self.points.T[input_index_val3])
			num_points = len(self.points)
			side_grid_points = np.ceil(np.sqrt(resolution_factor * num_points))

			val1_regular = np.linspace(min_val1, max_val1, side_grid_points)
			val2_regular = np.linspace(min_val2, max_val2, side_grid_points)
			### If the parameter used is mass, make a special exception
			### and grid it in log mass steps. Otherwise, we don't cover
			### the low-mass regime.
			if self.from_tuple[0] == "mass":
				val1_regular = np.logspace(np.log10(min_val1),
										   np.log10(max_val1),
										   side_grid_points)
			if self.from_tuple[1] == "mass":
				val2_regular = np.logspace(np.log10(min_val2),
										   np.log10(max_val2),
										   side_grid_points)
			val3_regular = np.linspace(min_val3, max_val3, side_grid_points_spot_alpha)

			v1,v2,v3 = np.meshgrid(val1_regular, val2_regular, val3_regular,
								   indexing = "ij")
			v1 = v1.flatten()
			v2 = v2.flatten()
			v3 = v3.flatten()

			# Will run into a MemoryError if the entire grid is passed in
			# at once, so we need to iterate over val1:
			#val1s = val1_regular[:,None]
			#val2s = val2_regular[None,:]
			param_array_val1 = np.zeros(len(val1_regular))
			param_array_val3 = np.zeros(len(val1_regular))
			grid_out_v1 = []
			grid_out_v2 = []

			self.A = interpolator1
			self.B = interpolator2

			grid_out_v1 = interpolator1(v1,v2,v3).reshape((len(val1_regular),len(val2_regular),len(val3_regular)))
			grid_out_v2 = interpolator2(v1,v2,v3).reshape((len(val1_regular),len(val2_regular),len(val3_regular)))

			if filename is not None:
				np.savez(filename,
						 grid_out_v1 = grid_out_v1,
						 grid_out_v2 = grid_out_v2,
						 val1_regular = val1_regular,
						 val2_regular = val2_regular,
						 val3_regular = val3_regular)

		self.interpolator1 =\
		self.wrap_fn(\
					 scipy.interpolate.RegularGridInterpolator((val1_regular,val2_regular,val3_regular),
												  grid_out_v1,
												  bounds_error=False,
												  method="linear",
												  fill_value=np.NaN)\
					 )

		self.interpolator2 =\
		self.wrap_fn(\
					 scipy.interpolate.RegularGridInterpolator((val1_regular,val2_regular,val3_regular),
												  grid_out_v2,
												  bounds_error=False,
												  method="linear",
												  fill_value=np.NaN)\
					 )

	@classmethod
	def subsample(self, points, N = None):
		"""Creates a random subsample of points of size N"""
		if N is None:
			N = len(points)
		elif N > len(points):
			N = len(points)
		sorting_list = list(range(len(points)))
		random.shuffle(sorting_list)
		return points[sorting_list[0:N]]
	@staticmethod
	def wrap_fn(fn):
		def helper_fn(*args):
			return fn(args)
		return helper_fn
	def get_guess(self, value1, value2, value3,
				  value1_prior = lambda points: 1.,
				  value2_prior = lambda points: 1.,
				  value3_prior = lambda points: 1.):
		"""Gets an initial guess to populate the solver"""
		if not hasattr(value1, "__len__"):
			len_value1 = 0
		else:
			len_value1 = len(value1)
		if not hasattr(value2, "__len__"):
			len_value2 = 0
		else:
			len_value2 = len(value2)
		if not hasattr(value3, "__len__"):
			len_value3 = 0
		else:
			len_value3 = len(value3)
		assert len_value1 == len_value2 == len_value3,\
		"Arrays or values passed not of same dimension!"

		# Find the indices corresponding to the desired input and output
		# In the interpolation.
		input_index_val1 = self.column_headers.index(self.from_tuple[0])
		input_index_val2 = self.column_headers.index(self.from_tuple[1])
		input_index_val3 = self.column_headers.index(self.from_tuple[2])
		output_index_val1 = self.column_headers.index(self.to_tuple[0])
		output_index_val2 = self.column_headers.index(self.to_tuple[1])
		### Obtain a fast initial guess for each value1, value2.

		#Polymorphism support: if value1 and value2 by extension are scalars:
		if len_value1 < 1:
			chisq_wo_sigma =\
			np.square((value1 - self.sample_points.T[input_index_val1])) +\
			np.square((value2 - self.sample_points.T[input_index_val2])) +\
			np.square((value3 - self.sample_points.T[input_index_val3]))
			# Obtain the log likelihood:
			with np.errstate(divide='ignore'):
				ln_likelihood =\
				np.log(value1_prior(self.sample_points))+\
				np.log(value2_prior(self.sample_points))+\
				np.log(value3_prior(self.sample_points))+\
				(-.5*chisq_wo_sigma)
				# This method produces infinities, so turn these into NaN's
				ln_likelihood = np.where(np.isfinite(ln_likelihood),
										 ln_likelihood,
										 np.zeros(len(ln_likelihood)) + np.NaN)
			#Find the highest likelihood:
			guess_output_val1 =\
			self.sample_points[np.nanargmax(ln_likelihood)][output_index_val1]
			guess_output_val2 =\
			self.sample_points[np.nanargmax(ln_likelihood)][output_index_val2]
			output_vals = (guess_output_val1, guess_output_val2)
		else:
			guess_output_val1 = np.zeros(len_value1) + np.nan
			guess_output_val2 = np.zeros(len_value2) + np.nan
			for val_index in range(len_value1):
				chisq_wo_sigma =\
				np.square((value1[val_index] -\
						   self.sample_points.T[input_index_val1])) +\
				np.square((value2[val_index] -\
						   self.sample_points.T[input_index_val2])) +\
				np.square((value3[val_index] -\
						   self.sample_points.T[input_index_val3]))
				# Obtain the log likelihood:
				with np.errstate(divide='ignore'):
					ln_likelihood =\
					np.log(value1_prior(self.sample_points))+\
					np.log(value2_prior(self.sample_points))+\
					np.log(value3_prior(self.sample_points))+\
					(-.5*chisq_wo_sigma)
					# This method produces infinities, so turn these into NaN's
					ln_likelihood = np.where(np.isfinite(ln_likelihood),
											 ln_likelihood,
											 np.zeros(len(ln_likelihood)) +\
											 np.NaN)
				#Find the highest likelihood:
				guess_output_val1[val_index] =\
				self.sample_points[np.nanargmax(ln_likelihood)][output_index_val1]
				guess_output_val2[val_index] =\
				self.sample_points[np.nanargmax(ln_likelihood)][output_index_val2]
			output_vals = (guess_output_val1, guess_output_val2)
		return output_vals
	def _evaluate(self, value1, value2, value3,
				  debug = True):
		"""Evalaute the 2D interpolated method"""
		### Polymorphism check.
		# First thing we check for is if a single error has been given
		# for each of the parameters:
		if not hasattr(value1, "__len__"):
			len_value1 = 0
		else:
			len_value1 = len(value1)
		if not hasattr(value2, "__len__"):
			len_value2 = 0
		else:
			len_value2 = len(value2)
		if not hasattr(value3, "__len__"):
			len_value3 = 0
		else:
			len_value3 = len(value3)
		assert len_value1 == len_value2 == len_value3,\
		"Arrays or values passed not of same dimension!"

		# Find the indices corresponding to the desired input and output
		# In the interpolation.
		input_index_val1 = self.column_headers.index(self.from_tuple[0])
		input_index_val2 = self.column_headers.index(self.from_tuple[1])
		input_index_val3 = self.column_headers.index(self.from_tuple[2])
		output_index_val1 = self.column_headers.index(self.to_tuple[0])
		output_index_val2 = self.column_headers.index(self.to_tuple[1])
		### Obtain a fast initial guess for each value1, value2.
		#initial_guess = self.get_guess(value1, value2,
		#                               value1_prior = value1_prior,
		#                               value2_prior = value2_prior)

		output_value =\
		(self.interpolator1(value1, value2, value3),
		 self.interpolator2(value1, value2, value3))

		return output_value
	def evaluate(self, value1, value2, value3, *args, **kwargs):
		"""Evaluate and interpolate a given array or value.
		Modified to accept two values for 2D interpolation purposes."""
		if isinstance(value1, u.Quantity):
			# If so, set it.
			value1 = value1.to(self._default_input_unit[0]).value
		if isinstance(value2, u.Quantity):
			# If so, set it.
			value2 = value2.to(self._default_input_unit[1]).value
		if isinstance(value3, u.Quantity):
			# If so, set it.
			value3 = value3.to(self._default_input_unit[2]).value
		### Polymorphism:
		### Check if additional arguments are given. If so, we need
		### to convert each input value systematically.
		if len(args) > 0:
			for index in range(len(args)):
				# Check to see if the values passed have a dimensional component
				if isinstance(args[index], u.Quantity):
					#Set the dimensional component for each value.
					args[index] =\
					args[index].to(self._default_input_unit[index+1]).value
			if len(kwargs.keys()) > 0:
				# Check whether kwargs is used as a key. (TODO: replace)
				if "kwargs" in kwargs.keys():
					output = self._evaluate(value1, value2, value3, *args)
				else:
					output = self._evaluate(value1, value2, value3, *args, **kwargs)
			else:
				output = self._evaluate(value1, value2, value3, *args)
		else:
			if len(kwargs.keys()) > 0:
				# Check whether kwargs is used as a key. (TODO: replace)
				if "kwargs" in kwargs.keys():
					output = self._evaluate(value1, value2, value3)
				else:
					output = self._evaluate(value1, value2, value3, **kwargs)
			else:
				output = self._evaluate(value1, value2, value3)
		# Convert from the default output unit to the specified output unit
		output_tuple =\
		((self.output_unit[0]/self._default_output_unit[0]).\
		in_units(u.dimensionless_unscaled)*output[0],
		(self.output_unit[1]/self._default_output_unit[1]).\
		in_units(u.dimensionless_unscaled)*output[1])
		## Another possibility, though it will not error if there is a
		## problem with units:
		#output *= (self.output_unit/self._default_output_unit).\
		#decompose().scale
		return output_tuple
	def __call__(self, value1, value2, value3, *args, **kwargs):
		"""Allows calling the class-level object with evaluate's arguments.
		Returns the values with proper units attached.
		To avoid returning the units as well, use evaluate()."""
		evaluated_value = self.evaluate(value1, value2, value3, *args, **kwargs)
		output_tuple =\
		(self.output_unit[0]*evaluated_value[0],
		 self.output_unit[1]*evaluated_value[1])
		return output_tuple

class SpottedInterpolator(SpTBaseInterpolator):
	"""Creates a spotted interpolator with a filling factor (f) and spot
	temperature contrast (x) from any color interpolator."""
	def __init__(self, ColorInterpolator, PhotometricSystem, **kwargs):
		"""Initialize SpottedInterpolator by grabbing available_colors,
		and other child values."""
		self.ColorInterpolator = ColorInterpolator
		if hasattr(ColorInterpolator, 'interpolators'):
			self.interpolators = ColorInterpolator.interpolators
		else:
			self.interpolators = [ColorInterpolator]
		self.PhotometricSystem = PhotometricSystem
		# self.column_headers = ColorInterpolator.column_headers
		if hasattr(ColorInterpolator, "available_colors"):
			self.available_colors = ColorInterpolator.available_colors
		else:
			# if the ColorInterpolator has available_input_colors
			# and available_output_colors, we assume that the usable colors
			# are in available_input_colors.
			if hasattr(ColorInterpolator, "available_input_colors") and\
			hasattr(ColorInterpolator, "available_output_colors"):
				self.available_colors =\
				ColorInterpolator.available_input_colors
			else:
				self.available_colors = ColorInterpolator.column_headers
		self.default_units = ColorInterpolator.default_units
		self.kwargs = kwargs
	def find_shortest_path(self, graph, start, end, path=[]):
		"""Code from https://www.python.org/doc/essays/graphs/
		Returns the shortest path between nodes in a supplied graph.
		3/19/2018"""
		path = path + [start]
		if start == end:
			return path
		if not start in graph:
			return None
		shortest = None
		#print "start", start, "end", end, "path", path + [end]
		if end in graph[start]:
			return path + [end]
		for node in graph[start]:
			if node not in path:
				newpath = self.find_shortest_path(graph, node, end, path)
				if newpath:
					if not shortest or len(newpath) < len(shortest):
						shortest = newpath
		return shortest
	@staticmethod
	def color_list_to_graphs(color_list):
		"""Generates a dictionary of graphs from a list of colors.
		Returns a bidirectional graph under the key "bidirectional",
		A forward-traversing graph under the key "forward",
		A backward-traversing graph under the key "backward".

		Accepts a list of color tuples like ("B", "V").
		Assumes tuple is a directed edge from the first color to
		the second color."""
		#Create the graph dictionaries:
		bidirectional_graph = {}
		forward_facing_graph = {}
		backward_facing_graph = {}
		#Traverse forward from each band to the final band.
		for initial_band, final_band in color_list:
			if not initial_band in forward_facing_graph:
				bidirectional_graph[initial_band] = [final_band]
				forward_facing_graph[initial_band] = [final_band]
			else:
				bidirectional_graph[initial_band].append(final_band)
				forward_facing_graph[initial_band].append(final_band)
		#Create a graph, going in reverse.
		for final_band, initial_band in color_list:
			if not initial_band in backward_facing_graph:
				backward_facing_graph[initial_band] = [final_band]
			else:
				backward_facing_graph[initial_band].append(final_band)
			if not initial_band in bidirectional_graph:
				bidirectional_graph[initial_band] = [final_band]
			else:
				bidirectional_graph[initial_band].append(final_band)
		#Return a dictionary of color lists.
		return {"forward":forward_facing_graph,
				"backward":backward_facing_graph,
				"bidirectional":bidirectional_graph}
	def set_f_x(self, f, x, **kwargs):
		temp_spotted_interp =\
		SpottedInterpolator(self.ColorInterpolator, self.PhotometricSystem, **self.kwargs)
		temp_spotted_interp.f = f
		temp_spotted_interp.x = x
		return temp_spotted_interp
	def on(self, input, output, interpolation_kind = "linear",
				 output_unit = None,
				 f = None, x = None, **kwargs):
		temp_spotted_interp =\
		SpottedInterpolator(self.ColorInterpolator, self.PhotometricSystem, **self.kwargs)
		temp_spotted_interp.f = self.f
		temp_spotted_interp.x = self.x
		temp_spotted_interp._on(input, output,
								interpolation_kind = interpolation_kind,
								output_unit = output_unit,
								f = f, x = x, **kwargs)
		return temp_spotted_interp
	def _on(self, input, output, interpolation_kind = "linear",
				 output_unit = None,
				 f = None, x = None, **kwargs):
		"""Spotted intrinsic color and teff tables! f denotes spot covering
		fraction and x is the fraction of the spot temperature to the ambient
		temperature."""
		from sippy.calculator.ColorCalculator import ColorMergeInterpolator
		kwargs.update(self.kwargs)
		condition_color = len(output.split("-")) > 1
		#assert input == "SpT" and\
		#(condition_color or output == "Teff"), \
		#"Supports only SpT -> Color and SpT -> Teff queries for now."

		## Save f, x:
		if f is not None:
			self.f = f
		if x is not None:
			self.x = x

		### If it's a SpT -> Teff or SpT -> Color query, need SpT -> Teff
		### in order to calculate 2Teff parameters.
		if hasattr(self.ColorInterpolator, "on"):
			self.spt_teff =\
			self.ColorInterpolator.on(input, "Teff", interpolation_kind =\
			interpolation_kind,
			output_unit = None, **kwargs)
			self.teff_bcv =\
			self.ColorInterpolator.on("Teff", "BCV", interpolation_kind =\
			interpolation_kind,
			output_unit = None, **kwargs)

			self.spt_output =\
			self.ColorInterpolator.on("SpT", output, interpolation_kind =\
			interpolation_kind,
			output_unit = None, **kwargs)
		else:
			self.spt_teff =\
			self.ColorInterpolator(input, "Teff", interpolation_kind =\
								   interpolation_kind,
								   output_unit = None, **kwargs)
			self.teff_bcv =\
			self.ColorInterpolator("Teff", "BCV", interpolation_kind =\
			interpolation_kind,
			output_unit = None, **kwargs)

			self.spt_output =\
			self.ColorInterpolator("SpT", output, interpolation_kind =\
			interpolation_kind,
			output_unit = None, **kwargs)

		### In order to inform the _evaluate function, we need to make a note
		### of the output value.

		self.output = output

		### Check if color:
		condition_color = len(self.output.split("-")) > 1
		if condition_color:
			mag1 = self.output.split("-")[0]
			mag2 = self.output.split("-")[1]
			###
			self.V_min_mag1_color =\
			ColorMergeInterpolator("V-"+mag1, ColorInterpolators = self.ColorInterpolator, input_val = "Teff", **kwargs)
			self.V_min_mag2_color =\
			ColorMergeInterpolator("V-"+mag2, ColorInterpolators = self.ColorInterpolator, input_val = "Teff", **kwargs)

		##### Basic housekeeping:

		# Set the units to output in
		self.output_unit = output_unit
		## Grab the column headers
		#column_headers = self.column_headers
		## Throw an error if input and output are not valid choices.
		#assert input in column_headers and output in column_headers,\
		#"Unsupported column headers chosen for input and output."+\
		#"Valid options: "+repr(column_headers)
		### Set the default inputs and outputs according to the default unit
		### of each column datatype.
		# Use the default units for the chosen input and output columns.
		self._default_input_unit = self.default_units[input]
		self._default_output_unit = self.default_units[output]

		## If the output unit is unset, then assume that the default units
		## are what's requested.

		if output_unit is None:
			self.output_unit = self._default_output_unit

		return self

	def _evaluate(self, values, *args, **kwargs):
		condition_color = len(self.output.split("-")) > 1
		teff = self.spt_teff.evaluate(values)
		if self.output == "Teff":
			#teff =\
			#np.power((1. - self.f)*np.power(teff,4)+\
			#         self.f*np.power(self.x*teff,4),0.25)
			### Teff is given!
			return teff
		elif len(self.output.split("-")) > 1: #color

			mag1 = self.output.split("-")[0]
			mag2 = self.output.split("-")[1]
			### First, calculate the ambient and spotted temperatures and colors
			T_amb = teff/((1. - self.f + self.f*self.x**4.)**0.25)
			T_spot = self.x * T_amb
			### Calculate the V_min_mag quantities:
			V_min_mag1_amb = self.V_min_mag1_color._evaluate(T_amb)
			V_min_mag1_spot = self.V_min_mag1_color._evaluate(T_spot)
			V_min_mag2_amb = self.V_min_mag2_color._evaluate(T_amb)
			V_min_mag2_spot = self.V_min_mag2_color._evaluate(T_spot)

			### Calculate colors!
			color_val =\
			-2.5*np.log10(\
			(1.-self.f)*np.power(T_amb/T_spot, 4.)*np.power(10., (self.teff_bcv.evaluate(T_amb) + V_min_mag1_amb)/2.5)+\
			self.f*np.power(1., 4.)*np.power(10., (self.teff_bcv.evaluate(T_spot) + V_min_mag1_spot)/2.5)\
			)\
			+2.5*np.log10(\
			(1.-self.f)*np.power(T_amb/T_spot, 4.)*np.power(10., (self.teff_bcv.evaluate(T_amb) + V_min_mag2_amb)/2.5)+\
			self.f*np.power(1., 4.)*np.power(10., (self.teff_bcv.evaluate(T_spot) + V_min_mag2_spot)/2.5)\
			)
			return color_val
		else:
			return self.spt_output.evaluate(values)
		# elif self.output == "BCV": #BCV = Mbol - V.
