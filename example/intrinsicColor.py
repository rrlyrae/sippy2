from __future__ import absolute_import
import matplotlib.pyplot as plt
import numpy as np
from astropy import units as u
from sippy.interpolator.BaseInterpolator import *
from sippy.interpolator.PecautMamajekInterpolator import *

def plot_SpT_Teff_PM13(interpolation_kind=u"linear"):
    u"""An example usage of a fallback SpT-Teff interpolation."""
    PecautMamajekDwarf =\
    PecautMamajekDwarfInterpolator(u"SpT", u"Teff",
                                   interpolation_kind=interpolation_kind)
    PecautMamajekYoung =\
    PecautMamajekYoungInterpolator(u"SpT", u"Teff",
                                   interpolation_kind=interpolation_kind)
    PecautMamajekExtendedDwarf =\
    PecautMamajekExtendedDwarfInterpolator(u"SpT", u"Teff",
                                           interpolation_kind=\
                                           interpolation_kind)
    PM_Young_Extended =\
    MergedFallbackInstantiatedInterpolator([PecautMamajekYoung,
                                            PecautMamajekExtendedDwarf])
    xs = np.linspace(0.,90.,500)

    # Three subplots sharing both x/y axes
    f, (ax1, ax2) = plt.subplots(2, sharex=True, sharey=True)
    ax1.plot(xs, PecautMamajekDwarf.evaluate(xs),
             label=u"Dwarf",
             lw = 2, color = u"red")
    ax1.plot(xs, PecautMamajekExtendedDwarf.evaluate(xs),
             label=u"ExtendedDwarf",
             lw = 2, color = u"blue", linestyle=u"dotted")
    ax1.plot(xs, PecautMamajekYoung.evaluate(xs),
             label=u"$5-30 \\, [Myr]$",
             lw = 2, color = u"orange")
    ax2.plot(xs, PM_Young_Extended.evaluate(xs),
             label=u"$5-30 \\, [Myr]$ + ExtendedDwarf",
             lw = 2, color = u"black")
    ax1.set_title(u'Merged Color Table Interpolator Demo')
    # Fine-tune figure; make subplots close to each other and hide x ticks for
    # all but bottom plot.
    f.subplots_adjust(hspace=0)
    plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)

    ax2.set_xlabel(u"Numerical SpT")
    for ax in [ax1, ax2]:
        ax.set_ylabel(u"Effective Temperature $[K]$")
        ax.semilogy()
        ax.legend()
    plt.show

def plot_TwoTeff_PM13_multiple_colors(colors=\
["B-V","V-Ic","V-Ks","J-H","H-Ks"],
f=0.5,delT=200, interpolation_kind=u"linear"):
    u"""An example usage of a two-Teff fallback Teff-Color interpolation.

    Defining 'f' as the starspot fraction, and 'delT' as the absolute
    difference in temperature between high and low in K."""
    plot_colors = ["red", "blue", "turquoise", "orange", "purple", "black"]
    plot_styles = ["solid", "dotted", "dashed"]
    plot_widths = [1,2,3]

    for color_idx, color in enumerate(colors):

        PecautMamajekYoung =\
        PecautMamajekYoungInterpolator(u"Teff", color,
                                       interpolation_kind=interpolation_kind)
        PecautMamajekExtendedDwarf =\
        PecautMamajekExtendedDwarfInterpolator(u"Teff", color,
                                               interpolation_kind=\
                                               interpolation_kind)
        PM_Young_Extended =\
        MergedFallbackInstantiatedInterpolator([PecautMamajekYoung,
                                                PecautMamajekExtendedDwarf])
        Teffs = np.logspace(2.7,4.7,500)

        plt.plot(Teffs, PM_Young_Extended.evaluate(Teffs),
                 label=color,
                 lw = plot_widths[int(color_idx/len(plot_colors))],
                 color = plot_colors[color_idx % len(plot_colors)])

        #New effective temperature is a weighted value:
        Avg_TwoTeffs = (1.-f)*Teffs + f*(Teffs + delT)

        color_val =\
        -2.5*np.log10(\
        np.power(10., -PM_Young_Extended.evaluate(Teffs)/2.5)*(1.-f)+\
        np.power(10., -PM_Young_Extended.evaluate(Teffs + delT)/2.5)*f\
        )

        plt.plot(Avg_TwoTeffs, color_val,
                 lw = plot_widths[int(color_idx/len(plot_colors))],
                 color = plot_colors[color_idx % len(plot_colors)],
                 ls = plot_styles[1])
    plt.title("Two-Teff Plots $f=%.1f$, $\\Delta T = %.1e \\, [K]$" % (f, delT))
    plt.plot([],[],label="2Teff"\
             , ls = plot_styles[1], color = "black")

    plt.xlabel(u"Effective Temperature, or averaged Effective Temperature ($T_{eff}$)")
    plt.ylabel(u"Color Value")
    plt.semilogx()
    plt.xlim(np.min([np.min(Teffs), np.min(Avg_TwoTeffs)]),
             np.max([np.max(Teffs), np.max(Avg_TwoTeffs)]))
    plt.legend()
    plt.gca().minorticks_on()
    plt.gca().set_axisbelow(True)
    plt.grid(b=True, which="major", color="lightgray", linestyle="-", lw= 1.2)
    plt.grid(b=True, which="minor", color="lightgray", linestyle="-", lw = 0.5)
    plt.show()
    plt.clf()
    ### Create difference plot

    for color_idx, color in enumerate(colors):

        PecautMamajekYoung =\
        PecautMamajekYoungInterpolator(u"Teff", color,
                                       interpolation_kind=interpolation_kind)
        PecautMamajekExtendedDwarf =\
        PecautMamajekExtendedDwarfInterpolator(u"Teff", color,
                                               interpolation_kind=\
                                               interpolation_kind)
        PM_Young_Extended =\
        MergedFallbackInstantiatedInterpolator([PecautMamajekYoung,
                                                PecautMamajekExtendedDwarf])
        Teffs = np.logspace(2.7,4.7,500)

        #New effective temperature is a weighted value:
        Avg_TwoTeffs = (1.-f)*Teffs + f*(Teffs + delT)

        color_val =\
        -2.5*np.log10(\
        np.power(10., -PM_Young_Extended.evaluate(Teffs)/2.5)*(1.-f)+\
        np.power(10., -PM_Young_Extended.evaluate(Teffs + delT)/2.5)*f\
        )

        plt.plot(Avg_TwoTeffs, color_val - PM_Young_Extended.evaluate(Teffs),
                 label=color,
                 lw = plot_widths[int(color_idx/len(plot_colors))],
                 color = plot_colors[color_idx % len(plot_colors)],
                 ls = plot_styles[0])
    plt.title("Two-Teff Plots $f=%.1f$, $\\Delta T = %.1e \\, [K]$" % (f, delT))

    plt.xlabel(u"Effective Temperature, or averaged Effective Temperature ($T_{eff}$)")
    plt.ylabel(u"Difference in Color Value between 2Teff and 1Teff")
    plt.semilogx()
    plt.xlim(np.min([np.min(Teffs), np.min(Avg_TwoTeffs)]),
             np.max([np.max(Teffs), np.max(Avg_TwoTeffs)]))
    plt.legend()
    plt.gca().minorticks_on()
    plt.gca().set_axisbelow(True)
    plt.grid(b=True, which="major", color="lightgray", linestyle="-", lw= 1.2)
    plt.grid(b=True, which="minor", color="lightgray", linestyle="-", lw = 0.5)
    plt.show()
    plt.clf()

def plot_TwoTeff_PM13_multiple_fs(color= "H-Ks",
fs=np.linspace(0.,1.,6,endpoint=True),delT=200, interpolation_kind=u"linear"):
    u"""An example usage of a two-Teff fallback Teff-Color interpolation.

    Defining 'f' as the starspot fraction, and 'delT' as the absolute
    difference in temperature between high and low in K."""
    plot_colors = ["red", "blue", "turquoise", "orange", "purple", "black"]
    plot_styles = ["solid", "dotted", "dashed"]
    plot_widths = [1,2,3]

    for color_idx, f in enumerate(fs):

        PecautMamajekYoung =\
        PecautMamajekYoungInterpolator(u"Teff", color,
                                       interpolation_kind=interpolation_kind)
        PecautMamajekExtendedDwarf =\
        PecautMamajekExtendedDwarfInterpolator(u"Teff", color,
                                               interpolation_kind=\
                                               interpolation_kind)
        PM_Young_Extended =\
        MergedFallbackInstantiatedInterpolator([PecautMamajekYoung,
                                                PecautMamajekExtendedDwarf])
        Teffs = np.logspace(2.7,4.7,500)

        Avg_TwoTeffs = (1.-f)*Teffs + f*(Teffs + delT)

        color_val =\
        -2.5*np.log10(\
        np.power(10., -PM_Young_Extended.evaluate(Teffs)/2.5)*(1.-f)+\
        np.power(10., -PM_Young_Extended.evaluate(Teffs + delT)/2.5)*f\
        )

        plt.plot(Avg_TwoTeffs, color_val -\
                 PM_Young_Extended.evaluate(Teffs),
                 lw = plot_widths[int(color_idx/len(plot_colors))],
                 color = str(1.-f),
                 ls = plot_styles[0], label="$f=%s$" % f)
    plt.title("Two-Teff Plots %s, $\\Delta T = %.1e \\, [K]$" % (color, delT))
    ### make a bold version of f = 0.5
    f = 0.5
    Avg_TwoTeffs = (1.-f)*Teffs + f*(Teffs + delT)

    color_val =\
    -2.5*np.log10(\
    np.power(10., -PM_Young_Extended.evaluate(Teffs)/2.5)*(1.-f)+\
    np.power(10., -PM_Young_Extended.evaluate(Teffs + delT)/2.5)*f\
    )
    plt.plot(Avg_TwoTeffs, color_val -\
             PM_Young_Extended.evaluate(Teffs),
             lw = 2,
             color = str(1.-f),
             ls = plot_styles[0], label="$f=%s$" % f)
    ###

    plt.xlabel(u"Effective Temperature, or averaged Effective Temperature\
($T_{eff}$)")
    plt.ylabel(u"Difference in Color Value between 2Teff and 1Teff")
    plt.semilogx()
    plt.xlim(np.min([np.min(Teffs), np.min(Avg_TwoTeffs)]),
             np.max([np.max(Teffs), np.max(Avg_TwoTeffs)]))
    plt.legend()
    plt.minorticks_on()
    plt.gca().set_axisbelow(True)
    plt.grid(b=True, which="major", color="lightgray", linestyle="-", lw= 1.2)
    plt.grid(b=True, which="minor", color="lightgray", linestyle="-", lw = 0.5)
    plt.show()

def plot_TwoTeff_PM13_multiple_delTs(color= "H-Ks",
f=0.5,delTs=np.linspace(0.,400.,6), interpolation_kind=u"linear"):
    u"""An example usage of a two-Teff fallback Teff-Color interpolation.

    Defining 'f' as the starspot fraction, and 'delT' as the absolute
    difference in temperature between high and low in K."""
    plot_colors = ["red", "blue", "turquoise", "orange", "purple", "black"]
    plot_styles = ["solid", "dotted", "dashed"]
    plot_widths = [1,2,3]

    for color_idx, delT in enumerate(delTs):

        PecautMamajekYoung =\
        PecautMamajekYoungInterpolator(u"Teff", color,
                                       interpolation_kind=interpolation_kind)
        PecautMamajekExtendedDwarf =\
        PecautMamajekExtendedDwarfInterpolator(u"Teff", color,
                                               interpolation_kind=\
                                               interpolation_kind)
        PM_Young_Extended =\
        MergedFallbackInstantiatedInterpolator([PecautMamajekYoung,
                                                PecautMamajekExtendedDwarf])
        Teffs = np.logspace(2.7,4.7,500)

        Avg_TwoTeffs = (1.-f)*Teffs + f*(Teffs + delT)

        color_val =\
        -2.5*np.log10(\
        np.power(10., -PM_Young_Extended.evaluate(Teffs)/2.5)*(1.-f)+\
        np.power(10., -PM_Young_Extended.evaluate(Teffs + delT)/2.5)*f\
        )

        plt.plot(Avg_TwoTeffs, color_val -\
                 PM_Young_Extended.evaluate(Teffs),
                 lw = plot_widths[int(color_idx/len(plot_colors))],
                 color = str(1.-(delT)/np.max(delTs)),
                 ls = plot_styles[0], label="$\\Delta T = %.2e$" % delT)
    plt.title("Two-Teff Plots %s, $f = %.1f$" % (color, f))
    ### make a bold version of delT = 200
    delT = 200
    Avg_TwoTeffs = (1.-f)*Teffs + f*(Teffs + delT)

    color_val =\
    -2.5*np.log10(\
    np.power(10., -PM_Young_Extended.evaluate(Teffs)/2.5)*(1.-f)+\
    np.power(10., -PM_Young_Extended.evaluate(Teffs + delT)/2.5)*f\
    )
    plt.plot(Avg_TwoTeffs, color_val -\
             PM_Young_Extended.evaluate(Teffs),
             lw = 2,
             color = str(1.-(delT)/np.max(delTs)),
             ls = plot_styles[0], label="$\\Delta T = %.2e$" % delT)
    ###

    plt.xlabel(u"Effective Temperature, or averaged Effective Temperature\
($T_{eff}$)")
    plt.ylabel(u"Difference in Color Value between 2Teff and 1Teff")
    plt.semilogx()
    plt.xlim(np.min([np.min(Teffs), np.min(Avg_TwoTeffs)]),
             np.max([np.max(Teffs), np.max(Avg_TwoTeffs)]))
    plt.legend()
    plt.minorticks_on()
    plt.gca().set_axisbelow(True)
    plt.grid(b=True, which="major", color="lightgray", linestyle="-", lw= 1.2)
    plt.grid(b=True, which="minor", color="lightgray", linestyle="-", lw = 0.5)
    plt.show()
