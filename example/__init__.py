from __future__ import absolute_import
from sippy.example import extinction
from sippy.example import intrinsicColor
from sippy.example import taurus
from sippy.example import merging
from sippy.example import age
from sippy.example import twoteff
