from __future__ import absolute_import
import matplotlib.pyplot as plt
import numpy as np
from astropy import units as u
from sippy.interpolator.BaseInterpolator import *
from sippy.interpolator.MathisInterpolator import *
from sippy.interpolator.WhitneyInterpolator import *
from sippy.interpolator.PecautMamajekInterpolator import *
from sippy.interpolator.BTSettlInterpolator import *
from sippy.calculator.BaseCalculator import *
from sippy.calculator.ColorCalculator import *
import itertools as it

### Example calculations of parameters on real data.

def plot_Av_taurus_PM13(interpolation_kind = u"linear"):
    filename = os.path.dirname(os.path.abspath(__file__))\
             + u"/Data/" + u"taurus.dat"
    taurus = np.genfromtxt(filename, skip_header=1,
                           usecols=(3,7,8,9,10,11,12,1), dtype=None)
    SpT_taurus = [x[0].decode(u"utf-8") for x in taurus]
    photometry = {u"V": [x[1] for x in taurus],
                  u"Rc": [x[2] for x in taurus],
                  u"Ic": [x[3] for x in taurus],
                  u"J":[x[4]+x[5]+x[6] for x in taurus],
                  u"H": [x[5]+x[6] for x in taurus],
                  u"Ks": [x[6] for x in taurus]}
    star_names = [x[7] for x in taurus]

    #Instantiate a photometric system.
    ps = PhotometricSystem()
    color_combinations =\
    list(it.combinations(sorted(photometry.keys(),key=ps), 2))
    #print (sorted(k, key=ps))

    # Create Whitney-Mathis extinction law interpolator:
    Whitney = WhitneyInterpolator(interpolation_kind=interpolation_kind)
    Mathis = MathisInterpolator(interpolation_kind=interpolation_kind)
    WhitneyMathis = MergedFallbackInstantiatedInterpolator([Whitney, Mathis])

    # Create Pecaut & Mamajek interpolators from the calculator:
    C_Av = {}

    # Iterate over all the ordered colors:
    for ordered_color in color_combinations:
        C_Av[u"-".join(ordered_color)] =\
        ColorMergeAvCalculator(u"-".join(ordered_color),
                          PhotometricSystem=ps, ColorInterpolators=[
                              PecautMamajekYoungInterpolator,
                              PecautMamajekDwarfInterpolator,
                              PecautMamajekExtendedDwarfInterpolator
                          ],
                          ExtinctionInterpolator=WhitneyMathis,
                          MergingInterpolator=\
                          MergedFallbackInstantiatedInterpolator,
                          interpolation_kind=u"linear")

    Av = {}
    for color in C_Av:
        Av[color] = C_Av[color].evaluate(\
                                SpT_taurus,
                                np.array(photometry[color.split(u"-")[0]]) -\
                                np.array(photometry[color.split(u"-")[1]]))

    plt.plot(np.linspace(0.,np.max([np.max(Av[color]) for color in Av])),
             np.linspace(0.,np.max([np.max(Av[color]) for color in Av])),
             label = u"1:1")

    plt.plot(np.linspace(0.,np.max([np.max(Av[color]) for color in Av]))+0.5,
    np.linspace(0.,np.max([np.max(Av[color]) for color in Av])), label=u"1:1 Rc-Ic + 0.5")

    for color in Av:
        if color != u"Rc-Ic" and not np.all(np.isnan(Av[color])):
            plt.scatter(np.clip(Av[u"Rc-Ic"], 0, np.inf),
                        np.clip(Av[color], 0, np.inf), label=color,
                        s=20)
    plt.xlabel(u"Av(Rc-Ic)")
    plt.axis(u'equal')
    plt.title(u"Taurus Av Color Estimates with PM13 YSO, Dwarf, EEM_dwarf")
    plt.legend()
    plt.show()

def plot_Av_taurus_BTSettl(interpolation_kind = u"linear"):
    filename = os.path.dirname(os.path.abspath(__file__))\
             + u"/Data/" + u"taurus.dat"
    taurus = np.genfromtxt(filename, skip_header=1,
                           usecols=(3,7,8,9,10,11,12,1), dtype=None)
    SpT_taurus = [x[0].decode(u"utf-8") for x in taurus]
    #As a pure example, pretend
    photometry = {u"V": [x[1] for x in taurus],
                  u"Rc": [x[2] for x in taurus],
                  u"Ic": [x[3] for x in taurus],
                  u"J":[x[4]+x[5]+x[6] for x in taurus],
                  u"H": [x[5]+x[6] for x in taurus],
                  u"Ks": [x[6] for x in taurus]}
    star_names = [x[7] for x in taurus]

    #Instantiate a photometric system.
    ps = PhotometricSystem()
    color_combinations =\
    list(it.combinations(sorted(photometry.keys(),key=ps), 2))
    #print (sorted(k, key=ps))

    # Create Whitney-Mathis extinction law interpolator:
    Whitney = WhitneyInterpolator(interpolation_kind=interpolation_kind)
    Mathis = MathisInterpolator(interpolation_kind=interpolation_kind)
    WhitneyMathis = MergedFallbackInstantiatedInterpolator([Whitney, Mathis])

    # Create BTSettl interpolators from the calculator:
    C_Av = {}

    # Iterate over all the ordered colors:
    for ordered_color in color_combinations:
        colorinterp = [PecautMamajekDwarfInterpolator, BTSettlLandoltInterpolator]
        links = [u"Teff"]
        PM_SpT_to_BTSettlLandoltInterpolator = LinkedInterpolator(colorinterp, links)

        colorinterp = [PecautMamajekDwarfInterpolator, BTSettl2MASSInterpolator]
        links = [u"Teff"]
        PM_SpT_to_BTSettl2MASSInterpolator = LinkedInterpolator(colorinterp, links)

        colorinterp = [PecautMamajekDwarfInterpolator, BTSettlSDSSInterpolator]
        links = [u"Teff"]
        PM_SpT_to_BTSettlSDSSInterpolator = LinkedInterpolator(colorinterp, links)

        Extended_BT_Settl = ExtendColorInterpolator([
                PM_SpT_to_BTSettlLandoltInterpolator,
                PM_SpT_to_BTSettl2MASSInterpolator,
                PM_SpT_to_BTSettlSDSSInterpolator
            ])
        C_Av[u"-".join(ordered_color)] =\
        ColorMergeAvCalculator(u"-".join(ordered_color),
                          PhotometricSystem=ps, ColorInterpolators=[
                                  Extended_BT_Settl
                              ],
                          ExtinctionInterpolator=WhitneyMathis,
                          MergingInterpolator=\
                          MergedFallbackInstantiatedInterpolator,
                          interpolation_kind=u"linear")
    Av = {}
    for color in C_Av:
        Av[color] = C_Av[color].evaluate(\
                                SpT_taurus,
                                np.array(photometry[color.split(u"-")[0]]) -\
                                np.array(photometry[color.split(u"-")[1]]))

    plt.plot(np.linspace(0.,np.nanmax([np.nanmax(Av[color]) for color in Av])),
             np.linspace(0.,np.nanmax([np.nanmax(Av[color]) for color in Av])),
             label = u"1:1")

    plt.plot(np.linspace(0.,np.nanmax([np.nanmax(Av[color]) for color in Av]))+0.5,
    np.linspace(0.,np.nanmax([np.nanmax(Av[color]) for color in Av])), label=u"1:1 Rc-Ic + 0.5")

    for color in Av:
        if color != u"Rc-Ic" and not np.all(np.isnan(Av[color])):
            plt.scatter(np.clip(Av[u"Rc-Ic"], 0, np.inf),
                        np.clip(Av[color], 0, np.inf), label=color,
                        s=20)
    plt.xlabel(u"Av(R-I)")
    plt.axis(u'equal')
    plt.title(u"Taurus Av Color Estimates with BT-Settl")
    plt.legend()
    plt.show()
    return C_Av

def plot_Av_taurus_PM13_SED_vs_weighted(interpolation_kind = u"linear"):
    filename = os.path.dirname(os.path.abspath(__file__))\
             + u"/Data/" + u"taurus.dat"
    taurus = np.genfromtxt(filename, skip_header=1,
                           usecols=(3,7,8,9,10,11,12,1), dtype=None)
    SpT_taurus = [x[0].decode(u"utf-8") for x in taurus]
    photometry = {u"V": [x[1] for x in taurus],
                  u"Rc": [x[2] for x in taurus],
                  u"Ic": [x[3] for x in taurus],
                  u"J":[x[4]+x[5]+x[6] for x in taurus],
                  u"H": [x[5]+x[6] for x in taurus],
                  u"Ks": [x[6] for x in taurus]}
    star_names = [x[7] for x in taurus]

    #Instantiate a photometric system.
    ps = PhotometricSystem()

    color_list =\
    [u"-".join(color_tuple) for \
     color_tuple in it.combinations(sorted(photometry.keys(),
                                           key = ps), 2)]

    # Create Whitney-Mathis extinction law interpolator:
    Whitney = WhitneyInterpolator(interpolation_kind=interpolation_kind)
    Mathis = MathisInterpolator(interpolation_kind=interpolation_kind)
    WhitneyMathis = MergedFallbackInstantiatedInterpolator([Whitney, Mathis])

    C_Av_SED =\
    ColorSEDAvCalculator(PhotometricSystem=ps, ColorInterpolators=[
            PecautMamajekYoungInterpolator,
            PecautMamajekDwarfInterpolator,
            PecautMamajekExtendedDwarfInterpolator
            ],
                         ExtinctionInterpolator=WhitneyMathis,
                         MergingInterpolator=\
                         MergedFallbackInstantiatedInterpolator,
                         interpolation_kind=interpolation_kind)

    Av_SED = C_Av_SED.evaluate(SpT_taurus, **photometry)

    C_Av_weighted =\
    ColorWeightedAvCalculator(color_list,
                              PhotometricSystem=ps,
                              ColorInterpolators=[
            PecautMamajekYoungInterpolator,
            PecautMamajekDwarfInterpolator,
            PecautMamajekExtendedDwarfInterpolator
            ],
                              ExtinctionInterpolator=WhitneyMathis,
                              MergingInterpolator=\
                              MergedFallbackInstantiatedInterpolator,
                              interpolation_kind=interpolation_kind)

    Av_weighted = C_Av_weighted.evaluate(SpT_taurus, **photometry)

    plt.title(u"$A_{V, SED}$ vs $A_{V, weighted}$ in Taurus molecular cloud")
    plt.xlabel(u"$A_{V, weighted}$, clipped to $A_V > 0$")
    plt.ylabel(u"$A_{V, SED}$, clipped between $10 > A_V > 0$")
    plt.minorticks_on()
    plt.grid(b=True, which=u'major', color=u'lightgray', linestyle=u'-')
    plt.grid(b=True, which=u'minor', color=u'lightgray', linestyle=u'-')
    plt.gca().set_axisbelow(True)
    xs = np.linspace(0.,np.max([np.max(Av_weighted),
                                np.max(Av_SED)]), 100)
    ys = xs[:]
    plt.fill_between(xs, ys-0.25, ys+0.25, label=u"$y = x \\pm 0.25$")
    plt.fill_between(xs, ys-0.1, ys+0.1, label=u"$y = x \\pm 0.1$")
    plt.plot(xs, ys, label=u"$y = x$", color = u"gray")

    plt.scatter(np.clip(Av_weighted, 0., None), Av_SED, s=15, color=u"black")
    plt.legend()
    plt.show()

def spt_errors(x):
    if x < 50.1: return 2.0
    elif x < 69.9: return 1.0
    elif np.isnan(x): return np.NaN
    else: return 0.5

def plot_Av_taurus_PM13_SED_vs_weighted_with_errorbars(interpolation_kind = u"linear", num_samples = 100):
    filename = os.path.dirname(os.path.abspath(__file__))\
             + u"/Data/" + u"taurus.dat"
    taurus = np.genfromtxt(filename, skip_header=1,
                           usecols=(3,7,8,9,10,11,12,1), dtype=None)
    SpT_taurus = [x[0].decode(u"utf-8") for x in taurus]
    SpT_taurus = RandomNumber(SpT_taurus,[spt_errors(spt) for spt in SpTBaseInterpolator.get_num_SpTs(SpT_taurus)],num_samples = num_samples).output
    SpT_taurus = [item for sublist in SpT_taurus for item in sublist]
    photometry = {u"V": [item if np.random.random() < 1.0 else np.NaN for sublist in RandomNumber([x[1] for x in taurus],0.01,num_samples = num_samples).output for item in sublist],
                  u"Rc": [item if np.random.random() < 1.0 else np.NaN for sublist in RandomNumber([x[2] for x in taurus],0.01,num_samples = num_samples).output for item in sublist],
                  u"Ic": [item if np.random.random() < 1.0 else np.NaN for sublist in RandomNumber([x[3] for x in taurus],0.01,num_samples = num_samples).output for item in sublist],
                  u"J": [item if np.random.random() < 1.0 else np.NaN for sublist in RandomNumber([x[4]+x[5]+x[6] for x in taurus],0.01,num_samples = num_samples).\
                  output for item in sublist],
                  u"H": [item if np.random.random() < 1.0 else np.NaN for sublist in RandomNumber([x[5]+x[6] for x in taurus],0.01,num_samples = num_samples).output for item in sublist],
                  u"Ks": [item if np.random.random() < 1.0 else np.NaN for sublist in RandomNumber([x[6] for x in taurus],0.01,num_samples = num_samples).output for item in sublist]}
    star_names = [x[7] for x in taurus]

    #Instantiate a photometric system.
    ps = PhotometricSystem()

    color_list =\
    [u"-".join(color_tuple) for \
     color_tuple in it.combinations(sorted(photometry.keys(),
                                           key = ps), 2)]

    # Create Whitney-Mathis extinction law interpolator:
    Whitney = WhitneyInterpolator(interpolation_kind=interpolation_kind)
    Mathis = MathisInterpolator(interpolation_kind=interpolation_kind)
    WhitneyMathis = MergedFallbackInstantiatedInterpolator([Whitney, Mathis])

    C_Av_SED =\
    ColorSEDAvCalculator(PhotometricSystem=ps, ColorInterpolators=[
            PecautMamajekYoungInterpolator,
            PecautMamajekDwarfInterpolator,
            PecautMamajekExtendedDwarfInterpolator
            ],
                         ExtinctionInterpolator=WhitneyMathis,
                         MergingInterpolator=\
                         MergedFallbackInstantiatedInterpolator,
                         interpolation_kind=interpolation_kind)

    Av_SED = C_Av_SED.evaluate(SpT_taurus, **photometry)

    C_Av_weighted =\
    ColorWeightedAvCalculator(color_list,
                              PhotometricSystem=ps,
                              ColorInterpolators=[
            PecautMamajekYoungInterpolator,
            PecautMamajekDwarfInterpolator,
            PecautMamajekExtendedDwarfInterpolator
            ],
                              ExtinctionInterpolator=WhitneyMathis,
                              MergingInterpolator=\
                              MergedFallbackInstantiatedInterpolator,
                              interpolation_kind=interpolation_kind)

    Av_weighted = C_Av_weighted.evaluate(SpT_taurus, **photometry)

    Av_SED_points = np.reshape(Av_SED,(len(star_names), num_samples))
    Av_weighted_points = np.reshape(Av_weighted,(len(star_names), num_samples))
    SED_1sig = np.zeros(len(star_names)) + np.nan
    weighted_1sig = np.zeros(len(star_names)) + np.nan
    for point_index in xrange(len(star_names)):
        SED_1sig[point_index] = np.std(Av_SED_points[point_index])
        weighted_1sig[point_index] = np.std(Av_weighted_points[point_index])

    plt.title(u"$A_{V, SED}$ vs $A_{V, weighted}$ in Taurus molecular cloud")
    plt.xlabel(u"$A_{V, weighted}$, clipped to $A_V > 0$")
    plt.ylabel(u"$A_{V, SED}$, clipped between $10 > A_V > 0$")
    plt.minorticks_on()
    plt.grid(b=True, which=u'major', color=u'lightgray', linestyle=u'-')
    plt.grid(b=True, which=u'minor', color=u'lightgray', linestyle=u'-')
    plt.gca().set_axisbelow(True)
    xs = np.linspace(0.,np.nanmax([np.nanmax(Av_weighted),
                                   np.nanmax(Av_SED)]), 100)
    ys = xs[:]
    plt.fill_between(xs, ys-0.25, ys+0.25, label=u"$y = x \\pm 0.25$")
    plt.fill_between(xs, ys-0.1, ys+0.1, label=u"$y = x \\pm 0.1$")
    plt.plot(xs, ys, label=u"$y = x$", color = u"gray")

    plt.errorbar(np.clip([Av[0] for Av in Av_weighted_points], 0., None),
                 [Av[0] for Av in Av_SED_points], yerr = SED_1sig, xerr = weighted_1sig, elinewidth=2, color = u"black", linewidth=0)

    #plt.scatter(np.clip(Av_weighted, 0., None), Av_SED, s=15, color="black")
    plt.legend()
    plt.show()

def plot_Av_taurus_PM13_SED_vs_weighted_with_errorbars_robustness(interpolation_kind = u"linear", num_samples = 100):
    filename = os.path.dirname(os.path.abspath(__file__))\
             + u"/Data/" + u"taurus.dat"
    taurus = np.genfromtxt(filename, skip_header=1,
                           usecols=(3,7,8,9,10,11,12,1), dtype=None)
    SpT_taurus = [x[0].decode(u"utf-8") for x in taurus]
    SpT_taurus = RandomNumber(SpT_taurus,[spt_errors(spt) for spt in SpTBaseInterpolator.get_num_SpTs(SpT_taurus)],num_samples = num_samples).output
    SpT_taurus = [item for sublist in SpT_taurus for item in sublist]
    photometry = {u"V": [item if np.random.random() < 0.9 else np.NaN for sublist in RandomNumber([x[1] for x in taurus],0.01,num_samples = num_samples).output for item in sublist],
                  u"Rc": [item if np.random.random() < 0.9 else np.NaN for sublist in RandomNumber([x[2] for x in taurus],0.01,num_samples = num_samples).output for item in sublist],
                  u"Ic": [item if np.random.random() < 0.9 else np.NaN for sublist in RandomNumber([x[3] for x in taurus],0.01,num_samples = num_samples).output for item in sublist],
                  u"J": [item if np.random.random() < 0.2 else np.NaN for sublist in RandomNumber([x[4]+x[5]+x[6] for x in taurus],0.01,num_samples = num_samples).\
                  output for item in sublist],
                  u"H": [item if np.random.random() < 0.9 else np.NaN for sublist in RandomNumber([x[5]+x[6] for x in taurus],0.01,num_samples = num_samples).output for item in sublist],
                  u"Ks": [item if np.random.random() < 0.9 else np.NaN for sublist in RandomNumber([x[6] for x in taurus],0.01,num_samples = num_samples).output for item in sublist]}
    robustphot = {u"V": [item if np.random.random() < 1.0 else np.NaN for sublist in RandomNumber([x[1] for x in taurus],0.01,num_samples = num_samples).output for item in sublist],
                  u"Rc": [item if np.random.random() < 1.0 else np.NaN for sublist in RandomNumber([x[2] for x in taurus],0.01,num_samples = num_samples).output for item in sublist],
                  u"Ic": [item if np.random.random() < 1.0 else np.NaN for sublist in RandomNumber([x[3] for x in taurus],0.01,num_samples = num_samples).output for item in sublist],
                  u"J": [item if np.random.random() < 1.0 else np.NaN for sublist in RandomNumber([x[4]+x[5]+x[6] for x in taurus],0.01,num_samples = num_samples).\
                  output for item in sublist],
                  u"H": [item if np.random.random() < 1.0 else np.NaN for sublist in RandomNumber([x[5]+x[6] for x in taurus],0.01,num_samples = num_samples).output for item in sublist],
                  u"Ks": [item if np.random.random() < 1.0 else np.NaN for sublist in RandomNumber([x[6] for x in taurus],0.01,num_samples = num_samples).output for item in sublist]}

    star_names = [x[7] for x in taurus]

    #Instantiate a photometric system.
    ps = PhotometricSystem()

    color_list =\
    [u"-".join(color_tuple) for \
     color_tuple in it.combinations(sorted(photometry.keys(),
                                           key = ps), 2)]

    # Create Whitney-Mathis extinction law interpolator:
    Whitney = WhitneyInterpolator(interpolation_kind=interpolation_kind)
    Mathis = MathisInterpolator(interpolation_kind=interpolation_kind)
    WhitneyMathis = MergedFallbackInstantiatedInterpolator([Whitney, Mathis])

    C_Av_SED =\
    ColorSEDAvCalculator(PhotometricSystem=ps, ColorInterpolators=[
            PecautMamajekYoungInterpolator,
            PecautMamajekDwarfInterpolator,
            PecautMamajekExtendedDwarfInterpolator
            ],
                         ExtinctionInterpolator=WhitneyMathis,
                         MergingInterpolator=\
                         MergedFallbackInstantiatedInterpolator,
                         interpolation_kind=interpolation_kind)

    Av_SED = C_Av_SED.evaluate(SpT_taurus, **photometry)
    Av_SED_robust = C_Av_SED.evaluate(SpT_taurus, **robustphot)

    C_Av_weighted =\
    ColorWeightedAvCalculator(color_list,
                              PhotometricSystem=ps,
                              ColorInterpolators=[
            PecautMamajekYoungInterpolator,
            PecautMamajekDwarfInterpolator,
            PecautMamajekExtendedDwarfInterpolator
            ],
                              ExtinctionInterpolator=WhitneyMathis,
                              MergingInterpolator=\
                              MergedFallbackInstantiatedInterpolator,
                              interpolation_kind=interpolation_kind)

    Av_weighted = C_Av_weighted.evaluate(SpT_taurus, **photometry)
    Av_weighted_robust = C_Av_weighted.evaluate(SpT_taurus, **robustphot)

    Av_SED_points = np.reshape(Av_SED,(len(star_names), num_samples))
    Av_SED_robust_points = np.reshape(Av_SED_robust,(len(star_names), num_samples))
    Av_weighted_points = np.reshape(Av_weighted,(len(star_names), num_samples))
    Av_weighted_robust_points = np.reshape(Av_weighted_robust,(len(star_names), num_samples))
    SED_1sig = np.zeros(len(star_names)) + np.nan
    SED_robust_1sig = np.zeros(len(star_names)) + np.nan
    weighted_1sig = np.zeros(len(star_names)) + np.nan
    weighted_robust_1sig = np.zeros(len(star_names)) + np.nan
    for point_index in xrange(len(star_names)):
        SED_1sig[point_index] = np.nanstd(Av_SED_points[point_index])
        SED_robust_1sig[point_index] = np.nanstd(Av_SED_robust_points[point_index])
        weighted_1sig[point_index] = np.nanstd(Av_weighted_points[point_index])
        weighted_robust_1sig[point_index] = np.nanstd(Av_weighted_robust_points[point_index])

    plt.title(u"Robustness Checks for the Taurus molecular cloud")
    plt.xlabel(u"Robust (complete photometric data)")
    plt.ylabel(u"Random $90 \\%$ of most data, $20 \\%$ of J-band photometry")
    plt.minorticks_on()
    plt.grid(b=True, which=u'major', color=u'lightgray', linestyle=u'-')
    plt.grid(b=True, which=u'minor', color=u'lightgray', linestyle=u'-')
    plt.gca().set_axisbelow(True)
    xs = np.linspace(0.,np.nanmax([np.nanmax(Av_weighted_points),
                                   np.nanmax(Av_SED_points)]), 100)
    ys = xs[:]
    plt.fill_between(xs, ys-0.25, ys+0.25, label=u"$y = x \\pm 0.25$")
    plt.fill_between(xs, ys-0.1, ys+0.1, label=u"$y = x \\pm 0.1$")
    plt.plot(xs, ys, label=u"$y = x$", color = u"gray")

    #plt.errorbar(np.clip([Av[0] for Av in Av_weighted_points], 0., None),
    #             [Av[0] for Av in Av_SED_points], yerr = SED_1sig, xerr = weighted_1sig, elinewidth=2, color = "black", linewidth=0)
    plt.errorbar(np.clip([Av[0] for Av in Av_SED_robust_points], 0., None),
                 np.clip([Av[0] for Av in Av_SED_points], 0., None), xerr = SED_robust_1sig, yerr = SED_1sig, elinewidth=2, color = u"gray", linewidth=0,
                 label = u"SED")
    plt.errorbar(np.clip([Av[0] for Av in Av_weighted_robust_points], 0., None),
                 np.clip([Av[0] for Av in Av_weighted_points], 0., None), xerr = weighted_robust_1sig, yerr = weighted_1sig, elinewidth=2, color = u"black", linewidth=0,
                 label = u"Weighted")


    #plt.scatter(np.clip(Av_weighted, 0., None), Av_SED, s=15, color="black")
    plt.legend()
    plt.show()

def plot_logL(interpolation_kind = u"linear", num_samples = 100):
    filename = os.path.dirname(os.path.abspath(__file__))\
             + u"/Data/" + u"taurus.dat"
    taurus = np.genfromtxt(filename, skip_header=1,
                           usecols=(3,7,8,9,10,11,12,1), dtype=None)
    SpT_taurus = [x[0].decode(u"utf-8") for x in taurus]
    SpT_taurus = RandomNumber(SpT_taurus,[spt_errors(spt) for spt in SpTBaseInterpolator.get_num_SpTs(SpT_taurus)],num_samples = num_samples).output
    SpT_taurus = [item for sublist in SpT_taurus for item in sublist]
    photometry = {u"V": [item if np.random.random() < 1.0 else np.NaN for sublist in RandomNumber([x[1] for x in taurus],0.01,num_samples = num_samples).output for item in sublist],
                  u"Rc": [item if np.random.random() < 1.0 else np.NaN for sublist in RandomNumber([x[2] for x in taurus],0.01,num_samples = num_samples).output for item in sublist],
                  u"Ic": [item if np.random.random() < 1.0 else np.NaN for sublist in RandomNumber([x[3] for x in taurus],0.01,num_samples = num_samples).output for item in sublist],
                  u"J": [item if np.random.random() < 1.0 else np.NaN for sublist in RandomNumber([x[4]+x[5]+x[6] for x in taurus],0.01,num_samples = num_samples).\
                  output for item in sublist],
                  u"H": [item if np.random.random() < 1.0 else np.NaN for sublist in RandomNumber([x[5]+x[6] for x in taurus],0.01,num_samples = num_samples).output for item in sublist],
                  u"Ks": [item if np.random.random() < 1.0 else np.NaN for sublist in RandomNumber([x[6] for x in taurus],0.01,num_samples = num_samples).output for item in sublist]}
    star_names = [x[7] for x in taurus]

    #Instantiate a photometric system.
    ps = PhotometricSystem()

    color_list =\
    [u"-".join(color_tuple) for \
     color_tuple in it.combinations(sorted(photometry.keys(),
                                           key = ps), 2)]

    # Create Whitney-Mathis extinction law interpolator:
    Whitney = WhitneyInterpolator(interpolation_kind=interpolation_kind)
    Mathis = MathisInterpolator(interpolation_kind=interpolation_kind)
    WhitneyMathis = MergedFallbackInstantiatedInterpolator([Whitney, Mathis])

    C_Av_SED =\
    ColorSEDAvCalculator(PhotometricSystem=ps, ColorInterpolators=[
            PecautMamajekYoungInterpolator,
            PecautMamajekDwarfInterpolator,
            PecautMamajekExtendedDwarfInterpolator
            ],
                         ExtinctionInterpolator=WhitneyMathis,
                         MergingInterpolator=\
                         MergedFallbackInstantiatedInterpolator,
                         interpolation_kind=interpolation_kind)

    Av_SED = C_Av_SED.evaluate(SpT_taurus, **photometry)

    LogL_calculator =\
    LogLCalculator(color_list,
                   PhotometricSystem=ps,
                   ColorInterpolators=[
PecautMamajekYoungInterpolator,
PecautMamajekDwarfInterpolator,
PecautMamajekExtendedDwarfInterpolator
],
                   ExtinctionInterpolator=WhitneyMathis,
                   MergingInterpolator=\
                   MergedFallbackInstantiatedInterpolator,
                   interpolation_kind=interpolation_kind)

    dist = np.zeros(Av_SED.shape) + 1e3

    LogL = LogL_calculator.evaluate(SpT_taurus, Av_SED, dist, **photometry)

    return LogL
