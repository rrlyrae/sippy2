from __future__ import absolute_import
import matplotlib.pyplot as plt
import numpy as np
from astropy import units as u
from sippy.interpolator.BaseInterpolator import *
from sippy.interpolator.WhitneyInterpolator import *
from sippy.interpolator.MathisInterpolator import *

def plot_merged_whitney_mathis(interpolation_kind=u"linear"):
    u"""An example usage of a merged interpolator which uses WhitneyInterpolator
    between 0.2 to 1000 microns, and falls back on MathisInterpolator for
    0.002 to < 0.2 microns."""
    Whitney = WhitneyInterpolator(interpolation_kind=interpolation_kind)
    Mathis = MathisInterpolator(interpolation_kind=interpolation_kind)
    WhitneyMathis = MergedFallbackInstantiatedInterpolator([Whitney, Mathis])
    xs = np.logspace(np.log10(0.002), np.log10(1000), 100)*u.micron

    # Three subplots sharing both x/y axes
    f, (ax1, ax2) = plt.subplots(2, sharex=True, sharey=True)
    ax1.plot(xs, Whitney.evaluate(xs), label=u"Whitney",
             lw = 2, color = u"red")
    ax1.plot(xs, Mathis.evaluate(xs), label=u"Mathis",
             lw = 2, color = u"orange")
    ax2.plot(xs, WhitneyMathis.evaluate(xs), label=u"Whitney + Mathis",
             lw = 2, color = u"blue")
    ax1.set_title(u'Merged Extinction Interpolator Demo')
    # Fine-tune figure; make subplots close to each other and hide x ticks for
    # all but bottom plot.
    f.subplots_adjust(hspace=0)
    plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)

    ax2.set_xlabel(u"Wavelength ($\\lambda \\, [\\mu m]$)")
    for ax in [ax1, ax2]:
        ax.set_ylabel(u"$A_ \\lambda / A_V$")
        ax.semilogx()
        ax.semilogy()
        ax.legend()
    plt.show()
