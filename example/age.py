from __future__ import absolute_import
import matplotlib.pyplot as plt
import numpy as np
import os
from astropy import units as u
from sippy.interpolator.BaseInterpolator import *
from sippy.calculator.AgeCalculator import *
from sippy.interpolator.MISTInterpolator import MISTIsochroneInterpolator
from sippy.interpolator.DartmouthInterpolator import DartmouthIsochroneInterpolator
from sippy.interpolator.BCAHInterpolator import \
BCAH15IsochroneInterpolator, BCAH98IsochroneInterpolator
from sippy.interpolator.SomersInterpolator import SomersIsochroneInterpolator

### Create age prior:
def generated_uniform_age_prior(max_age):
	def uniform_age_prior(ages):
		prior_array = np.zeros(len(ages))
		cond_not_nan = np.logical_not(np.isnan(prior_array))
		prior_array[cond_not_nan] =\
		np.where(ages[cond_not_nan] > max_age,
				 np.zeros(len(ages[cond_not_nan])),
				 np.zeros(len(ages[cond_not_nan]))+1.)
		return prior_array
	return uniform_age_prior

n = 1.5
membership_cut_style = "multiple"

def single_membership_cut(vals, vals_e, n = 2.):
	return np.square(np.abs(vals - np.nanmedian(vals))/vals_e) <= n*n

def multiple_membership_cut(total_vals, total_vals_e, n = 2.):
	N = len(total_vals)
	med_vals = np.nanmedian(total_vals, axis=1)
	temp_total_vals = np.zeros(total_vals.shape)
	for index in range(len(total_vals)):
		temp_total_vals[index] = total_vals[index] - med_vals[index]
	return np.sum(np.square(np.abs(temp_total_vals)/total_vals_e), axis=0) <= N*n*n

def membership(ra_deg, dec_deg,
			   plx, plx_e,
			   pm_ra, pm_ra_e,
			   pm_dec, pm_dec_e,
			   n = 2.9,
			   plx_zero_pt = 0.521):
	### convert parallax to distance
	dist = 1000./(plx + plx_zero_pt)
	log_dist = np.log10(dist)
	dist_e = 1000.*plx_e/np.square(plx + plx_zero_pt)
	log_dist_e = plx_e/plx
	print "dist_e", dist_e
	#
	cond = np.isfinite(log_dist) #np.array([True] * len(pm_ra))
	temp_cond = np.array([False] * len(pm_ra))
	while not np.all(temp_cond == cond):
		temp_cond[:] = cond[:]

		### get ra scatter and dec scatter:
		ra_deg_e = np.std(ra_deg[cond])
		dec_deg_e = np.std(dec_deg[cond])

		if membership_cut_style == "single":

			cond[cond] =\
			single_membership_cut(pm_ra[cond],
								  np.sqrt(np.square(np.nanstd(pm_ra[cond]))+\
										  np.square(pm_ra_e[cond])), n=n)[:]

			cond[cond] =\
			single_membership_cut(pm_dec[cond],
								  np.sqrt(np.square(np.nanstd(pm_dec[cond]))+\
										  np.square(pm_dec_e[cond])), n=n)[:]

			cond[cond] =\
			single_membership_cut(log_dist[cond],
								  np.sqrt(np.square(np.nanstd(log_dist[cond]))+\
										  np.square(log_dist_e[cond])), n=n)[:]

			cond[cond] =\
			single_membership_cut(ra_deg[cond], ra_deg_e, n=n)[:]

			cond[cond] =\
			single_membership_cut(dec_deg[cond], dec_deg_e, n=n)[:]

		elif membership_cut_style == "multiple":
			total_values = [pm_ra[cond], pm_dec[cond], log_dist[cond], ra_deg[cond], dec_deg[cond]]
			total_errors = [np.sqrt(np.square(np.nanstd(pm_ra[cond]))+np.square(pm_ra_e[cond])),
							np.sqrt(np.square(np.nanstd(pm_dec[cond]))+np.square(pm_dec_e[cond])),
							np.sqrt(np.square(np.nanstd(log_dist[cond]))+np.square(log_dist_e[cond])),
							np.zeros(np.sum(cond))+ra_deg_e,
							np.zeros(np.sum(cond))+dec_deg_e]

			cond[cond] =\
			multiple_membership_cut(np.array(total_values), np.array(total_errors), n=n)

		print ("# elems:", np.sum(cond), "Prev #:", np.sum(temp_cond))
	return cond

ordered_column_names =\
["ra_deg",
"dec_deg",
"si",
"log_Teff",
"log_Teff_e",
"log_L_star",
"log_L_star_e",
"M_star",
"M_star_e",
"log_age_star",
"log_age_star_e",
"av",
"av_e",
"plx",
"plx_e",
"pm_ra",
"pm_ra_e",
"pm_dec",
"pm_dec_e",
"rv",
"rv_e",
"mem_num",
"spot_f",
"spot_x"]

def do_plots_isochronal(logT = 3.65, logL = -0.25, sigma_logT = 0.01, sigma_logL = 0.1):
	import sippy
	from sippy.interpolator.MISTInterpolator import MISTIsochroneInterpolator
	from sippy.interpolator.DartmouthInterpolator import DartmouthIsochroneInterpolator
	from sippy.interpolator.BCAHInterpolator import BCAH98IsochroneInterpolator
	from sippy.interpolator.SomersInterpolator import SomersIsochroneInterpolator
	MIST = sippy.calculator.SingleStarAgeMassCalculator(MISTIsochroneInterpolator)
	Dartmouth = sippy.calculator.SingleStarAgeMassCalculator(DartmouthIsochroneInterpolator)
	BCAH98 = sippy.calculator.SingleStarAgeMassCalculator(BCAH98IsochroneInterpolator)
	Somers = sippy.calculator.SingleStarTripleTupleAgeMassCalculator(SomersIsochroneInterpolator)
	interpolators = {"MIST16": MIST, "Dartmouth14": Dartmouth, "BCAH98": BCAH, "Somers": Somers}
	spot_alphas = np.arange(0.35,1.0+1e-9,0.05)
	for interp_key in interpolators:
		if interp_key == "Somers":
			for spot_alpha in spot_alphas:
				interpolators[interp_key]._evaluate(spot_alpha, logT, logL,
				sigma_logT=sigma_logT, sigma_logL=sigma_logL,
				filename=str(logT)+"_"+str(logL)+"_"+str(sigma_logT)+"_"+str(sigma_logL)+"_"+\
				interp_key+"_spot_alpha_"+str(spot_alpha)+\
				".png", title = interp_key, track_label = interp_key)
		else:
			interpolators[interp_key]._evaluate(logT, logL,
			sigma_logT=sigma_logT, sigma_logL=sigma_logL,
			filename=str(logT)+"_"+str(logL)+"_"+str(sigma_logT)+"_"+str(sigma_logL)+"_"+\
			interp_key+".png", title = interp_key, track_label = interp_key)

logTs = [3.65, 3.70, 4.10, 3.82, 3.75]
logLs = [-0.25, 0.15, -0.05, 0.85, 0.3]
sigma_logTs = [0.01]*5
sigma_logLs = [0.1]*5
def test_isochronal(tracks, logTs = logTs, logLs = logLs, sigma_logTs = sigma_logTs, sigma_logLs = sigma_logLs):
	return tracks._evaluate(logTs, logLs,
	sigma_logTs=sigma_logTs, sigma_logLs=sigma_logLs,
	filename="cluster.png", title = "Test Isochronal")

MergedIsochroneInterpolatorOnMass.load_isochrones([MISTIsochroneInterpolator, BCAH15IsochroneInterpolator])

tracks =\
{"pm13_fkp17_mist": {"track_name": "PM13 + FKP17 + MIST",
"tracks": MultipleStarsAgeMassCalculator(MISTIsochroneInterpolator),
"filename_prefix": "pm13_fkp17_mist",
"load_filename": "MIST_starspots_8_4_2018_25x4_mb_Teff_from_MIST_luisa.Ple.data.17jan2018_xmatch_gaiaDR2_Pan-STARRS_WISE.1as.data"},
"pm13_jordi05_mist": {"track_name": "PM13, extended SDSS + MIST",
"tracks": MultipleStarsAgeMassCalculator(MISTIsochroneInterpolator),
"filename_prefix": "pm13_jordi05_mist",
"load_filename": "lam_ori_extended_sdss.dat"},
"pm13_fang17_mist": {"track_name": "PM13, Fang17 + MIST",
"tracks": MultipleStarsAgeMassCalculator(MISTIsochroneInterpolator),
"filename_prefix": "pm13_fang17_mist",
"load_filename": "lam_ori_fang.dat"},
"pm13_jordi05_mist_somers": {"track_name": "PM13, extended SDSS, MIST",
"tracks": MultipleStarsAgeMassCalculator(MISTIsochroneInterpolator),
"filename_prefix": "pm13_jordi05_mist_somers",
"load_filename": "lam_ori_extended_sdss_somers.dat"},
"pm13_jordi05_somers": {"track_name": "PM13, extended SDSS, Somers",
"tracks": MultipleStarsTripleAgeMassCalculator(SomersIsochroneInterpolator),
"filename_prefix": "pm13_jordi05_somers",
"load_filename": "lam_ori_extended_sdss_somers.dat"},
"pm13_praesepe_somers": {"track_name": "PM13, Praesepe-Calibrated, Somers",
"tracks": MultipleStarsTripleAgeMassCalculator(SomersIsochroneInterpolator),
"filename_prefix": "pm13_praesepe_somers",
"load_filename": "lam_ori_extended_praesepe_somers.dat"},
"pm13_jordi05_bcah15": {"track_name": "PM13, extended SDSS + BCAH15",
"tracks": MultipleStarsAgeMassCalculator(BCAH15IsochroneInterpolator),
"filename_prefix": "pm13_jordi05_mist",
"load_filename": "lam_ori_extended_sdss.dat"},
"pm13_jordi05_mist_bcah15": {"track_name": "MIST+BCAH15",
"tracks": MultipleStarsAgeMassCalculator(MergedIsochroneInterpolatorOnMass),
"filename_prefix": "pm13_jordi05_mist_bcah15",
"load_filename": "lam_ori_extended_sdss.dat"},
"bt_settl_mist_bcah15": {"track_name": "MIST+BCAH15",
"tracks": MultipleStarsAgeMassCalculator(MergedIsochroneInterpolatorOnMass),
"filename_prefix": "bt_settl_mist_bcah15",
"load_filename": "lam_ori_extended_sdss_bt_settl.dat"}
}

# {"pm13_fkp17_mist": {"track_name": "PM13 + FKP17 + MIST",
# "tracks": MultipleStarsAgeMassCalculator(MISTIsochroneInterpolator),
# "filename_prefix": "pm13_fkp17_mist",
# "load_filename": "lam_ori_PM13_FKP17.dat"}}

# {"pm13_somers": {"track_name": "PM13 + Somers & Pinsonneault",
# "tracks": MultipleStarsAgeMassCalculator(SomersIsochroneInterpolator),
# "filename_prefix": "pm13_somers",
# "load_filename": "lam_ori_spotted"},
# "pm13_mist": {"track_name": "PM13 + MIST",
# "tracks": MultipleStarsAgeMassCalculator(MISTIsochroneInterpolator),
# "filename_prefix": "pm13_mist",
# "load_filename": "lam_ori_PM13.dat"},
# "btsettl_somers": {"track_name": "BT-Settl + Somers & Pinsonneault",
# "tracks": MultipleStarsAgeMassCalculator(SomersIsochroneInterpolator),
# "filename_prefix": "btsettl_somers",
# "load_filename": "lam_ori_spotted_bt_settl"}}
clusters =\
{"Lam Ori summed log": {"name": "Lam Ori",
"label": "lam_ori_log",
"min_logage": 5.,
"max_logage": 8.,
"min_mass": 0.01,
"max_mass": 8,
"num_bins": 501,
"summed_posterior": True,
"log_mass": True},
"Lam Ori summed": {"name": "",
"label": "lam_ori",
"min_logage": 5.,
"max_logage": 8.,
"min_mass": 0.1,
"max_mass": 1,
"num_bins": 501,
"summed_posterior": True,
"log_mass": False},
"Lam Ori multiplied": {"name": "",
"label": "lam_ori",
"min_logage": 6.4,
"max_logage": 6.6,
"min_mass": 0.7,
"max_mass": 0.8,
"num_bins": 501,
"summed_posterior": False,
"log_mass": False},
"Lam Ori multiplied log": {"name": "",
"label": "lam_ori_log",
"min_logage": 5.,
"max_logage": 8.,
"min_mass": 0.01,
"max_mass": 8,
"num_bins": 501,
"summed_posterior": False,
"log_mass": True}}
# {"Pleiades summed log": {"name": "Pleiades with Gaia DR2 membership",
# "label": "pleiades_log",
# "min_logage": 5.5,
# "max_logage": 10.,
# "min_mass": 0.1,
# "max_mass": 8,
# "num_bins": 501,
# "summed_posterior": False,
# "log_mass": True},
# "Pleiades summed": {"name": "",
# "label": "pleiades",
# "min_logage": 5.5,
# "max_logage": 10.,
# "min_mass": 0.1,
# "max_mass": 1,
# "num_bins": 501,
# "summed_posterior": False,
# "log_mass": False},
# "Pleiades multiplied": {"name": "",
# "label": "pleiades",
# "min_logage": 6,
# "max_logage": 9,
# "min_mass": 0.1,
# "max_mass": 1.,
# "num_bins": 501,
# "summed_posterior": True,
# "log_mass": False}}

def cluster_plot(track_label = "bt_settl_mist_bcah15", cluster_label = "Lam Ori summed log", n = 3.):
	summed_posterior = clusters[cluster_label]["summed_posterior"]
	if summed_posterior:
		multiplied_string = "_summed"
	else:
		multiplied_string = "_multiplied"
	filename = os.path.dirname(os.path.abspath(__file__))\
	+ u"/Data/" + tracks[track_label]["load_filename"]
	lam_ori = np.genfromtxt(filename, missing_values = "nan", filling_values = np.NaN, skip_header = 1)
	logTs = lam_ori.T[ordered_column_names.index("log_Teff")]
	sigma_logTs = lam_ori.T[ordered_column_names.index("log_Teff_e")]
	logLs = lam_ori.T[ordered_column_names.index("log_L_star")]
	sigma_logLs = lam_ori.T[ordered_column_names.index("log_L_star_e")]
	ra_deg = lam_ori.T[ordered_column_names.index("ra_deg")]
	dec_deg = lam_ori.T[ordered_column_names.index("dec_deg")]
	plx = lam_ori.T[ordered_column_names.index("plx")]
	plx_e = lam_ori.T[ordered_column_names.index("plx_e")]
	pm_ra = lam_ori.T[ordered_column_names.index("pm_ra")]
	pm_ra_e = lam_ori.T[ordered_column_names.index("pm_ra_e")]
	pm_dec = lam_ori.T[ordered_column_names.index("pm_dec")]
	pm_dec_e = lam_ori.T[ordered_column_names.index("pm_dec_e")]
	# members = membership(ra_deg, dec_deg,
	# plx, plx_e,
	# pm_ra, pm_ra_e,
	# pm_dec, pm_dec_e,
	# n = n)
	# print "Acceptance rate:", float(np.sum(members))/len(members)
	print zip(np.logical_not(np.isnan(logTs)), np.logical_not(np.isnan(sigma_logTs)),
	np.logical_not(np.isnan(logLs)), np.logical_not(np.isnan(sigma_logLs)))
	members = [True for x in logTs]
	cond = np.all(np.logical_not([np.isnan(logTs), np.isnan(sigma_logTs),
	np.isnan(logLs), np.isnan(sigma_logLs), np.logical_not(members)]), axis=0)
	#print "logTs", logTs[cond]
	tracks[track_label]["tracks"]._evaluate(logTs[cond], logLs[cond],
	sigma_logTs=sigma_logTs[cond], sigma_logLs=sigma_logLs[cond],
	filename= tracks[track_label]["filename_prefix"] + "_" +\
	clusters[cluster_label]["label"] + multiplied_string + ".png",
	title = clusters[cluster_label]["name"],
	track_label=tracks[track_label]["track_name"],
	min_logage = clusters[cluster_label]["min_logage"],
	max_logage = clusters[cluster_label]["max_logage"],
	min_mass = clusters[cluster_label]["min_mass"],
	max_mass = clusters[cluster_label]["max_mass"],
	num_bins = clusters[cluster_label]["num_bins"],
	sum_chisq = summed_posterior,
	log_mass = clusters[cluster_label]["log_mass"],
	age_prior = generated_uniform_age_prior(clusters[cluster_label]["max_logage"] + 1.))

	return tracks[track_label]["tracks"]._flip_eval(logTs[cond], logLs[cond],
	sigma_logTs=sigma_logTs[cond], sigma_logLs=sigma_logLs[cond],
	filename= tracks[track_label]["filename_prefix"] + "_" +\
	clusters[cluster_label]["label"] + multiplied_string + ".png",
	title = clusters[cluster_label]["name"],
	track_label=tracks[track_label]["track_name"],
	min_logage = clusters[cluster_label]["min_logage"],
	max_logage = clusters[cluster_label]["max_logage"],
	min_mass = clusters[cluster_label]["min_mass"],
	max_mass = clusters[cluster_label]["max_mass"],
	num_bins = clusters[cluster_label]["num_bins"],
	sum_chisq = summed_posterior,
	log_mass = clusters[cluster_label]["log_mass"],
	age_prior = generated_uniform_age_prior(clusters[cluster_label]["max_logage"] + 1.))

def spot_cluster_plot(track_label = "pm13_praesepe_somers", cluster_label = "Lam Ori summed log", n = 3.):
	summed_posterior = clusters[cluster_label]["summed_posterior"]
	if summed_posterior:
		multiplied_string = "_multiplied"
	else:
		multiplied_string = "_summed"
	filename = os.path.dirname(os.path.abspath(__file__))\
	+ u"/Data/" + tracks[track_label]["load_filename"]
	lam_ori = np.genfromtxt(filename, missing_values = "nan", filling_values = np.NaN, skip_header = 1)
	logTs = lam_ori.T[ordered_column_names.index("log_Teff")]
	sigma_logTs = lam_ori.T[ordered_column_names.index("log_Teff_e")]
	logLs = lam_ori.T[ordered_column_names.index("log_L_star")]
	sigma_logLs = lam_ori.T[ordered_column_names.index("log_L_star_e")]
	ra_deg = lam_ori.T[ordered_column_names.index("ra_deg")]
	dec_deg = lam_ori.T[ordered_column_names.index("dec_deg")]
	plx = lam_ori.T[ordered_column_names.index("plx")]
	plx_e = lam_ori.T[ordered_column_names.index("plx_e")]
	pm_ra = lam_ori.T[ordered_column_names.index("pm_ra")]
	pm_ra_e = lam_ori.T[ordered_column_names.index("pm_ra_e")]
	pm_dec = lam_ori.T[ordered_column_names.index("pm_dec")]
	pm_dec_e = lam_ori.T[ordered_column_names.index("pm_dec_e")]
	spot_f = lam_ori.T[ordered_column_names.index("spot_f")]
	spot_x = lam_ori.T[ordered_column_names.index("spot_x")]
	spot_alphas = (1.-spot_f+spot_f*np.power(spot_x,4.))
	# members = membership(ra_deg, dec_deg,
	# plx, plx_e,
	# pm_ra, pm_ra_e,
	# pm_dec, pm_dec_e,
	# n = n)
	# print "Acceptance rate:", float(np.sum(members))/len(members)
	members = [True for x in logTs]
	cond = np.all(np.logical_not([np.isnan(logTs), np.isnan(sigma_logTs),
	np.isnan(logLs), np.isnan(sigma_logLs), np.logical_not(members)]), axis=0)
	return tracks[track_label]["tracks"]._evaluate(spot_alphas[cond],
	logTs[cond], logLs[cond], sigma_logTs=sigma_logTs[cond], sigma_logLs=sigma_logLs[cond],
	filename= "spotted_"+tracks[track_label]["filename_prefix"] + "_" +\
	clusters[cluster_label]["label"] + multiplied_string + ".png",
	title = clusters[cluster_label]["name"],
	track_label=tracks[track_label]["track_name"],
	min_logage = clusters[cluster_label]["min_logage"],
	max_logage = clusters[cluster_label]["max_logage"],
	min_mass = clusters[cluster_label]["min_mass"],
	max_mass = clusters[cluster_label]["max_mass"],
	num_bins = clusters[cluster_label]["num_bins"],
	sum_chisq = summed_posterior,
	log_mass = clusters[cluster_label]["log_mass"],
	age_prior = generated_uniform_age_prior(clusters[cluster_label]["max_logage"] + 1.))

def teff_cuts(track_label = "pm13_somers", cluster_label = "Lam Ori", n = 3.,
summed_posterior = False,
num_teff_cuts = 10):
	if summed_posterior:
		multiplied_string = "_multiplied"
	else:
		multiplied_string = "_summed"
	filename = os.path.dirname(os.path.abspath(__file__))\
	+ u"/Data/" + tracks[track_label]["load_filename"]
	lam_ori = np.genfromtxt(filename, delimiter="|")
	logTs = lam_ori.T[ordered_column_names.index("log_Teff")]
	sigma_logTs = lam_ori.T[ordered_column_names.index("log_Teff_e")]
	logLs = lam_ori.T[ordered_column_names.index("log_L_star")]
	sigma_logLs = lam_ori.T[ordered_column_names.index("log_L_star_e")]
	ra_deg = lam_ori.T[ordered_column_names.index("ra_deg")]
	dec_deg = lam_ori.T[ordered_column_names.index("dec_deg")]
	plx = lam_ori.T[ordered_column_names.index("plx")]
	plx_e = lam_ori.T[ordered_column_names.index("plx_e")]
	pm_ra = lam_ori.T[ordered_column_names.index("pm_ra")]
	pm_ra_e = lam_ori.T[ordered_column_names.index("pm_ra_e")]
	pm_dec = lam_ori.T[ordered_column_names.index("pm_dec")]
	pm_dec_e = lam_ori.T[ordered_column_names.index("pm_dec_e")]
	members = membership(ra_deg, dec_deg,
	plx, plx_e,
	pm_ra, pm_ra_e,
	pm_dec, pm_dec_e,
	n = n)
	print "Acceptance rate:", float(np.sum(members))/len(members)
	cond = np.all(np.logical_not([np.isnan(logTs), np.isnan(sigma_logTs),
	np.isnan(logLs), np.isnan(sigma_logLs), np.logical_not(members)]), axis=0)
	min_logT = np.nanmin(logTs[cond])-0.0001
	max_logT = np.nanmax(logTs[cond])+0.0001
	for idx in range(num_teff_cuts):
		left_logT = min_logT+float(idx)/num_teff_cuts*(max_logT - min_logT)
		right_logT = min_logT+float(idx+1)/num_teff_cuts*(max_logT - min_logT)
		cond = np.all(np.logical_not([np.isnan(logTs), np.isnan(sigma_logTs),
		np.isnan(logLs), np.isnan(sigma_logLs), logTs > right_logT, logTs <= left_logT,
		np.logical_not(members)]), axis=0)
		tracks[track_label]["tracks"]._evaluate(logTs[cond], logLs[cond],
		sigma_logTs=sigma_logTs[cond], sigma_logLs=sigma_logLs[cond],
		filename= tracks[track_label]["filename_prefix"] + "_" +\
		clusters[cluster_label]["label"] + multiplied_string +\
		"-teff-cut-" + str(idx) + ".png",
		title = clusters[cluster_label]["name"],
		track_label=tracks[track_label]["track_name"],
		min_logage = clusters[cluster_label]["min_logage"],
		max_logage = clusters[cluster_label]["max_logage"],
		min_mass = clusters[cluster_label]["min_mass"],
		max_mass = clusters[cluster_label]["max_mass"],
		num_bins = clusters[cluster_label]["num_bins"],
		sum_chisq = summed_posterior)

def run_teff_cuts_lam_ori(n = 3.):
	for track_label in tracks:
		for multiplied_bool in [True, False]:
			teff_cuts(track_label = track_label,
			cluster_label = "Lam Ori", n = 3.,
			summed_posterior = multiplied_bool)

#run_teff_cuts_lam_ori()
