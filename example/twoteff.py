import sippy, numpy as np, matplotlib.pyplot as plt

### Create 2-Teff PM
def plot_2teff():
    PMYoungInterpolator = sippy.interpolator.PecautMamajekYoungInterpolator
    PMExtendedDwarfInterpolator = sippy.interpolator.PecautMamajekExtendedDwarfInterpolator
    TTYoungInterpolator = sippy.interpolator.SpottedInterpolator(PMYoungInterpolator)
    TTExtendedDwarfInterpolator = sippy.interpolator.SpottedInterpolator(PMExtendedDwarfInterpolator)
    xs = np.linspace(3500, 5500)
    p0 = TTYoungInterpolator.on("Teff","B-V", f = 0)
    p0_25 = TTYoungInterpolator.on("Teff","B-V", f = 0.25)
    p0_5 = TTYoungInterpolator.on("Teff","B-V", f = 0.5)
    p0_75 = TTYoungInterpolator.on("Teff","B-V", f = 0.75)
    p1 = TTYoungInterpolator.on("Teff","B-V", f = 1)
    q = PMYoungInterpolator("Teff","B-V")
    plt.plot(xs, q(xs), label = "PM13")
    plt.plot(xs, p0(xs), label = "2TEFF-PM13, $f=0$")
    plt.plot(xs, p0_25(xs), label = "2TEFF-PM13, $f=0.25$, $x = 0.8$")
    plt.plot(xs, p0_5(xs), label = "2TEFF-PM13, $f=0.5$, $x = 0.8$")
    plt.plot(xs, p0_75(xs), label = "2TEFF-PM13, $f=0.75$, $x = 0.8$")
    plt.plot(xs, p1(xs), label = "2TEFF-PM13, $f=1$, $x = 0.8$")
    plt.legend();plt.show()
