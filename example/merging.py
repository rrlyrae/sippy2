from __future__ import absolute_import
import matplotlib.pyplot as plt
import numpy as np
from astropy import units as u
from sippy.interpolator.BaseInterpolator import *
from sippy.interpolator.PecautMamajekInterpolator import *
from sippy.interpolator.DavenportInterpolator import *
from sippy.interpolator.FangInterpolator import *
from sippy.interpolator.StephensInterpolator import *

def plot_colors_davenport(interpolation_kind=u"linear"):
    u"""An example usage of Davenport 2014's available colors."""
    pmy = PecautMamajekYoungInterpolator
    pmed = PecautMamajekExtendedDwarfInterpolator
    pmt = MergedFallbackInterpolator([pmy,pmed])
    d = DavenportInterpolator
    f = FangInterpolator
    li_J_H = LinkedInterpolator([pmt,d], [u"J-H"])
    mf_J_H = MergedFallbackInterpolator([f, li_J_H])
    color_calculator_J_H = {}
    for color in set(d.available_colors):
        if color != u"J-H":
            color_calculator_J_H[color] =\
            li_J_H.on(u"SpT", color, interpolation_kind = interpolation_kind)
    li_H_Ks = LinkedInterpolator([pmt,d], [u"H-Ks"])
    mf_H_Ks = MergedFallbackInterpolator([f, li_H_Ks])
    color_calculator_H_Ks = {}
    for color in set(d.available_colors):
        if color != u"H-Ks":
            color_calculator_H_Ks[color] =\
            li_H_Ks.on(u"SpT", color, interpolation_kind = interpolation_kind)

    xs = np.linspace(0.,100.,500)

    # Three subplots sharing both x/y axes
    f, (ax1, ax2, ax3) = plt.subplots(3, sharex=True, sharey=False)
    for color in color_calculator_J_H:
        ax1.plot(xs, color_calculator_J_H[color].evaluate(xs), label=color,
                 lw = 2)
    for color in color_calculator_H_Ks:
        ax2.plot(xs, color_calculator_H_Ks[color].evaluate(xs), label=color,
                  lw = 2)
    # Plot PM colors:
    ax3.plot(xs, pmt.on(u"SpT", u"J-H").evaluate(xs), lw = 2, label=u"PM13 J-H")
    ax3.plot(xs, pmt.on(u"SpT", u"H-Ks").evaluate(xs), lw = 2, label=u"PM13 H-Ks")
    ax1.set_ylabel(u"Color value, [J-H] fixed")
    ax3.set_xlabel(u"Numerical SpT, O5V = 5")
    ax2.set_ylabel(u"Color value, [H-Ks] fixed")
    ax3.set_ylabel(u"PM13 Color values")
    ax1.set_title(u'Demo, Extending Pecaut Mamajek 2013 Young+Dwarf colors with Davenport 2014')
    # Fine-tune figure; make subplots close to each other and hide x ticks for
    # all but bottom plot.
    f.subplots_adjust(hspace=0)
    plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)

    for ax in [ax1, ax2, ax3]:
        ax.legend()
    plt.show()


def plot_colors_davenport_fang(interpolation_kind=u"linear"):
    u"""An example usage of Davenport 2014's available colors."""
    pmy = PecautMamajekYoungInterpolator
    pmed = PecautMamajekExtendedDwarfInterpolator
    pmt = MergedFallbackInterpolator([pmy,pmed])
    d = DavenportInterpolator
    f = FangInterpolator
    li_J_H = LinkedInterpolator([pmt,d], [u"J-H"])
    mf_J_H = MergedFallbackInterpolator([f, li_J_H])
    color_calculator_J_H = {}
    for color in set(d.available_colors).intersection(set(f.available_colors)):
        if color != u"J-H":
            color_calculator_J_H[color] =\
            mf_J_H.on(u"SpT", color, interpolation_kind = interpolation_kind)
    li_H_Ks = LinkedInterpolator([pmt,d], [u"H-Ks"])
    mf_H_Ks = MergedFallbackInterpolator([f, li_H_Ks])
    color_calculator_H_Ks = {}
    for color in set(d.available_colors).intersection(set(f.available_colors)):
        if color != u"H-Ks":
            color_calculator_H_Ks[color] =\
            mf_H_Ks.on(u"SpT", color, interpolation_kind = interpolation_kind)
    #li_J_Ks = LinkedInterpolator([pmt,d], ["J-Ks"])

    xs = np.linspace(0.,100.,500)

    # Three subplots sharing both x/y axes
    f, (ax1, ax2, ax3) = plt.subplots(3, sharex=True, sharey=False)
    for color in color_calculator_J_H:
        ax1.plot(xs, color_calculator_J_H[color].evaluate(xs), label=color,
                 lw = 2)
    for color in color_calculator_H_Ks:
        ax2.plot(xs, color_calculator_H_Ks[color].evaluate(xs), label=color,
                  lw = 2)
    # Plot PM colors:
    ax3.plot(xs, pmt.on(u"SpT", u"J-H").evaluate(xs), lw = 2, label=u"PM13 J-H")
    ax3.plot(xs, pmt.on(u"SpT", u"H-Ks").evaluate(xs), lw = 2, label=u"PM13 H-Ks")
    ax1.set_ylabel(u"Color value, [J-H] fixed")
    ax3.set_xlabel(u"Numerical SpT, O5V = 5")
    ax2.set_ylabel(u"Color value, [H-Ks] fixed")
    ax3.set_ylabel(u"PM13 Color values")
    ax1.set_title(u'Demo, Extending Pecaut Mamajek 2013 Young+Dwarf colors with Fang 2017 and Davenport 2014')
    # Fine-tune figure; make subplots close to each other and hide x ticks for
    # all but bottom plot.
    f.subplots_adjust(hspace=0)
    plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)

    for ax in [ax1, ax2, ax3]:
        ax.legend()
    plt.show()

def plot_J_H_vs_i_z():
    d = DavenportInterpolator
    JH = np.linspace(0.1,0.9,500)
    plt.plot(JH,d(u"J-H",u"i-z")(JH))
    plt.show()


def plot_H_Ks_vs_u_g():
    d = DavenportInterpolator
    HKs = np.linspace(0.0,0.9,100)
    print d(u"H-Ks",u"e_u-g")(HKs)
    #plt.plot(HKs,d("H-Ks","u-g", interpolation_kind="linear")(HKs))
    plt.errorbar(HKs,d(u"H-Ks",u"u-g", interpolation_kind=u"linear").evaluate(HKs),\
                 yerr=d(u"H-Ks",u"e_u-g").evaluate(HKs))
    plt.xlabel(u"H-Ks")
    plt.ylabel(u"u-g")
    plt.title(u"Davenport colors")
    plt.show()

def plot_merged_spt_scale(interpolation_kind=u"linear"):
    u"""An example usage of a merged interpolator which uses PM13
    SpT - Teff relation to late M, then using Stephens+09 to late T."""
    PMD = PecautMamajekDwarfInterpolator("SpT", "Teff", interpolation_kind=interpolation_kind)
    Stephens = StephensInterpolator("SpT", "Teff", interpolation_kind=interpolation_kind)
    PMD_Stephens = MergedFallbackInstantiatedInterpolator([PMD, Stephens])
    xs = np.linspace(0.,100.)

    # Three subplots sharing both x/y axes
    f, (ax1, ax2) = plt.subplots(2, sharex=True, sharey=True)
    ax1.plot(xs, PMD.evaluate(xs), label=u"PM13 Dwarf",
             lw = 2, color = u"red")
    ax1.plot(xs, Stephens.evaluate(xs), label=u"Stephens+09",
             lw = 2, color = u"orange")
    ax2.plot(xs, PMD_Stephens.evaluate(xs), label=u"PM13D + Stephens+09",
             lw = 2, color = u"blue")
    ax1.set_title(u'Merged SpT-Teff Interpolator Demo')
    # Fine-tune figure; make subplots close to each other and hide x ticks for
    # all but bottom plot.
    f.subplots_adjust(hspace=0)
    plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)

    ax2.set_xlabel(u"Numerical SpT, M=60")
    for ax in [ax1, ax2]:
        ax.set_ylabel(u"$T_{eff}$ ($K$)")
        ax.semilogy()
        ax.legend()
    plt.show()
