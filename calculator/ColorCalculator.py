"""
sippy.calculator.ColorCalculator:
Python classes and methods for calculating stellar parameters with colors.

Philosophically, a Calculator is different from an Interpolator because it
performs some kind of nontrivial calculation. That being said, there are
Interpolators in this Calculator file because there are components that only
do interpolation that belong here.
"""

from __future__ import division
from __future__ import absolute_import
from sippy.calculator.BaseCalculator import *
from sippy.interpolator.PecautMamajekInterpolator import \
PecautMamajekDwarfInterpolator,\
PecautMamajekYoungInterpolator,\
PecautMamajekExtendedDwarfInterpolator
from sippy.interpolator.WhitneyInterpolator import WhitneyInterpolator
from sippy.interpolator.MathisInterpolator import MathisInterpolator
import os
import numpy as np
import scipy
import scipy.interpolate
import matplotlib.pyplot as plt
from astropy import units as u
import itertools as it
from itertools import izip
from sippy.interpolator import ExtendColorInterpolator, SDSSBaseColorInterpolator
from sippy.interpolator import SpTBaseInterpolator
import scipy.stats

class PhotometricSystem(object):
	"""A system of photometric bands with filter abbreviations.
	Loads default data from calculator/Data/photometric_bands.dat
	Which is a tab-separated file with filter abbreviations and effective
	wavelengths.

	The photometric system can be modified in place with methods in this
	class."""
	# Define the bands for the photometric system:
	bands = {}
	filter_width = {}
	flux_zeropoint_per_lambda = {}
	filename_bands = "photometric_bands.dat"
	filename_fluxes = "photometric_fluxes.dat"
	# The input file uses tab to delimit entries, so denote that:
	delimiter = "\t"
	# Define the datatypes:
	dtype = ["|S10", float]
	dtype_fluxes = ["|S10", float, float]

	# Default unit is set by the data file:
	_default_unit = u.micron
	def __init__(self):
		filename_bands = os.path.dirname(os.path.abspath(__file__))\
				 + "/Data/" + self.filename_bands
		data_bands = np.genfromtxt(filename_bands, comments = "#",
								   dtype=self.dtype)
		for data_band in data_bands:
			# Set the band abbreviation and effective wavelength dictionary
			# in self.bands.
			self.bands[data_band[0]] =\
			data_band[1]

		filename_fluxes = os.path.dirname(os.path.abspath(__file__))\
				 + "/Data/" + self.filename_fluxes
		data_fluxes = np.genfromtxt(filename_fluxes, comments = "#",
									dtype=self.dtype_fluxes)

		for data_flux in data_fluxes:
			# Set the band abbreviation and effective wavelength dictionary
			# in self.bands.
			self.filter_width[data_flux[0]] =\
			data_flux[1]
			self.flux_zeropoint_per_lambda[data_flux[0]] =\
			data_flux[2]
	def set_band(self, band, lambda_eff):
		"""Set the band named "band"'s effective wavelength to lambda_eff.
		Creates this band if it doesn't already exist."""
		self.bands[band] = lambda_eff
	def remove_band(self, band):
		"""Removes a band from this photometric system."""
		self.bands.pop(band, 0)
	def __call__(self, band):
		"""Returns the effective wavelength of the band(s) requested, or
		a KeyError if the band is not loaded in the PhotometricSystem.

		A neat application of this __call__ is to get a sorted list of bands
		with the following code:

		ps = sippy.calculator.ColorCalculator.PhotometricSystem()
		bands = ["", "V", "B", "J", "Ks", "H"]
		sorted(bands, key=ps)"""
		return self.__getitem__(band)*self._default_unit
	def __getitem__(self, band):
		"""Returns the effective wavelength of the band(s) requested, or
		a KeyError if the band is not loaded in the PhotometricSystem."""
		if hasattr(band, "__len__") and (not isinstance(band, basestring)):
			#This is a list!
			bands = band
			return [self.bands[band] for band in bands]
		else:
			return self.bands[band]
	def get_flux(self, band, mag):
		return self.flux_zeropoint_per_lambda[band] * np.power(10., -0.4*np.array(mag))
	def get_flux_units(self, band, mag):
		return self.get_flux(band, mag) * u.erg / u.cm / u.cm / u.s / u.Angstrom
	def get_abs_df_dmag(self, band, mag):
		return np.abs(self.flux_zeropoint_per_lambda[band] * np.log(10.) * (-0.4) * np.power(10., -0.4 * np.array(mag)))

class BandWeights(object):
	@staticmethod
	def log10_planck(wavelength_microns, teff):
		h_cgs = 6.6260755e-27 #erg * s = g*cm^2/s
		c_cgs = 2.99792458e10 #cm/s
		k_B_cgs = 1.380658e-16 #erg/K = g*cm^2/s^2/K
		micron_per_cm = 1e-4
		h = h_cgs/(micron_per_cm**2)
		c = c_cgs/micron_per_cm
		k_B = k_B_cgs/(micron_per_cm**2)
		result =\
		np.log10(2.)+np.log10(h)+2*np.log10(c)-5*np.log10(wavelength_microns)-\
		np.log10(np.exp(h*c/(wavelength_microns*k_B*teff)) - 1.)
		# wavelength = wavelength_microns / 1e6
		# result =\
		# np.log10(2.)+np.log10(h_cgs)+2*np.log10(c_cgs)-5*np.log10(wavelength)-\
		# np.log10(np.exp(h_cgs*c_cgs/(wavelength_microns*k_B_cgs*teff)) - 1.)
		return result
	@staticmethod
	def flux_weights(PhotometricSystem, teffs):
		"""Weight by lambda * f_lambda."""
		log_curr_weights = {}
		curr_weights = {}
		for band in PhotometricSystem.bands:
			### Do flux weighting by B_lambda
			log10_planck = BandWeights.log10_planck(PhotometricSystem[band],
						teffs)

			log_curr_weights[band] =\
			log10_planck
		for band in log_curr_weights:
			curr_weights[band] = np.power(10., log_curr_weights[band] - np.nanmax(log_curr_weights.values()))

		# curr_weights["r"] /= 1e3

		return curr_weights
	@staticmethod
	def flux_weights_preMS(PhotometricSystem, teffs):
		"""Weight by lambda * f_lambda."""
		log_curr_weights = {}
		curr_weights = {}
		for band in PhotometricSystem.bands:
			### Do flux weighting by B_lambda
			log10_planck = BandWeights.log10_planck(PhotometricSystem[band],
						teffs)

			log_curr_weights[band] =\
			log10_planck
			if PhotometricSystem[band] > 1.: # Suppress weights past 1 micron!
				log_curr_weights[band] -= 12. + 3.*(PhotometricSystem[band] - 1.)
		for band in log_curr_weights:
			curr_weights[band] = np.power(10., log_curr_weights[band] - np.nanmax(log_curr_weights.values()))

		return curr_weights
	@staticmethod
	def log_flux_weights(PhotometricSystem, teffs):
		curr_weights = {}
		for band in PhotometricSystem.bands:
			curr_weights[band] =\
			np.clip(\
						BandWeights.log10_planck(PhotometricSystem[band],
									np.array(teffs)), 0, None)

		return curr_weights
	@staticmethod
	def uniform_weights(PhotometricSystem, teffs):
		curr_weights = {}
		for band in PhotometricSystem.bands:
			if hasattr(teffs, "__len__"):
				curr_weights[band] = np.zeros(len(teffs)) + np.NaN
				curr_weights[band][np.logical_not(np.isnan(teffs))] = 1.
			else:
				curr_weights[band] = 1.

		return curr_weights

class ColorWeights(object):
	def __init__(self, scheme = "default"):
		self.scheme = scheme
	def __getitem__(self, color_text):
		band_A, band_B = color_text.split("-")
		if self.scheme == "default":
			### Ignore cross terms from different photometric systems.
			photometric_groups =\
			[["g","r","i","z","y"], ["U", "B", "V", "Rc", "Ic"], ["U", "B", "V", "Rk", "Ik"], ["J", "H", "Ks"], ["W1", "W2", "W3", "W4"]]
			terms_from_same_system = False
			for photometric_group in photometric_groups:
				if not terms_from_same_system:
					if band_A in photometric_group and band_B in photometric_group:
						terms_from_same_system = True
			if terms_from_same_system:
				return 1.
			else:
				return 0.
		elif self.scheme == "uniform":
			### Do nothing, the weight applied is simply 1.
			return 1.

class ColorMergeInterpolator(BaseInterpolator):
	"""Takes a list (or single) color interpolator, finds the selected color,
	and merges them with passed MergingInterpolator logic.

	The default MergingInterpolator is MergedFallbackInstantiatedInterpolator,
	and under this scenario, the ColorInterpolator takes the first color
	interpolator under the specified color (i.e., B-V), and at runtime finds all
	values that are NaN and "falls back" through the available list of
	color interpolators.

	Accepts "SpT" input at runtime."""
	#The input value for SpT actually isn't decided by ColorCalculator
	#because it is handled entirely by the ColorInterpolator(s).
	_default_input_unit = u.dimensionless_unscaled
	#The output unit is in mags.
	_default_output_unit = u.mag
	def __init__(self,
				 color,
				 PhotometricSystem = PhotometricSystem(),
				 ColorInterpolators = PecautMamajekYoungInterpolator,
				 MergingInterpolator = MergedFallbackInstantiatedInterpolator,
				 interpolation_kind = "linear",
				 output_unit = None,
				 input_val = "SpT"):
		if input_val == "Teff":
			self._default_input_unit = u.K
		# Set the units to output in
		self.output_unit = output_unit
		### If output unit is unset, set it to the default.
		if output_unit is None:
			self.output_unit = self._default_output_unit
		"""Accepts a color (i.e., "B-V"), PhotometricSystem,
		ColorInterpolators, and MergingInterpolator."""
		### Initialize the base color interpolators.
		self.interpolator = None
		# Save the color:
		self.color = color
		# Save the photometric system:
		self.PhotometricSystem = PhotometricSystem
		# Save the merging interpolator, which is a base interpolator
		# that accepts multiple interpolators and the submethod:
		# add_interpolator_last.
		self.MergingInterpolator = MergingInterpolator
		# Split the color into the constituent photometric bands.
		color_tuple = color.split("-")
		# Save the color tuple:
		self.color_tuple = color_tuple
		assert len(color_tuple) == 2, "Invalid color length chosen."

		### Calculate the shortest_path:
		if hasattr(ColorInterpolators, "on"):
			# If the ColorInterpolator has available_colors defined at
			# the class level, then we can simply look for it:
			if hasattr(ColorInterpolators, "available_colors"):
				AvailableColors = ColorInterpolators.available_colors
			else:
				if hasattr(ColorInterpolators,
						   "available_output_colors"):
					AvailableColors =\
					ColorInterpolators.available_output_colors
			ColorInterpolator = ColorInterpolators.on
		else:
			ColorInterpolator = ColorInterpolators
			AvailableColors = ColorInterpolators.available_colors
		color_list = []
		#Grab all available bands by splitting each column_header entry
		#by "-" and checking which ones have two items in the resulting
		#tuple.
		for interpolator_color in AvailableColors:
			interpolator_color_tuple = interpolator_color.split("-")
			if len(interpolator_color_tuple) == 2:
				color_list.append(interpolator_color_tuple)
		#Create the dictionary of graphs corresponding
		#to the directed traversals from photometric band to band.
		graph_dictionary = self.color_list_to_graphs(color_list)

		shortest_path =\
		self.find_shortest_path(graph_dictionary["bidirectional"],
		self.color_tuple[0],
		self.color_tuple[1])
		### Generate the color interpolators.
		self.interpolator =\
		self.generateColorInterpolator(shortest_path,
		ColorInterpolators,
		interpolation_kind = interpolation_kind,
		input_val = input_val)
	def _has_interpolator(self):
		return self.interpolator != None
	def _evaluate(self, spts):
		"""Returns the requested intrinsic color for a given SpT value."""
		if self.interpolator is not None:
			intrinsic_color_values = self.interpolator.evaluate(spts)
		else:
			intrinsic_color_values = np.zeros(len(spts))+np.nan
		return intrinsic_color_values
	def generateColorInterpolator(self, path, ColorInterpolators,
								  interpolation_kind = "linear",
								  input_val = "SpT"):
		"""Generates ColorInterpolators and returns the complete
		interpolator."""
		#Initialize the interpolator object:
		interpolator_obj = None
		#Find available colors in each interpolator programmatically.
		# For compatibility purposes, some subclasses of ColorInterpolators
		# have a method called "on" that is the equivalent of __init__
		# for most ColorInterpolators.
		# If a subclass has "on" defined, we should use "on" instead.
		# In all cases where subclasses have "on" defined, they should
		# have an internal list of interpolators.
		# This particular function is concerned with the available_colors
		# of the input interpolator.
		if hasattr(ColorInterpolators, "on"):
			# If the ColorInterpolator has available_colors defined at
			# the class level, then we can simply look for it:
			if hasattr(ColorInterpolators, "available_colors"):
				AvailableColors = ColorInterpolators.available_colors
			else:
				if hasattr(ColorInterpolators,
						   "available_output_colors"):
					AvailableColors =\
					ColorInterpolators.available_output_colors
			ColorInterpolator = ColorInterpolators.on
		else:
			ColorInterpolator = ColorInterpolators
			if hasattr(ColorInterpolators, "available_colors"):
				AvailableColors = ColorInterpolators.available_colors
			else:
				if hasattr(ColorInterpolators,
						   "available_output_colors"):
					AvailableColors =\
					ColorInterpolators.available_output_colors
		color_list = []
		#Grab all available bands by splitting each column_header entry
		#by "-" and checking which ones have two items in the resulting
		#tuple.
		for interpolator_color in AvailableColors:
			interpolator_color_tuple = interpolator_color.split("-")
			if len(interpolator_color_tuple) == 2:
				color_list.append(interpolator_color_tuple)
		#Create the dictionary of graphs corresponding
		#to the directed traversals from photometric band to band.
		graph_dictionary = self.color_list_to_graphs(color_list)
		#Initialize the interpolator for this color sequence:
		interpolator = None
		#This path needs to be converted into a series of colors and
		#positive or negative coefficients.
		if len(path) == 1:
			dummy_band = graph_dictionary["forward"][path[0]][0]
			path = [path[0], dummy_band, path[0]]
		if path is not None:
			for index in xrange(len(path) - 1):
				#Look at index, index + 1 as a single color
				#(i.e., band pair.)
				band_1 = path[index]
				band_2 = path[index + 1]
				#This is either a positive-facing path or negative
				#facing one.
				#Check by looking at the directional graphs:
				if band_1 in graph_dictionary["forward"]:
					if band_2 in graph_dictionary["forward"][band_1]:
						#The band_1-band_2 color exists.
						desired_color = "-".join([band_1, band_2])
						if interpolator is None:
							interpolator =\
							ColorInterpolator(input_val,
											  desired_color,
											  interpolation_kind =\
											  interpolation_kind)
						else:
							interpolator =\
							interpolator + \
							ColorInterpolator(input_val,
											  desired_color,
											  interpolation_kind =\
											  interpolation_kind)
				if band_1 in graph_dictionary["backward"]:
					if band_2 in graph_dictionary["backward"][band_1]:
						#The band_2-band_1 color exists.
						desired_color = "-".join([band_2, band_1])
						if interpolator is None:
							temp_interp =\
							ColorInterpolator(input_val,
											  desired_color,
											  interpolation_kind =\
											  interpolation_kind)
							interpolator =\
							NegativeInterpolator(temp_interp)
						else:
							interpolator =\
							interpolator - \
							ColorInterpolator(input_val,
											  desired_color,
											  interpolation_kind =\
											  interpolation_kind)
				else:
					#Band not found! Whatever.
					pass
			#We now have the correct interpolator for this particular
			#ColorInterpolator sequence.
			#We need to merge it or set it as the output interpolator:
			if interpolator_obj is None:
				#If the interpolator has not been set,
				interpolator_obj =\
				self.MergingInterpolator([interpolator])
			else:
				#The interpolator has been set, so we need to merge it.
				interpolator_obj.add_interpolator_last(interpolator)
		return interpolator_obj
	def find_shortest_path(self, graph, start, end, path=[]):
		"""Code from https://www.python.org/doc/essays/graphs/
		Returns the shortest path between nodes in a supplied graph.
		3/19/2018"""
		path = path + [start]
		if start == end:
			return path
		if not start in graph:
			return None
		shortest = None
		#print "start", start, "end", end, "path", path + [end]
		if end in graph[start]:
			return path + [end]
		for node in graph[start]:
			if node not in path:
				newpath = self.find_shortest_path(graph, node, end, path)
				if newpath:
					if not shortest or len(newpath) < len(shortest):
						shortest = newpath
		return shortest
	@staticmethod
	def color_list_to_graphs(color_list):
		"""Generates a dictionary of graphs from a list of colors.
		Returns a bidirectional graph under the key "bidirectional",
		A forward-traversing graph under the key "forward",
		A backward-traversing graph under the key "backward".

		Accepts a list of color tuples like ("B", "V").
		Assumes tuple is a directed edge from the first color to
		the second color."""
		#Create the graph dictionaries:
		bidirectional_graph = {}
		forward_facing_graph = {}
		backward_facing_graph = {}
		#Traverse forward from each band to the final band.
		for initial_band, final_band in color_list:
			if not initial_band in forward_facing_graph:
				bidirectional_graph[initial_band] = [final_band]
				forward_facing_graph[initial_band] = [final_band]
			else:
				bidirectional_graph[initial_band].append(final_band)
				forward_facing_graph[initial_band].append(final_band)
		#Create a graph, going in reverse.
		for final_band, initial_band in color_list:
			if not initial_band in backward_facing_graph:
				backward_facing_graph[initial_band] = [final_band]
			else:
				backward_facing_graph[initial_band].append(final_band)
			if not initial_band in bidirectional_graph:
				bidirectional_graph[initial_band] = [final_band]
			else:
				bidirectional_graph[initial_band].append(final_band)
		#Return a dictionary of color lists.
		return {"forward":forward_facing_graph,
				"backward":backward_facing_graph,
				"bidirectional":bidirectional_graph}

class ColorGraphInterpolator(ColorMergeInterpolator):
	"""Takes a list (or single) color interpolator, finds the selected color,
	and merges them with passed MergingInterpolator logic.

	The default MergingInterpolator is MergedFallbackInstantiatedInterpolator,
	and under this scenario, the ColorInterpolator takes the first color
	interpolator under the specified color (i.e., B-V), and at runtime finds all
	values that are NaN and "falls back" through the available list of
	color interpolators.

	Accepts "SpT" input at runtime."""
	#The input value for SpT actually isn't decided by ColorCalculator
	#because it is handled entirely by the ColorInterpolator(s).
	_default_input_unit = u.dimensionless_unscaled
	#The output unit is in mags.
	_default_output_unit = u.mag
	def __init__(self,
				 color,
				 PhotometricSystem = PhotometricSystem(),
				 ColorInterpolators = PecautMamajekYoungInterpolator,
				 MergingInterpolator = MergedFallbackInstantiatedInterpolator,
				 interpolation_kind = "linear",
				 output_unit = None,
				 input_val = "SpT"):
		if input_val == "Teff":
			self._default_input_unit = u.K
		# Set the units to output in
		self.output_unit = output_unit
		### If output unit is unset, set it to the default.
		if output_unit is None:
			self.output_unit = self._default_output_unit
		"""Accepts a color (i.e., "B-V"), PhotometricSystem,
		ColorInterpolators, and MergingInterpolator."""
		### Initialize the base color interpolators.
		self.interpolator = None
		# Save the color:
		self.color = color
		# Save the photometric system:
		self.PhotometricSystem = PhotometricSystem
		# Save the merging interpolator, which is a base interpolator
		# that accepts multiple interpolators and the submethod:
		# add_interpolator_last.
		self.MergingInterpolator = MergingInterpolator
		# Split the color into the constituent photometric bands.
		color_tuple = color.split("-")
		# Save the color tuple:
		self.color_tuple = color_tuple
		assert len(color_tuple) == 2, "Invalid color length chosen."

		### Calculate the shortest_path:
		if hasattr(ColorInterpolators, "on"):
			# If the ColorInterpolator has available_colors defined at
			# the class level, then we can simply look for it:
			if hasattr(ColorInterpolators, "available_colors"):
				AvailableColors = ColorInterpolators.available_colors
			else:
				if hasattr(ColorInterpolators,
						   "available_output_colors"):
					AvailableColors =\
					ColorInterpolators.available_output_colors
			ColorInterpolator = ColorInterpolators.on
		else:
			ColorInterpolator = ColorInterpolators
			if hasattr(ColorInterpolators, "available_colors"):
				AvailableColors = ColorInterpolators.available_colors
			else:
				if hasattr(ColorInterpolators,
						   "available_output_colors"):
					AvailableColors =\
					ColorInterpolators.available_output_colors
		color_list = []
		# #Grab all available bands by splitting each column_header entry
		# #by "-" and checking which ones have two items in the resulting
		# #tuple.
		# for interpolator_color in AvailableColors:
		# 	interpolator_color_tuple = interpolator_color.split("-")
		# 	if len(interpolator_color_tuple) == 2:
		# 		color_list.append(interpolator_color_tuple)
		# #Create the dictionary of graphs corresponding
		# #to the directed traversals from photometric band to band.
		# graph_dictionary = self.color_list_to_graphs(color_list)
		#
		# shortest_path =\
		# self.find_shortest_path(graph_dictionary["bidirectional"],
		# self.color_tuple[0],
		# self.color_tuple[1])
		### Generate the color interpolators.
		self.interpolators = []
		paths =\
		self.generate_unique_paths(AvailableColors,
		color_tuple[0], color_tuple[1])
		for path in paths:
			self.interpolators.append(\
			self.generateColorInterpolator(path,
			ColorInterpolators,
			interpolation_kind = interpolation_kind,
			input_val = input_val))
	def generate_unique_paths(self, AvailableColors, start, end, max_depth = 5):
		"""Generates a list of color paths by iterating over increasingly longer
		paths until all bands are used."""
		# print "path", start+"-"+end
		paths = [[start]]
		staging_paths = []
		working_paths = []

		color_list = []
		#Grab all available bands by splitting each column_header entry
		#by "-" and checking which ones have two items in the resulting
		#tuple.
		for interpolator_color in AvailableColors:
			interpolator_color_tuple = interpolator_color.split("-")
			if len(interpolator_color_tuple) == 2:
				color_list.append(interpolator_color_tuple)
		#Create the dictionary of graphs corresponding
		#to the directed traversals from photometric band to band.
		graph_dictionary = self.color_list_to_graphs(color_list)

		##### Note: we are not trying to find the directionality of this path
		##### That can be found by the parent routines.
		### Solve the trivial case:
		if end in graph_dictionary["bidirectional"][start]:
			paths[0].append(end)
			#print paths
			return paths

		### Breadth-first search:
		depth = np.max([len(x) for x in paths])
		appended = True
		while (not ((not (depth < max_depth)) or (not appended))):
			depth = np.max([len(x) for x in paths])
			appended = False
			for single_path in paths:
				if len(single_path) == depth:
					for node in graph_dictionary["bidirectional"][single_path[-1]]:
						if node not in single_path:
							staging_paths.append(single_path + [node])
							appended = True
			paths = staging_paths[:]
			# print paths, appended
		paths = []
		for path in staging_paths:
			if path[-1] == end:
				paths.append(path)

		#print start + "-" + end
		return paths
	def _evaluate(self, spts):
		"""Returns the requested intrinsic color for a given SpT value."""
		intrinsic_color_values = np.zeros(len(spts))+np.nan
		### Replace all NaN values as we iterate through the interpolators:
		# print len(self.interpolators)
		for interpolator in self.interpolators:
			if interpolator is not None:
				cond_nan = np.isnan(intrinsic_color_values)
				intrinsic_color_values[cond_nan] = interpolator.evaluate(spts[cond_nan])
		# print intrinsic_color_values
		return intrinsic_color_values

class ColorMergeAvCalculator(ColorMergeInterpolator, BaseCalculator):
	"""Calculates Av for a given color (ie. J-Ks).

	Takes a list (or single) color interpolator, finds the selected color,
	and merges them with passed MergingInterpolator logic.

	The default MergingInterpolator is MergedFallbackInstantiatedInterpolator,
	and under this scenario, the ColorInterpolator takes the first color
	interpolator under the specified color (i.e., B-V), and at runtime finds all
	values that are NaN and "falls back" through the available list of
	color interpolators.

	Uses these as the colors to use through the calculations to Av."""
	_inputs = ["photometry", "spt", "veiling"]
	_outputs = ["Av"]
	#The input unit is in mags.
	_default_input_unit = u.mag
	#The output unit is in mags.
	_default_output_unit = u.mag
	def __init__(self,
				 color,
				 PhotometricSystem = PhotometricSystem(),
				 ColorInterpolators = PecautMamajekYoungInterpolator,
				 MergingInterpolator = MergedFallbackInstantiatedInterpolator,
				 interpolation_kind = "linear",
				 output_unit = None,
				 input_val = "SpT"):
		if input_val == "Teff":
			self._default_input_unit = u.K
		# Set the units to output in
		self.output_unit = output_unit
		### If output unit is unset, set it to the default.
		if output_unit is None:
			self.output_unit = self._default_output_unit
		"""Accepts a color (i.e., "B-V"), PhotometricSystem,
		ColorInterpolators, and MergingInterpolator."""
		### Initialize the base color interpolators.
		self.interpolator = None
		# Save the color:
		self.color = color
		# Save the photometric system:
		self.PhotometricSystem = PhotometricSystem
		# Save the merging interpolator, which is a base interpolator
		# that accepts multiple interpolators and the submethod:
		# add_interpolator_last.
		self.MergingInterpolator = MergingInterpolator
		# Split the color into the constituent photometric bands.
		color_tuple = color.split("-")
		# Save the color tuple:
		self.color_tuple = color_tuple
		assert len(color_tuple) == 2, "Invalid color length chosen."

		### Calculate the shortest_path:
		if hasattr(ColorInterpolators, "on"):
			# If the ColorInterpolator has available_colors defined at
			# the class level, then we can simply look for it:
			if hasattr(ColorInterpolators, "available_colors"):
				AvailableColors = ColorInterpolators.available_colors
			else:
				if hasattr(ColorInterpolators,
						   "available_output_colors"):
					AvailableColors =\
					ColorInterpolators.available_output_colors
			ColorInterpolator = ColorInterpolators.on
		else:
			ColorInterpolator = ColorInterpolators
			AvailableColors = ColorInterpolators.available_colors
		color_list = []
		#Grab all available bands by splitting each column_header entry
		#by "-" and checking which ones have two items in the resulting
		#tuple.
		for interpolator_color in AvailableColors:
			interpolator_color_tuple = interpolator_color.split("-")
			if len(interpolator_color_tuple) == 2:
				color_list.append(interpolator_color_tuple)
		#Create the dictionary of graphs corresponding
		#to the directed traversals from photometric band to band.
		graph_dictionary = self.color_list_to_graphs(color_list)

		shortest_path =\
		self.find_shortest_path(graph_dictionary["bidirectional"],
		self.color_tuple[0],
		self.color_tuple[1])
		### Generate the color interpolators.
		self.interpolator =\
		self.generateColorInterpolator(shortest_path,
		ColorInterpolators,
		interpolation_kind = interpolation_kind,
		input_val = input_val)
	def _evaluate(self, spts, color_values):
		"""Returns a list of Avs for an input list of values for a color,
		(i.e., photometry for B - photometry for V), and SpT's."""
		#Find the wavelengths corresponding to the photometric bands in the
		#color:
		wavelength_1 = self.PhotometricSystem(self.color_tuple[0])
		wavelength_2 = self.PhotometricSystem(self.color_tuple[1])
		if self.interpolator is not None:
			Av = (np.array(color_values) -\
				  self.interpolator.evaluate(spts))/\
			(self.ExtinctionInterpolator(wavelength_1) -\
			 self.ExtinctionInterpolator(wavelength_2))
		else:
			Av = np.nan + color_values
		return Av

class ColorSEDAvCalculator(ColorMergeInterpolator, BaseCalculator):
	"""Calculates the Av for a given series of photometries and
	ColorInterpolators with the SED fitting technique on pairs of bands."""
	_inputs = ["photometry", "spt", "veiling"]
	_outputs = ["Av"]
	#The input unit is in mags.
	_default_input_unit = u.mag
	#The output unit is in mags.
	_default_output_unit = u.mag
	def __init__(self,
				 PhotometricSystem = PhotometricSystem(),
				 ColorInterpolators = ExtendColorInterpolator([\
				 PecautMamajekDwarfInterpolator,
				 SDSSBaseColorInterpolator(\
				 PecautMamajekDwarfInterpolator)]),
				 ExtinctionInterpolator =\
				 MergedFallbackInstantiatedInterpolator([
						 WhitneyInterpolator(interpolation_kind = "linear"),
						 MathisInterpolator(interpolation_kind = "linear")
					 ]),
				 MergingInterpolator = MergedFallbackInstantiatedInterpolator,
				 interpolation_kind = "linear",
				 output_unit = None,
				 pivot_band = "Y",
				 required_dof = 2,
				 flux_weighted = True,
				 log_flux_weighted = False,
				 preload_bands = ["B", "V", "Rc", "Ic", "J", "H", "Ks", "g", "r", "i", "z"],
				 **kwargs):
		"""Accepts a PhotometricSystem,
		ColorInterpolators, ExtinctionInterpolator, and MergingInterpolator.

		Usage: call evaluate() with values for a series of photometries
		(i.e., photometric values for "B", "V") and SpT's. Returns Av_SED.

		Generates color interpolators for use in calculating Av's.

		pivot_band is the name of a band defined in PhotometricSystem
		that is the preferred band wavelength to transform colors in
		ColorInterpolators to photometries. If pivot_band doesn't exist in
		the attached ColorInterpolators, or if it doesn't exist in the provided
		photometries, the code will look for an adjacent photometry in
		wavelength space, iterating until it finds one.

		i.e., pivot_band = "Y" at 1.035 microns, the code might choose
		"J" if "Y" doesn't exist, because it's at 1.25 microns. Then,
		It will use the J band to define ColorInterpolator's colors.

		NB: SED fitting is not possible with just one point. It must have
		At least two points in common, but three or more is better. The
		degrees of freedom is defined as the number of photometry points minus
		one.

		The parameter "required_dof" is used to set the number of degrees of
		freedom required before an Av is reported at all. Otherwise, it will
		report numpy.NaN.

		The parameter "flux_weighted" is used to set whether the weights of
		the chi-squared SED fitting process be fit with the
		Planck function for the Teff assumed from the SpT.

		"log_flux_weighted" corresponds to logarithmic Planck weights.

		Additional keyword arguments can be passed in."""
		# Set the units to output in
		self.output_unit = output_unit
		### If output unit is unset, set it to the default.
		if output_unit is None:
		   self.output_unit = self._default_output_unit
		### Initialize the base color interpolators.
		self.interpolator = None
		# Save the photometric system:
		self.PhotometricSystem = PhotometricSystem
		# Save the color interpolator:
		self.ColorInterpolators = ColorInterpolators
		# Save the extinction interpolator:
		self.ExtinctionInterpolator = ExtinctionInterpolator
		# Save the merging interpolator, which is a base interpolator
		# that accepts multiple interpolators and the submethod:
		# add_interpolator_last.
		self.MergingInterpolator = MergingInterpolator
		# Save interpolation_kind:
		self.interpolation_kind = interpolation_kind
		# Save pivot_band:
		self.pivot_band = pivot_band
		# Save the required DOF:
		self.required_dof = required_dof
		# Save the flux weighting preference:
		self.flux_weighted = flux_weighted
		# Save the log flux weighting preference:
		self.log_flux_weighted = log_flux_weighted
		# Save kwargs:
		self.kwargs = kwargs

		# Obtain SpT -> Teff interpolators for the flux-weighting.
		spt_teff_interpolators = []
		if hasattr(ColorInterpolators, "on"):
			spt_teff_interpolators.append(ColorInterpolators.on("SpT", "Teff",
															**kwargs))
		else:
			spt_teff_interpolators.append(ColorInterpolators("SpT", "Teff",
															**kwargs))
		#print spt_teff_interpolators
		self.spt_teff_interpolator =\
		self.MergingInterpolator(spt_teff_interpolators, **kwargs)

		self.color_graph_interpolators = {}
		self.extinction_vals = {}
		self.partial_ij = {}

		spts_range = np.linspace(10.,80.,50)

		### "Preload" ColorGraphs and Partials

		for selection_band in preload_bands:
			self.extinction_vals[selection_band] =\
			ExtinctionInterpolator.evaluate(PhotometricSystem(selection_band))
			for secondary_band in preload_bands:
				if secondary_band != selection_band:
					color_text = selection_band + "-" + secondary_band
					# print "Creating ColorGraph", color_text
					self.color_graph_interpolators[color_text] =\
					ColorGraphInterpolator(color_text,
					PhotometricSystem =\
					PhotometricSystem,
					ColorInterpolators =\
					ColorInterpolators,
					MergingInterpolator =\
					MergingInterpolator,
					interpolation_kind =\
					interpolation_kind,
					output_unit = output_unit)

					partial_ij =\
					self.partial_intrinsic_color_partial_SpT(spts_range,
					self.color_graph_interpolators[color_text], spt_seps = [0.5])

					cond_partial =\
					np.logical_not(np.isnan(partial_ij))

					def helper_fn(interpolating_fn):
						def wrapped(spt):
							return interpolating_fn(self.ColorInterpolators.interpolators[0].get_num_SpTs(spt))
						return wrapped

					if np.sum(cond_partial) > 0:
						self.partial_ij[color_text] =\
						helper_fn(scipy.interpolate.interp1d(spts_range[cond_partial],
						partial_ij[cond_partial], bounds_error = False))
	@staticmethod
	def cdf_from_pdf(xs, pdf):
		cdf = np.zeros_like(pdf)
		for idx in range(len(pdf)):
				cdf[idx] = np.trapz(pdf[:idx+1], xs[:idx+1])
		cdf /= cdf[-1]
		return cdf
	def confidence(self, xs, p_xs, interv):
		"""Function calculates a numerical confidence interval.
		It searches from the left-hand side and interpolates to right.
		arguments: xs - parameter array
		p_xs - probability density array
		interv - proportion of the conf. interv.
		Modified to interpolate and resample."""
		if len(xs) < 2 or len(p_xs) < 2 or np.any(np.isnan(xs)) or np.any(np.isnan(p_xs)):
			return [np.NaN, np.NaN, np.NaN]
		#from matplotlib import pyplot as plt
		#xs = np.array([x[0] for x in sorted(zip(xs,p_xs),key=lambda y:y[1])])
		#p_xs = np.array(sorted(p_xs)).flatten()
		#print xs
		#print xs[1:] - xs[:-1]
		#print xs.ndim
		xs_new = np.arange(np.min(xs), np.max(xs), np.min(xs[1:]-xs[0:-1])/2.)
		p_xs_new = np.interp(xs_new, xs, p_xs)
		cdf = self.cdf_from_pdf(xs_new, p_xs_new) #Create CDF.
		#plt.plot(xs_new, cdf)
		#plt.show()
		xs_sort = np.array([x[0] for x in sorted(zip(xs_new, cdf),key=lambda y:y[1])])
		cdf_sort = np.array(sorted(cdf))
		#cdf_mask = cdf_sort[1:] - cdf_sort[:-1] > 0.
		#cdf_mask = np.array([True] + cdf_mask.tolist())
		#xs_sort = xs_sort[cdf_mask]
		#cdf_sort = cdf_sort[cdf_mask]
		#print cdf_sort[1:] - cdf_sort[:-1]
		cdf_interp = scipy.interpolate.interp1d(cdf_sort, xs_sort)
		#interpolate on the y-axis so we can put a p level and get an x.
		best_confidence_left = -np.inf
		best_confidence_right = np.inf
		for idx in range(len(xs_sort)):
			bottom_p = cdf_sort[idx] #cdf_interp(xs_sort[idx]) #bottom prob value
			confidence_left = xs_sort[idx] #bottom x value
			if bottom_p + interv < 1.:
				confidence_right = cdf_interp(bottom_p + interv)
				#find the appropriate interpolated x value
				#that corresponds to the other side of the conf interv.
				if best_confidence_right - best_confidence_left > \
				confidence_right - confidence_left:
					best_confidence_left = confidence_left
					#replace "best" left
					best_confidence_right = float(confidence_right)
					#replace "best" right
		mean_conf = xs_new[np.argmax(p_xs_new)]
		if best_confidence_left > mean_conf:
			best_confidence_left = mean_conf
		if best_confidence_right < mean_conf:
			best_confidence_right = mean_conf
		return [best_confidence_left, mean_conf,
		best_confidence_right]
		#return left, peak location, and right confidence intervals.
	def find_confidence_interval(self,best_fit_Av,Alambda_div_Av_dictionary,
	intrinsic_spectrum,photometries,photometric_errors,weights,curr_weights,num_bands,
	num_samples = 34, av_min = 0., av_max = 10.,
	total_log_interval_Av = 3.):#,si=None):
		### Create a log list, called 'log_Avs', from a piecewise interval
		### with log boundings centered at best_fit_Av.
		# log_center_Av = np.log10(best_fit_Av)
		# if not np.isfinite(log_center_Av):
		# 	print "log center Av changed to -2"
		# 	log_center_Av = -2
		# log_left_Av = np.log10(av_min+(best_fit_Av - av_min)*0.1)
		# log_right_Av = np.log10(av_max+(best_fit_Av - av_max)*0.1)
		# log_left_list_Av = [av_min] + np.logspace(np.max([log_center_Av-total_log_interval_Av/2., log_left_Av]),log_center_Av,endpoint=False,num=num_samples/2-1).tolist()
		# log_right_list_Av = np.logspace(log_center_Av,np.min([log_center_Av+total_log_interval_Av/2., log_right_Av]),endpoint=True,num=num_samples/2-1).tolist() + [av_max]
		# log_interval_Avs = np.array(log_left_list_Av + log_right_list_Av)
		log_interval_Avs = np.logspace(-2.,np.log10(av_max),num=num_samples)
		band_chisq = {}
		for band in photometries:
			band_chisq[band] =\
			((log_interval_Avs*(Alambda_div_Av_dictionary[band])+\
			intrinsic_spectrum[band] -\
			photometries[band])/\
			photometric_errors[band])**2
		if num_bands > 1:
			chisq_results =\
			np.nansum([band_chisq[band]*curr_weights[band]\
			for band in photometries], axis=0)/(weights + 1e-16)
			ci_left, ci_middle, ci_right =\
			self.confidence(log_interval_Avs,np.exp(-chisq_results),0.68)
			confidence_interval =\
			(ci_left, ci_right)
		else:
			confidence_interval = (np.NaN, np.NaN)

		#print log_interval_Avs
		#print np.exp(-chisq_results)
		# plt.scatter(log_interval_Avs,np.exp(-chisq_results))
		# if si is not None:
		# 	plt.title(si)
		# plt.show()
		return confidence_interval
	@staticmethod
	def sum_of_squares(x, *args):
		return np.nansum(np.square(np.array(args) - x))
	# def chisq_from_av_old(self, Av_guess, spts, photometries, photometric_errors,
	# curr_weights, PhotometricSystem = PhotometricSystem(),
	# ColorInterpolators = PecautMamajekYoungInterpolator,
	# ExtinctionInterpolator =\
	# MergedFallbackInstantiatedInterpolator([
	# WhitneyInterpolator(interpolation_kind = "linear"),
	# MathisInterpolator(interpolation_kind = "linear")]),
	# MergingInterpolator = MergedFallbackInstantiatedInterpolator,
	# interpolation_kind = "linear",
	# output_unit = None):
	# 	array_len = len(list(photometries.values())[0])
	# 	chisq = np.zeros(array_len)
	# 	weights = np.zeros(array_len)
	# 	chisq_bands = {} # This is defined as the chisq statistic for any given band
	# 	num_dof = np.zeros(array_len)
	#
	# 	for selection_band in photometries:
	# 		chisq_bands[selection_band] = np.zeros(array_len)
	# 		for secondary_band in photometries:
	# 			if secondary_band != selection_band:
	# 				color_text = selection_band + "-" + secondary_band
	# 				if not (color_text in self.color_graph_interpolators):
	# 					self.color_graph_interpolators[color_text] =\
	# 					ColorGraphInterpolator(color_text,
	# 					PhotometricSystem =\
	# 					PhotometricSystem,
	# 					ColorInterpolators =\
	# 					ColorInterpolators,
	# 					MergingInterpolator =\
	# 					MergingInterpolator,
	# 					interpolation_kind =\
	# 					interpolation_kind,
	# 					output_unit = output_unit)
	#
	# 				intrinsic_color =\
	# 				self.color_graph_interpolators[color_text].evaluate(spts)
	#
	# 				if not (selection_band in self.extinction_vals):
	# 					self.extinction_vals[selection_band] =\
	# 					ExtinctionInterpolator.evaluate(PhotometricSystem(selection_band))
	# 				if not (secondary_band in self.extinction_vals):
	# 					self.extinction_vals[secondary_band] =\
	# 					ExtinctionInterpolator.evaluate(PhotometricSystem(secondary_band))
	#
	# 				chisq_value =\
	# 				((-Av_guess*\
	# 				( self.extinction_vals[selection_band] \
	# 				- self.extinction_vals[secondary_band])+\
	# 				(photometries[selection_band] -\
	# 				photometries[secondary_band]) -\
	# 				(intrinsic_color))/\
	# 				 (np.sqrt(np.square(photometric_errors[selection_band]) +\
	# 				 np.square(photometric_errors[secondary_band]))))**2
	# 				print "Av chisq", selection_band, secondary_band, chisq_value
	#
	# 				for chisq_idx, chisq_val in enumerate(chisq_value):
	# 					if not np.isnan(chisq_val):
	# 						## Add this chisq to the bulk chisq:
	# 						chisq[chisq_idx] += 0.5*curr_weights[selection_band][chisq_idx]*curr_weights[secondary_band][chisq_idx]*chisq_val
	# 						## Add this chisq to the band chisq:
	# 						chisq_bands[selection_band][chisq_idx] += curr_weights[selection_band][chisq_idx]*curr_weights[secondary_band][chisq_idx]*chisq_val
	# 						## Add this as a weights term:
	# 						weights[chisq_idx] += curr_weights[selection_band][chisq_idx]*curr_weights[secondary_band][chisq_idx]
	# 						## Note this in DOF:
	# 						num_dof[chisq_idx] += 0.5
	#
	# 	chisq /= (weights - 1e-16)
	#
	# 	### Make sure to give a NaN chisq if the num_dof is 0.
	# 	cond_num_dof_0 = num_dof < 1e-16
	# 	chisq[cond_num_dof_0] = np.NaN
	#
	# 	### Create a dictionary of return values:
	# 	return_dict = {}
	# 	return_dict["weights"] = weights
	# 	return_dict["num_dof"] = num_dof
	# 	return_dict["chisq_bands"] = chisq_bands
	# 	return_dict["chisq"] = chisq
	#
	# 	return return_dict
	# def chisq_from_av_SED(self, Av_guess, spts, photometries, photometric_errors,
	# curr_weights, PhotometricSystem = PhotometricSystem(),
	# ColorInterpolators = PecautMamajekYoungInterpolator,
	# ExtinctionInterpolator =\
	# MergedFallbackInstantiatedInterpolator([
	# WhitneyInterpolator(interpolation_kind = "linear"),
	# MathisInterpolator(interpolation_kind = "linear")]),
	# MergingInterpolator = MergedFallbackInstantiatedInterpolator,
	# interpolation_kind = "linear",
	# output_unit = None):
	# 	array_len = len(list(photometries.values())[0])
	# 	chisq = np.zeros(array_len)
	# 	chisq_num_sqrt = np.zeros(array_len)
	# 	chisq_denom = np.zeros(array_len)
	# 	# weights = np.zeros(array_len)
	# 	chisq_bands = {} # This is defined as the chisq statistic for any given band
	# 	num_dof = np.zeros(array_len)
	#
	# 	for selection_band in photometries:
	# 		chisq_bands[selection_band] = np.zeros(array_len)
	# 		chisq_denom_per_band_sqrt = np.zeros(array_len)
	# 		for secondary_band in photometries:
	# 			if secondary_band != selection_band:
	# 				color_text = selection_band + "-" + secondary_band
	# 				if not (color_text in self.color_graph_interpolators):
	# 					self.color_graph_interpolators[color_text] =\
	# 					ColorGraphInterpolator(color_text,
	# 					PhotometricSystem =\
	# 					PhotometricSystem,
	# 					ColorInterpolators =\
	# 					ColorInterpolators,
	# 					MergingInterpolator =\
	# 					MergingInterpolator,
	# 					interpolation_kind =\
	# 					interpolation_kind,
	# 					output_unit = output_unit)
	#
	# 				intrinsic_color =\
	# 				self.color_graph_interpolators[color_text].evaluate(spts)
	#
	# 				if not (selection_band in self.extinction_vals):
	# 					self.extinction_vals[selection_band] =\
	# 					ExtinctionInterpolator.evaluate(PhotometricSystem(selection_band))
	# 				if not (secondary_band in self.extinction_vals):
	# 					self.extinction_vals[secondary_band] =\
	# 					ExtinctionInterpolator.evaluate(PhotometricSystem(secondary_band))
	#
	# 				Av_resids =\
	# 				(-Av_guess*\
	# 				( self.extinction_vals[selection_band] \
	# 				- self.extinction_vals[secondary_band])+\
	# 				(photometries[selection_band] -\
	# 				photometries[secondary_band]) -\
	# 				(intrinsic_color))#/\
	# 				 # (np.sqrt(np.square(photometric_errors[selection_band]) +\
	# 				 # np.square(photometric_errors[secondary_band]))))**2
	#
	# 				for chisq_idx, Av_resid in enumerate(Av_resids):
	# 					if not np.isnan(Av_resid):# and not np.isnan(photometric_errors[selection_band][chisq_idx]):
	# 						## Add this chisq to the bulk chisq:
	# 						chisq_num_sqrt[chisq_idx] +=\
	# 						0.5*curr_weights[selection_band][chisq_idx]*\
	# 						curr_weights[secondary_band][chisq_idx]*\
	# 						np.square(Av_resid)
	#
	# 						chisq_denom_per_band_sqrt[chisq_idx] +=\
	# 						0.5*2.*np.abs(curr_weights[selection_band][chisq_idx]*\
	# 						curr_weights[secondary_band][chisq_idx]*\
	# 						Av_resid*photometric_errors[selection_band][chisq_idx])
	#
	# 						## Add this chisq to the band chisq:
	# 						# chisq_bands[selection_band][chisq_idx] += curr_weights[selection_band][chisq_idx]*curr_weights[secondary_band][chisq_idx]*Av_resid
	# 						## Add this as a weights term:
	# 						# weights[chisq_idx] += curr_weights[selection_band][chisq_idx]*curr_weights[secondary_band][chisq_idx]
	# 						## Note this in DOF:
	# 						num_dof[chisq_idx] += 0.5
	#
	# 		chisq_denom +=\
	# 		np.square(chisq_denom_per_band_sqrt)
	#
	#
	# 	# chisq /= (weights - 1e-16)
	# 	chisq = np.square(chisq_num_sqrt)/chisq_denom
	#
	# 	### Make sure to give a NaN chisq if the num_dof is 0.
	# 	cond_num_dof_0 = num_dof < 1e-16
	# 	chisq[cond_num_dof_0] = np.NaN
	#
	# 	### Create a dictionary of return values:
	# 	return_dict = {}
	# 	# return_dict["weights"] = weights
	# 	return_dict["num_dof"] = num_dof
	# 	return_dict["chisq_bands"] = chisq_bands
	# 	return_dict["chisq"] = chisq
	#
	# 	return return_dict

	def chisq_from_av_old(self, Av_guess, spts, spt_errors, photometries, photometric_errors,
	curr_weights, color_weights, PhotometricSystem = PhotometricSystem(),
	ColorInterpolators = PecautMamajekYoungInterpolator,
	ExtinctionInterpolator =\
	MergedFallbackInstantiatedInterpolator([
	WhitneyInterpolator(interpolation_kind = "linear"),
	MathisInterpolator(interpolation_kind = "linear")]),
	MergingInterpolator = MergedFallbackInstantiatedInterpolator,
	interpolation_kind = "linear",
	output_unit = None):
		array_len = len(list(photometries.values())[0])
		chisq = np.zeros(array_len)
		# weights = np.zeros(array_len)
		# chisq_bands = {} # This is defined as the chisq statistic for any given band
		num_dof = np.zeros(array_len)

		summed_weights_single = np.zeros(array_len)
		number_weights_single = np.zeros(array_len)
		summed_weights_double = np.zeros(array_len)
		number_weights_double = np.zeros(array_len)

		### Construct the weights
		for selection_band in photometries:
			cond_selection = np.logical_not(np.isnan(photometries[selection_band]))
			summed_weights_single[cond_selection] += curr_weights[selection_band][cond_selection]
			number_weights_single[cond_selection] += 1
			for secondary_band in photometries:
				if secondary_band != selection_band:
					cond_selection =\
					np.logical_not(np.logical_or(\
					np.isnan(photometries[selection_band]),
					np.isnan(photometries[secondary_band])))
					summed_weights_double[cond_selection] +=\
					np.abs(\
					ExtinctionInterpolator.evaluate(PhotometricSystem(selection_band)) -\
					ExtinctionInterpolator.evaluate(PhotometricSystem(secondary_band)))
					number_weights_double[cond_selection] += 0.5

		for selection_band in photometries:
			# chisq_bands[selection_band] = np.zeros(array_len)
			for secondary_band in photometries:
				if secondary_band != selection_band:
					color_text = selection_band + "-" + secondary_band
					if not (color_text in self.color_graph_interpolators):
						self.color_graph_interpolators[color_text] =\
						ColorGraphInterpolator(color_text,
						PhotometricSystem =\
						PhotometricSystem,
						ColorInterpolators =\
						ColorInterpolators,
						MergingInterpolator =\
						MergingInterpolator,
						interpolation_kind =\
						interpolation_kind,
						output_unit = output_unit)

					intrinsic_color =\
					self.color_graph_interpolators[color_text].evaluate(spts)

					if not (selection_band in self.extinction_vals):
						self.extinction_vals[selection_band] =\
						ExtinctionInterpolator.evaluate(PhotometricSystem(selection_band))
					if not (secondary_band in self.extinction_vals):
						self.extinction_vals[secondary_band] =\
						ExtinctionInterpolator.evaluate(PhotometricSystem(secondary_band))

					sigma_Av_squared =\
					((np.square(photometric_errors[selection_band]/\
					 curr_weights[selection_band]*summed_weights_single/number_weights_single) +\
					 np.square(photometric_errors[secondary_band]/\
					 curr_weights[secondary_band]*summed_weights_single/number_weights_single) +\
					 np.square(self.partial_intrinsic_color_partial_SpT(spts,
 					 self.color_graph_interpolators[color_text]))*\
 					 np.square(spt_errors))/\
					np.square\
					( self.extinction_vals[selection_band] \
					- self.extinction_vals[secondary_band]))*np.square(color_weights[color_text])

					# print "sigma_Av_squared", sigma_Av_squared

					Av_resids =\
					np.square(-Av_guess +\
					((photometries[selection_band] -\
					photometries[secondary_band]) -\
					(intrinsic_color))/\
					( self.extinction_vals[selection_band] \
					- self.extinction_vals[secondary_band]))*np.square(color_weights[color_text])

					# print "zip(sigma_Av_squared, Av_resids)", zip(sigma_Av_squared, Av_resids)

					for chisq_idx, Av_resid in enumerate(Av_resids):
						if not np.isnan(Av_resid/sigma_Av_squared[chisq_idx]):
							print "chisq_idx", chisq_idx, "color_text", color_text
							print "Av_resid", np.sqrt(Av_resid)
							print "sig_Av", np.sqrt(sigma_Av_squared[chisq_idx])
							print "fractional_spt", np.square(self.partial_intrinsic_color_partial_SpT(spts,
							self.color_graph_interpolators[color_text]))*\
							np.square(spt_errors)/(
							(np.square(photometric_errors[selection_band]/\
							 curr_weights[selection_band]*summed_weights_single/number_weights_single) +\
							 np.square(photometric_errors[secondary_band]/\
							 curr_weights[secondary_band]*summed_weights_single/number_weights_single)) +\
							 np.square(self.partial_intrinsic_color_partial_SpT(spts,
							 self.color_graph_interpolators[color_text]))*\
							 np.square(spt_errors))
							print "chisq", Av_resid/sigma_Av_squared[chisq_idx]
							chisq[chisq_idx] += Av_resid/sigma_Av_squared[chisq_idx]
							num_dof[chisq_idx] += 1.

		### Make sure to give a NaN chisq if the num_dof is 0.
		cond_num_dof_0 = num_dof < 1e-16
		chisq[cond_num_dof_0] = np.NaN

		### Create a dictionary of return values:
		return_dict = {}
		# return_dict["weights"] = weights
		return_dict["num_dof"] = num_dof
		# return_dict["chisq_bands"] = chisq_bands
		return_dict["chisq"] = chisq

		print 'return_dict["chisq"]', return_dict["chisq"]
		print 'return_dict["num_dof"]', return_dict["num_dof"]

		return return_dict
	def chisq_from_av(self, Av_guess, spts, spt_errors, photometries, photometric_errors,
	curr_weights, color_weights, PhotometricSystem = PhotometricSystem(),
	ColorInterpolators = PecautMamajekYoungInterpolator,
	ExtinctionInterpolator =\
	MergedFallbackInstantiatedInterpolator([
	WhitneyInterpolator(interpolation_kind = "linear"),
	MathisInterpolator(interpolation_kind = "linear")]),
	MergingInterpolator = MergedFallbackInstantiatedInterpolator,
	interpolation_kind = "linear",
	output_unit = None):
		array_len = len(list(photometries.values())[0])
		chisq = np.zeros(array_len)
		chisq_band = {}
		# weights = np.zeros(array_len)
		# chisq_bands = {} # This is defined as the chisq statistic for any given band
		num_dof = np.zeros(array_len)

		summed_weights_single = np.zeros(array_len)
		number_weights_single = np.zeros(array_len)
		summed_weights_double = np.zeros(array_len)
		number_weights_double = np.zeros(array_len)

		### Construct the weights
		for selection_band in photometries:
			chisq_band[selection_band] = {}
			cond_selection = np.logical_not(np.isnan(photometries[selection_band]))
			summed_weights_single[cond_selection] += curr_weights[selection_band][cond_selection]
			number_weights_single[cond_selection] += 1
			for secondary_band in photometries:
				if secondary_band != selection_band:
					cond_selection =\
					np.logical_not(np.logical_or(\
					np.isnan(photometries[selection_band]),
					np.isnan(photometries[secondary_band])))
					summed_weights_double[cond_selection] +=\
					np.abs(\
					ExtinctionInterpolator.evaluate(PhotometricSystem(selection_band)) -\
					ExtinctionInterpolator.evaluate(PhotometricSystem(secondary_band)))
					number_weights_double[cond_selection] += 0.5

		for selection_band in photometries:
			#Creating a chisq for this selection_band.
			# chisq_bands[selection_band] = np.zeros(array_len)
			for secondary_band in photometries:
				if secondary_band != selection_band:
					chisq_band[selection_band][secondary_band] = np.zeros(array_len) + np.NaN
					color_text = selection_band + "-" + secondary_band
					if not (color_text in self.color_graph_interpolators):
						self.color_graph_interpolators[color_text] =\
						ColorGraphInterpolator(color_text,
						PhotometricSystem =\
						PhotometricSystem,
						ColorInterpolators =\
						ColorInterpolators,
						MergingInterpolator =\
						MergingInterpolator,
						interpolation_kind =\
						interpolation_kind,
						output_unit = output_unit)

					intrinsic_color =\
					self.color_graph_interpolators[color_text].evaluate(spts)

					if not (selection_band in self.extinction_vals):
						self.extinction_vals[selection_band] =\
						ExtinctionInterpolator.evaluate(PhotometricSystem(selection_band))
					if not (secondary_band in self.extinction_vals):
						self.extinction_vals[secondary_band] =\
						ExtinctionInterpolator.evaluate(PhotometricSystem(secondary_band))

					sigma_Av_squared =\
					((np.square(photometric_errors[selection_band]/\
					 curr_weights[selection_band]*summed_weights_single/number_weights_single) +\
					 np.square(photometric_errors[secondary_band]/\
					 curr_weights[secondary_band]*summed_weights_single/number_weights_single) +\
					 np.square(self.partial_intrinsic_color_partial_SpT(spts,
 					 self.color_graph_interpolators[color_text]))*\
 					 np.square(spt_errors))/\
					np.square\
					( self.extinction_vals[selection_band] \
					- self.extinction_vals[secondary_band]))*np.square(color_weights[color_text])

					# print "sigma_Av_squared", sigma_Av_squared

					Av_resids =\
					np.square(-Av_guess +\
					((photometries[selection_band] -\
					photometries[secondary_band]) -\
					(intrinsic_color))/\
					( self.extinction_vals[selection_band] \
					- self.extinction_vals[secondary_band]))*np.square(color_weights[color_text])

					# print "zip(sigma_Av_squared, Av_resids)", zip(sigma_Av_squared, Av_resids)

					for chisq_idx, Av_resid in enumerate(Av_resids):
						if not np.isnan(Av_resid/sigma_Av_squared[chisq_idx]):
							# print "chisq_idx", chisq_idx, "color_text", color_text
							# print "Av_resid", np.sqrt(Av_resid)
							# print "sig_Av", np.sqrt(sigma_Av_squared[chisq_idx])
							# print "fractional_spt", np.square(self.partial_intrinsic_color_partial_SpT(spts,
							# self.color_graph_interpolators[color_text]))*\
							# np.square(spt_errors)/(
							# (np.square(photometric_errors[selection_band]/\
							#  curr_weights[selection_band]*summed_weights_single/number_weights_single) +\
							#  np.square(photometric_errors[secondary_band]/\
							#  curr_weights[secondary_band]*summed_weights_single/number_weights_single)) +\
							#  np.square(self.partial_intrinsic_color_partial_SpT(spts,
							#  self.color_graph_interpolators[color_text]))*\
							#  np.square(spt_errors))
							# print "chisq", Av_resid/sigma_Av_squared[chisq_idx]
							chisq[chisq_idx] += Av_resid/sigma_Av_squared[chisq_idx]
							num_dof[chisq_idx] += 1.

							chisq_band[selection_band][secondary_band][chisq_idx] = Av_resid/sigma_Av_squared[chisq_idx]

		### Make sure to give a NaN chisq if the num_dof is 0.
		cond_num_dof_0 = num_dof < 1e-16
		chisq[cond_num_dof_0] = np.NaN

		### Create a dictionary of return values:
		return_dict = {}
		# return_dict["weights"] = weights
		return_dict["num_dof"] = num_dof
		# return_dict["chisq_bands"] = chisq_bands
		return_dict["chisq"] = chisq
		return_dict["chisq_band"] = chisq_band

		print 'return_dict["chisq"]', return_dict["chisq"]
		print 'return_dict["chisq_band"]', return_dict["chisq_band"]
		print 'return_dict["num_dof"]', return_dict["num_dof"]

		return return_dict
	def get_Av_solution_mean_weighted(self, spts, spt_errors, photometries, photometric_errors,
	curr_weights, color_weights, PhotometricSystem = PhotometricSystem(),
	ColorInterpolators = PecautMamajekYoungInterpolator,
	ExtinctionInterpolator =\
	MergedFallbackInstantiatedInterpolator([
	WhitneyInterpolator(interpolation_kind = "linear"),
	MathisInterpolator(interpolation_kind = "linear")]),
	MergingInterpolator = MergedFallbackInstantiatedInterpolator,
	interpolation_kind = "linear",
	output_unit = None, power_Ai_Av = 2.,
	av_min = 0., av_max = 30.):
		Av_color_calculators = {}
		for selection_band in photometries:
			for secondary_band in photometries:
				if PhotometricSystem[selection_band] < PhotometricSystem[secondary_band]:
					Av_color_calculators[selection_band + "-" + secondary_band] =\
					SingleColorAvCalculator(selection_band + "-" + secondary_band,
					PhotometricSystem = PhotometricSystem,
					ColorInterpolators = ColorInterpolators,
					ExtinctionInterpolator = ExtinctionInterpolator,
					MergingInterpolator = MergingInterpolator,
					interpolation_kind = interpolation_kind,
					output_unit = output_unit)

		wmean_Av_val = np.zeros_like(spts)
		wmean_Av_err = np.zeros_like(spts)

		for selection_band in photometries:
			for secondary_band in photometries:
				if PhotometricSystem[selection_band] < PhotometricSystem[secondary_band] and \
				color_weights[selection_band + "-" + secondary_band] > 0:
					Av_colors =\
					Av_color_calculators[selection_band + "-" + secondary_band].\
					evaluate(spts, av_min = -np.inf, av_max = np.inf, \
					**{selection_band:photometries[selection_band], \
					secondary_band:photometries[secondary_band]})

					Av_errors =\
					Av_color_calculators[selection_band + "-" + secondary_band].\
					_get_err(spts, spt_errors, \
					{selection_band:photometric_errors[selection_band], \
					secondary_band:photometric_errors[secondary_band]})

					cond = np.logical_and(np.logical_not(np.isnan(Av_colors)), np.logical_not(np.isnan(Av_errors)))

					# print "curr_weights", curr_weights

					print "Av_colors", Av_colors

					wmean_Av_val[cond] += Av_colors[cond]/Av_errors[cond]/Av_errors[cond]#*(curr_weights[selection_band][cond]*curr_weights[selection_band][cond] + curr_weights[secondary_band][cond]*curr_weights[secondary_band][cond])
					wmean_Av_err[cond] += 1./Av_errors[cond]/Av_errors[cond]#*(curr_weights[selection_band][cond]*curr_weights[selection_band][cond] + curr_weights[secondary_band][cond]*curr_weights[secondary_band][cond])

		wmean_Av = wmean_Av_val/wmean_Av_err
		wmean_Av = np.where(wmean_Av > av_min, wmean_Av, av_min)
		wmean_Av = np.where(wmean_Av < av_max, wmean_Av, av_max)
		wmean_Av = np.where(wmean_Av_err > 0, wmean_Av, np.zeros_like(spts) + np.NaN)

		### Create a dictionary of return values:
		return_dict = {}
		return_dict["Av_sol"] = wmean_Av
		# return_dict["num_dof"] = num_dof
		# return_dict["chisq"] = chisq

		return return_dict
	def get_Av_solution_median_weighted(self, spts, spt_errors, photometries, photometric_errors,
	curr_weights, color_weights, PhotometricSystem = PhotometricSystem(),
	ColorInterpolators = PecautMamajekYoungInterpolator,
	ExtinctionInterpolator =\
	MergedFallbackInstantiatedInterpolator([
	WhitneyInterpolator(interpolation_kind = "linear"),
	MathisInterpolator(interpolation_kind = "linear")]),
	MergingInterpolator = MergedFallbackInstantiatedInterpolator,
	interpolation_kind = "linear",
	output_unit = None, power_Ai_Av = 2.,
	av_min = 0., av_max = 30.):
		array_len = len(list(photometries.values())[0])
		Av_color_calculators = {}
		Av_sol_lists = [[] for x in range(array_len)]
		Av_sol_weights = [[] for x in range(array_len)]
		Av_sol = np.zeros(array_len) + np.NaN
		for selection_band in photometries:
			for secondary_band in photometries:
				if PhotometricSystem[selection_band] < PhotometricSystem[secondary_band]:
					Av_color_calculators[selection_band + "-" + secondary_band] =\
					SingleColorAvCalculator(selection_band + "-" + secondary_band,
					PhotometricSystem = PhotometricSystem,
					ColorInterpolators = ColorInterpolators,
					ExtinctionInterpolator = ExtinctionInterpolator,
					MergingInterpolator = MergingInterpolator,
					interpolation_kind = interpolation_kind,
					output_unit = output_unit)

		wmean_Av_val = np.zeros_like(spts)
		wmean_Av_err = np.zeros_like(spts)

		for selection_band in photometries:
			cond_unfilled = np.ones(array_len, dtype = bool)
			for secondary_band in photometries:
				if PhotometricSystem[selection_band] < PhotometricSystem[secondary_band] and \
				color_weights[selection_band + "-" + secondary_band] > 0:
					Av_colors =\
					Av_color_calculators[selection_band + "-" + secondary_band].\
					evaluate(spts, av_min = -np.inf, av_max = np.inf, \
					**{selection_band:photometries[selection_band], \
					secondary_band:photometries[secondary_band]})

					Av_errors =\
					Av_color_calculators[selection_band + "-" + secondary_band].\
					_get_err(spts, spt_errors, \
					{selection_band:photometric_errors[selection_band], \
					secondary_band:photometric_errors[secondary_band]})

					cond = np.logical_and(np.logical_not(np.isnan(Av_colors)), np.logical_not(np.isnan(Av_errors)))

					# print "curr_weights", curr_weights

					print "Av_colors", Av_colors

					# wmean_Av_val[cond] += Av_colors[cond]/Av_errors[cond]/Av_errors[cond]#*(curr_weights[selection_band][cond]*curr_weights[selection_band][cond] + curr_weights[secondary_band][cond]*curr_weights[secondary_band][cond])
					# wmean_Av_err[cond] += 1./Av_errors[cond]/Av_errors[cond]#*(curr_weights[selection_band][cond]*curr_weights[selection_band][cond] + curr_weights[secondary_band][cond]*curr_weights[secondary_band][cond])

					for si_iter in range(array_len):
						if cond[si_iter]:
							Av_sol_lists[si_iter].\
							append(Av_colors[si_iter])
							Av_sol_weights[si_iter].\
							append(1./np.square(Av_errors[si_iter]))
							cond_unfilled[si_iter] = False

		# The Av_sol is the weighted median of the Av_sol_lists list.
		for av_iter in range(array_len):
			if len(Av_sol_weights[av_iter]) > 1:
				# Create a CDF.
				Av_sol_sorted = sorted(Av_sol_lists[av_iter])

				Av_weight_sorted = \
				[x[1] for x in \
				sorted(zip(Av_sol_lists[av_iter], Av_sol_weights[av_iter]),
				key = lambda y: y[0])]

				Av_CDF_weighted =\
				((np.cumsum(Av_weight_sorted) - Av_weight_sorted[0])/\
				np.sum(Av_weight_sorted[1:]))

				try:
					Av_sol[av_iter] =\
					scipy.interpolate.interp1d(\
					Av_CDF_weighted,
					Av_sol_sorted,
					kind = interpolation_kind \
					)(0.5)
				except:
					Av_sol[av_iter] = np.NaN

				# plt.hist(Av_sol_sorted, bins = 50)
				# plt.axvline(Av_sol[av_iter])
				# plt.show()

				# print zip(Av_CDF_weighted, Av_sol_sorted), "0.5:", Av_sol[av_iter]
			else:
				pass

		### Remove all SpT = NaN values
		cond_spt_nan = np.isnan(spts)
		Av_sol[cond_spt_nan] = np.NaN

		Av_sol = np.clip(Av_sol, av_min, av_max)

		### Create a dictionary of return values:
		return_dict = {}
		return_dict["Av_sol"] = Av_sol
		# return_dict["num_dof"] = num_dof
		# return_dict["chisq"] = chisq

		return return_dict
	def get_Av_solution_mean_SED_band(self, spts, spt_errors, photometries, photometric_errors,
	curr_weights, color_weights, PhotometricSystem = PhotometricSystem(),
	ColorInterpolators = PecautMamajekYoungInterpolator,
	ExtinctionInterpolator =\
	MergedFallbackInstantiatedInterpolator([
	WhitneyInterpolator(interpolation_kind = "linear"),
	MathisInterpolator(interpolation_kind = "linear")]),
	MergingInterpolator = MergedFallbackInstantiatedInterpolator,
	interpolation_kind = "linear",
	output_unit = None, power_Ai_Av = 2.,
	av_min = 0., av_max = 30.):
		"""Calculates the Av through SED fitting.
		To disable clipping of solutions, set av_min to -np.inf, and av_max to np.inf."""
		array_len = len(list(photometries.values())[0])
		Av_sol = np.zeros(array_len) + np.NaN
		Av_sol_numerator = np.zeros(array_len)
		Av_sol_denominator = np.zeros(array_len)
		Av_sol_bands_numerator = {}
		Av_sol_bands_denominator = {}
		num_dof = np.zeros(array_len)

		photometry_list = sorted(photometries.keys(), key=\
								 lambda x:\
								 abs(self.PhotometricSystem(x) - \
									 self.PhotometricSystem(self.pivot_band)))

		for selection_band in photometries:
			cond_unfilled = np.ones(array_len, dtype = bool)
			for secondary_band in photometry_list:
				if secondary_band != selection_band:
					color_text = selection_band + "-" + secondary_band
					if not (color_text in self.color_graph_interpolators):
						self.color_graph_interpolators[color_text] =\
						ColorGraphInterpolator(color_text,
						PhotometricSystem =\
						PhotometricSystem,
						ColorInterpolators =\
						ColorInterpolators,
						MergingInterpolator =\
						MergingInterpolator,
						interpolation_kind =\
						interpolation_kind,
						output_unit = output_unit)

					intrinsic_color =\
					self.color_graph_interpolators[color_text].evaluate(spts)
					### Fill in only those that are still not filled in.

					if not (selection_band in self.extinction_vals):
						self.extinction_vals[selection_band] =\
						ExtinctionInterpolator.evaluate(PhotometricSystem(selection_band))
					# if not (secondary_band in self.extinction_vals):
					# 	self.extinction_vals[secondary_band] =\
					# 	ExtinctionInterpolator.evaluate(PhotometricSystem(secondary_band))

					Av_sol_numerator_part =\
					((photometries[selection_band] -\
					photometries[secondary_band]) -\
					(intrinsic_color))*\
					np.power( self.extinction_vals[selection_band] - self.extinction_vals[secondary_band], power_Ai_Av - 1)/\
					(np.square(photometric_errors[selection_band]/curr_weights[selection_band]))*np.square(color_weights[color_text])

					# print "Av_sol",  color_text, Av_sol_numerator_part[0]

					Av_sol_denominator_part =\
					np.power\
					( self.extinction_vals[selection_band] - self.extinction_vals[secondary_band], power_Ai_Av)/\
					np.square(photometric_errors[selection_band]/curr_weights[selection_band])*np.square(color_weights[color_text])

					cond_not_nan =\
					np.logical_and(np.logical_not(np.isnan(Av_sol_numerator_part)),
					np.logical_not(np.isnan(Av_sol_denominator_part)),
					cond_unfilled)

					Av_sol_numerator[cond_not_nan] +=\
					Av_sol_numerator_part[cond_not_nan]

					Av_sol_denominator[cond_not_nan] +=\
					Av_sol_denominator_part[cond_not_nan]

					if np.square(color_weights[color_text]) > 0:
						num_dof[cond_not_nan] += 1.

					cond_unfilled[cond_not_nan] = False

		cond_enough_dof = num_dof > 0
		Av_sol[cond_enough_dof] =\
		Av_sol_numerator[cond_enough_dof] /\
		Av_sol_denominator[cond_enough_dof]

		### Remove all SpT = NaN values
		cond_spt_nan = np.isnan(spts)
		Av_sol[cond_spt_nan] = np.NaN

		Av_sol = np.clip(Av_sol, av_min, av_max)

		### Calculate the chisq value:

		chisq = np.zeros(array_len) + np.NaN

		for selection_band in photometries:
			cond_unfilled = np.ones(array_len, dtype = bool)
			for secondary_band in photometry_list:
				if secondary_band != selection_band:
					color_text = selection_band + "-" + secondary_band
					if not (color_text in self.color_graph_interpolators):
						self.color_graph_interpolators[color_text] =\
						ColorGraphInterpolator(color_text,
						PhotometricSystem =\
						PhotometricSystem,
						ColorInterpolators =\
						ColorInterpolators,
						MergingInterpolator =\
						MergingInterpolator,
						interpolation_kind =\
						interpolation_kind,
						output_unit = output_unit)

					intrinsic_color =\
					self.color_graph_interpolators[color_text].evaluate(spts)
					### Fill in only those that are still not filled in.

					if not (selection_band in self.extinction_vals):
						self.extinction_vals[selection_band] =\
						ExtinctionInterpolator.evaluate(PhotometricSystem(selection_band))
					# if not (secondary_band in self.extinction_vals):
					# 	self.extinction_vals[secondary_band] =\
					# 	ExtinctionInterpolator.evaluate(PhotometricSystem(secondary_band))

					chisq_part =\
					np.square((photometries[selection_band] -\
					photometries[secondary_band]) -\
					(intrinsic_color) - Av_sol * (self.extinction_vals[selection_band] -\
					self.extinction_vals[secondary_band]))/\
					(np.square(photometric_errors[selection_band]/curr_weights[selection_band]))*np.square(color_weights[color_text])

					# print "chisq_part", chisq_part

					cond_not_nan =\
					np.logical_and(np.logical_not(np.isnan(chisq_part)),
					cond_unfilled)


					chisq[np.logical_and(np.isnan(chisq), cond_not_nan)] = 0.

					chisq[cond_not_nan] += chisq_part[cond_not_nan]

					cond_unfilled[cond_not_nan] = False

		### Create a dictionary of return values:
		return_dict = {}
		return_dict["Av_sol"] = Av_sol
		return_dict["num_dof"] = num_dof
		return_dict["chisq"] = chisq

		return return_dict
	def get_Av_solution_median_SED_band(self, spts, spt_errors, photometries, photometric_errors,
	curr_weights, color_weights, PhotometricSystem = PhotometricSystem(),
	ColorInterpolators = PecautMamajekYoungInterpolator,
	ExtinctionInterpolator =\
	MergedFallbackInstantiatedInterpolator([
	WhitneyInterpolator(interpolation_kind = "linear"),
	MathisInterpolator(interpolation_kind = "linear")]),
	MergingInterpolator = MergedFallbackInstantiatedInterpolator,
	interpolation_kind = "linear",
	output_unit = None, power_Ai_Av = 2.,
	av_min = 0., av_max = 30.):
		array_len = len(list(photometries.values())[0])
		Av_sol = np.zeros(array_len) + np.NaN
		Av_sol_lists = [[] for x in range(array_len)]
		Av_sol_weights = [[] for x in range(array_len)]
		num_dof = np.zeros(array_len)

		photometry_list = sorted(photometries.keys(), key=\
								 lambda x:\
								 abs(self.PhotometricSystem(x) - \
									 self.PhotometricSystem(self.pivot_band)))

		for selection_band in photometries:
			cond_unfilled = np.ones(array_len, dtype = bool)
			for secondary_band in photometry_list:
				if secondary_band != selection_band:
					color_text = selection_band + "-" + secondary_band
					if not (color_text in self.color_graph_interpolators):
						self.color_graph_interpolators[color_text] =\
						ColorGraphInterpolator(color_text,
						PhotometricSystem =\
						PhotometricSystem,
						ColorInterpolators =\
						ColorInterpolators,
						MergingInterpolator =\
						MergingInterpolator,
						interpolation_kind =\
						interpolation_kind,
						output_unit = output_unit)

					intrinsic_color =\
					self.color_graph_interpolators[color_text].evaluate(spts)

					if not (selection_band in self.extinction_vals):
						self.extinction_vals[selection_band] =\
						ExtinctionInterpolator.evaluate(PhotometricSystem(selection_band))
					if not (secondary_band in self.extinction_vals):
						self.extinction_vals[secondary_band] =\
						ExtinctionInterpolator.evaluate(PhotometricSystem(secondary_band))

					Av_sol_part =\
					((photometries[selection_band] -\
					photometries[secondary_band]) -\
					(intrinsic_color))/\
					( self.extinction_vals[selection_band] \
					- self.extinction_vals[secondary_band])

					if color_text in self.partial_ij:
						partial_ij = self.partial_ij[color_text](spts)
					else:
						partial_ij =\
						self.partial_intrinsic_color_partial_SpT(spts,
						self.color_graph_interpolators[color_text])

					Av_weight_part =\
					np.power\
					( self.extinction_vals[selection_band] \
					- self.extinction_vals[secondary_band], power_Ai_Av)/\
 					(np.square(photometric_errors[selection_band]/curr_weights[selection_band]) +\
 					np.square(photometric_errors[secondary_band]/curr_weights[secondary_band]) +\
 					np.square(spt_errors)*np.square(partial_ij)/(np.square(curr_weights[selection_band]) + np.square(curr_weights[secondary_band])))

					# print "Av_sol",  color_text, Av_sol_numerator_part[0]

					cond_not_nan =\
					np.logical_and(np.logical_not(np.isnan(Av_sol_part)),np.logical_not(np.isnan(Av_weight_part)),
					cond_unfilled)

					# Av_sol_numerator[cond_not_nan] +=\
					# Av_sol_numerator_part[cond_not_nan]

					for si_iter in range(array_len):
						if cond_not_nan[si_iter]:
							Av_sol_lists[si_iter].\
							append(Av_sol_part[si_iter])
							Av_sol_weights[si_iter].\
							append(Av_weight_part[si_iter])
							cond_unfilled[si_iter] = False

					num_dof[cond_not_nan] += 1.

		cond_enough_dof = num_dof > 0

		# The Av_sol is the weighted median of the Av_sol_lists list.
		for av_iter in range(array_len):
			if cond_enough_dof[av_iter]:
				# Create a CDF.
				Av_sol_sorted = sorted(Av_sol_lists[av_iter])

				Av_weight_sorted = \
				[x[1] for x in \
				sorted(zip(Av_sol_lists[av_iter], Av_sol_weights[av_iter]),
				key = lambda y: y[0])]

				Av_CDF_weighted =\
				((np.cumsum(Av_weight_sorted) - Av_weight_sorted[0])/\
				np.sum(Av_weight_sorted[1:]))

				print "Av_CDF_weighted", Av_CDF_weighted
				print "Av_sol_sorted", Av_sol_sorted

				try:
					Av_sol[av_iter] =\
					scipy.interpolate.interp1d(\
					Av_CDF_weighted,
					Av_sol_sorted,
					kind = interpolation_kind \
					)(0.5)
				except:
					Av_sol[av_iter] = np.NaN

				# plt.hist(Av_sol_sorted, bins = 50)
				# plt.axvline(Av_sol[av_iter])
				# plt.show()

				# print zip(Av_CDF_weighted, Av_sol_sorted), "0.5:", Av_sol[av_iter]

		### Remove all SpT = NaN values
		cond_spt_nan = np.isnan(spts)
		Av_sol[cond_spt_nan] = np.NaN

		Av_sol = np.clip(Av_sol, av_min, av_max)

		### Create a dictionary of return values:
		return_dict = {}
		return_dict["Av_sol"] = Av_sol

		return return_dict
	def get_Av_solution_mean_SED(self, spts, spt_errors, photometries, photometric_errors,
	curr_weights, color_weights, PhotometricSystem = PhotometricSystem(),
	ColorInterpolators = PecautMamajekYoungInterpolator,
	ExtinctionInterpolator =\
	MergedFallbackInstantiatedInterpolator([
	WhitneyInterpolator(interpolation_kind = "linear"),
	MathisInterpolator(interpolation_kind = "linear")]),
	MergingInterpolator = MergedFallbackInstantiatedInterpolator,
	interpolation_kind = "linear",
	output_unit = None, power_Ai_Av = 2., av_min = 0., av_max = 30.):
		array_len = len(list(photometries.values())[0])
		Av_sol = np.zeros(array_len) + np.NaN
		Av_sol_numerator = np.zeros(array_len)
		Av_sol_denominator = np.zeros(array_len)
		Av_sol_bands = {} # This is defined as the chisq statistic for any given band
		Av_sol_bands_numerator = {}
		Av_sol_bands_denominator = {}
		num_dof = np.zeros(array_len)
		num_dof_bands = {}

		for selection_band in photometries:
			Av_sol_bands[selection_band] = np.zeros(array_len) + np.NaN
			Av_sol_bands_numerator[selection_band] = np.zeros(array_len)
			Av_sol_bands_denominator[selection_band] = np.zeros(array_len)
			num_dof_bands[selection_band] = np.zeros(array_len)
			for secondary_band in photometries:
				if secondary_band != selection_band:
					color_text = selection_band + "-" + secondary_band
					# print color_text
					if not (color_text in self.color_graph_interpolators):
						self.color_graph_interpolators[color_text] =\
						ColorGraphInterpolator(color_text,
						PhotometricSystem =\
						PhotometricSystem,
						ColorInterpolators =\
						ColorInterpolators,
						MergingInterpolator =\
						MergingInterpolator,
						interpolation_kind =\
						interpolation_kind,
						output_unit = output_unit)

					intrinsic_color =\
					self.color_graph_interpolators[color_text].evaluate(spts)

					if not (selection_band in self.extinction_vals):
						self.extinction_vals[selection_band] =\
						ExtinctionInterpolator.evaluate(PhotometricSystem(selection_band))
					if not (secondary_band in self.extinction_vals):
						self.extinction_vals[secondary_band] =\
						ExtinctionInterpolator.evaluate(PhotometricSystem(secondary_band))

					if color_text in self.partial_ij:
						partial_ij = self.partial_ij[color_text](spts)
					else:
						partial_ij =\
						self.partial_intrinsic_color_partial_SpT(spts,
						self.color_graph_interpolators[color_text])

					Av_sol_numerator_part =\
					((photometries[selection_band] -\
					photometries[secondary_band]) -\
					(intrinsic_color))*\
					np.power( self.extinction_vals[selection_band] \
					- self.extinction_vals[secondary_band], power_Ai_Av - 1)/\
					(np.square(photometric_errors[selection_band]/curr_weights[selection_band]) +\
					np.square(photometric_errors[secondary_band]/curr_weights[secondary_band]) +\
					np.square(spt_errors)*np.square(partial_ij)/(np.square(curr_weights[selection_band]) + np.square(curr_weights[secondary_band])))*np.square(color_weights[color_text])


					# print "Av_sol",  color_text, Av_sol_numerator_part[0]

					Av_sol_denominator_part =\
					np.power\
					( self.extinction_vals[selection_band] \
					- self.extinction_vals[secondary_band], power_Ai_Av)/\
					(np.square(photometric_errors[selection_band]/curr_weights[selection_band]) +\
					np.square(photometric_errors[secondary_band]/curr_weights[secondary_band]) +\
					np.square(spt_errors)*np.square(partial_ij)/(np.square(curr_weights[selection_band]) + np.square(curr_weights[secondary_band])))*np.square(color_weights[color_text])

					cond_not_nan = np.logical_and(np.logical_not(np.isnan(Av_sol_numerator_part)), np.logical_not(np.isnan(Av_sol_denominator_part)))

					Av_sol_numerator[cond_not_nan] +=\
					Av_sol_numerator_part[cond_not_nan]

					Av_sol_bands_numerator[selection_band][cond_not_nan] +=\
					Av_sol_numerator_part[cond_not_nan]

					Av_sol_denominator[cond_not_nan] +=\
					Av_sol_denominator_part[cond_not_nan]

					Av_sol_bands_denominator[selection_band][cond_not_nan] +=\
					Av_sol_denominator_part[cond_not_nan]

					if np.square(color_weights[color_text]) > 0:
						num_dof[cond_not_nan] += 0.5
						num_dof_bands[selection_band][cond_not_nan] += 0.5

			# Consider num_dof_bands:
			# Only calculate Av_sol_bands
			cond_enough_dof = num_dof_bands[selection_band] > 0
			Av_sol_bands[selection_band][cond_enough_dof] =\
			Av_sol_bands_numerator[selection_band][cond_enough_dof] /\
			Av_sol_bands_denominator[selection_band][cond_enough_dof]

		cond_enough_dof = num_dof > 0
		Av_sol[cond_enough_dof] =\
		Av_sol_numerator[cond_enough_dof] /\
		Av_sol_denominator[cond_enough_dof]

		### Remove all SpT = NaN values
		cond_spt_nan = np.isnan(spts)
		Av_sol[cond_spt_nan] = np.NaN

		Av_sol = np.clip(Av_sol, av_min, av_max)

		### Calculate the chisq value:

		chisq = np.zeros(array_len) + np.NaN

		for selection_band in photometries:
			for secondary_band in photometries:
				if secondary_band != selection_band:
					color_text = selection_band + "-" + secondary_band
					if not (color_text in self.color_graph_interpolators):
						self.color_graph_interpolators[color_text] =\
						ColorGraphInterpolator(color_text,
						PhotometricSystem =\
						PhotometricSystem,
						ColorInterpolators =\
						ColorInterpolators,
						MergingInterpolator =\
						MergingInterpolator,
						interpolation_kind =\
						interpolation_kind,
						output_unit = output_unit)

					intrinsic_color =\
					self.color_graph_interpolators[color_text].evaluate(spts)
					### Fill in only those that are still not filled in.

					if not (selection_band in self.extinction_vals):
						self.extinction_vals[selection_band] =\
						ExtinctionInterpolator.evaluate(PhotometricSystem(selection_band))
					# if not (secondary_band in self.extinction_vals):
					# 	self.extinction_vals[secondary_band] =\
					# 	ExtinctionInterpolator.evaluate(PhotometricSystem(secondary_band))

					chisq_part =\
					np.square((photometries[selection_band] -\
					photometries[secondary_band]) -\
					(intrinsic_color) - Av_sol * (self.extinction_vals[selection_band] -\
					self.extinction_vals[secondary_band]))/\
					(np.square(photometric_errors[selection_band]/curr_weights[selection_band]) +\
					np.square(photometric_errors[secondary_band]/curr_weights[secondary_band]) +\
					np.square(spt_errors)*np.square(partial_ij)/(np.square(curr_weights[selection_band]) + np.square(curr_weights[secondary_band])))*np.square(color_weights[color_text])

					cond_not_nan =\
					np.logical_not(np.isnan(chisq_part))

					chisq[np.logical_and(np.isnan(chisq), cond_not_nan)] = 0.

					chisq[cond_not_nan] += chisq_part[cond_not_nan]

		for selection_band in photometries:
			Av_sol_bands[selection_band][cond_spt_nan] = np.NaN

		### Create a dictionary of return values:
		return_dict = {}
		return_dict["Av_sol"] = Av_sol
		return_dict["num_dof"] = num_dof
		return_dict["Av_sol_bands"] = Av_sol_bands
		return_dict["chisq"] = chisq

		return return_dict
	def get_Av_solution_median_SED(self, spts, spt_errors, photometries, photometric_errors,
	curr_weights, color_weights, PhotometricSystem = PhotometricSystem(),
	ColorInterpolators = PecautMamajekYoungInterpolator,
	ExtinctionInterpolator =\
	MergedFallbackInstantiatedInterpolator([
	WhitneyInterpolator(interpolation_kind = "linear"),
	MathisInterpolator(interpolation_kind = "linear")]),
	MergingInterpolator = MergedFallbackInstantiatedInterpolator,
	interpolation_kind = "linear",
	output_unit = None, av_min = 0., av_max = 30., power_Ai_Av = 2.):
		array_len = len(list(photometries.values())[0])
		Av_sol = np.zeros(array_len) + np.NaN
		Av_sol_lists = [[] for x in range(array_len)]
		Av_sol_weights = [[] for x in range(array_len)]
		num_dof = np.zeros(array_len)

		for selection_band in photometries:
			for secondary_band in photometries:
				if secondary_band != selection_band:
					color_text = selection_band + "-" + secondary_band
					if not (color_text in self.color_graph_interpolators):
						self.color_graph_interpolators[color_text] =\
						ColorGraphInterpolator(color_text,
						PhotometricSystem =\
						PhotometricSystem,
						ColorInterpolators =\
						ColorInterpolators,
						MergingInterpolator =\
						MergingInterpolator,
						interpolation_kind =\
						interpolation_kind,
						output_unit = output_unit)

					intrinsic_color =\
					self.color_graph_interpolators[color_text].evaluate(spts)

					if not (selection_band in self.extinction_vals):
						self.extinction_vals[selection_band] =\
						ExtinctionInterpolator.evaluate(PhotometricSystem(selection_band))
					if not (secondary_band in self.extinction_vals):
						self.extinction_vals[secondary_band] =\
						ExtinctionInterpolator.evaluate(PhotometricSystem(secondary_band))

					Av_sol_part =\
					((photometries[selection_band] -\
					photometries[secondary_band]) -\
					(intrinsic_color))/\
					( self.extinction_vals[selection_band] \
					- self.extinction_vals[secondary_band])

					if color_text in self.partial_ij:
						partial_ij = self.partial_ij[color_text](spts)
					else:
						partial_ij =\
						self.partial_intrinsic_color_partial_SpT(spts,
						self.color_graph_interpolators[color_text])

					Av_weight_part =\
					np.power\
					( self.extinction_vals[selection_band] \
					- self.extinction_vals[secondary_band], power_Ai_Av)/\
 					(np.square(photometric_errors[selection_band]/curr_weights[selection_band]) +\
 					np.square(photometric_errors[secondary_band]/curr_weights[secondary_band]) +\
 					np.square(spt_errors)*np.square(partial_ij)/(np.square(curr_weights[selection_band]) + np.square(curr_weights[secondary_band])))*np.square(color_weights[color_text])


					# print "Av_sol",  color_text, Av_sol_numerator_part[0]

					cond_not_nan =\
					np.logical_not(np.isnan(Av_sol_part))

					# Av_sol_numerator[cond_not_nan] +=\
					# Av_sol_numerator_part[cond_not_nan]

					for si_iter in range(array_len):
						if cond_not_nan[si_iter]:
							Av_sol_lists[si_iter].\
							append(Av_sol_part[si_iter])
							Av_sol_weights[si_iter].\
							append(Av_weight_part[si_iter])

					if np.square(color_weights[color_text]) > 0:
						num_dof[cond_not_nan] += 0.5

		cond_enough_dof = num_dof > 0

		# The Av_sol is the weighted median of the Av_sol_lists list.
		for av_iter in range(array_len):
			if cond_enough_dof[av_iter]:
				# Create a CDF.
				Av_sol_sorted = sorted(Av_sol_lists[av_iter])

				Av_weight_sorted = \
				[x[1] for x in \
				sorted(zip(Av_sol_lists[av_iter], Av_sol_weights[av_iter]),
				key = lambda y: y[0])]

				Av_CDF_weighted =\
				((np.cumsum(Av_weight_sorted) - Av_weight_sorted[0])/\
				np.sum(Av_weight_sorted[1:]))

				try:
					Av_sol[av_iter] =\
					scipy.interpolate.interp1d(\
					Av_CDF_weighted,
					Av_sol_sorted,
					kind = interpolation_kind \
					)(0.5)
				except:
					Av_sol[av_iter] = np.NaN

				# plt.hist(Av_sol_sorted, bins = 50)
				# plt.axvline(Av_sol[av_iter])
				# plt.show()

				# print zip(Av_CDF_weighted, Av_sol_sorted), "0.5:", Av_sol[av_iter]

		### Remove all SpT = NaN values
		cond_spt_nan = np.isnan(spts)
		Av_sol[cond_spt_nan] = np.NaN

		Av_sol = np.clip(Av_sol, av_min, av_max)

		### Remove all chisq = NaN values
		# chisq_result =\
		# self.chisq_from_av(Av_sol, spts, photometries, photometric_errors,
		# curr_weights, PhotometricSystem = PhotometricSystem,
		# ColorInterpolators = ColorInterpolators,
		# ExtinctionInterpolator = ExtinctionInterpolator,
		# MergingInterpolator = MergingInterpolator,
		# interpolation_kind = interpolation_kind,
		# output_unit = output_unit)
		# cond_chisq_nan = np.isnan(chisq_result["chisq"])
		# Av_sol[cond_chisq_nan] = np.NaN

		### Create a dictionary of return values:
		return_dict = {}
		return_dict["Av_sol"] = Av_sol

		return return_dict
	@staticmethod
	def partial_intrinsic_color_partial_SpT(spts,
	ColorInterpolator,
	spt_seps = [0.25,0.5,1.]):
		"""Calculate the partial (band_i - band_j) / partial SpT for
		a ColorInterpolator by averaging a range of SpT offsets.

		Pass in as ColorInterpolator an interpolator that takes
		SpT input and returns intrinsic color output."""
		spts = np.array(spts)
		jacobian_spts = []
		intrinsic_color_center =\
		ColorInterpolator.evaluate(spts)
		for spt_sep in spt_seps:
			jacobian_result = np.zeros(len(spts)) + np.NaN

			intrinsic_color_minus_spt =\
			ColorInterpolator.evaluate(spts - spt_sep)
			intrinsic_color_plus_spt =\
			ColorInterpolator.evaluate(spts + spt_sep)

			jacobian_left =\
			(intrinsic_color_center - intrinsic_color_minus_spt)/\
			(spt_sep)
			cond_left_not_nan =\
			np.logical_not(np.isnan(jacobian_left))

			jacobian_right =\
			(intrinsic_color_plus_spt - intrinsic_color_center)/\
			(spt_sep)
			cond_right_not_nan =\
			np.logical_not(np.isnan(jacobian_right))

			jacobian_avg =\
			0.5*(jacobian_left + jacobian_right)
			cond_avg_not_nan =\
			np.logical_not(np.isnan(jacobian_avg))

			### Replace values to populate the Jacobian, replacing values if
			### the subsequent value is "better"
			jacobian_result[cond_left_not_nan] =\
			jacobian_left[cond_left_not_nan]
			jacobian_result[cond_right_not_nan] =\
			jacobian_right[cond_right_not_nan]
			jacobian_result[cond_avg_not_nan] =\
			jacobian_avg[cond_avg_not_nan]

			jacobian_spts.append(jacobian_result)
			# print jacobian_left[0],jacobian_right[0],jacobian_avg[0]

		return np.nanmean(jacobian_spts, axis = 0)

	# def partial_intrinsic_color_partial_SpT_interpolator(self, spts, ColorInterpolators, colors,
	# spt_seps = [0.25,0.5,1.]):
	# 	"""Creates an interpolator to accelerate the computation of
	# 	partial_intrinsic_color_partial_SpT."""
	# 	partial_intrinsic_color_partial_SpT_interpolators = {}

	def grad_chisq_from_av(self, Av_guess, spts, photometries, photometric_errors,
	curr_weights, PhotometricSystem = PhotometricSystem(),
	ColorInterpolators = PecautMamajekYoungInterpolator,
	ExtinctionInterpolator =\
	MergedFallbackInstantiatedInterpolator([
	WhitneyInterpolator(interpolation_kind = "linear"),
	MathisInterpolator(interpolation_kind = "linear")]),
	MergingInterpolator = MergedFallbackInstantiatedInterpolator,
	interpolation_kind = "linear",
	output_unit = None):
		array_len = len(list(photometries.values())[0])
		grad_chisq = np.zeros(array_len)
		weights = np.zeros(array_len)
		grad_chisq_bands = {} # This is defined as the chisq statistic for any given band
		num_dof = np.zeros(array_len)

		for selection_band in photometries:
			grad_chisq_bands[selection_band] = np.zeros(array_len)
			for secondary_band in photometries:
				if secondary_band != selection_band:
					color_text = selection_band + "-" + secondary_band
					if not (color_text in self.color_graph_interpolators):
						self.color_graph_interpolators[color_text] =\
						ColorGraphInterpolator(color_text,
						PhotometricSystem =\
						PhotometricSystem,
						ColorInterpolators =\
						ColorInterpolators,
						MergingInterpolator =\
						MergingInterpolator,
						interpolation_kind =\
						interpolation_kind,
						output_unit = output_unit)

					if not (selection_band in self.extinction_vals):
						self.extinction_vals[selection_band] =\
						ExtinctionInterpolator.evaluate(PhotometricSystem(selection_band))
					if not (secondary_band in self.extinction_vals):
						self.extinction_vals[secondary_band] =\
						ExtinctionInterpolator.evaluate(PhotometricSystem(secondary_band))

					intrinsic_color =\
					self.color_graph_interpolators[color_text].evaluate(spts)

					grad_chisq_value =\
					-(-Av_guess*\
					( self.extinction_vals[selection_band] \
					- self.extinction_vals[secondary_band])+\
					(photometries[selection_band] -\
					photometries[secondary_band]) -\
					(intrinsic_color))/\
					 (np.square(photometric_errors[selection_band]) +\
					 np.square(photometric_errors[secondary_band]))*\
					( self.extinction_vals[selection_band] \
					- self.extinction_vals[secondary_band])

					for chisq_idx, chisq_val in enumerate(grad_chisq_value):
						if not np.isnan(chisq_val):
							## Add this chisq to the bulk chisq:
							grad_chisq[chisq_idx] += 0.5*curr_weights[selection_band][chisq_idx]*curr_weights[secondary_band][chisq_idx]*chisq_val
							## Add this chisq to the band chisq:
							grad_chisq_bands[selection_band][chisq_idx] += curr_weights[selection_band][chisq_idx]*curr_weights[secondary_band][chisq_idx]*chisq_val
							## Add this as a weights term:
							weights[chisq_idx] += curr_weights[selection_band][chisq_idx]*curr_weights[secondary_band][chisq_idx]
							## Note this in DOF:
							num_dof[chisq_idx] += 0.5

		grad_chisq /= (weights - 1e-16)

		### Create a dictionary of return values:
		return_dict = {}
		return_dict["weights"] = weights
		return_dict["num_dof"] = num_dof
		return_dict["grad_chisq_bands"] = grad_chisq_bands
		return_dict["grad_chisq"] = grad_chisq

		return return_dict
	def get_Av_error(self, spts, spt_errors, photometries, photometric_errors,
	curr_weights, color_weights, PhotometricSystem = PhotometricSystem(),
	ColorInterpolators = PecautMamajekYoungInterpolator,
	ExtinctionInterpolator =\
	MergedFallbackInstantiatedInterpolator([
	WhitneyInterpolator(interpolation_kind = "linear"),
	MathisInterpolator(interpolation_kind = "linear")]),
	MergingInterpolator = MergedFallbackInstantiatedInterpolator,
	interpolation_kind = "linear",
	output_unit = None):
		array_len = len(list(photometries.values())[0])
		Av_err_denominator = np.zeros(array_len)
		Av_err_bands_for_mag = {} # This is defined as the chisq statistic for any given band
		Av_err_bands_for_mag_numerator = {}
		Av_err_for_SpT = np.zeros(array_len)
		Av_err_for_SpT_numerator = np.zeros(array_len)
		Av_err_bands_denominator = {}
		squared_Av_error = np.zeros(array_len)
		num_dof = np.zeros(array_len)
		num_dof_bands = {}

		for selection_band in photometries:
			Av_err_bands_for_mag_numerator[selection_band] = np.zeros(array_len)
			Av_err_bands_for_mag[selection_band] = np.zeros(array_len)
			Av_err_bands_denominator[selection_band] = np.zeros(array_len)
			num_dof_bands[selection_band] = np.zeros(array_len)
			for secondary_band in photometries:
				if secondary_band != selection_band:
					color_text = selection_band + "-" + secondary_band
					if not (color_text in self.color_graph_interpolators):
						self.color_graph_interpolators[color_text] =\
						ColorGraphInterpolator(color_text,
						PhotometricSystem =\
						PhotometricSystem,
						ColorInterpolators =\
						ColorInterpolators,
						MergingInterpolator =\
						MergingInterpolator,
						interpolation_kind =\
						interpolation_kind,
						output_unit = output_unit)

					if not (selection_band in self.extinction_vals):
						self.extinction_vals[selection_band] =\
						ExtinctionInterpolator.evaluate(PhotometricSystem(selection_band))
					if not (secondary_band in self.extinction_vals):
						self.extinction_vals[secondary_band] =\
						ExtinctionInterpolator.evaluate(PhotometricSystem(secondary_band))

					intrinsic_color =\
					self.color_graph_interpolators[color_text].evaluate(spts)


					if color_text in self.partial_ij:
						partial_ij = self.partial_ij[color_text](spts)
					else:
						partial_ij =\
						self.partial_intrinsic_color_partial_SpT(spts,
						self.color_graph_interpolators[color_text])

					Av_err_for_SpT_numerator_part =\
					curr_weights[selection_band]*\
					curr_weights[secondary_band]*\
					( self.extinction_vals[selection_band] \
					- self.extinction_vals[secondary_band])/\
					(np.square(photometric_errors[selection_band]) +\
					np.square(photometric_errors[secondary_band]))*\
					partial_ij

					cond_not_nan = np.logical_not(np.isnan(Av_err_for_SpT_numerator_part))

					Av_err_for_SpT_numerator[cond_not_nan] +=\
					Av_err_for_SpT_numerator_part[cond_not_nan]

					Av_err_bands_for_mag_numerator_part =\
					curr_weights[selection_band]*\
					curr_weights[secondary_band]*\
					( self.extinction_vals[selection_band] \
					- self.extinction_vals[secondary_band])/\
					(np.square(photometric_errors[selection_band]) +\
					np.square(photometric_errors[secondary_band]))

					Av_err_denominator_part =\
					curr_weights[selection_band]*\
					curr_weights[secondary_band]*\
					np.square\
					( self.extinction_vals[selection_band] \
					- self.extinction_vals[secondary_band])/\
					(np.square(photometric_errors[selection_band]) +\
					np.square(photometric_errors[secondary_band]))

					cond_not_nan =\
					np.logical_and(np.logical_not(np.isnan(Av_err_bands_for_mag_numerator_part)),
					np.logical_not(np.isnan(Av_err_denominator_part)),
					np.logical_not(np.isnan(Av_err_denominator_part)))

					Av_err_bands_for_mag_numerator[selection_band][cond_not_nan] +=\
					Av_err_bands_for_mag_numerator_part[cond_not_nan]

					Av_err_denominator[cond_not_nan] +=\
					Av_err_denominator_part[cond_not_nan]

					Av_err_bands_denominator[selection_band][cond_not_nan] +=\
					Av_err_denominator_part[cond_not_nan]

					num_dof[cond_not_nan] += 0.5
					num_dof_bands[selection_band][cond_not_nan] += 0.5

					# print "Av_err_for_SpT_numerator_part", Av_err_for_SpT_numerator_part
					# print "Av_err_bands_for_mag_numerator_part", Av_err_bands_for_mag_numerator_part
					# print "Av_err_denominator_part", Av_err_denominator_part
					# print "cond_not_nan", cond_not_nan

			# Consider num_dof_bands:
			# Only calculate Av_err_bands
			cond_enough_dof = num_dof_bands[selection_band] > 0
			Av_err_bands_for_mag[selection_band][cond_enough_dof] =\
			np.square(\
			Av_err_bands_for_mag_numerator[selection_band][cond_enough_dof]/\
			Av_err_bands_denominator[selection_band][cond_enough_dof])

			cond_not_nan =\
			np.logical_and(np.logical_not(np.isnan(Av_err_bands_for_mag[selection_band])),
			np.logical_not(np.isnan(photometric_errors[selection_band])))

			cond_valid = np.logical_and(cond_not_nan, cond_enough_dof)

			squared_Av_error[cond_valid] += \
			Av_err_bands_for_mag[selection_band][cond_valid]*\
			np.square(photometric_errors[selection_band][cond_valid])

		cond_enough_dof = num_dof > 0

		squared_Av_error_from_mags = np.copy(squared_Av_error)

		# print "Error from magnitudes", np.sqrt(squared_Av_error_from_mags)

		Av_err_for_SpT[cond_enough_dof] =\
		np.square(\
		Av_err_for_SpT_numerator[cond_enough_dof] /\
		Av_err_denominator[cond_enough_dof])

		squared_Av_error[cond_enough_dof] += \
		Av_err_for_SpT[cond_enough_dof]*\
		np.square(spt_errors[cond_enough_dof])

		squared_Av_error_from_SpT =\
		squared_Av_error - squared_Av_error_from_mags

		# print "Error from SpT", np.sqrt(squared_Av_error_from_SpT)

		Av_error =\
		np.zeros(array_len) + np.NaN

		Av_error[cond_enough_dof] =\
		np.sqrt(squared_Av_error[cond_enough_dof])

		# print "Av_err_mag", Av_err_bands_for_mag
		# print "Av_err_SpT", Av_err_for_SpT*np.square(spt_errors[cond_enough_dof])
		#
		# print "Av_error", Av_error

		### Remove all SpT = NaN values
		cond_spt_nan = np.isnan(spts)
		Av_error[cond_spt_nan] = np.NaN

		### Remove all chisq = NaN values
		chisq_result =\
		self.chisq_from_av(np.zeros(array_len), spts, spt_errors, photometries, photometric_errors,
		curr_weights, color_weights, PhotometricSystem = PhotometricSystem,
		ColorInterpolators = ColorInterpolators,
		ExtinctionInterpolator = ExtinctionInterpolator,
		MergingInterpolator = MergingInterpolator,
		interpolation_kind = interpolation_kind,
		output_unit = output_unit)
		cond_chisq_nan = np.isnan(chisq_result["chisq"])
		Av_error[cond_chisq_nan] = np.NaN

		for selection_band in photometries:
			Av_err_bands_for_mag[selection_band][cond_spt_nan] = np.NaN
			Av_err_bands_for_mag[selection_band][cond_chisq_nan] = np.NaN

		Av_err_for_mag = np.zeros(array_len) + np.NaN

		for selection_band in photometries:
			for array_idx in range(array_len):
				if np.isnan(Av_err_for_mag[array_idx]):
					Av_err_for_mag[array_idx] = Av_err_bands_for_mag[selection_band][array_idx]
				else:
					Av_err_for_mag[array_idx] += Av_err_bands_for_mag[selection_band][array_idx]

		Av_sqrt_err_bands_for_mag = {}
		for selection_band in photometries:
			Av_sqrt_err_bands_for_mag[selection_band] = np.sqrt(Av_err_bands_for_mag[selection_band])

		### Create a dictionary of return values:
		return_dict = {}
		return_dict["Av_error"] = Av_error
		return_dict["Av_err_bands_for_mag"] =\
		Av_sqrt_err_bands_for_mag
		return_dict["Av_err_for_mag"] =\
		np.sqrt(Av_err_for_mag)
		return_dict["Av_err_for_SpT"] =\
		np.sqrt(Av_err_for_SpT)

		return return_dict
	def calculate_effective_N_SED(self, photometries, photometric_errors,
	curr_weights, color_weights, PhotometricSystem = PhotometricSystem(),
	ColorInterpolators = PecautMamajekYoungInterpolator,
	ExtinctionInterpolator =\
	MergedFallbackInstantiatedInterpolator([
	WhitneyInterpolator(interpolation_kind = "linear"),
	MathisInterpolator(interpolation_kind = "linear")]),
	MergingInterpolator = MergedFallbackInstantiatedInterpolator,
	interpolation_kind = "linear",
	output_unit = None, power_Ai_Av = 2.):
		"""Calculate effective N for a combination of weights and errors."""
		array_len = len(list(photometries.values())[0])
		Av_err_bands_for_mag = {} # This is defined as the chisq statistic for any given band
		Av_err_bands_for_mag_numerator = {}
		Av_err_bands_for_mag_denominator = {}
		squared_Av_error = np.zeros(array_len)
		summed_Av_error = np.zeros(array_len)
		num_dof = np.zeros(array_len)
		num_dof_bands = {}

		effective_N = np.zeros(array_len) + np.NaN

		for selection_band in photometries:
			Av_err_bands_for_mag_numerator[selection_band] = np.zeros(array_len)
			Av_err_bands_for_mag[selection_band] = np.zeros(array_len)
			Av_err_bands_for_mag_denominator[selection_band] = np.zeros(array_len)
			num_dof_bands[selection_band] = np.zeros(array_len)
			for secondary_band in photometries:
				if secondary_band != selection_band:
					color_text = selection_band + "-" + secondary_band
					if not (color_text in self.color_graph_interpolators):
						self.color_graph_interpolators[color_text] =\
						ColorGraphInterpolator(color_text,
						PhotometricSystem =\
						PhotometricSystem,
						ColorInterpolators =\
						ColorInterpolators,
						MergingInterpolator =\
						MergingInterpolator,
						interpolation_kind =\
						interpolation_kind,
						output_unit = output_unit)

					if not (selection_band in self.extinction_vals):
						self.extinction_vals[selection_band] =\
						ExtinctionInterpolator.evaluate(PhotometricSystem(selection_band))
					if not (secondary_band in self.extinction_vals):
						self.extinction_vals[secondary_band] =\
						ExtinctionInterpolator.evaluate(PhotometricSystem(secondary_band))

					Av_err_bands_for_mag_numerator_part =\
					np.power( self.extinction_vals[selection_band] \
					- self.extinction_vals[secondary_band], power_Ai_Av - 1)/\
					(np.square(photometric_errors[selection_band]/curr_weights[selection_band]) +\
					np.square(photometric_errors[secondary_band]/curr_weights[secondary_band]))*np.square(color_weights[color_text])

					Av_err_bands_for_mag_denominator_part =\
					np.power\
					( self.extinction_vals[selection_band] \
					- self.extinction_vals[secondary_band], power_Ai_Av)/\
					(np.square(photometric_errors[selection_band]/curr_weights[selection_band]) +\
					np.square(photometric_errors[secondary_band]/curr_weights[secondary_band]))*np.square(color_weights[color_text])

					cond_not_nan =\
					np.logical_and(np.logical_not(np.isnan(Av_err_bands_for_mag_numerator_part)),
					np.logical_not(np.isnan(Av_err_bands_for_mag_denominator_part)))

					Av_err_bands_for_mag_numerator[selection_band][cond_not_nan] +=\
					Av_err_bands_for_mag_numerator_part[cond_not_nan]

					Av_err_bands_for_mag_denominator[selection_band][cond_not_nan] +=\
					Av_err_bands_for_mag_denominator_part[cond_not_nan]

					num_dof[cond_not_nan] += 0.5
					num_dof_bands[selection_band][cond_not_nan] += 0.5

			# Consider num_dof_bands:
			# Only calculate Av_err_bands
			cond_enough_dof = num_dof_bands[selection_band] > 0
			Av_err_bands_for_mag[selection_band][cond_enough_dof] =\
			np.square(\
			Av_err_bands_for_mag_numerator[selection_band][cond_enough_dof]/\
			Av_err_bands_for_mag_denominator[selection_band][cond_enough_dof])

			cond_not_nan =\
			np.logical_and(np.logical_not(np.isnan(Av_err_bands_for_mag[selection_band])),
			np.logical_not(np.isnan(photometric_errors[selection_band])))

			cond_valid = np.logical_and(cond_not_nan, cond_enough_dof)

			squared_Av_error[cond_valid] += \
			Av_err_bands_for_mag[selection_band][cond_valid]*\
			np.square(photometric_errors[selection_band][cond_valid])

			summed_Av_error[cond_valid] += \
			np.sqrt(Av_err_bands_for_mag[selection_band][cond_valid])*\
			np.abs(photometric_errors[selection_band][cond_valid])

		effective_N[cond_valid] =\
		1./(np.sqrt(squared_Av_error[cond_valid])/summed_Av_error[cond_valid])

		print "N_eff", effective_N

		return effective_N
	def calculate_effective_N(self, photometries, photometric_errors,
	curr_weights, color_weights, PhotometricSystem = PhotometricSystem(),
	ColorInterpolators = PecautMamajekYoungInterpolator,
	ExtinctionInterpolator =\
	MergedFallbackInstantiatedInterpolator([
	WhitneyInterpolator(interpolation_kind = "linear"),
	MathisInterpolator(interpolation_kind = "linear")]),
	MergingInterpolator = MergedFallbackInstantiatedInterpolator,
	interpolation_kind = "linear",
	output_unit = None, power_Ai_Av = 2.):
		"""Calculate effective N for a combination of weights and errors."""
		array_len = len(list(photometries.values())[0])
		Av_err_bands_for_mag = {} # This is defined as the chisq statistic for any given band
		Av_err_bands_for_mag_numerator = {}
		Av_err_bands_for_mag_denominator = {}
		squared_Av_error = np.zeros(array_len)
		summed_Av_error = np.zeros(array_len)
		num_dof = np.zeros(array_len)
		num_dof_bands = {}

		effective_N = np.zeros(array_len) + np.NaN

		for selection_band in photometries:
			Av_err_bands_for_mag_numerator[selection_band] = np.zeros(array_len)
			Av_err_bands_for_mag[selection_band] = np.zeros(array_len)
			Av_err_bands_for_mag_denominator[selection_band] = np.zeros(array_len)
			num_dof_bands[selection_band] = np.zeros(array_len)
			for secondary_band in photometries:
				if secondary_band != selection_band:
					color_text = selection_band + "-" + secondary_band
					if not (color_text in self.color_graph_interpolators):
						self.color_graph_interpolators[color_text] =\
						ColorGraphInterpolator(color_text,
						PhotometricSystem =\
						PhotometricSystem,
						ColorInterpolators =\
						ColorInterpolators,
						MergingInterpolator =\
						MergingInterpolator,
						interpolation_kind =\
						interpolation_kind,
						output_unit = output_unit)

					if not (selection_band in self.extinction_vals):
						self.extinction_vals[selection_band] =\
						ExtinctionInterpolator.evaluate(PhotometricSystem(selection_band))
					if not (secondary_band in self.extinction_vals):
						self.extinction_vals[secondary_band] =\
						ExtinctionInterpolator.evaluate(PhotometricSystem(secondary_band))

					Av_err_bands_for_mag_numerator_part =\
					np.power( self.extinction_vals[selection_band] \
					- self.extinction_vals[secondary_band], power_Ai_Av - 1)/\
					(np.square(photometric_errors[selection_band]/curr_weights[selection_band]) +\
					np.square(photometric_errors[secondary_band]/curr_weights[secondary_band]))*np.square(color_weights[color_text])

					Av_err_bands_for_mag_denominator_part =\
					np.power\
					( self.extinction_vals[selection_band] \
					- self.extinction_vals[secondary_band], power_Ai_Av)/\
					(np.square(photometric_errors[selection_band]/curr_weights[selection_band]) +\
					np.square(photometric_errors[secondary_band]/curr_weights[secondary_band]))*np.square(color_weights[color_text])

					cond_not_nan =\
					np.logical_and(np.logical_not(np.isnan(Av_err_bands_for_mag_numerator_part)),
					np.logical_not(np.isnan(Av_err_bands_for_mag_denominator_part)))

					Av_err_bands_for_mag_numerator[selection_band][cond_not_nan] +=\
					Av_err_bands_for_mag_numerator_part[cond_not_nan]

					Av_err_bands_for_mag_denominator[selection_band][cond_not_nan] +=\
					Av_err_bands_for_mag_denominator_part[cond_not_nan]

					num_dof[cond_not_nan] += 0.5
					num_dof_bands[selection_band][cond_not_nan] += 0.5

			# Consider num_dof_bands:
			# Only calculate Av_err_bands
			cond_enough_dof = num_dof_bands[selection_band] > 0
			Av_err_bands_for_mag[selection_band][cond_enough_dof] =\
			np.square(\
			Av_err_bands_for_mag_numerator[selection_band][cond_enough_dof]/\
			Av_err_bands_denominator[selection_band][cond_enough_dof])

			cond_not_nan =\
			np.logical_and(np.logical_not(np.isnan(Av_err_bands_for_mag[selection_band])),
			np.logical_not(np.isnan(photometric_errors[selection_band])))

			cond_valid = np.logical_and(cond_not_nan, cond_enough_dof)

			squared_Av_error[cond_valid] += \
			Av_err_bands_for_mag[selection_band][cond_valid]*\
			np.square(photometric_errors[selection_band][cond_valid])

			summed_Av_error[cond_valid] += \
			np.sqrt(Av_err_bands_for_mag[selection_band][cond_valid])*\
			np.abs(photometric_errors[selection_band][cond_valid])

		effective_N[cond_valid] =\
		np.sqrt(squared_Av_error[cond_valid])/summed_Av_error[cond_valid]

		print "N_eff", effective_N

		return effective_N
	# def calculate_effective_N_old(self, photometries, photometric_errors,
	# curr_weights, PhotometricSystem = PhotometricSystem(),
	# ColorInterpolators = PecautMamajekYoungInterpolator,
	# ExtinctionInterpolator =\
	# MergedFallbackInstantiatedInterpolator([
	# WhitneyInterpolator(interpolation_kind = "linear"),
	# MathisInterpolator(interpolation_kind = "linear")]),
	# MergingInterpolator = MergedFallbackInstantiatedInterpolator,
	# interpolation_kind = "linear",
	# output_unit = None):
	# 	"""Calculate effective N for a combination of weights and errors."""
	# 	array_len = len(list(photometries.values())[0])
	# 	Av_err_denominator = np.zeros(array_len)
	# 	Av_err_bands_for_mag = {} # This is defined as the chisq statistic for any given band
	# 	Av_err_bands_for_mag_numerator = {}
	# 	Av_err_bands_denominator = {}
	# 	squared_Av_error = np.zeros(array_len)
	# 	summed_Av_error = np.zeros(array_len)
	# 	num_dof = np.zeros(array_len)
	# 	num_dof_bands = {}
	#
	# 	effective_N = np.zeros(array_len) + np.NaN
	#
	# 	for selection_band in photometries:
	# 		Av_err_bands_for_mag_numerator[selection_band] = np.zeros(array_len)
	# 		Av_err_bands_for_mag[selection_band] = np.zeros(array_len)
	# 		Av_err_bands_denominator[selection_band] = np.zeros(array_len)
	# 		num_dof_bands[selection_band] = np.zeros(array_len)
	# 		for secondary_band in photometries:
	# 			if secondary_band != selection_band:
	# 				color_text = selection_band + "-" + secondary_band
	# 				if not (color_text in self.color_graph_interpolators):
	# 					self.color_graph_interpolators[color_text] =\
	# 					ColorGraphInterpolator(color_text,
	# 					PhotometricSystem =\
	# 					PhotometricSystem,
	# 					ColorInterpolators =\
	# 					ColorInterpolators,
	# 					MergingInterpolator =\
	# 					MergingInterpolator,
	# 					interpolation_kind =\
	# 					interpolation_kind,
	# 					output_unit = output_unit)
	#
	# 				if not (selection_band in self.extinction_vals):
	# 					self.extinction_vals[selection_band] =\
	# 					ExtinctionInterpolator.evaluate(PhotometricSystem(selection_band))
	# 				if not (secondary_band in self.extinction_vals):
	# 					self.extinction_vals[secondary_band] =\
	# 					ExtinctionInterpolator.evaluate(PhotometricSystem(secondary_band))
	#
	# 				Av_err_bands_for_mag_numerator_part =\
	# 				curr_weights[selection_band]*\
	# 				curr_weights[secondary_band]*\
	# 				np.power( self.extinction_vals[selection_band] \
	# 				- self.extinction_vals[secondary_band], 5.)/\
	# 				(np.square(photometric_errors[selection_band]) +\
	# 				np.square(photometric_errors[secondary_band]))
	#
	# 				Av_err_denominator_part =\
	# 				curr_weights[selection_band]*\
	# 				curr_weights[secondary_band]*\
	# 				np.power\
	# 				( self.extinction_vals[selection_band] \
	# 				- self.extinction_vals[secondary_band], 6.)/\
	# 				(np.square(photometric_errors[selection_band]) +\
	# 				np.square(photometric_errors[secondary_band]))
	#
	# 				cond_not_nan =\
	# 				np.logical_and(np.logical_not(np.isnan(Av_err_bands_for_mag_numerator_part)),
	# 				np.logical_not(np.isnan(Av_err_denominator_part)),
	# 				np.logical_not(np.isnan(Av_err_denominator_part)))
	#
	# 				Av_err_bands_for_mag_numerator[selection_band][cond_not_nan] +=\
	# 				Av_err_bands_for_mag_numerator_part[cond_not_nan]
	#
	# 				Av_err_denominator[cond_not_nan] +=\
	# 				Av_err_denominator_part[cond_not_nan]
	#
	# 				Av_err_bands_denominator[selection_band][cond_not_nan] +=\
	# 				Av_err_denominator_part[cond_not_nan]
	#
	# 				num_dof[cond_not_nan] += 0.5
	# 				num_dof_bands[selection_band][cond_not_nan] += 0.5
	#
	# 		# Consider num_dof_bands:
	# 		# Only calculate Av_err_bands
	# 		cond_enough_dof = num_dof_bands[selection_band] > 0
	# 		Av_err_bands_for_mag[selection_band][cond_enough_dof] =\
	# 		np.square(\
	# 		Av_err_bands_for_mag_numerator[selection_band][cond_enough_dof]/\
	# 		Av_err_bands_denominator[selection_band][cond_enough_dof])
	#
	# 		cond_not_nan =\
	# 		np.logical_and(np.logical_not(np.isnan(Av_err_bands_for_mag[selection_band])),
	# 		np.logical_not(np.isnan(photometric_errors[selection_band])))
	#
	# 		cond_valid = np.logical_and(cond_not_nan, cond_enough_dof)
	#
	# 		squared_Av_error[cond_valid] += \
	# 		Av_err_bands_for_mag[selection_band][cond_valid]*\
	# 		np.square(photometric_errors[selection_band][cond_valid])
	#
	# 		summed_Av_error[cond_valid] += \
	# 		np.sqrt(Av_err_bands_for_mag[selection_band][cond_valid])*\
	# 		np.abs(photometric_errors[selection_band][cond_valid])
	#
	# 		effective_N[cond_valid] =\
	# 		0.5 + np.sqrt(0.25 +\
	# 		summed_Av_error[cond_valid]/squared_Av_error[cond_valid])
	#
	# 	print "N_eff", effective_N
	#
	# 	return effective_N
	# def calculate_effective_N_bcv(self, photometries, photometric_errors,
	# curr_weights, PhotometricSystem = PhotometricSystem(),
	# ColorInterpolators = PecautMamajekYoungInterpolator,
	# ExtinctionInterpolator =\
	# MergedFallbackInstantiatedInterpolator([
	# WhitneyInterpolator(interpolation_kind = "linear"),
	# MathisInterpolator(interpolation_kind = "linear")]),
	# MergingInterpolator = MergedFallbackInstantiatedInterpolator,
	# interpolation_kind = "linear",
	# output_unit = None,
	# secondary_band = "V"):
	# 	"""Calculate effective N for a combination of weights and errors."""
	# 	array_len = len(list(photometries.values())[0])
	# 	Av_err_denominator = np.zeros(array_len)
	# 	Av_err_bands_for_mag = {} # This is defined as the chisq statistic for any given band
	# 	Av_err_bands_for_mag_numerator = {}
	# 	Av_err_bands_denominator = {}
	# 	squared_Av_error = np.zeros(array_len)
	# 	summed_Av_error = np.zeros(array_len)
	# 	num_dof = np.zeros(array_len)
	# 	num_dof_bands = {}
	#
	# 	effective_N = np.zeros(array_len) + np.NaN
	#
	# 	for selection_band in photometries:
	# 		Av_err_bands_for_mag_numerator[selection_band] = np.zeros(array_len)
	# 		Av_err_bands_for_mag[selection_band] = np.zeros(array_len)
	# 		Av_err_bands_denominator[selection_band] = np.zeros(array_len)
	# 		num_dof_bands[selection_band] = np.zeros(array_len)
	# 		if secondary_band != selection_band:
	# 			color_text = selection_band + "-" + secondary_band
	# 			if not (color_text in self.color_graph_interpolators):
	# 				self.color_graph_interpolators[color_text] =\
	# 				ColorGraphInterpolator(color_text,
	# 				PhotometricSystem =\
	# 				PhotometricSystem,
	# 				ColorInterpolators =\
	# 				ColorInterpolators,
	# 				MergingInterpolator =\
	# 				MergingInterpolator,
	# 				interpolation_kind =\
	# 				interpolation_kind,
	# 				output_unit = output_unit)
	#
	# 			if not (selection_band in self.extinction_vals):
	# 				self.extinction_vals[selection_band] =\
	# 				ExtinctionInterpolator.evaluate(PhotometricSystem(selection_band))
	# 			if not (secondary_band in self.extinction_vals):
	# 				self.extinction_vals[secondary_band] =\
	# 				ExtinctionInterpolator.evaluate(PhotometricSystem(secondary_band))
	#
	# 			Av_err_bands_for_mag_numerator_part =\
	# 			curr_weights[selection_band]*\
	# 			( self.extinction_vals[selection_band] \
	# 			- self.extinction_vals[secondary_band])/\
	# 			(np.square(photometric_errors[selection_band]))
	#
	# 			Av_err_denominator_part =\
	# 			curr_weights[selection_band]*\
	# 			np.square\
	# 			( self.extinction_vals[selection_band] \
	# 			- self.extinction_vals[secondary_band])/\
	# 			(np.square(photometric_errors[selection_band]))
	#
	# 			cond_not_nan =\
	# 			np.logical_and(np.logical_not(np.isnan(Av_err_bands_for_mag_numerator_part)),
	# 			np.logical_not(np.isnan(Av_err_denominator_part)),
	# 			np.logical_not(np.isnan(Av_err_denominator_part)))
	#
	# 			Av_err_bands_for_mag_numerator[selection_band][cond_not_nan] +=\
	# 			Av_err_bands_for_mag_numerator_part[cond_not_nan]
	#
	# 			Av_err_denominator[cond_not_nan] +=\
	# 			Av_err_denominator_part[cond_not_nan]
	#
	# 			Av_err_bands_denominator[selection_band][cond_not_nan] +=\
	# 			Av_err_denominator_part[cond_not_nan]
	#
	# 			num_dof[cond_not_nan] += 1.
	# 			num_dof_bands[selection_band][cond_not_nan] += 1.
	#
	# 		# Consider num_dof_bands:
	# 		# Only calculate Av_err_bands
	# 		cond_enough_dof = num_dof_bands[selection_band] > 0
	# 		Av_err_bands_for_mag[selection_band][cond_enough_dof] =\
	# 		np.square(\
	# 		Av_err_bands_for_mag_numerator[selection_band][cond_enough_dof]/\
	# 		Av_err_bands_denominator[selection_band][cond_enough_dof])
	#
	# 		cond_not_nan =\
	# 		np.logical_and(np.logical_not(np.isnan(Av_err_bands_for_mag[selection_band])),
	# 		np.logical_not(np.isnan(photometric_errors[selection_band])))
	#
	# 		cond_valid = np.logical_and(cond_not_nan, cond_enough_dof)
	#
	# 		squared_Av_error[cond_valid] += \
	# 		Av_err_bands_for_mag[selection_band][cond_valid]*\
	# 		np.square(photometric_errors[selection_band][cond_valid])
	#
	# 		summed_Av_error[cond_valid] += \
	# 		np.sqrt(Av_err_bands_for_mag[selection_band][cond_valid])*\
	# 		np.abs(photometric_errors[selection_band][cond_valid])
	#
	# 		effective_N[cond_valid] =\
	# 		0.5 + np.sqrt(0.25 +\
	# 		summed_Av_error[cond_valid]/squared_Av_error[cond_valid])
	#
	# 	print "N_eff bcv", effective_N
	#
	# 	return effective_N
	def calculate_guess_av(self, spts, photometries, photometric_errors,
	curr_weights, PhotometricSystem = PhotometricSystem(),
	ColorInterpolators = PecautMamajekYoungInterpolator,
	ExtinctionInterpolator =\
	MergedFallbackInstantiatedInterpolator([
	WhitneyInterpolator(interpolation_kind = "linear"),
	MathisInterpolator(interpolation_kind = "linear")]),
	MergingInterpolator = MergedFallbackInstantiatedInterpolator,
	interpolation_kind = "linear",
	output_unit = None):
		array_len = len(list(photometries.values())[0])
		Av_guess = np.zeros(array_len) + np.NaN

		for selection_band in photometries:
			for secondary_band in photometries:
				if secondary_band != selection_band:
					color_text = selection_band + "-" + secondary_band
					if not (color_text in self.color_graph_interpolators):
						self.color_graph_interpolators[color_text] =\
						ColorGraphInterpolator(color_text,
						PhotometricSystem =\
						PhotometricSystem,
						ColorInterpolators =\
						ColorInterpolators,
						MergingInterpolator =\
						MergingInterpolator,
						interpolation_kind =\
						interpolation_kind,
						output_unit = output_unit)

					if not (selection_band in self.extinction_vals):
						self.extinction_vals[selection_band] =\
						ExtinctionInterpolator.evaluate(PhotometricSystem(selection_band))
					if not (secondary_band in self.extinction_vals):
						self.extinction_vals[secondary_band] =\
						ExtinctionInterpolator.evaluate(PhotometricSystem(secondary_band))

					intrinsic_color =\
					self.color_graph_interpolators[color_text].evaluate(spts)

					Band_Av_guess =\
					((photometries[selection_band] -\
					photometries[secondary_band]) -\
					(intrinsic_color))/\
					( self.extinction_vals[selection_band] \
					- self.extinction_vals[secondary_band])

					cond_is_nan =\
					np.isnan(Av_guess)

					### Update Av_guess with Band_Av_guess if a guess hasn't
					### already been given:

					Av_guess[cond_is_nan] = Band_Av_guess[cond_is_nan]

		return Av_guess
	def helper_grad_chisq_from_av(self,
	Av_guess, spts, spt_errors, photometries, photometric_errors,
	curr_weights, PhotometricSystem,
	ColorInterpolators,
	ExtinctionInterpolator,
	MergingInterpolator,
	interpolation_kind,
	output_unit):
		grad_chisq_dict =\
		self.grad_chisq_from_av(Av_guess, spts, spt_errors, photometries, photometric_errors,
		curr_weights, PhotometricSystem = self.PhotometricSystem,
		ColorInterpolators = self.ColorInterpolators,
		ExtinctionInterpolator = self.ExtinctionInterpolator,
		MergingInterpolator = self.MergingInterpolator,
		interpolation_kind = self.interpolation_kind,
		output_unit = self.output_unit)
		# print "grad_chisq", grad_chisq_dict["grad_chisq"]
		grad_val = grad_chisq_dict["grad_chisq"]
		return grad_val
	def minimize_helper_chisq_from_av(self,
	Av_guess, spts, spt_errors, photometries, photometric_errors,
	curr_weights, PhotometricSystem,
	ColorInterpolators,
	ExtinctionInterpolator,
	MergingInterpolator,
	interpolation_kind,
	output_unit):
		"""This routine calculates a single chisq statistic for an input vector."""
		chisq_dict =\
		self.chisq_from_av(Av_guess, spts, spt_errors, photometries, photometric_errors,
		curr_weights, PhotometricSystem = PhotometricSystem,
		ColorInterpolators = ColorInterpolators,
		ExtinctionInterpolator = ExtinctionInterpolator,
		MergingInterpolator = MergingInterpolator,
		interpolation_kind = interpolation_kind,
		output_unit = output_unit)
		chisq_array =\
		chisq_dict["chisq"]
		log_chisq_statistic =\
		np.nansum(np.log10(chisq_array)[np.isfinite(np.log10(chisq_array))])
		#print "log_chisq_statistic", log_chisq_statistic
		return log_chisq_statistic
	def get_av_and_good_bands(self, spts, curr_weights, av_tol = 1e-2, av_min = 0., av_max = 10.,
	chisq_max = 10., test_and_plot_individual_bands = False,
	test_and_plot_grouped_teff_bands = False, num_iqr_outlier = 1.5,
	num_samples = 34, get_confidence_interval_from_chisq = True,
	photometric_errors = None,
				  **photometries):
		"""Evaluates Av_SED with a given series of SpTs and photometries.

		Returns Av value, as well as the photometries and photometric errors corresponding to the good bands.
		"""
		# Make sure input SpT's are in a Numpy array.
		spts = np.array(spts)
		### Sort photometries according to how close they are to the desired
		### pivot band.
		photometry_list = photometries.keys()
		photometry_list = sorted(photometry_list, key=\
								 lambda x:\
								 abs(self.PhotometricSystem(x) - \
									 self.PhotometricSystem(self.pivot_band)))

		color_combinations =\
		list(it.combinations(sorted(photometries.keys(),
		key=self.PhotometricSystem), 2))

		# ### Now, we simply construct the Av reasonable left and right
		# ### golden section search.
		# ### However, there might not be a minimum chisq for the Av.
		# ### The termination condition used should just be that the bounding
		# ### size of the Av box, in Av space, is smaller than some tolerance.
		# ### This corresponds to \delta Av < tol
		array_len = len(list(photometries.values())[0])

		if photometric_errors is None:
			photometric_errors = {}
			for band in photometries:
				photometric_errors[band] = 1.

		# teffs =\
		# self.spt_teff_interpolator.evaluate(spts)

		# Av_guess =\
		# np.clip(self.calculate_guess_av(spts, photometries, photometric_errors,
		# curr_weights, PhotometricSystem = self.PhotometricSystem,
		# ColorInterpolators = self.ColorInterpolators,
		# ExtinctionInterpolator = self.ExtinctionInterpolator,
		# MergingInterpolator = self.MergingInterpolator,
		# interpolation_kind = self.interpolation_kind,
		# output_unit = self.output_unit), av_min, av_max)

		### The condition is if Av_result["Av_sol_bands"]

		Av_result =\
		self.get_Av_solution(spts, photometries, photometric_errors,
		curr_weights, PhotometricSystem = self.PhotometricSystem,
		ColorInterpolators = self.ColorInterpolators,
		ExtinctionInterpolator = self.ExtinctionInterpolator,
		MergingInterpolator = self.MergingInterpolator,
		interpolation_kind = self.interpolation_kind,
		output_unit = self.output_unit)

		Av_soln =\
		np.clip(Av_result["Av_sol"], av_min, av_max)

		Av_sol_bands = {}

		### Clip Av_sol_bands:
		for band in Av_result["Av_sol_bands"]:
			Av_sol_bands[band] =\
			np.clip(Av_result["Av_sol_bands"][band], av_min, av_max)

		### Specifically remove all bands for stars that give an outlier for Av
		### On an individual basis.

		bool_Av_discrepant = {}
		# test_statistic = {}

		for band in Av_result["Av_sol_bands"]:
			Av_sol_band = {}
			for x in Av_result["Av_sol_bands"]:
				Av_sol_band[x] = np.zeros(len(spts)) + np.NaN
			for star_idx in range(len(spts)):

				### First of all, calculate an Av group starting from the max and min value
				### and marching outwards in "reasonable" increments.
				### This "reasonable" increment is designed to help prevent islands of solutions.
				Av_and_band_name_tuple =\
				zip(*sorted([(Av_result["Av_sol_bands"][x][star_idx], x) \
				for x in Av_result["Av_sol_bands"] \
				if not np.isnan(Av_result["Av_sol_bands"][x][star_idx]) and \
				self.PhotometricSystem[x] < 1.], key = lambda y: y[0]))

				if len(Av_and_band_name_tuple) > 0 and len(Av_and_band_name_tuple[0]) > 1:

					Av_vals, band_names_sorted = Av_and_band_name_tuple

					Av_range = Av_vals[len(Av_vals) - 1] - Av_vals[0]

					Av_max_block = []
					Av_min_block = []

					# March from the max.
					if Av_vals[len(Av_vals) - 2] - Av_vals[len(Av_vals) - 1] < Av_range/(len(Av_vals) * 10.):
						Av_max_block.append(Av_vals[len(Av_vals) - 1])
						Av_max_block.append(Av_vals[len(Av_vals) - 2])
						for x in range(len(Av_vals) - 3):
							if -Av_vals[len(Av_vals) - 3 - x] + Av_max_block[-1] < Av_range/(len(Av_vals) * 10.):
								Av_max_block.append(Av_vals[len(Av_vals) - 3 - x])
						# print "group detected at max!", Av_max_block

					# March from the min.
					if Av_vals[1] - Av_vals[0] < Av_range/(len(Av_vals) * 10.):
						Av_min_block.append(Av_vals[0])
						Av_min_block.append(Av_vals[1])
						for x in range(len(Av_vals) - 3):
							if Av_vals[x + 2] - Av_min_block[-1] < Av_range/(len(Av_vals) * 10.):
								Av_min_block.append(Av_vals[x + 2])
						# print "group detected at min!", Av_min_block

					# print Av_vals

					dispersion_not_removed = np.std(Av_vals)
					# print "dispersion, not removed", dispersion_not_removed

					# Try removing the max group, calculate the Av dispersion.
					# print Av_vals[0:len(Av_vals) - len(Av_max_block)]
					dispersion_removed_max = np.std(Av_vals[0:len(Av_vals) - len(Av_max_block)])
					# print "dispersion, removed max", dispersion_removed_max

					# Try removing the min group, calculate the Av dispersion.
					print Av_vals[len(Av_min_block):]
					dispersion_removed_min = np.std(Av_vals[len(Av_min_block):])
					# print "dispersion, removed min", dispersion_removed_min

					### If our dispersion gets better by a factor of 2 relative
					### to not removing any block at all, then it is worth it
					### to remove the block outliers.
					### Otherwise, do nothing.

					if dispersion_not_removed > 2.*np.min([dispersion_removed_min, dispersion_removed_max]):
						if dispersion_removed_max < dispersion_removed_min:
							#Then we should remove the block corresponding to the max.
							for block_idx, block_band_added in enumerate(band_names_sorted[0:len(Av_vals) - len(Av_max_block)]):
								Av_sol_band[block_band_added][star_idx] = Av_vals[block_idx]
						else:
							#Then we should remove the block corresponding to the min.
							for block_idx, block_band_added in enumerate(band_names_sorted[len(Av_min_block):]):
								Av_sol_band[block_band_added][star_idx] = Av_vals[len(Av_min_block)+block_idx]
					else:
						for block_idx, block_band_added in enumerate(band_names_sorted):
							Av_sol_band[block_band_added][star_idx] = Av_vals[block_idx]

			### Calculate IQR, and calculate the perturbed IQR.

			iqr =\
			[scipy.stats.iqr([Av_sol_band[x][star_idx] \
			for x in Av_sol_band \
			if not np.isnan(Av_sol_band[x][star_idx]) and \
			self.PhotometricSystem[x] < 1.]) for star_idx in range(len(spts))]

			# perturbed_iqr_right_side =\
			# [np.min([100, 75./\
			# (1. - 1./\
			# np.sum([np.logical_not(np.isnan(\
			# Av_result["Av_sol_bands"][x][star_idx])) for \
			# x in Av_result["Av_sol_bands"]]))]) \
			# for star_idx in range(len(spts))]
			#
			# perturbed_iqr_range = \
			# [(100 - perturbed_iqr_right_side[star_idx], perturbed_iqr_right_side[star_idx]) \
			# for star_idx in range(len(spts))]
			#
			# print "perturbed_iqr_range", perturbed_iqr_range
			#
			# print [(len([Av_result["Av_sol_bands"][x][star_idx] \
			# for x in Av_result["Av_sol_bands"] \
			# if not np.isnan(Av_result["Av_sol_bands"][x][star_idx]) and \
			# self.PhotometricSystem[x] < 1.]), len([Av_result["Av_sol_bands"][x][star_idx] \
			# for x in Av_result["Av_sol_bands"] \
			# if not np.isnan(Av_result["Av_sol_bands"][x][star_idx]) \
			# and self.PhotometricSystem[x] < 1. and \
			# Av_result["Av_sol_bands"][x][star_idx] < \
			# np.nanmax([Av_result["Av_sol_bands"][y][star_idx] for y in \
			# Av_result["Av_sol_bands"]]) \
			# and \
			# Av_result["Av_sol_bands"][x][star_idx] > \
			# np.nanmin([Av_result["Av_sol_bands"][y][star_idx] for y in \
			# Av_result["Av_sol_bands"]])])) for star_idx in range(len(spts))]
			#
			# print "Av test0", [Av_result["Av_sol_bands"][x][star_idx] \
			# for x in Av_result["Av_sol_bands"] \
			# if not np.isnan(Av_result["Av_sol_bands"][x][star_idx]) and \
			# self.PhotometricSystem[x] < 1.]
			#
			# print "Av test1", [Av_result["Av_sol_bands"][x][star_idx] \
			# for x in Av_result["Av_sol_bands"] \
			# if not np.isnan(Av_result["Av_sol_bands"][x][star_idx]) \
			# and self.PhotometricSystem[x] < 1. and \
			# Av_result["Av_sol_bands"][x][star_idx] < \
			# np.nanmax([Av_result["Av_sol_bands"][y][star_idx] for y in \
			# Av_result["Av_sol_bands"]]) \
			# and \
			# Av_result["Av_sol_bands"][x][star_idx] > \
			# np.nanmin([Av_result["Av_sol_bands"][y][star_idx] for y in \
			# Av_result["Av_sol_bands"]])]
			#
			# iqr_perturbed =\
			# [scipy.stats.iqr([Av_result["Av_sol_bands"][x][star_idx] \
			# for x in Av_result["Av_sol_bands"] \
			# if not np.isnan(Av_result["Av_sol_bands"][x][star_idx]) \
			# and self.PhotometricSystem[x] < 1. and \
			# Av_result["Av_sol_bands"][x][star_idx] < \
			# np.nanmax([Av_result["Av_sol_bands"][y][star_idx] for y in \
			# Av_result["Av_sol_bands"]]) \
			# and \
			# Av_result["Av_sol_bands"][x][star_idx] > \
			# np.nanmin([Av_result["Av_sol_bands"][y][star_idx] for y in \
			# Av_result["Av_sol_bands"]])], rng = perturbed_iqr_range[star_idx]) \
			# for star_idx in range(len(spts))]
			#
			# print "IQR", zip(iqr, iqr_perturbed)
			#
			# iqr = np.nanmin([iqr, iqr_perturbed], axis = 0)
			#
			# print "iqr", iqr

			bool_Av_discrepant[band] =\
			[np.abs(Av_sol_band[band][star_idx] -\
			np.nanmedian([Av_sol_band[x][star_idx] for x in Av_sol_band if self.PhotometricSystem[x] < 1.]))\
			> num_iqr_outlier*\
			iqr[star_idx] \
			or \
			np.isnan(Av_sol_band[band][star_idx])
			for star_idx in range(len(spts))]

			print band, \
			[np.abs(Av_sol_band[band][star_idx] -\
			np.nanmedian([Av_sol_band[x][star_idx] for \
			x in Av_sol_band if self.PhotometricSystem[x] < 1.]))\
			/(scipy.stats.iqr([Av_sol_band[x][star_idx] for \
			x in Av_sol_band if not \
			np.isnan(Av_sol_band[x][star_idx]) and \
			self.PhotometricSystem[x] < 1.])) for star_idx in range(len(spts))], bool_Av_discrepant[band]

			print band, "Av", \
			Av_sol_band[band]

			# test_statistic[band] =\
			# [np.abs(Av_result["Av_sol_bands"][band][star_idx] -\
			# np.nanmean([Av_result["Av_sol_bands"][x][star_idx] for x in Av_result["Av_sol_bands"] if self.PhotometricSystem[x] < 1.]))\
			# /scipy.stats.iqr([Av_result["Av_sol_bands"][x][star_idx] for x in Av_result["Av_sol_bands"] if not np.isnan(Av_result["Av_sol_bands"][x][star_idx]) and self.PhotometricSystem[x] < 1.])/1.35 \
			# for star_idx in range(len(spts))]

		photometries_well_behaved = {}
		photometric_errors_well_behaved = {}

		for band in bool_Av_discrepant:
			photometries_well_behaved[band] = np.zeros(len(spts)) + np.NaN
			photometric_errors_well_behaved[band] = np.zeros(len(spts)) + np.NaN
			for star_idx in range(len(spts)):
				if not bool_Av_discrepant[band][star_idx]:
					photometries_well_behaved[band][star_idx] = photometries[band][star_idx]
					photometric_errors_well_behaved[band][star_idx] = photometric_errors[band][star_idx]

		# print photometries_well_behaved

		Av_result =\
		self.get_Av_solution(spts, photometries_well_behaved, photometric_errors_well_behaved,
		curr_weights, PhotometricSystem = self.PhotometricSystem,
		ColorInterpolators = self.ColorInterpolators,
		ExtinctionInterpolator = self.ExtinctionInterpolator,
		MergingInterpolator = self.MergingInterpolator,
		interpolation_kind = self.interpolation_kind,
		output_unit = self.output_unit)

		Av_soln =\
		np.clip(Av_result["Av_sol"], av_min, av_max)

		### Clip Av_sol_bands:
		for band in Av_result["Av_sol_bands"]:
			Av_sol_bands[band] =\
			np.clip(Av_result["Av_sol_bands"][band], av_min, av_max)

		# print "Av", Av_result

		# cond_not_nan = np.logical_not(np.isnan(Av_soln))
		#
		# plt.scatter(Av_soln[cond_not_nan], Av_guess[cond_not_nan])
		# plt.show()

		return {"Av": Av_soln, "photometries": photometries_well_behaved, "photometric_errors": photometric_errors_well_behaved}
	def _evaluate(self, spts, curr_weights, av_tol = 1e-2, av_min = 0., av_max = 10.,
	chisq_max = 10., test_and_plot_individual_bands = False,
	test_and_plot_grouped_teff_bands = False, num_iqr_outlier = 1.5,
	num_samples = 34, get_confidence_interval_from_chisq = True,
	photometric_errors = None, spt_errors = None,
				  **photometries):

		value_dict =\
		self.get_Av_solution_SED(spts, spt_errors, photometries, photometric_errors,
		curr_weights, PhotometricSystem = self.PhotometricSystem,
		ColorInterpolators = self.ColorInterpolators,
		ExtinctionInterpolator = self.ExtinctionInterpolator,
		MergingInterpolator = self.MergingInterpolator,
		interpolation_kind = self.interpolation_kind,
		output_unit = self.output_unit)

		# value_dict = self.get_av_and_good_bands(spts, curr_weights, av_tol = av_tol, av_min = av_min, av_max = av_max,
		# chisq_max = chisq_max, test_and_plot_individual_bands = test_and_plot_individual_bands,
		# test_and_plot_grouped_teff_bands = test_and_plot_grouped_teff_bands, num_iqr_outlier = num_iqr_outlier,
		# num_samples = num_samples, get_confidence_interval_from_chisq = get_confidence_interval_from_chisq,
		# photometric_errors = photometric_errors,
		# 			  **photometries)
		# return value_dict["Av"]

		Av =\
		np.clip(value_dict["Av_sol"], av_min, av_max)

		return Av
	def _get_err(self, spts, spt_errors, photometries, photometric_errors, curr_weights, av_min = 0., av_max = 10.,
	chisq_max = 10.):
		"""Get Av error."""
		# Make sure input SpT's are in a Numpy array.
		spts = np.array(spts)
		### Sort photometries according to how close they are to the desired
		### pivot band.
		photometry_list = photometries.keys()
		photometry_list = sorted(photometry_list, key=\
								 lambda x:\
								 abs(self.PhotometricSystem(x) - \
									 self.PhotometricSystem(self.pivot_band)))

		color_combinations =\
		list(it.combinations(sorted(photometries.keys(),
		key=self.PhotometricSystem), 2))

		array_len = len(list(photometries.values())[0])

		Av_error =\
		self.get_Av_error(spts, spt_errors, photometries, photometric_errors,
		curr_weights, PhotometricSystem = self.PhotometricSystem,
		ColorInterpolators = self.ColorInterpolators,
		ExtinctionInterpolator = self.ExtinctionInterpolator,
		MergingInterpolator = self.MergingInterpolator,
		interpolation_kind = self.interpolation_kind,
		output_unit = self.output_unit)

		return Av_error["Av_error"]
	# def _do_chisq_map(self, spts, av_tol = 1e-2, av_min = 0., av_max = 10.,
	# chisq_max = 10., plot_av = None, save_plot = False, photometric_errors = None,
	# 			  **photometries):
	# 	"""Evaluates Av_SED with a given series of SpTs and photometries.
	# 	av_tol is the bounding box over which chi-sq minimization is done.
	# 	"""
	# 	# Make sure input SpT's are in a Numpy array.
	# 	spts = np.array(spts)
	# 	### Sort photometries according to how close they are to the desired
	# 	### pivot band.
	# 	photometry_list = photometries.keys()
	# 	photometry_list = sorted(photometry_list, key=\
	# 							 lambda x:\
	# 							 abs(self.PhotometricSystem(x) - \
	# 								 self.PhotometricSystem(self.pivot_band)))
	#
	# 	color_combinations =\
	# 	list(it.combinations(sorted(photometries.keys(),
	# 	key=self.PhotometricSystem), 2))
	#
	# 	# ### Now, we simply construct the Av reasonable left and right
	# 	# ### golden section search.
	# 	# ### However, there might not be a minimum chisq for the Av.
	# 	# ### The termination condition used should just be that the bounding
	# 	# ### size of the Av box, in Av space, is smaller than some tolerance.
	# 	# ### This corresponds to \delta Av < tol
	# 	array_len = len(list(photometries.values())[0])
	#
	# 	if photometric_errors is None:
	# 		photometric_errors = {}
	# 		for band in photometries:
	# 			photometric_errors[band] = 1.
	#
	# 	teffs =\
	# 	self.spt_teff_interpolator.evaluate(spts)
	#
	# 	curr_weights = {}
	#
	# 	for band in photometries:
	#
	# 		if self.flux_weighted == True:
	# 			### Do flux weighting by B_lambda
	# 			curr_weights[band] =\
	# 			np.power(10.,
	# 						self.log10_planck(self.PhotometricSystem[band],
	# 									teffs))
	# 		elif self.log_flux_weighted == True:
	# 			### Do flux weighting by log B_lambda
	# 			curr_weights[band] =\
	# 			np.clip(\
	# 						self.log10_planck(self.PhotometricSystem[band],
	# 									teffs), 0, None)
	# 		else:
	# 			curr_weights[band] = np.zeros(array_len) + np.NaN
	# 			curr_weights[band][np.logical_not(np.isnan(teffs))] = 1.
	#
	# 	Av_guess =\
	# 	np.clip(self.calculate_guess_av(spts, photometries, photometric_errors,
	# 	curr_weights, PhotometricSystem = self.PhotometricSystem,
	# 	ColorInterpolators = self.ColorInterpolators,
	# 	ExtinctionInterpolator = self.ExtinctionInterpolator,
	# 	MergingInterpolator = self.MergingInterpolator,
	# 	interpolation_kind = self.interpolation_kind,
	# 	output_unit = self.output_unit), av_min, av_max)
	#
	# 	Av_result =\
	# 	self.get_Av_solution(spts, photometries, photometric_errors,
	# 	curr_weights, PhotometricSystem = self.PhotometricSystem,
	# 	ColorInterpolators = self.ColorInterpolators,
	# 	ExtinctionInterpolator = self.ExtinctionInterpolator,
	# 	MergingInterpolator = self.MergingInterpolator,
	# 	interpolation_kind = self.interpolation_kind,
	# 	output_unit = self.output_unit)
	#
	# 	Av_soln =\
	# 	np.clip(Av_result["Av_sol"], av_min, av_max)
	#
	# 	Av_guess_list =\
	# 	np.arange(av_min, av_max, av_tol)
	#
	# 	# Run over the entire guess_list
	# 	guess_av_map = {}
	# 	chisq_map = {}
	#
	# 	for selection_band in photometries:
	# 		for elem_idx in range(len(spts)):
	# 			chisq_map[selection_band,elem_idx] = []
	#
	#
	# 	for elem_idx in range(len(spts)):
	# 		guess_av_map[elem_idx] = []
	#
	# 	for guess_idx in range(len(Av_guess_list)):
	# 		print guess_idx, "/", len(Av_guess_list)
	# 		# Initialize the golden section routine, by calculating Avs for a,b,bp,c
	# 		# and their associated chisq's.
	#
	# 		for selection_band in photometries:
	# 			band_chisq = np.zeros(array_len)
	# 			for secondary_band in photometries:
	# 				if secondary_band != selection_band:
	# 					chisq_result =\
	# 					self.chisq_from_av(Av_guess_list[guess_idx],
	# 					spts, photometries, photometric_errors,
	# 					curr_weights, PhotometricSystem = self.PhotometricSystem,
	# 					ColorInterpolators = self.ColorInterpolators,
	# 					ExtinctionInterpolator = self.ExtinctionInterpolator,
	# 					MergingInterpolator = self.MergingInterpolator,
	# 					interpolation_kind = self.interpolation_kind,
	# 					output_unit = self.output_unit)
	#
	# 					band_chisq =\
	# 					chisq_result["chisq_bands"][selection_band]
	#
	# 			for elem_idx in range(array_len):
	# 				#print selection_band,elem_idx,band_chisq[elem_idx]
	# 				chisq_map[selection_band,elem_idx].append(band_chisq[elem_idx])
	#
	# 		for elem_idx in range(array_len):
	# 			guess_av_map[elem_idx].append(Av_guess_list[guess_idx])
	#
	# 	if save_plot:
	# 		import matplotlib.pyplot as plt
	# 		for idx in guess_av_map:
	# 			#print zip(guess_av_map[idx],chisq_map[idx])
	# 			# plt.scatter(guess_av_map[idx],np.log10(chisq_map[idx]), marker=',')
	# 			for chisq_band in photometries:
	# 				plt.scatter(guess_av_map[idx], np.log10(chisq_map[chisq_band,idx]), marker = ',', label = chisq_band)
	# 			if plot_av is not None:
	# 				plt.axvline(plot_av[idx], linestyle = "dashed")
	# 				plt.axvline(Av_soln[idx], label = "$A_V$ soln")
	# 			plt.xlabel("$A_V$ $[mag]$")
	# 			plt.ylabel("Reduced $\\chi ^2$")
	# 			# plt.xlim(0.,5.)
	# 			plt.legend()
	# 			plt.savefig('chisq_comparison/%s.png' % (idx))
	# 			plt.clf()
	def plot_flux(self, current_si, yso_spts,
	star_name_dict, full_photometries, full_yso_photometry_e, photometries, yso_photometry_e, Av, Av_e,
	log_L, yso_dists,
	curr_weights,
	color_weights,
	chisq_from_av,
	output_location_flux = "",
	PhotometricSystem = PhotometricSystem(),
	ColorInterpolators = PecautMamajekYoungInterpolator,
	ExtinctionInterpolator =\
	MergedFallbackInstantiatedInterpolator([
	WhitneyInterpolator(interpolation_kind = "linear"),
	MathisInterpolator(interpolation_kind = "linear")]),
	MergingInterpolator = MergedFallbackInstantiatedInterpolator,
	interpolation_kind = "linear",
	plot = True,
	text = None):
		"""Plot fluxes alongside photometries. full_photometries denotes the photometry of the full system, and photometries denotes these after outlier rejection."""
		teffs =\
		self.spt_teff_interpolator.evaluate(yso_spts)
		### Do the flux plots vs. model!
		dereddened_photometry = {}
		dereddened_flux_dict = {}
		dereddened_flux_e_dict = {}

		reddened_photometry = {}
		reddened_flux_dict = {}
		reddened_flux_e_dict = {}

		for band in photometries:
			dereddened_photometry[band] = np.zeros(len(current_si)) + np.NaN
			dereddened_flux_dict[band] = np.zeros(len(current_si)) + np.NaN
			dereddened_flux_e_dict[band] = np.zeros(len(current_si)) + np.NaN
			reddened_photometry[band] = np.zeros(len(current_si)) + np.NaN
			reddened_flux_dict[band] = np.zeros(len(current_si)) + np.NaN
			reddened_flux_e_dict[band] = np.zeros(len(current_si)) + np.NaN
			for si_index, si_val in enumerate(current_si):
				dereddened_photometry[band][si_index] =\
				- Av[si_index] * ExtinctionInterpolator.evaluate(PhotometricSystem[band]) +\
				photometries[band][current_si.index(si_val)]

				reddened_photometry[band][si_index] =\
				photometries[band][current_si.index(si_val)]

		for band in photometries:
			for si_index, si_val in enumerate(current_si):
				dereddened_flux_dict[band][si_index] =\
				PhotometricSystem.get_flux(band, \
				dereddened_photometry[band][si_index] \
				)
				dereddened_flux_e_dict[band][si_index] =\
				np.sqrt(np.square(yso_photometry_e[band][si_index]) +\
				np.square(Av_e[si_index] * ExtinctionInterpolator.evaluate(PhotometricSystem[band]))) *\
				PhotometricSystem.get_abs_df_dmag(band, \
				dereddened_photometry[band][si_index] \
				)
				reddened_flux_dict[band][si_index] =\
				PhotometricSystem.get_flux(band, \
				reddened_photometry[band][si_index] \
				)
				reddened_flux_e_dict[band][si_index] =\
				np.sqrt(np.square(yso_photometry_e[band][si_index]) +\
				np.square(Av_e[si_index] * ExtinctionInterpolator.evaluate(PhotometricSystem[band]))) *\
				PhotometricSystem.get_abs_df_dmag(band, \
				reddened_photometry[band][si_index] \
				)

		reddened_full_flux_dict = {}
		reddened_full_flux_e_dict = {}
		for band in full_photometries:
			reddened_full_flux_dict[band] = np.zeros(len(current_si)) + np.NaN
			reddened_full_flux_e_dict[band] = np.zeros(len(current_si)) + np.NaN
			for si_index, si_val in enumerate(current_si):
				reddened_full_flux_dict[band][si_index] =\
				PhotometricSystem.get_flux(band, \
				full_photometries[band][si_index] \
				)
				# print "full_yso_photometry_e[band][si_index]", full_yso_photometry_e[band][si_index]
				# print "get_abs_df_dmag", PhotometricSystem.get_abs_df_dmag(band, \
				# full_photometries[band][si_index] \
				# )
				# print "Av_e[si_index] * ExtinctionInterpolator.evaluate(PhotometricSystem[band])", Av_e[si_index] * ExtinctionInterpolator.evaluate(PhotometricSystem[band])
				reddened_full_flux_e_dict[band][si_index] =\
				np.sqrt(np.square(full_yso_photometry_e[band][si_index]) +\
				np.square(Av_e[si_index] * ExtinctionInterpolator.evaluate(PhotometricSystem[band]))) *\
				PhotometricSystem.get_abs_df_dmag(band, \
				full_photometries[band][si_index] \
				)
		# print "reddened_flux_e_dict", reddened_flux_e_dict
		# print "reddened_full_flux_e_dict", reddened_full_flux_e_dict

		chisq_flux = np.zeros(len(current_si)) + np.NaN

		intrinsic_spectrum_calc =\
		IntrinsicSpectrumCalculator(PhotometricSystem = PhotometricSystem,
		ColorInterpolators = ColorInterpolators,
		ExtinctionInterpolator = ExtinctionInterpolator,
		MergingInterpolator = MergedFallbackInstantiatedInterpolator,
		interpolation_kind = interpolation_kind,
		output_unit = None,
		required_dof = 2,
		pivot_band = "Y")

		# Calculate flux from color tables.
		intrinsic_spectrum =\
		intrinsic_spectrum_calc.generate_spectrum(yso_spts, yso_dists, log_L, dereddened_photometry.keys())
		intrinsic_spectrum_flux = {}
		for band in intrinsic_spectrum:
			for si_index, si_val in enumerate(current_si):
				intrinsic_spectrum_flux[band] =\
				PhotometricSystem.get_flux(band, \
				intrinsic_spectrum[band] +\
				Av * ExtinctionInterpolator.evaluate(PhotometricSystem[band])\
				)

		for si_index, si_val in enumerate(current_si):
			wavelength_arrays = []
			flux_arrays = []
			flux_e_arrays = []
			intrinsic_spectrum_flux_arrays = []
			weights = []
			for band in photometries:
				wavelength_arrays.append(PhotometricSystem[band])
				flux_arrays.append(reddened_flux_dict[band][si_index])
				flux_e_arrays.append(reddened_flux_e_dict[band][si_index])
				weights.append(curr_weights[band][si_index])
				#print intrinsic_spectrum_flux
				#print intrinsic_spectrum_flux[band]
				#print si_index
				intrinsic_spectrum_flux_arrays.\
				append(intrinsic_spectrum_flux[band][si_index])
			full_wavelength_arrays = []
			full_flux_arrays = []
			full_flux_e_arrays = []
			full_intrinsic_spectrum_flux_arrays = []
			for band in full_photometries:
				full_wavelength_arrays.append(PhotometricSystem[band])
				full_flux_arrays.append(reddened_full_flux_dict[band][si_index])
				full_flux_e_arrays.append(reddened_full_flux_e_dict[band][si_index])
				full_intrinsic_spectrum_flux_arrays.\
				append(intrinsic_spectrum_flux[band][si_index])

			if not np.all(np.isnan(flux_arrays + intrinsic_spectrum_flux_arrays)):
				#Sort!
				flux_arrays = [x[1] for x in sorted(zip(wavelength_arrays, flux_arrays), key = lambda y: y[0])]
				flux_e_arrays = [x[1] for x in sorted(zip(wavelength_arrays, flux_e_arrays), key = lambda y: y[0])]
				full_flux_arrays = [x[1] for x in sorted(zip(full_wavelength_arrays, full_flux_arrays), key = lambda y: y[0])]
				full_flux_e_arrays = [x[1] for x in sorted(zip(full_wavelength_arrays, full_flux_e_arrays), key = lambda y: y[0])]

				# print "full_flux_e_arrays", full_flux_e_arrays
				intrinsic_spectrum_flux_arrays = [x[1] for x in sorted(zip(wavelength_arrays, intrinsic_spectrum_flux_arrays), key = lambda y: y[0])]
				full_intrinsic_spectrum_flux_arrays = [x[1] for x in sorted(zip(full_wavelength_arrays, full_intrinsic_spectrum_flux_arrays), key = lambda y: y[0])]
				weights = [x[1] for x in sorted(zip(wavelength_arrays, weights), key = lambda y: y[0])]
				wavelength_arrays = sorted(wavelength_arrays)
				full_wavelength_arrays = sorted(full_wavelength_arrays)
				# print "hi"
				# print intrinsic_spectrum_flux
				# print "intrinsic_spectrum_flux_arrays", intrinsic_spectrum_flux_arrays
				# print "flux_arrays", flux_arrays
				# print "full_flux_arrays", full_flux_arrays
				### Normalize flux_arrays at the peak of the intrinsic SED which has a counterpart in the actual spectrum.
				intrinsic_SED_matched_with_actual_spectrum =\
				np.where(np.isnan(flux_arrays), np.zeros(len(flux_arrays)) + np.NaN,
				intrinsic_spectrum_flux_arrays)
				flux_arrays_matched_with_model =\
				np.where(np.isnan(intrinsic_SED_matched_with_actual_spectrum),
				np.zeros(len(flux_arrays)) + np.NaN,
				flux_arrays)
				# print "hello"
				if np.all(np.isnan(intrinsic_SED_matched_with_actual_spectrum)):
					# No common band. Don't plot for this star.
					pass
				else:
					### Construct a new chisq statistic for the log flux:
					# chisq_log_flux =\
					# np.sum([np.square((np.log10(intrinsic_spectrum_flux_arrays[band]) -\
					# np.log10(flux_arrays[band]))/np.log10(flux_e_arrays[band])) for band in intrinsic_spectrum])

					##### Following is deprecated!
					# log10_f_offset = 0.
					### Use the analytical solution to the chisq statistic:
					log10_f_offset =\
					np.nansum((np.log10(flux_arrays_matched_with_model) - np.log10(intrinsic_spectrum_flux_arrays))\
					*weights/np.square(flux_e_arrays/flux_arrays_matched_with_model)\
					)/\
					np.nansum(weights/\
					np.square(flux_e_arrays/flux_arrays_matched_with_model)\
					)

					### Calculate reduced chisq:
					chisq_flux[si_index] =\
					np.nansum(weights/\
					np.square(flux_e_arrays)\
					* np.square(-np.array(flux_arrays_matched_with_model) +\
					np.array(intrinsic_spectrum_flux_arrays) *\
					np.power(10.,log10_f_offset)))/np.nansum(weights)

					### The following is a statistic that will be either NaN or some value.
					#It is useful for showing how many DOF's we actually have:

					# sum_val = weights/\
					# np.square(flux_e_arrays/flux_arrays_matched_with_model)\
					# * (-np.log10(flux_arrays_matched_with_model) +\
					# np.log10(intrinsic_spectrum_flux_arrays) +\
					# np.square(log10_f_offset))

					# print "si_index", si_index, "chisq_flux", chisq_flux[si_index], "red_chisq_flux", chisq_flux[si_index]/(np.sum(np.logical_not(np.isnan(sum_val))) - 1.)

					chisq_flux[si_index] /= np.sum(np.logical_not(np.isnan(weights/\
					np.square(flux_e_arrays)\
					* np.square(-np.array(flux_arrays_matched_with_model) +\
					np.array(intrinsic_spectrum_flux_arrays) *\
					np.power(10.,log10_f_offset))))) - 1.

					f_offset = np.power(10., log10_f_offset)

					# print "intrinsic_spectrum_flux_arrays", intrinsic_spectrum_flux_arrays
					# print "f_offset", f_offset

					if plot:
						plt.clf()
						# fig = plt.figure(figsize=(6, 6))
						fig, (ax1, ax2) = plt.subplots(1, 2, figsize = (16,6)) # Plot the observed vs predicted colors with errors.

						ax1.errorbar(wavelength_arrays, flux_arrays,
						yerr = flux_e_arrays,
						fmt = 'k.', label = "Spectrum") #k for black.
						cond_full = [np.isnan(photometries[x][si_index]) for x in sorted(photometries.keys(), key = PhotometricSystem)]
						ax1.errorbar(np.array(full_wavelength_arrays)[cond_full], np.array(full_flux_arrays)[cond_full],
						yerr = np.array(full_flux_e_arrays)[cond_full],
						fmt = 'r.', label = "Rejected")

						# plt.scatter(wavelength_arrays,
						# np.array(intrinsic_spectrum_flux_arrays) * f_offset,
						# label = "Reddened Model", color = "black")

						ax1.scatter(full_wavelength_arrays,
						np.array(full_intrinsic_spectrum_flux_arrays) * f_offset,
						label = "Reddened Model", color = "black")

						ax1.semilogy()
						# print "here!"
						for band_name in sorted(full_photometries.keys(), key = PhotometricSystem):
							if not np.isnan(photometries[band_name][si_index]):
								band_idx = sorted(photometries.keys(), key = PhotometricSystem).index(band_name)
								# flux_val_for_plot = flux_arrays[band_idx] + flux_e_arrays[band_idx]
								# if np.isnan(flux_arrays[band_idx] + flux_e_arrays[band_idx]):
								# 	flux_val_for_plot = intrinsic_spectrum_flux_arrays[band_idx]

								# plt.text(wavelength_arrays[band_idx], flux_val_for_plot*1.1, band_name, horizontalalignment="center")
								# print "non-full", band_name, intrinsic_spectrum_flux_arrays[band_idx]*f_offset*1.1
								ax1.text(wavelength_arrays[band_idx], intrinsic_spectrum_flux_arrays[band_idx]*f_offset*1.1, band_name, horizontalalignment="center")
							else:
								band_idx = sorted(full_photometries.keys(), key = PhotometricSystem).index(band_name)
								# flux_val_for_plot = full_flux_arrays[band_idx] + full_flux_e_arrays[band_idx]
								# if np.isnan(full_flux_arrays[band_idx] + full_flux_e_arrays[band_idx]):
								# 	flux_val_for_plot = full_flux_arrays[band_idx]

								# plt.text(full_wavelength_arrays[band_idx], flux_val_for_plot*1.1, band_name, horizontalalignment="center", color = "red")
								# print "full", band_name, full_intrinsic_spectrum_flux_arrays[band_idx]*f_offset*1.1
								ax1.text(full_wavelength_arrays[band_idx], full_intrinsic_spectrum_flux_arrays[band_idx]*f_offset*1.1, band_name, horizontalalignment="center", color = "red")
						# print "np.array(full_flux_arrays)", np.array(full_flux_arrays)
						# print "np.array(full_flux_e_arrays)", np.array(full_flux_e_arrays)
						if not np.isnan(np.nanmin(np.array(full_flux_arrays) - np.array(full_flux_e_arrays))):
							# "here too!"
							ax1.set_ylim(np.nanmin(np.array(full_flux_arrays) - np.array(full_flux_e_arrays)) / 1.25,
							np.nanmax(np.array(full_flux_arrays) + np.array(full_flux_e_arrays)) * 1.25 * 1.1)
							ax1.set_title("Fluxes for " + star_name_dict[si_val])
							if isinstance(yso_spts[si_index], basestring):
								spt_val = yso_spts[si_index]
							else:
								spt_val = SpTBaseInterpolator.num_to_SpT(yso_spts[si_index])
							if np.isnan(teffs[si_index]):
								teff_string = "nan"
							else:
								teff_string = '%i' % teffs[si_index]
							ax1.text(0.0, 1.0, 'SpT: %s, $T_{eff} = %s$, $A_V = %.2f \\pm %.2f$' % (spt_val, teff_string, Av[si_index], Av_e[si_index]), transform = plt.gca().transAxes, verticalalignment='top')
							ax1.text(0.0, 0.0, 'Red. $\\chi ^2 _{Flux} = %.2e$' % (chisq_flux[si_index]), transform = ax1.transAxes)
							if not text is None:
								ax1.text(0.0, 0.05, text[si_index], transform = ax1.transAxes, fontsize=8)
							ax1.set_xlabel("Wavelength ($\\mu m$)")
							ax1.set_ylabel("Flux ($erg \\, cm ^{-2} \\, s^{-1} \\, \\AA^{-1}$)")
							ax1.yaxis.set_label_position("right")
							ax1.legend(loc = "upper right", framealpha = 0.25)

							obs = []
							pred = []
							err = []
							for first_band in photometries:
								for second_band in photometries:
									if (first_band != second_band) and (PhotometricSystem[first_band] < PhotometricSystem[second_band]):
										color_text = first_band + "-" + second_band
										if color_weights[color_text] > 0:
											obs_val = photometries[first_band][si_index] - photometries[second_band][si_index]
											pred_val = intrinsic_spectrum[first_band][si_index] - intrinsic_spectrum[second_band][si_index]
											err_val = np.sqrt(np.square(yso_photometry_e[first_band][si_index]) + np.square(yso_photometry_e[second_band][si_index]))
											obs.append(obs_val)
											pred.append(pred_val)
											err.append(err_val)
											ax2.text(obs_val, pred_val, color_text, horizontalalignment = 'center', verticalalignment='top')

							ax2.errorbar(obs, pred, xerr = err, yerr = err, fmt = 'none')
							ax2.set_xlabel("Observed Color")
							ax2.set_ylabel("Predicted Color")
							ax2.yaxis.set_label_position("right")
							ax2.plot(np.linspace(np.nanmin([np.nanmin(obs), np.nanmin(pred)]), np.nanmax([np.nanmax(obs), np.nanmax(pred)])), np.linspace(np.nanmin([np.nanmin(obs), np.nanmin(pred)]), np.nanmax([np.nanmax(obs), np.nanmax(pred)])))
							ax2.text(0.0, 0.0, 'Red. $\\chi ^2 _{A_V} = %.2e$' % (chisq_from_av["chisq"][si_index]), transform = ax2.transAxes)

							print "trying for output_location_flux", output_location_flux
							if not os.path.exists(output_location_flux):
								os.makedirs(output_location_flux)
							plt.savefig(output_location_flux + "/" + str(si_val) + ".png")
							# plt.show()
							plt.clf()
					plt.close(fig)

		return chisq_flux

class BaseIntrinsicSpectrumCalculator(object):
	"""A class that generates an intrinsic spectrum for any SpT and photometry"""
	def __init__(self, PhotometricSystem = PhotometricSystem(),
	ColorInterpolators = ExtendColorInterpolator([\
	PecautMamajekYoungInterpolator,
	PecautMamajekExtendedDwarfInterpolator]),
	ExtinctionInterpolator =\
	MergedFallbackInstantiatedInterpolator([
			WhitneyInterpolator(interpolation_kind = "linear"),
			MathisInterpolator(interpolation_kind = "linear")
		]),
	MergingInterpolator = MergedFallbackInstantiatedInterpolator,
	interpolation_kind = "linear",
	output_unit = None,
	required_dof = 2,
	pivot_band = "Y"):
		self.PhotometricSystem = PhotometricSystem
		self.ColorInterpolators = ColorInterpolators
		self.ExtinctionInterpolator = ExtinctionInterpolator
		self.MergingInterpolator = MergingInterpolator
		self.interpolation_kind = interpolation_kind
		self.output_unit = output_unit
		self.required_dof = required_dof
		self.pivot_band = pivot_band

		if hasattr(self.ColorInterpolators, "on"):
			self.BCVCalculator = self.ColorInterpolators.on("SpT", "BCV")
		else:
			self.BCVCalculator = self.ColorInterpolators("SpT", "BCV")

	def generate_spectrum(self, spts, requested_bands, start_band = "V", start_band_mag = 0.):
		"""Generates a list of dictionaries, with keys being bands and values being magnitudes.

		Arbitrarily scaled with start_band = start_band_mag."""
		# Make sure input SpT's are in a Numpy array.
		spts = np.array(spts)
		intrinsic_spectrums = {}
		### Sort photometries according to how close they are to the desired
		### pivot band.
		photometry_list = requested_bands
		photometry_list = sorted(photometry_list, key=\
								 lambda x:\
								 abs(self.PhotometricSystem(x) - \
									 self.PhotometricSystem(start_band)))

		intrinsic_spectrums[start_band] = np.zeros(len(spts)) + start_band_mag

		# Iterate over each other band requested:
		for step_band in [z for z in requested_bands if z != start_band]:
			# Create a new band in all the spectrums:
			color = [step_band, start_band]
			color_interp =\
			ColorMergeInterpolator("-".join(color),
								   PhotometricSystem =\
								   self.PhotometricSystem,
								   ColorInterpolators =\
								   self.ColorInterpolators,
								   MergingInterpolator =\
								   self.MergingInterpolator,
								   interpolation_kind =\
								   self.interpolation_kind,
								   output_unit = self.output_unit)
			if color_interp._has_interpolator():
				intrinsic_spectrums[step_band] =\
				color_interp._evaluate(spts) + intrinsic_spectrums[start_band]

		return intrinsic_spectrums
	def basic_generate_spectrum(self, spts, photometries):
		# Make sure input SpT's are in a Numpy array.
		spts = np.array(spts)
		### Sort photometries according to how close they are to the desired
		### pivot band.
		photometry_list = photometries.keys()
		photometry_list = sorted(photometry_list, key=\
								 lambda x:\
								 abs(self.PhotometricSystem(x) - \
									 self.PhotometricSystem(self.pivot_band)))
		# Prepare a dictionary that stores the intrinsic photometries,
		# initially populated with NaN's.
		intrinsic_spectrum = {}
		# Create two lists: one called:
		# "attempt_index", which contains booleans that state whether a given
		# set of data will attempt calculation with its colors.
		# Initially, it is all set to True, but it will be set to False once
		# enough spectral types are calculated.
		attempt_index = np.array([True]*len(list(photometries.values())[0]),
								 dtype = "bool")
		self._log_color_used = np.array([u'']* len(attempt_index), dtype="<U2")
		for photometric_band in photometries:
			intrinsic_spectrum[photometric_band] =\
			np.zeros(len(photometries[photometric_band]))+np.nan
			#Make sure each photometry is a np.array.
			photometries[photometric_band] =\
			np.array(photometries[photometric_band])
		# Iterate over the photometry list in order to obtain colors:
		for color_idx, color_second in enumerate(photometry_list):
			for color_first in photometry_list:
				if color_first != color_second:
					# Create a color!
					color = [color_first, color_second]
					# Create a color interpolator for this color:
					# The color name is "color_first - color_second"
					color_interp =\
					ColorMergeInterpolator("-".join(color),
										   PhotometricSystem =\
										   self.PhotometricSystem,
										   ColorInterpolators =\
										   self.ColorInterpolators,
										   MergingInterpolator =\
										   self.MergingInterpolator,
										   interpolation_kind =\
										   self.interpolation_kind,
										   output_unit = self.output_unit)
					if color_interp._has_interpolator():
						# Find the indices for which photometries is not NaN.
						# The intrinsic spectrum should only be populated if:
						# 1. the intrinsic spectrum at this band is NaN.
						# 2. the photometries is not NaN
						# Obtain a spectrum by adding the photometry for
						# "color_second" to the intrinsic color,
						# color_first - color_second:
						condition = np.logical_and(attempt_index,\
						np.logical_and(\
								np.isnan(intrinsic_spectrum[color_first]),
								np.logical_not(np.isnan(photometries\
														[color_second]))))
						intrinsic_spectrum[color_first][condition] =\
						color_interp.evaluate(spts[condition]) +\
						photometries[color_second][condition]
						# print color_idx, ":", color_second, np.sum(condition)
			# Set the fixed spectral point:
			#intrinsic_spectrum[color_second] = photometries[color_second]
			# Set the _log_color_used:
			self._log_color_used[attempt_index] =\
			[color_second]*np.sum(attempt_index)
			# Try again (give an attempt of True) if the number of
			# DOF's is less than the required degrees of freedom.
			for index in xrange(len(intrinsic_spectrum[color_second])):
				attempt_index[index] =\
				np.sum([np.logical_not(\
				np.isnan(intrinsic_spectrum[intrinsic_color][index]))\
				for intrinsic_color in intrinsic_spectrum]) < self.required_dof\
				-1
			# Unset the _log_color_used parameter for spectra deemed unfit.
			self._log_color_used[attempt_index] = [u'']*np.sum(attempt_index)
			# Unset the values for all photometries for unfit spectra:
			for temp_color in intrinsic_spectrum:
				intrinsic_spectrum[temp_color][attempt_index] =\
				[np.NaN]*np.sum(attempt_index)
		# Set the fixed spectral point:
		for index in xrange(len(self._log_color_used)):
			#print(index,[x[index] for x in photometries.values()]) #DEBUG
			color_used = self._log_color_used[index]
			if len(color_used) > 0:
				intrinsic_spectrum[color_used][index] =\
				photometries[color_used][index]

		return intrinsic_spectrum

class IntrinsicSpectrumCalculator(BaseIntrinsicSpectrumCalculator):
	"""A class that generates a specific spectrum --- using a luminosity and a distance to constrain."""
	def generate_spectrum(self, spts, dists, logLs, requested_bands):
		### logLs given as log10 of the luminosity in solar units.
		### Calculate the bolometric magnitude with a luminosity.
		### M1 - M2 = -2.5 log (L1/L2) + 5 log (d1/d2)
		### Choosing M2 to be the solar parameters,
		### M2 = 4.74, L2 = L_sun, d2 = 10 pc:
		Mbol = 4.74 - 2.5 * logLs + 5. * np.log10(dists/10.)
		# Calculate the V-band magnitude, which is given by the bolometric correction:
		Mv = Mbol - self.BCVCalculator.evaluate(spts)

		return \
		super(IntrinsicSpectrumCalculator, self).\
		generate_spectrum(spts, requested_bands, start_band = "V",
		start_band_mag = Mv)

class SingleColorAvCalculator(ColorMergeInterpolator, BaseCalculator):
	"""Calculation of a single color Av."""
	_inputs = ["photometry", "spt", "veiling"]
	_outputs = ["Av"]
	#The input unit is in mags.
	_default_input_unit = u.mag
	#The output unit is in mags.
	_default_output_unit = u.mag
	def __init__(self, color,
	PhotometricSystem = PhotometricSystem(),
	ColorInterpolators = PecautMamajekYoungInterpolator,
	ExtinctionInterpolator =\
	MergedFallbackInstantiatedInterpolator([
			WhitneyInterpolator(interpolation_kind = "linear"),
			MathisInterpolator(interpolation_kind = "linear")
		]),
	MergingInterpolator = MergedFallbackInstantiatedInterpolator,
	interpolation_kind = "linear",
	output_unit = None,
	**kwargs):# Set the units to output in
		self.output_unit = output_unit
		### If output unit is unset, set it to the default.
		if output_unit is None:
		   self.output_unit = self._default_output_unit
		### Initialize the base color interpolators.
		self.interpolator = None
		# Save the photometric system:
		self.PhotometricSystem = PhotometricSystem
		# Save the color interpolator:
		self.ColorInterpolators = ColorInterpolators
		# Save the extinction interpolator:
		self.ExtinctionInterpolator = ExtinctionInterpolator
		# Save the merging interpolator, which is a base interpolator
		# that accepts multiple interpolators and the submethod:
		# add_interpolator_last.
		self.MergingInterpolator = MergingInterpolator
		# Save interpolation_kind:
		self.interpolation_kind = interpolation_kind
		# Save color:
		self.color = color
		# Save kwargs:
		self.kwargs = kwargs

		self.color_graph_interpolator =\
		ColorGraphInterpolator(color,
		PhotometricSystem =\
		PhotometricSystem,
		ColorInterpolators =\
		ColorInterpolators,
		MergingInterpolator =\
		MergingInterpolator,
		interpolation_kind =\
		interpolation_kind,
		output_unit = output_unit)

		self.extinction_vals = {}
		selection_band, secondary_band = self.color.split("-")
		self.extinction_vals[selection_band] =\
		ExtinctionInterpolator.evaluate(PhotometricSystem(selection_band))
		self.extinction_vals[secondary_band] =\
		ExtinctionInterpolator.evaluate(PhotometricSystem(secondary_band))

	def _evaluate(self, spts, av_min = 0., av_max = 10., **photometries):
		"""Evaluates Av single color with a given series of SpTs and photometries.
		"""

		print photometries
		selection_band, secondary_band = self.color.split("-")
		intrinsic_color = self.color_graph_interpolator._evaluate(spts)

		Av =\
		((photometries[selection_band] - photometries[secondary_band]) -\
		(intrinsic_color))/\
		(self.ExtinctionInterpolator(self.PhotometricSystem[selection_band]) -\
		self.ExtinctionInterpolator(self.PhotometricSystem[secondary_band]))

		Av =\
		np.clip(Av, av_min, av_max)

		return Av
	@staticmethod
	def partial_intrinsic_color_partial_SpT(spts,
	ColorInterpolator,
	spt_seps = [0.25,0.5,1.]):
		"""Calculate the partial (band_i - band_j) / partial SpT for
		a ColorInterpolator by averaging a range of SpT offsets.

		Pass in as ColorInterpolator an interpolator that takes
		SpT input and returns intrinsic color output."""
		spts = np.array(spts)
		jacobian_spts = []
		intrinsic_color_center =\
		ColorInterpolator.evaluate(spts)
		for spt_sep in spt_seps:
			jacobian_result = np.zeros(len(spts)) + np.NaN

			intrinsic_color_minus_spt =\
			ColorInterpolator.evaluate(spts - spt_sep)
			intrinsic_color_plus_spt =\
			ColorInterpolator.evaluate(spts + spt_sep)

			jacobian_left =\
			(intrinsic_color_center - intrinsic_color_minus_spt)/\
			(spt_sep)
			cond_left_not_nan =\
			np.logical_not(np.isnan(jacobian_left))

			jacobian_right =\
			(intrinsic_color_plus_spt - intrinsic_color_center)/\
			(spt_sep)
			cond_right_not_nan =\
			np.logical_not(np.isnan(jacobian_right))

			jacobian_avg =\
			0.5*(jacobian_left + jacobian_right)
			cond_avg_not_nan =\
			np.logical_not(np.isnan(jacobian_avg))

			### Replace values to populate the Jacobian, replacing values if
			### the subsequent value is "better"
			jacobian_result[cond_left_not_nan] =\
			jacobian_left[cond_left_not_nan]
			jacobian_result[cond_right_not_nan] =\
			jacobian_right[cond_right_not_nan]
			jacobian_result[cond_avg_not_nan] =\
			jacobian_avg[cond_avg_not_nan]

			jacobian_spts.append(jacobian_result)
			# print jacobian_left[0],jacobian_right[0],jacobian_avg[0]

		return np.nanmean(jacobian_spts, axis = 0)
	def _get_err(self, spts, spt_errors, photometric_errors):
		selection_band, secondary_band = self.color.split("-")
		intrinsic_color = self.color_graph_interpolator._evaluate(spts)

		Av_err =\
		np.sqrt(np.square(\
		self.partial_intrinsic_color_partial_SpT(spts,
		self.color_graph_interpolator) * spt_errors /\
		(self.extinction_vals[selection_band] \
		- self.extinction_vals[secondary_band])) +\
		np.square(\
		1./(self.extinction_vals[selection_band] \
		- self.extinction_vals[secondary_band]))\
		*(np.square(photometric_errors[selection_band]) +\
		np.square(photometric_errors[secondary_band])))

		return Av_err


class ColorSEDTeffCalculator(ColorSEDAvCalculator):
	"""Calculates the Teff for a given series of photometries and
	ColorInterpolators with the SED fitting technique, for a given Av
	(default Av = 0)."""
	_inputs = ["photometry", "spt", "veiling"]
	_outputs = ["Teff"]
	#The input unit is in mags.
	_default_input_unit = u.mag
	#The output unit is in Kelvin.
	_default_output_unit = u.K
	def _do_chisq_map(self, avs, log_teff_min = 3.6, log_teff_max = 4.2, chisq_max = 1e99, **photometries):
		"""Does a uniform map in chisq."""
		# Make sure input av's are in a Numpy array.
		avs = np.array(avs)
		### Sort photometries according to how close they are to the desired
		### pivot band.
		photometry_list = photometries.keys()
		photometry_list = sorted(photometry_list, key=\
								 lambda x:\
								 abs(self.PhotometricSystem(x) - \
									 self.PhotometricSystem(self.pivot_band)))

		# Calculate A_lambda/A_v:

		# The pivot point is still the first entry of photometry_list,
		# So we can add A(band) - A(band_pivot) to introduce a zero-point offset
		# That fixes the pivot point to the observed photometries.
		Alambda_div_Av_dictionary =\
		dict(izip(photometry_list,
				 self.ExtinctionInterpolator(\
				 self.PhotometricSystem(photometry_list))))
		### Now, we simply construct the Av reasonable left and right
		### golden section search.
		### However, there might not be a minimum chisq for the Av.
		### The termination condition used should just be that the bounding
		### size of the Av box, in Av space, is smaller than some tolerance.
		### This corresponds to \delta Av < tol
		#DEBUG: setting photometric_errors[band] to be a dictionary with artificial errors
		photometric_errors = {}
		for band in photometries:
			photometric_errors[band] = 1.
		#END DEBUG
		array_len = len(list(photometries.values())[0])
		log_teff_guess_list = np.linspace(log_teff_min, log_teff_max, 200)
		pivot_band = photometry_list[0]

		num_bands = np.zeros(array_len)
		weights = np.zeros(array_len)

		# Iterate over the photometry list in order to obtain colors:
		color_interp = {}
		for color_second in photometry_list:
			color_interp[color_second] = {}
			for color_first in photometry_list:
				if color_first != color_second:
					# Create a color!
					color = [color_first, color_second]
					# Create a color interpolator for this color:
					# The color name is "color_first - color_second"
					color_interp[color_second][color_first] =\
					ColorMergeInterpolator("-".join(color),
										   PhotometricSystem =\
										   self.PhotometricSystem,
										   ColorInterpolators =\
										   self.ColorInterpolators,
										   MergingInterpolator =\
										   self.MergingInterpolator,
										   interpolation_kind =\
										   self.interpolation_kind,
										   output_unit = u.mag,
										   input_val = "Teff")

		### Prepare the parameters for calculating the initial guess spectra
		### for guess log Teff's.

		guess_log_teff_map = {}
		chisq_map = {}

		for elem_idx in range(len(avs)):
			chisq_map[elem_idx] = []
			guess_log_teff_map[elem_idx] = []

		for guess_idx in range(len(log_teff_guess_list)):
			chisq = np.zeros(array_len)

			# Prepare a dictionary that stores the intrinsic photometries,
			# initially populated with NaN's.
			intrinsic_spectrum = {}

			# Create two lists: one called:
			# "attempt_index", which contains booleans that state whether a given
			# set of data will attempt calculation with its colors.
			# Initially, it is all set to True, but it will be set to False once
			# enough spectral types .
			attempt_index = np.array([True]*len(list(photometries.values())[0]),
									 dtype = "bool")
			self._log_color_used = np.array([u'']* len(attempt_index), dtype="<U2")
			for photometric_band in photometries:
				intrinsic_spectrum[photometric_band] =\
				np.zeros(len(photometries[photometric_band]))+np.nan
				#Make sure each photometry is a np.array.
				photometries[photometric_band] =\
				np.array(photometries[photometric_band])

			for color_second in photometry_list:
				for color_first in photometry_list:
					if color_first != color_second:
						# Create a color!
						color = [color_first, color_second]
						if color_interp[color_second][color_first].\
						_has_interpolator():
							# Find the indices for which photometries is not NaN.
							# The intrinsic spectrum should only be populated if:
							# 1. the intrinsic spectrum at this band is NaN.
							# 2. the photometries is not NaN
							# Obtain a spectrum by adding the photometry for
							# "color_second" to the intrinsic color,
							# color_first - color_second:
							condition = np.logical_and(attempt_index,\
							np.logical_and(\
									np.isnan(intrinsic_spectrum[color_first]),
									np.logical_not(np.isnan(photometries\
															[color_second]))))

							intrinsic_spectrum[color_first][condition] =\
							color_interp[color_second][color_first].\
							evaluate(np.power(10.,log_teff_guess_list[guess_idx])) +\
							photometries[color_second][condition]
				# Set the fixed spectral point:
				#intrinsic_spectrum[color_second] = photometries[color_second]
				# Set the _log_color_used:
				self._log_color_used[attempt_index] =\
				[color_second]*np.sum(attempt_index)
				# Try again (give an attempt of True) if the number of
				# DOF's is less than the required degrees of freedom.
				for index in xrange(len(intrinsic_spectrum[color_second])):
					attempt_index[index] =\
					np.sum([np.logical_not(\
					np.isnan(intrinsic_spectrum[intrinsic_color][index]))\
					for intrinsic_color in intrinsic_spectrum]) <\
					self.required_dof\
					-1
				# Unset the _log_color_used parameter for spectra deemed unfit.
				self._log_color_used[attempt_index] = [u'']*np.sum(attempt_index)
				# Unset the values for all photometries for unfit spectra:
				for temp_color in intrinsic_spectrum:
					intrinsic_spectrum[temp_color][attempt_index] =\
					[np.NaN]*np.sum(attempt_index)

			# Calculating Avs for the point
			# and its associated chisq's.
			for band in photometries:

				band_chisq =\
				((avs*(Alambda_div_Av_dictionary[band])+\
				intrinsic_spectrum[band] - photometries[band])/\
				 photometric_errors[band])**2

				#Have to check whether entries have NaN values. Only add non-NaN
				#values, and only increment num_bands for non-NaN values.

				condition = np.logical_not(np.isnan(band_chisq))

				condition_notnan = condition

				if self.flux_weighted == True:
					### Do flux weighting by B_lambda
					curr_weights =\
					np.nan_to_num(
					np.power(10.,
								self.log10_planck(self.PhotometricSystem[band],
											np.power(10., log_teff_guess_list[guess_idx]))))

				elif self.log_flux_weighted == True:
					### Do flux weighting by log B_lambda
					curr_weights =\
					np.clip(np.nan_to_num(
								self.log10_planck(self.PhotometricSystem[band],
											np.power(10., log_teff_guess_list[guess_idx]))), 0, None)

				else:
					curr_weights = np.zeros(array_len) + 1.

				weights += curr_weights

				chisq[condition_notnan] += band_chisq[condition_notnan]*\
				curr_weights

				num_bands[condition_notnan] += 1
			# Calculate the reduced chisq, setting values with DOF < required_dof
			# to NaN.
			condition_not_enough_dofs = num_bands < self.required_dof
			num_bands[condition_not_enough_dofs] = np.NaN
			chisq /= (weights - 1e-16)

			#print "chisq_d", chisq_d

			for elem_idx in range(len(chisq)):
				chisq_map[elem_idx].append(chisq[elem_idx])
				guess_log_teff_map[elem_idx].append(log_teff_guess_list[guess_idx])

		#import matplotlib.pyplot as plt
		#for idx in guess_log_teff_map:
			#print zip(guess_log_teff_map[idx],chisq_map[idx])
			#plt.scatter(guess_log_teff_map[idx],np.log10(chisq_map[idx]), marker=',')
			#plt.semilogy()
			#plt.show()
		solution_log_teff = np.zeros(len(chisq)) + np.NaN
		for si_idx in guess_log_teff_map:
			### Only allow finite solutions
			cond_finite = np.isfinite(np.log10(chisq_map[si_idx]))
			### Grab the index of the minimum chisq:
			if np.sum(cond_finite) > 0:
				idx_min = np.nanargmin(np.array(chisq_map[si_idx])[cond_finite])
				solution_log_teff[si_idx] =\
				np.array(guess_log_teff_map[si_idx])[cond_finite][idx_min]
		return solution_log_teff
	def _evaluate(self, avs, log_teff_tol = 1e-3, log_teff_min = 3.6, log_teff_max = 4.2, chisq_max = 1e99, **photometries):
		"""Evaluates Teff_SED with a given series of extinctions and photometries.
		log_teff_tol is the bounding box over which chi-sq minimization is done.
		"""
		# Make sure input av's are in a Numpy array.
		avs = np.array(avs)
		### Sort photometries according to how close they are to the desired
		### pivot band.
		photometry_list = photometries.keys()
		photometry_list = sorted(photometry_list, key=\
								 lambda x:\
								 abs(self.PhotometricSystem(x) - \
									 self.PhotometricSystem(self.pivot_band)))

		# Calculate A_lambda/A_v:

		# The pivot point is still the first entry of photometry_list,
		# So we can add A(band) - A(band_pivot) to introduce a zero-point offset
		# That fixes the pivot point to the observed photometries.
		Alambda_div_Av_dictionary =\
		dict(izip(photometry_list,
				 self.ExtinctionInterpolator(\
				 self.PhotometricSystem(photometry_list))))
		### Now, we simply construct the Av reasonable left and right
		### golden section search.
		### However, there might not be a minimum chisq for the Av.
		### The termination condition used should just be that the bounding
		### size of the Av box, in Av space, is smaller than some tolerance.
		### This corresponds to \delta Av < tol
		#DEBUG: setting photometric_errors[band] to be a dictionary with artificial errors
		photometric_errors = {}
		for band in photometries:
			photometric_errors[band] = 1.
		#END DEBUG
		golden_ratio = 0.38197
		array_len = len(list(photometries.values())[0])
		log_teff_guess_a = np.zeros(array_len)+\
		log_teff_min
		log_teff_guess_c = np.zeros(array_len)+\
		log_teff_max
		log_teff_guess_b = log_teff_guess_a + golden_ratio*(log_teff_guess_c - log_teff_guess_a)
		# pivot_band = photometry_list[0]
		# Iterate the golden section routine until we converge to an acceptably
		# small region.
		# Note: in some cases, a completely NaN series of photometries is given
		# That result should return a NaN reduced chisq.
		# Find the guess Av, ie the point b' in between the larger of the
		# intervals a-b, b-c. Denote b' as bp.
		log_teff_diff_ab = log_teff_guess_b - log_teff_guess_a
		log_teff_diff_bc = log_teff_guess_c - log_teff_guess_b
		log_teff_guess_bp = log_teff_guess_b +\
		golden_ratio*np.where(log_teff_diff_ab < log_teff_diff_bc,
							  log_teff_diff_bc,
							  log_teff_diff_ab)
		chisq_a = np.zeros(array_len)
		chisq_b = np.zeros(array_len)
		chisq_c = np.zeros(array_len)
		chisq_d = np.zeros(array_len)
		num_bands = np.zeros(array_len)
		weights_a = np.zeros(array_len)
		weights_b = np.zeros(array_len)
		weights_c = np.zeros(array_len)
		weights_d = np.zeros(array_len)

		### Prepare the parameters for calculating the initial guess spectra
		### for guess log Teff's.

		# Prepare a dictionary that stores the intrinsic photometries,
		# initially populated with NaN's.
		intrinsic_spectrum_a = {}
		intrinsic_spectrum_b = {}
		intrinsic_spectrum_bp = {}
		intrinsic_spectrum_c = {}
		# Create two lists: one called:
		# "attempt_index", which contains booleans that state whether a given
		# set of data will attempt calculation with its colors.
		# Initially, it is all set to True, but it will be set to False once
		# enough spectral types .
		attempt_index_a = np.array([True]*len(list(photometries.values())[0]),
								 dtype = "bool")

		attempt_index_b = np.array([True]*len(list(photometries.values())[0]),
								 dtype = "bool")

		attempt_index_bp = np.array([True]*len(list(photometries.values())[0]),
								 dtype = "bool")

		attempt_index_c = np.array([True]*len(list(photometries.values())[0]),
								 dtype = "bool")
		self._log_color_used_a = np.array([u'']* len(attempt_index_a), dtype="<U2")
		self._log_color_used_b = np.array([u'']* len(attempt_index_a), dtype="<U2")
		self._log_color_used_bp = np.array([u'']* len(attempt_index_a), dtype="<U2")
		self._log_color_used_c = np.array([u'']* len(attempt_index_a), dtype="<U2")
		for photometric_band in photometries:
			intrinsic_spectrum_a[photometric_band] =\
			np.zeros(len(photometries[photometric_band]))+np.nan
			intrinsic_spectrum_b[photometric_band] =\
			np.zeros(len(photometries[photometric_band]))+np.nan
			intrinsic_spectrum_bp[photometric_band] =\
			np.zeros(len(photometries[photometric_band]))+np.nan
			intrinsic_spectrum_c[photometric_band] =\
			np.zeros(len(photometries[photometric_band]))+np.nan
			#Make sure each photometry is a np.array.
			photometries[photometric_band] =\
			np.array(photometries[photometric_band])
		# Iterate over the photometry list in order to obtain colors:
		color_interp = {}
		for color_second in photometry_list:
			color_interp[color_second] = {}
			for color_first in photometry_list:
				if color_first != color_second:
					# Create a color!
					color = [color_first, color_second]
					# Create a color interpolator for this color:
					# The color name is "color_first - color_second"
					color_interp[color_second][color_first] =\
					ColorMergeInterpolator("-".join(color),
										   PhotometricSystem =\
										   self.PhotometricSystem,
										   ColorInterpolators =\
										   self.ColorInterpolators,
										   MergingInterpolator =\
										   self.MergingInterpolator,
										   interpolation_kind =\
										   self.interpolation_kind,
										   output_unit = u.mag,
										   input_val = "Teff")

		for color_second in photometry_list:
			for color_first in photometry_list:
				if color_first != color_second:
					# Create a color!
					color = [color_first, color_second]
					if color_interp[color_second][color_first].\
					_has_interpolator():
						# Find the indices for which photometries is not NaN.
						# The intrinsic spectrum should only be populated if:
						# 1. the intrinsic spectrum at this band is NaN.
						# 2. the photometries is not NaN
						# Obtain a spectrum by adding the photometry for
						# "color_second" to the intrinsic color,
						# color_first - color_second:
						condition_a = np.logical_and(attempt_index_a,\
						np.logical_and(\
								np.isnan(intrinsic_spectrum_a[color_first]),
								np.logical_not(np.isnan(photometries\
														[color_second]))))

						intrinsic_spectrum_a[color_first][condition_a] =\
						color_interp[color_second][color_first].\
						evaluate(np.power(10.,log_teff_guess_a[condition_a])) +\
						photometries[color_second][condition_a]

						condition_b = np.logical_and(attempt_index_b,\
						np.logical_and(\
								np.isnan(intrinsic_spectrum_b[color_first]),
								np.logical_not(np.isnan(photometries\
														[color_second]))))

						intrinsic_spectrum_b[color_first][condition_b] =\
						color_interp[color_second][color_first].\
						evaluate(np.power(10.,log_teff_guess_b[condition_b])) +\
						photometries[color_second][condition_b]

						condition_bp = np.logical_and(attempt_index_bp,\
						np.logical_and(\
								np.isnan(intrinsic_spectrum_bp[color_first]),
								np.logical_not(np.isnan(photometries\
														[color_second]))))

						intrinsic_spectrum_bp[color_first][condition_bp] =\
						color_interp[color_second][color_first].\
						evaluate(np.power(10.,log_teff_guess_bp[condition_bp])) +\
						photometries[color_second][condition_bp]

						condition_c = np.logical_and(attempt_index_c,\
						np.logical_and(\
								np.isnan(intrinsic_spectrum_c[color_first]),
								np.logical_not(np.isnan(photometries\
														[color_second]))))

						intrinsic_spectrum_c[color_first][condition_c] =\
						color_interp[color_second][color_first].\
						evaluate(np.power(10.,log_teff_guess_c[condition_c])) +\
						photometries[color_second][condition_c]
			# Set the fixed spectral point:
			#intrinsic_spectrum[color_second] = photometries[color_second]
			# Set the _log_color_used:
			self._log_color_used_a[attempt_index_a] =\
			[color_second]*np.sum(attempt_index_a)
			self._log_color_used_b[attempt_index_b] =\
			[color_second]*np.sum(attempt_index_b)
			self._log_color_used_bp[attempt_index_bp] =\
			[color_second]*np.sum(attempt_index_bp)
			self._log_color_used_c[attempt_index_c] =\
			[color_second]*np.sum(attempt_index_c)
			# Try again (give an attempt of True) if the number of
			# DOF's is less than the required degrees of freedom.
			for index in xrange(len(intrinsic_spectrum_a[color_second])):
				attempt_index_a[index] =\
				np.sum([np.logical_not(\
				np.isnan(intrinsic_spectrum_a[intrinsic_color][index]))\
				for intrinsic_color in intrinsic_spectrum_a]) <\
				self.required_dof\
				-1
			for index in xrange(len(intrinsic_spectrum_b[color_second])):
				attempt_index_b[index] =\
				np.sum([np.logical_not(\
				np.isnan(intrinsic_spectrum_b[intrinsic_color][index]))\
				for intrinsic_color in intrinsic_spectrum_b]) <\
				self.required_dof\
				-1
			for index in xrange(len(intrinsic_spectrum_bp[color_second])):
				attempt_index_bp[index] =\
				np.sum([np.logical_not(\
				np.isnan(intrinsic_spectrum_bp[intrinsic_color][index]))\
				for intrinsic_color in intrinsic_spectrum_bp]) <\
				self.required_dof\
				-1
			for index in xrange(len(intrinsic_spectrum_c[color_second])):
				attempt_index_c[index] =\
				np.sum([np.logical_not(\
				np.isnan(intrinsic_spectrum_c[intrinsic_color][index]))\
				for intrinsic_color in intrinsic_spectrum_c]) <\
				self.required_dof\
				-1
			# Unset the _log_color_used parameter for spectra deemed unfit.
			self._log_color_used_a[attempt_index_a] = [u'']*np.sum(attempt_index_a)
			self._log_color_used_b[attempt_index_b] = [u'']*np.sum(attempt_index_b)
			self._log_color_used_bp[attempt_index_bp] = [u'']*np.sum(attempt_index_bp)
			self._log_color_used_c[attempt_index_c] = [u'']*np.sum(attempt_index_c)
			# Unset the values for all photometries for unfit spectra:
			for temp_color in intrinsic_spectrum_a:
				intrinsic_spectrum_a[temp_color][attempt_index_a] =\
				[np.NaN]*np.sum(attempt_index_a)
			for temp_color in intrinsic_spectrum_b:
				intrinsic_spectrum_b[temp_color][attempt_index_b] =\
				[np.NaN]*np.sum(attempt_index_b)
			for temp_color in intrinsic_spectrum_bp:
				intrinsic_spectrum_bp[temp_color][attempt_index_bp] =\
				[np.NaN]*np.sum(attempt_index_bp)
			for temp_color in intrinsic_spectrum_c:
				intrinsic_spectrum_c[temp_color][attempt_index_c] =\
				[np.NaN]*np.sum(attempt_index_c)
		##Set the fixed spectral point:
		# for index in xrange(len(self._log_color_used_a)):
			#print(index,[x[index] for x in photometries.values()]) #DEBUG
			# color_used = self._log_color_used_a[index]
			# if len(color_used) > 0:
				# intrinsic_spectrum_a[color_used][index] =\
				# photometries[color_used][index]
				# intrinsic_spectrum_b[color_used][index] =\
				# photometries[color_used][index]
				# intrinsic_spectrum_bp[color_used][index] =\
				# photometries[color_used][index]
				# intrinsic_spectrum_c[color_used][index] =\
				# photometries[color_used][index]

		# Initialize the golden section routine, by calculating Avs for a,b,bp,c
		# and their associated chisq's.
		for band in photometries:

			band_chisq_a =\
			((avs*(Alambda_div_Av_dictionary[band])+\
			intrinsic_spectrum_a[band] - photometries[band])/\
			 photometric_errors[band])**2

			band_chisq_b =\
			((avs*(Alambda_div_Av_dictionary[band])+\
			intrinsic_spectrum_b[band] - photometries[band])/\
			photometric_errors[band])**2

			band_chisq_c =\
			((avs*(Alambda_div_Av_dictionary[band])+\
			intrinsic_spectrum_bp[band] - photometries[band])/\
			photometric_errors[band])**2

			band_chisq_d =\
			((avs*(Alambda_div_Av_dictionary[band])+\
			intrinsic_spectrum_c[band] - photometries[band])/\
			photometric_errors[band])**2

			#Have to check whether entries have NaN values. Only add non-NaN
			#values, and only increment num_bands for non-NaN values.

			condition_a = np.logical_not(np.isnan(band_chisq_a))
			condition_b = np.logical_not(np.isnan(band_chisq_b))
			condition_bp = np.logical_not(np.isnan(band_chisq_c))
			condition_c = np.logical_not(np.isnan(band_chisq_d))

			condition_notnan = np.logical_and(np.logical_and(\
			np.logical_and(condition_a, condition_b),
			condition_c),
			condition_bp)

			if self.flux_weighted == True:
				### Do flux weighting by B_lambda
				curr_weights_a =\
				np.nan_to_num(
				np.power(10.,
							self.log10_planck(self.PhotometricSystem[band],
										np.power(10., log_teff_guess_a))))

				curr_weights_b =\
				np.nan_to_num(
				np.power(10.,
							self.log10_planck(self.PhotometricSystem[band],
										np.power(10., log_teff_guess_b))))

				curr_weights_c =\
				np.nan_to_num(
				np.power(10.,
							self.log10_planck(self.PhotometricSystem[band],
										np.power(10., log_teff_guess_bp))))

				curr_weights_d =\
				np.nan_to_num(
				np.power(10.,
							self.log10_planck(self.PhotometricSystem[band],
										np.power(10., log_teff_guess_c))))

			elif self.log_flux_weighted == True:
				### Do flux weighting by log B_lambda
				curr_weights_a =\
				np.clip(np.nan_to_num(
							self.log10_planck(self.PhotometricSystem[band],
										np.power(10., log_teff_guess_a))), 0, None)
				curr_weights_b =\
				np.clip(np.nan_to_num(
							self.log10_planck(self.PhotometricSystem[band],
										np.power(10., log_teff_guess_b))), 0, None)
				curr_weights_c =\
				np.clip(np.nan_to_num(
							self.log10_planck(self.PhotometricSystem[band],
										np.power(10., log_teff_guess_bp))), 0, None)
				curr_weights_d =\
				np.clip(np.nan_to_num(
							self.log10_planck(self.PhotometricSystem[band],
										np.power(10., log_teff_guess_c))), 0, None)

			else:
				curr_weights_a = np.zeros(array_len) + 1.
				curr_weights_b = np.zeros(array_len) + 1.
				curr_weights_c = np.zeros(array_len) + 1.
				curr_weights_d = np.zeros(array_len) + 1.

			weights_a[condition_notnan] += curr_weights_a[condition_notnan]
			weights_b[condition_notnan] += curr_weights_b[condition_notnan]
			weights_c[condition_notnan] += curr_weights_c[condition_notnan]
			weights_d[condition_notnan] += curr_weights_d[condition_notnan]

			chisq_a[condition_notnan] += band_chisq_a[condition_notnan]*\
			curr_weights_a[condition_notnan]

			chisq_b[condition_notnan] += band_chisq_b[condition_notnan]*\
			curr_weights_b[condition_notnan]

			chisq_c[condition_notnan] += band_chisq_c[condition_notnan]*\
			curr_weights_c[condition_notnan]

			chisq_d[condition_notnan] += band_chisq_d[condition_notnan]*\
			curr_weights_d[condition_notnan]
			# print "band_chisq_d", band_chisq_d[condition_notnan]
			# print "curr_weights_d", curr_weights_d[condition_notnan]

			num_bands[condition_notnan] += 1
		# Calculate the reduced chisq, setting values with DOF < required_dof
		# to NaN.
		condition_not_enough_dofs = num_bands < self.required_dof
		num_bands[condition_not_enough_dofs] = np.NaN
		chisq_a /= (weights_a - 1e-16)
		chisq_b /= (weights_b - 1e-16)
		chisq_c /= (weights_c - 1e-16)
		chisq_d /= (weights_d - 1e-16)

		#print "chisq_d", chisq_d

		guess_log_teff_map = {}
		chisq_map = {}

		for elem_idx in range(len(chisq_a)):
			chisq_map[elem_idx] = []
			guess_log_teff_map[elem_idx] = []

		for elem_idx in range(len(chisq_a)):
			chisq_map[elem_idx].append(chisq_a[elem_idx])
			chisq_map[elem_idx].append(chisq_b[elem_idx])
			chisq_map[elem_idx].append(chisq_d[elem_idx])
			guess_log_teff_map[elem_idx].append(log_teff_guess_a[elem_idx])
			guess_log_teff_map[elem_idx].append(log_teff_guess_b[elem_idx])
			guess_log_teff_map[elem_idx].append(log_teff_guess_c[elem_idx])

		while np.max(log_teff_guess_c - log_teff_guess_a) > log_teff_tol:
			### In the golden section routine, a new triplet (a,b,c) is chosen
			### To be either a,min(bp,b),max(bp,b), or min(bp,b),max(bp,b),c.
			### This is chosen by comparing the reduced chisq's of a and c.
			for elem_idx in range(len(chisq_a)):
				chisq_map[elem_idx].append(chisq_c[elem_idx])
				guess_log_teff_map[elem_idx].append(log_teff_guess_bp[elem_idx])

			condition_a_lt_c = chisq_a < chisq_d

			#print zip(chisq_a, chisq_d, condition_a_lt_c)
			condition_b_lt_bp = log_teff_guess_b < log_teff_guess_bp
			chisq_min_b_bp = np.where(condition_b_lt_bp,
									  chisq_b,
									  chisq_c)
			chisq_max_b_bp = np.where(condition_b_lt_bp,
									  chisq_c,
									  chisq_b)
			log_teff_guess_min_b_bp = np.where(condition_b_lt_bp,
										 log_teff_guess_b,
										 log_teff_guess_bp)
			log_teff_guess_max_b_bp = np.where(condition_b_lt_bp,
										 log_teff_guess_bp,
										 log_teff_guess_b)
			# Calculate the log Teff guesses and the Reduced Chisq values:
			log_teff_guess_a = np.where(condition_a_lt_c,
								  log_teff_guess_a,
								  log_teff_guess_min_b_bp)
			chisq_a = np.where(condition_a_lt_c,
							   chisq_a,
							   chisq_min_b_bp)
			log_teff_guess_c = np.where(condition_a_lt_c,
								  log_teff_guess_max_b_bp,
								  log_teff_guess_c)
			chisq_d = np.where(condition_a_lt_c,
							   chisq_max_b_bp,
							   chisq_d)
			log_teff_guess_b = np.where(condition_a_lt_c,
								  log_teff_guess_min_b_bp,
								  log_teff_guess_max_b_bp)
			chisq_b = np.where(condition_a_lt_c,
							   chisq_min_b_bp,
							   chisq_max_b_bp)
			# A new b' point is now chosen:
			log_teff_diff_ab = log_teff_guess_b - log_teff_guess_a
			log_teff_diff_bc = log_teff_guess_c - log_teff_guess_b
			log_teff_guess_bp = log_teff_guess_b +\
			golden_ratio*np.where(log_teff_diff_ab < log_teff_diff_bc,
								  log_teff_diff_bc,
								  -log_teff_diff_ab)

			# Prepare to calculate the red_chisq of point b':
			num_bands = np.zeros(array_len)
			chisq_c = np.zeros(array_len)

			for color_second in photometry_list:
				for color_first in photometry_list:
					if color_first != color_second:
						# Create a color!
						color = [color_first, color_second]
						if color_interp[color_second][color_first].\
						_has_interpolator():
							# Find the indices for which photometries is not NaN.
							# The intrinsic spectrum should only be populated if:
							# 1. the intrinsic spectrum at this band is NaN.
							# 2. the photometries is not NaN
							# Obtain a spectrum by adding the photometry for
							# "color_second" to the intrinsic color,
							# color_first - color_second:

							condition_bp = np.logical_and(attempt_index_bp,\
							np.logical_and(\
									np.isnan(intrinsic_spectrum_bp[color_first]),
									np.logical_not(np.isnan(photometries\
															[color_second]))))
							intrinsic_spectrum_bp[color_first][condition_bp] =\
							color_interp[color_second][color_first].\
							evaluate(np.power(10.,log_teff_guess_bp[condition_bp])) +\
							photometries[color_second][condition_bp]
				# Set the fixed spectral point:
				#intrinsic_spectrum[color_second] = photometries[color_second]
				# Set the _log_color_used:
				self._log_color_used_bp[attempt_index_bp] =\
				[color_second]*np.sum(attempt_index_bp)
				# Try again (give an attempt of True) if the number of
				# DOF's is less than the required degrees of freedom.
				for index in xrange(len(intrinsic_spectrum_bp[color_second])):
					attempt_index_bp[index] =\
					np.sum([np.logical_not(\
					np.isnan(intrinsic_spectrum_bp[intrinsic_color][index]))\
					for intrinsic_color in intrinsic_spectrum_bp]) <\
					self.required_dof\
					-1
				# Unset the _log_color_used parameter for spectra deemed unfit.
				self._log_color_used_bp[attempt_index_bp] = [u'']*np.sum(attempt_index_bp)
				# Unset the values for all photometries for unfit spectra:
				for temp_color in intrinsic_spectrum_bp:
					intrinsic_spectrum_bp[temp_color][attempt_index_bp] =\
					[np.NaN]*np.sum(attempt_index_bp)

			intrinsic_spectrum_bp[band] =\
			np.zeros(len(photometries[band]))+np.nan

			for color_second in photometry_list:
				for color_first in photometry_list:
					if color_first != color_second:
						# Create a color!
						color = [color_first, color_second]
						if color_interp[color_second][color_first].\
						_has_interpolator():

							condition_bp = np.logical_and(attempt_index_bp,\
							np.logical_and(\
									np.isnan(intrinsic_spectrum_bp[color_first]),
									np.logical_not(np.isnan(photometries\
															[color_second]))))

							intrinsic_spectrum_bp[color_first][condition_bp] =\
							color_interp[color_second][color_first].\
							evaluate(np.power(10.,log_teff_guess_bp[condition_bp])) +\
							photometries[color_second][condition_bp]

			for band in photometries:
				#Have to check whether entries have NaN values. Only add non-NaN
				#values, and only increment num_bands for non-NaN values.
				band_chisq_c =\
				((avs*(Alambda_div_Av_dictionary[band])+\
				intrinsic_spectrum_bp[band] - photometries[band])/\
				photometric_errors[band])**2
				condition_bp = np.logical_not(np.isnan(band_chisq_c))
				chisq_c[condition_bp] += band_chisq_c[condition_bp]
				num_bands[condition_bp] += 1
			condition_not_enough_dofs = num_bands < self.required_dof
			num_bands[condition_not_enough_dofs] = np.NaN
			chisq_c /= (num_bands - 1)

			#print "a,b,bp,c", zip(chisq_a, chisq_b, chisq_c, chisq_d)
		### Return the values with the lowest reduced chisq:
		lowest_reduced_chisq = np.zeros(array_len) + np.inf
		log_teff_SED = np.zeros(array_len) + np.nan
		for log_teff_guess, chisq in \
		izip([log_teff_guess_a, log_teff_guess_b, log_teff_guess_bp, log_teff_guess_c],
			[chisq_a, chisq_b, chisq_c, chisq_d]):
			# Only replace lowest_reduced_chisq if chisq is non-NaN and
			# smaller!
			#print log_teff_guess, chisq
			condition_not_nan = np.logical_not(np.isnan(chisq))
			condition_chisq_smaller = chisq < lowest_reduced_chisq
			condition_replace = np.logical_and(condition_not_nan,
											   condition_chisq_smaller)
			# Replace the lowest reduced chisq with cases where the condition
			# is met!
			lowest_reduced_chisq[condition_replace] = chisq[condition_replace]
			log_teff_SED[condition_replace] = log_teff_guess[condition_replace]
		# Save the reduced chisq values.
		self._lowest_reduced_chisq = lowest_reduced_chisq
		chisq_dond = self._lowest_reduced_chisq < chisq_max
		### The chisq condition also applies to stars with no valid Av.
		chisq_dond =\
		np.logical_and(chisq_dond, np.logical_not(np.isnan(avs)))


		import matplotlib.pyplot as plt
		for idx in guess_log_teff_map:
			# print zip(guess_log_teff_map[idx],chisq_map[idx])
			plt.scatter(guess_log_teff_map[idx],chisq_map[idx], marker=',')
			plt.semilogy()
			plt.show()
		#xs = sorted(intrinsic_spectrum_bp.keys(), key=self.PhotometricSystem)
		### Do a debug basic plot of intrinsic spectrum vs photometry:
		#for test_idx in range(len(Av_SED)):
			#plt.plot(self.PhotometricSystem[xs], [intrinsic_spectrum[x][test_idx] for x in xs], linestyle="dotted")
			#print [[band, photometries[band][test_idx]] for band in photometries]
			#phot_slice = dict([[band, photometries[band][test_idx]] for band in photometries])
			#phot_xs = sorted(phot_slice.keys(), key=self.PhotometricSystem)
			#plt.plot(self.PhotometricSystem[phot_xs], [phot_slice[phot_x] for phot_x in phot_xs], linewidth = 3)
			#plt.show()
		return np.where(chisq_dond, log_teff_SED, np.zeros(array_len) + np.nan)


class ColorWeightedAvCalculator(ColorMergeInterpolator, BaseCalculator):
	"""Calculates Av for a network of possible colors and weights them together.

	Takes a list (or single) color interpolator, finds the selected color,
	and merges them with passed MergingInterpolator logic.

	The default MergingInterpolator is MergedFallbackInstantiatedInterpolator,
	and under this scenario, the ColorInterpolator takes the first color
	interpolator under the specified color (i.e., B-V), and at runtime finds all
	values that are NaN and "falls back" through the available list of
	color interpolators.

	Uses these as the colors to use through the calculations to Av."""
	_inputs = ["photometry", "spt", "veiling"]
	_outputs = ["Av"]
	#The input unit is in mags.
	_default_input_unit = u.mag
	#The output unit is in mags.
	_default_output_unit = u.mag
	# Statistical weights for colors:
	weights = {}
	# Weights for calculating weighted average values of parameters.
	weights["Ic"]            = 1.0
	weights["Rc"]            = 1.0
	weights["V"]             = 0.9
	weights["J"]             = 0.7
	weights["H"]             = 0.3
	weights["Ks"]            = 0.2
	weights["default"]       = 0.
	def __init__(self,
				 color_combinations,
				 PhotometricSystem = PhotometricSystem(),
				 ColorInterpolators = [PecautMamajekYoungInterpolator,
				 PecautMamajekDwarfInterpolator,
				 PecautMamajekExtendedDwarfInterpolator],
				 ExtinctionInterpolator =\
				 MergedFallbackInstantiatedInterpolator([
						 WhitneyInterpolator(interpolation_kind = "linear"),
						 MathisInterpolator(interpolation_kind = "linear")
					 ]),
				 MergingInterpolator = MergedFallbackInstantiatedInterpolator,
				 interpolation_kind = "linear",
				 output_unit = None,
				 flux_weighted = False):
		"""Accepts a list of colors --- a color combination, PhotometricSystem,
		ColorInterpolators, ExtinctionInterpolator, and MergingInterpolator.

		Usage: call evaluate() with SpT's and photometries.

		Generates color interpolators for use in calculating Av's.

		The parameter "flux_weighted" is used to set whether the weights of
		the Av weighting fitting process be fit with the
		Planck function for the Teff assumed from the SpT."""
		# Set the units to output in
		self.output_unit = output_unit
		### If output unit is unset, set it to the default.
		if output_unit is None:
		   self.output_unit = self._default_output_unit
		# Save the color:
		# Save the photometric system:
		self.PhotometricSystem = PhotometricSystem
		# Save the extinction interpolator:
		self.ExtinctionInterpolator = ExtinctionInterpolator
		# Save the merging interpolator, which is a base interpolator
		# that accepts multiple interpolators and the submethod:
		# add_interpolator_last.
		self.MergingInterpolator = MergingInterpolator
		 ### Initialize the base color interpolators.
		self.interpolators = {}
		for color in color_combinations:
			self.interpolators[color] =\
			ColorMergeAvCalculator(color,
			PhotometricSystem = PhotometricSystem,
			ColorInterpolators = ColorInterpolators,
			ExtinctionInterpolator = ExtinctionInterpolator,
			MergingInterpolator = MergingInterpolator,
			interpolation_kind = interpolation_kind,
			output_unit = output_unit)
		# Save the flux weighting preference:
		self.flux_weighted = flux_weighted

		# Obtain SpT -> Teff interpolators for the flux-weighting.
		spt_teff_interpolators = []
		for temp_color_interp in ColorInterpolators:
			if hasattr(temp_color_interp, "on"):
				temp_color_interp = temp_color_interp.on
			spt_teff_interpolators.append(temp_color_interp("SpT", "Teff"))
		self.spt_teff_interpolator =\
		self.MergingInterpolator(spt_teff_interpolators)
	def _evaluate(self, spts, **photometries):
		"""Returns a list of Avs for an input list of values for SpT's and
		photometries."""
		# Create possible color combinations as Band1-Band2:
		color_combinations =\
		["-".join(color_tuple) for color_tuple in \
		 it.combinations(sorted(photometries.keys(),
								key=self.PhotometricSystem), 2)]
		Avs = {}
		weights = {}
		if self.flux_weighted:
			### Calculate Teff's for SpT's.
			teffs = self.spt_teff_interpolator.evaluate(spts)
			# Iterate over each color provided.
			for color in color_combinations:
				Avs[color] = self.interpolators[color].\
				evaluate(spts,
						 np.array(photometries[color.split("-")[0]]) -\
						 np.array(photometries[color.split("-")[1]]))
				weights[color] = np.zeros(len(Avs[color])) + np.NaN
				### Designate multiplicative weights, ie: the weights
				### for a single color are the multiplied weights
				### for each of the photometric bands.
				weights[color] =\
				np.nan_to_num(
				np.power(10.,
				self.log10_planck(self.PhotometricSystem[color.split("-")[0]],
				teffs)))*\
				np.nan_to_num(
				np.power(10.,
				self.log10_planck(self.PhotometricSystem[color.split("-")[1]],
				teffs)))
			Av =\
			np.nansum([Avs[color]*weights[color] for color in\
					   color_combinations],
					  axis=0)/\
			np.nansum([weights[color] for color in color_combinations],
					  axis=0)
		else:
			# Iterate over each color provided.
			for color in color_combinations:
				Avs[color] = self.interpolators[color].\
				evaluate(spts,
						 np.array(photometries[color.split("-")[0]]) -\
						 np.array(photometries[color.split("-")[1]]))
				weights[color] = np.zeros(len(Avs[color])) + np.NaN
				# Make sure only the non-NaN values for Avs are used.
				not_NaN = np.logical_not(np.isnan(Avs[color]))
				### Designate multiplicative weights, ie: the weights
				### for a single color are the multiplied weights
				### for each of the photometric bands.
				weights[color][not_NaN] =\
				self.weights.get(color.split("-")[0],
								 self.weights["default"])*\
				self.weights.get(color.split("-")[1],
								 self.weights["default"])
			Av =\
			np.nansum([Avs[color]*weights[color] for color in\
					   color_combinations],
					  axis=0)/\
			np.nansum([weights[color] for color in color_combinations],
					  axis=0)
		return Av

class LogLCalculator(ColorMergeInterpolator, BaseCalculator):
	"""Calculates LogL for photometries, spectral types, and distances."""
	def __init__(self,
				 color_combinations,
				 PhotometricSystem = PhotometricSystem(),
				 ColorInterpolators = [PecautMamajekYoungInterpolator,
				 PecautMamajekDwarfInterpolator,
				 PecautMamajekExtendedDwarfInterpolator],
				 ExtinctionInterpolator =\
				 MergedFallbackInstantiatedInterpolator([
						 WhitneyInterpolator(interpolation_kind = "linear"),
						 MathisInterpolator(interpolation_kind = "linear")
					 ]),
				 MergingInterpolator = MergedFallbackInstantiatedInterpolator,
				 interpolation_kind = "linear",
				 output_unit = None,
				 pivot_band = "Y",
				 required_dof = 2):
		"""Accepts a list of colors --- a color combination, PhotometricSystem,
		ColorInterpolators, ExtinctionInterpolator, and MergingInterpolator.

		Usage: call evaluate() with SpT's and photometries.

		Generates color interpolators for use in calculating Av's."""
		# Set the units to output in
		self.output_unit = output_unit
		### If output unit is unset, set it to the default.
		if output_unit is None:
		   self.output_unit = self._default_output_unit
		### Initialize the base color interpolators.
		self.interpolator = None
		# Save the photometric system:
		self.PhotometricSystem = PhotometricSystem
		# Save the color interpolator:
		self.ColorInterpolators = ColorInterpolators
		# Save the extinction interpolator:
		self.ExtinctionInterpolator = ExtinctionInterpolator
		# Save the merging interpolator, which is a base interpolator
		# that accepts multiple interpolators and the submethod:
		# add_interpolator_last.
		self.MergingInterpolator = MergingInterpolator
		# Save interpolation_kind:
		self.interpolation_kind = interpolation_kind
		# Save pivot_band:
		self.pivot_band = pivot_band
		# Save the required DOF:
		self.required_dof = required_dof
	def _evaluate(self, spts, av, dist, band = "V", **photometries):
		"""Returns a list of logL's for an input list of values for SpT's and
		photometries.

		Assume dist is given in units of parsecs."""
		Alam_Av = self.ExtinctionInterpolator.evaluate(self.PhotometricSystem[band])
		if isinstance(dist, u.Quantity):
			# If so, set it.
			dist = dist.to(u.pc).value
		### In order to generate a V-band photometry where it might not exist,
		### Create an intrinsic spectrum:
		# Make sure input SpT's are in a Numpy array.
		spts = np.array(spts)
		### Sort photometries according to how close they are to the desired
		### pivot band.
		photometry_list = photometries.keys()
		photometry_list = sorted(photometry_list, key=\
								 lambda x:\
								 abs(self.PhotometricSystem(x) - \
									 self.PhotometricSystem(self.pivot_band)))
		# Prepare a dictionary that stores the intrinsic photometries

		intrinsic_spectrum_calc =\
		BaseIntrinsicSpectrumCalculator(PhotometricSystem = self.PhotometricSystem,
		ColorInterpolators = self.ColorInterpolators,
		ExtinctionInterpolator = self.ExtinctionInterpolator,
		MergingInterpolator = self.MergingInterpolator,
		interpolation_kind = self.interpolation_kind,
		output_unit = self.output_unit,
		required_dof = self.required_dof,
		pivot_band = self.pivot_band)

		intrinsic_spectrum =\
		intrinsic_spectrum_calc.generate_spectrum(spts,
		photometry_list)

		# print intrinsic_spectrum

		zp_not_set = True

		for photometric_band in photometry_list:
			if (photometric_band in intrinsic_spectrum) and zp_not_set:
				mag_diff = photometries[photometric_band] - intrinsic_spectrum[photometric_band]
				for bands in intrinsic_spectrum:
					intrinsic_spectrum[bands] += mag_diff
				zp_not_set = False

		if not zp_not_set:
			av = np.clip(av[:], 0, None)
			if hasattr(self.ColorInterpolators, "on"):
				BCV = self.ColorInterpolators.on("SpT", "BCV").evaluate(spts)
			else:
				BCV = self.ColorInterpolators("SpT", "BCV").evaluate(spts)
			logl = 0.4*(4.75 + av*Alam_Av + 5. * np.log10(dist/10.) -\
			np.where(np.isnan(photometries.get(band, np.zeros(len(av))+np.NaN)),
			intrinsic_spectrum[band], photometries.get(band)) - BCV - intrinsic_spectrum["V"] + intrinsic_spectrum[band])
			logl = logl.astype(np.float)
		else:
			logl = np.zeros(len(spts)) + np.NaN
		return logl
	def _get_err(self, distances, distance_errors, av_errors, photometric_errors):
		V_band_errors = photometric_errors.get("V",np.zeros(len(av_errors))+np.NaN)
		photometry_list = photometric_errors.keys()
		photometry_list = sorted(photometry_list, key=\
								 lambda x:\
								 abs(self.PhotometricSystem(x) - \
									 self.PhotometricSystem(self.pivot_band)))
		for band in photometry_list:
			cond_nan = np.isnan(V_band_errors)
			V_band_errors[cond_nan] = photometric_errors[band][cond_nan]
		errs = \
		np.sqrt(np.square(2./5.*av_errors) +\
		np.square(2./5.*V_band_errors) +\
		np.square(2.*distance_errors/(distances)))

		# print "LogL contribution from Av_errors", 2./5.*av_errors
		# print "LogL contribution from mag_errors", 2./5.*V_band_errors
		# print "LogL contribution from distance_errors", 2.*distance_errors/(distances)

		return errs

class RadiusCalculator(ColorMergeInterpolator, BaseCalculator):
	"""Calculates R/Rsun for logL, logTeff."""
	R_sun = 6.957e10 #cm
	L_sun = 3.839e33 #erg/s
	sigma = 5.67051e-5 #cgs
	def __init__(self):
		pass
	def _evaluate(self, log_L, log_Teff):
		R =\
		np.sqrt((np.power(10.,log_L) * np.power(10.,4.*log_Teff))/\
		(4.*np.pi*self.sigma))/self.R_sun
		return R
	def _get_err(self, log_L, log_L_e, log_Teff, log_Teff_e):
		squared_partial_R_partial_L =\
		1./(16.*np.pi*self.sigma*np.power(10.,log_L)*np.power(10.,4.*log_Teff))
		sigma_L = np.power(10., log_L)*log_L_e
		squared_partial_R_partial_T =\
		log_L/(np.pi*self.sigma*np.power(10.,6.*log_Teff))
		sigma_Teff = np.power(10., log_Teff)*log_Teff_e
		R_err =\
		np.sqrt(squared_partial_R_partial_L*np.square(sigma_L) +\
		squared_partial_R_partial_T*np.square(sigma_Teff))
		return R_err
