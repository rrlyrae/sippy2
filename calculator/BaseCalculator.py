"""
sippy.calculator.BaseCalculator:
Base python class and methods for creating calculators between
stellar parameters.

Author: Lyra Cao
3/15/2018
"""

from __future__ import division
from __future__ import absolute_import
import numpy as np
from astropy import units as u
import re
from sippy.interpolator.BaseInterpolator import *
from itertools import izip

class BaseCalculator(BaseInterpolator):
    """Base class for calculators."""

class RandomNumber(SpTBaseInterpolator):
    """Class for a random variable drawn out of a symmetric or asymmetric
    Gaussian.

    Set `seed` to a random number seed. If set to None, it simply picks one at
    pseudorandom.

    Usage:

    x = RandomNumber(0.,1.,num_samples = 1000)
    """
    def __init__(self, *args, **_3to2kwargs):
        if 'seed' in _3to2kwargs: seed = _3to2kwargs['seed']; del _3to2kwargs['seed']
        else: seed =  None
        if 'num_samples' in _3to2kwargs: num_samples = _3to2kwargs['num_samples']; del _3to2kwargs['num_samples']
        else: num_samples =  100
        """Supports passing values as a single value, list, or numpy array, and
        errors as a single value, list, or numpy array.

        The shape of the values and errors can either be 1:1 or there can be
        a single value for each of the error bounds, which is assumed to be
        repeated for all elements."""
        self.set_spectral_types()
        self.set_SpT_zero_point()
        #If this number is being initiated, assume that it is Gaussian.
        if len(args) == 2:
            ### Value and a symmetric error 68% bound.
            #Set the seed value:
            np.random.seed(seed)
            vals = args[0]
            symmetric_errs = args[1]
            # Find the length of the values vector, supporting polymorphism.
            if hasattr(vals, "__len__"):
                values = vals[:]
                len_values = len(values)
            else:
                values = [vals]
                len_values = 1
            # Create symmetric error array of the same shape as the values
            # vector.
            if hasattr(symmetric_errs, "__len__"):
                symmetric_errors = symmetric_errs[:]
                len_symmetric_errors = len(symmetric_errors)
            else:
                symmetric_errors = [symmetric_errs]
                len_symmetric_errors = 1
            # In the case that a single symmetric error is provided for
            # an array of values:
            if len_values > 1 and len_symmetric_errors == 1:
                symmetric_errors =\
                [symmetric_errors[0] for x in xrange(len_values)]
            #Obtain the standard normal distribution samples:
            std_normal_output = np.random.randn(len_values, num_samples-1)
            # If values is a string, assume it is a SpT.
            if isinstance(values[0], unicode) or isinstance(values[0], str):
                # Get numerical SpT:
                interp_values = self.get_num_SpTs(values)
                self.output =\
                np.array(\
                [[interp_values[x]] + list(std_normal_output[x] * symmetric_errors[x] + interp_values[x])\
                 for x in xrange(len_values)])
                #self.output = self.get_SpTs(output)
            else:
                self.output =\
                np.array(\
                [[values[x]] + list(std_normal_output[x] * symmetric_errors[x] + values[x])\
                 for x in xrange(len_values)])
        elif len(args) == 3:
            ### Value and a left and right 68% error bounds.
            raise NotImplementedError
        else:
            raise NameError("Invalid number of arguments. RandomNumber \
requires a value and one error (or a value with a left and a \
right error).")

def find_conf_interval_68_95(counts):
  all_counts = sorted(counts.flatten(), key = lambda x: -x)
  all_counts_sum = sum(all_counts)
  total = 0.
  level_68 = -np.inf
  level_95 = -np.inf
  for idx in xrange(len(counts.flatten())):
      total += all_counts[idx]/all_counts_sum
      if total > 0.68:
          if np.isinf(level_68):
              level_68 = all_counts[idx]
      if total > 0.95:
          if np.isinf(level_95):
              level_95 = all_counts[idx]
  return [level_95, level_68]
def cdf_from_pdf(xs, pdf):
    cdf = np.zeros_like(pdf)
    for idx in xrange(len(pdf)):
        cdf[idx] = np.trapz(pdf[:idx+1], xs[:idx+1])
    cdf /= cdf[-1]
    return cdf

def confidence(xs, p_xs, interv):
    """Function calculates a numerical confidence interval.
    It searches from the left-hand side and interpolates to right.
    arguments: xs - parameter array
               p_xs - probability density array
               interv - proportion of the conf. interv.
    Modified to interpolate and resample."""
    from scipy import interpolate
    #from matplotlib import pyplot as plt
    #xs = np.array([x[0] for x in sorted(zip(xs,p_xs),key=lambda y:y[1])])
    #p_xs = np.array(sorted(p_xs)).flatten()
    #print xs
    #print xs[1:] - xs[:-1]
    #print xs.ndim
    xs_new = np.arange(np.min(xs), np.max(xs), np.min(xs[1:]-xs[0:-1])/2.)
    p_xs_new = np.interp(xs_new, xs, p_xs)
    cdf = cdf_from_pdf(xs_new, p_xs_new) #Create CDF.
    #plt.plot(xs_new, cdf)
    #plt.show()
    xs_sort = np.array([x[0] for x in sorted(izip(xs_new, cdf),key=lambda y:y[1])])
    cdf_sort = np.array(sorted(cdf))
    #cdf_mask = cdf_sort[1:] - cdf_sort[:-1] > 0.
    #cdf_mask = np.array([True] + cdf_mask.tolist())
    #xs_sort = xs_sort[cdf_mask]
    #cdf_sort = cdf_sort[cdf_mask]
    #print cdf_sort[1:] - cdf_sort[:-1]
    cdf_interp = interpolate.interp1d(cdf_sort, xs_sort)
    #interpolate on the y-axis so we can put a p level and get an x.
    best_confidence_left = -np.inf
    best_confidence_right = np.inf
    for idx in xrange(len(xs_sort)):
        bottom_p = cdf_sort[idx] #cdf_interp(xs_sort[idx]) #bottom prob value
        confidence_left = xs_sort[idx] #bottom x value
        if bottom_p + interv < 1.:
            confidence_right = cdf_interp(bottom_p + interv)
            #find the appropriate interpolated x value
            #that corresponds to the other side of the conf interv.
            if best_confidence_right - best_confidence_left > \
            confidence_right - confidence_left:
                best_confidence_left = confidence_left
                #replace "best" left
                best_confidence_right = float(confidence_right)
                #replace "best" right
    mean_conf = xs_new[np.argmax(p_xs_new)]
    if best_confidence_left > mean_conf:
        best_confidence_left = mean_conf
    if best_confidence_right < mean_conf:
        best_confidence_right = mean_conf
    return [best_confidence_left, mean_conf,
            best_confidence_right]
    #return left, peak location, and right confidence intervals.
