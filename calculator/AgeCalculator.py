"""
sippy.calculator.AgeCalculator:
Base python class for inferring any two of age, mass, logT, and logL
given information about the other two parameters.

Uses Bayesian techniques to give confidence intervals for stellar ages.

Author: Lyra Cao
4/12/2018
"""

import numpy as np
from astropy import units as u
import re
from sippy.interpolator.BaseInterpolator import *
import scipy.optimize
from sippy.interpolator.DartmouthInterpolator import \
DartmouthIsochroneInterpolator
from sippy.interpolator.BCAHInterpolator import \
BCAH98IsochroneInterpolator
from sippy.interpolator.MISTInterpolator import \
MISTIsochroneInterpolator
from sippy.interpolator.SomersInterpolator import \
SomersIsochroneInterpolator
import matplotlib.pyplot as plt
from matplotlib.ticker import NullFormatter
from scipy.spatial import ConvexHull
import matplotlib.ticker as ticker
import os
from sippy.calculator import RandomNumber
import scipy.interpolate
import matplotlib.colors as colors

class priors(object):
	@staticmethod
	def uniform_age_prior(ages):
		max_age = 8.
		prior_array = np.zeros(len(ages))
		cond_not_nan = np.logical_not(np.isnan(prior_array))
		prior_array[cond_not_nan] =\
		np.where(ages[cond_not_nan] > max_age,
				 np.zeros(len(ages[cond_not_nan])),
				 np.zeros(len(ages[cond_not_nan]))+1.)
		return prior_array
	@staticmethod
	def chabrier_mass_prior(masses):
		prior_array = np.zeros(len(masses))
		cond_not_nan = np.logical_not(np.isnan(prior_array))
		prior_array[cond_not_nan] =\
		np.where(masses[cond_not_nan] < 1.,
				 0.093*np.exp(-np.square(np.log10(masses[cond_not_nan])-\
										 np.log10(0.2))/(2*(0.55**2))),
				 0.041*np.power(masses[cond_not_nan],-1.35))
		return prior_array
	@staticmethod
	def uniform_mass_prior(masses):
		max_mass = 1e3
		min_mass = 0.
		flattened_masses = masses.flatten()
		prior_array = np.zeros(len(flattened_masses))
		cond_not_nan = np.logical_not(np.isnan(flattened_masses))
		prior_array[cond_not_nan] =\
		np.where(np.logical_and(flattened_masses[cond_not_nan] >= min_mass, flattened_masses[cond_not_nan] <= max_mass),
				 np.zeros(len(flattened_masses[cond_not_nan]))+1.,
				 np.zeros(len(flattened_masses[cond_not_nan])))
		return prior_array.reshape(masses.shape)

class SingleStarAgeMassCalculator(BaseIsochroneInterpolator):
	title = ""
	track_label = "MIST"
	def __init__(self, IsochroneInterpolator =\
				 MISTIsochroneInterpolator, scheme="linear"):
		self.IsochroneInterpolator = IsochroneInterpolator
		self.logage_mass_interp =\
		self.IsochroneInterpolator(("logT", "logL"),
								   ("logage", "mass"),
								   scheme=scheme)

		self.logT_logL_interp =\
		self.IsochroneInterpolator(("logage", "mass"),
								   ("logT", "logL"),
								   scheme=scheme)
		#Create the nearest-neighbor interpolators
		#in the case of going off the grid.
		self.nearest_logage_mass_interp =\
		self.IsochroneInterpolator(("logT", "logL"),
								   ("logage", "mass"),
								   scheme="nearest")

		self.nearest_logT_logL_interp =\
		self.IsochroneInterpolator(("logage", "mass"),
								   ("logT", "logL"),
								   scheme="nearest")
		#Save indices of parameters.
		index_logage = self.logage_mass_interp.column_headers.index("logage")
		index_mass = self.logage_mass_interp.column_headers.index("mass")
		index_logT = self.logage_mass_interp.column_headers.index("logT")
		index_logL = self.logage_mass_interp.column_headers.index("logL")
		#Create convex hull of the tracks for HRD plotting.
		self.points_hrd =\
		np.array([[point[index_logT], point[index_logL]] for point in\
				  self.logage_mass_interp.points])
		self.hull_hrd = ConvexHull(self.points_hrd)
		#Create max ranges of the isochronal points.
		mins = np.nanmin(self.logage_mass_interp.points, axis = 0)
		maxes = np.nanmax(self.logage_mass_interp.points, axis = 0)
		self.range_mass = (mins[index_mass], maxes[index_mass])
		self.range_logage = (mins[index_logage], maxes[index_logage])
		self.range_logT = (mins[index_logT], maxes[index_logT])
		self.range_logL = (mins[index_logL], maxes[index_logL])
	@staticmethod
	def cdf_from_pdf(xs, pdf):
		cdf = np.zeros_like(pdf)
		for idx in range(len(pdf)):
				cdf[idx] = np.trapz(pdf[:idx+1], xs[:idx+1])
		cdf /= cdf[-1]
		return cdf
	@classmethod
	def confidence(self, xs, p_xs, interv):
		"""Function calculates a numerical confidence interval.
		It searches from the left-hand side and interpolates to right.
		arguments: xs - parameter array
		p_xs - probability density array
		interv - proportion of the conf. interv.
		Modified to interpolate and resample."""
		if len(xs) < 2 or len(p_xs) < 2 or np.any(np.isnan(xs)) or np.any(np.isnan(p_xs)):
			return [np.NaN, np.NaN, np.NaN]
		#from matplotlib import pyplot as plt
		#xs = np.array([x[0] for x in sorted(zip(xs,p_xs),key=lambda y:y[1])])
		#p_xs = np.array(sorted(p_xs)).flatten()
		#print xs
		#print xs[1:] - xs[:-1]
		#print xs.ndim
		xs_new = np.arange(np.min(xs), np.max(xs), np.min(xs[1:]-xs[0:-1])/2.)
		p_xs_new = np.interp(xs_new, xs, p_xs)
		cdf = self.cdf_from_pdf(xs_new, p_xs_new) #Create CDF.
		#plt.plot(xs_new, cdf)
		#plt.show()
		xs_sort = np.array([x[0] for x in sorted(zip(xs_new, cdf),key=lambda y:y[1])])
		cdf_sort = np.array(sorted(cdf))
		#cdf_mask = cdf_sort[1:] - cdf_sort[:-1] > 0.
		#cdf_mask = np.array([True] + cdf_mask.tolist())
		#xs_sort = xs_sort[cdf_mask]
		#cdf_sort = cdf_sort[cdf_mask]
		#print cdf_sort[1:] - cdf_sort[:-1]
		cdf_interp = scipy.interpolate.interp1d(cdf_sort, xs_sort)
		#interpolate on the y-axis so we can put a p level and get an x.
		best_confidence_left = -np.inf
		best_confidence_right = np.inf
		for idx in range(len(xs_sort)):
			bottom_p = cdf_sort[idx] #cdf_interp(xs_sort[idx]) #bottom prob value
			confidence_left = xs_sort[idx] #bottom x value
			if bottom_p + interv < 1.:
				confidence_right = cdf_interp(bottom_p + interv)
				#find the appropriate interpolated x value
				#that corresponds to the other side of the conf interv.
				if best_confidence_right - best_confidence_left > \
				confidence_right - confidence_left:
					best_confidence_left = confidence_left
					#replace "best" left
					best_confidence_right = float(confidence_right)
					#replace "best" right
		mean_conf = xs_new[np.argmax(p_xs_new)]
		if best_confidence_left > mean_conf:
			best_confidence_left = mean_conf
		if best_confidence_right < mean_conf:
			best_confidence_right = mean_conf
		return [best_confidence_left, mean_conf,
		best_confidence_right]
		#return left, peak location, and right confidence intervals.
	@staticmethod
	def min_helper(fn, target_x0):
		"""Creates a wrapper for functions that do not have value-vector inputs.
		   (to help with scipy minimization).
		   Also gets rid of NaN's, which are problematic for SciPy."""
		def helper_fn(x0):
			big = 1e10
			result = np.sum(np.square(np.array(fn(*x0)) - target_x0))
			if np.isnan(result): result = big
			return result
		return helper_fn
	def _evaluate(self, logT, logL, sigma_logT = None, sigma_logL = None,
				  title = None,
				  sigma_limit = 3,
				  initial_sigma_lim = 5,
				  min_sigma = 0.1,
				  filename = None,
				  num_bins = 101,
				  n_samples = 1000,
				  mass_prior = priors.chabrier_mass_prior,
				  age_prior = priors.uniform_age_prior,
				  track_label = "MIST"):
		"""Returns a tuple with the confidence intervals of the logage
		and then mass if sigma_logT and sigma_logL are specified.
		Otherwise, returns a best value with
		the attached IsochroneInterpolator.
		If `filename` is specified, do a plot and save it to the directory
		requested. If it is not specified, show it to the screen.

		n_samples determines the number of samples used to calculate the
		age and mass sigma values."""

		### Set the title.
		if title is not None:
			self.title = title

		if track_label is not None:
			self.track_label = track_label

		# Obtain an initial guess for logage and mass:
		guess_logage_mass =\
		self.logage_mass_interp.evaluate(logT, logL)

		if np.any(np.isnan(guess_logage_mass)):
				guess_logage_mass = self.nearest_logage_mass_interp.evaluate(logT, logL)
		res = scipy.optimize.minimize(self.min_helper(self.logT_logL_interp.evaluate, np.array([logT, logL])), guess_logage_mass, options={"maxiter": 10, "disp": False})
		guess_logage_mass = res.x

		if np.any(np.isnan(guess_logage_mass)):
			print ("Warning: outside interpolation bounds.")
		if sigma_logT is None or sigma_logL is None:
			#Did not provide sigmas, so just return best ages and masses.
			return guess_logage_mass
		### To find the confidence interval, we need to sample the function at
		### regular intervals up to a max of sigma_limit * sigma in each
		### parameter.
		if not hasattr(sigma_logT, "__len__"):
			len_sigma_logT = 0
			if hasattr(logT, "__len__"):
				sigma_logT = np.zeros(len(logT)) + sigma_logT
		if not hasattr(sigma_logL, "__len__"):
			len_sigma_logL = 0
			if hasattr(logL, "__len__"):
				sigma_logL = np.zeros(len(logL)) + sigma_logL
		if not hasattr(logT, "__len__"):
			len_logT = 0
		else:
			len_logT = len(logT)
		if not hasattr(logL, "__len__"):
			len_logL = 0
		else:
			len_logL = len(logL)
		assert len_logT == len_logL and\
		len_logT == len_sigma_logT and\
		len_logL == len_sigma_logL,\
		"Arrays or values passed not of same dimension!"

		##### Estimation of age/mass sigmas with an empirical method:
		###If I pass in a variety of logT's and logL's, I can get a variety of
		###ages and masses. I compute marginalized distributions and find
		###the confidence intervals. Then I regularly sample
		###in age, mass coordinates.
		#logT_samples = logT+sigma_logT*np.random.randn(n_samples)
		#logL_samples = logL+sigma_logL*np.random.randn(n_samples)
		#logage_est, mass_est =\
		#self.logage_mass_interp.\
		#evaluate(logT_samples, logL_samples)

		#chisq =\
		#np.square((logT - logT_samples)/sigma_logT) +\
		#np.square((logL - logL_samples)/sigma_logL)

		#ln_likelihood = -0.5*chisq
		#ln_posterior =\
		#np.log(age_prior(logage_est))+\
		#np.log(mass_prior(mass_est))+\
		#ln_likelihood ###unnormalized

		# Arbitrarily normalize it by setting the ln peak to 0 to prevent
		# overflow issues.
		#ln_posterior -= np.nanmax(ln_posterior)

		##Calculate the posterior:
		#posterior = np.exp(ln_posterior)
		#posterior /= np.nansum(posterior)

		### Calculate the errors that will define the range of the
		### grid Bayesian analysis.
		# Obtain the "best guess" from first pass.
		#guess_idx = np.nanargmax(ln_likelihood)
		#guess_logage_mass = (logage_est[guess_idx], mass_est[guess_idx])
		#posterior, logage_est, mass_est =\
		#zip(*sorted(zip(posterior, logage_est, mass_est), key = lambda x: -x[0]))
		#posterior_sum = 0.
		#index_posterior = 0
		#while posterior_sum < 0.68:
		#	posterior_sum += posterior[index_posterior]
		#	index_posterior += 1
		#sigma_logage =\
		#np.nanmax(np.abs(logage_est[0:index_posterior+1] -\
		#guess_logage_mass[0])) + 1e-5 ##The range has to be at least 0.5 dex
		#sigma_mass =\
		#np.nanmax(np.abs(mass_est[0:index_posterior+1] -\
		#guess_logage_mass[1])) + 1e-1 ##The range has to be at least 0.1 Msun

		##### Estimation of age/mass sigmas with a minimization technique:
		### In order to find the 2D viewing window, we first find the minimized best fit value for logL, logT.
		### Our initial guess is the logage, mass value for logL, logT.
		### If this initial guess is out of bounds (i.e., NaN), then our initial guess
		### is that from the nearest neighbor calculation.

		### Need to calculate sigma_logage, sigma_mass as functions of sigma_logL, sigma_logT.
		### We construct an ellipse in logL, logT space with sigma_logL, sigma_logT
		### Then for each of these values, we find a corresponding best fit age and mass.
		### Finally, take only the median value, ignoring NaN's
		num_ellipse_points = 10
		angles = np.linspace(0., 2.*np.pi, num = num_ellipse_points, endpoint=False)
		convergence_tol = 0.1

		list_sigma_logage = np.zeros(num_ellipse_points) + np.NaN
		list_sigma_mass = np.zeros(num_ellipse_points) + np.NaN
		for index_angle, angle in enumerate(angles):
				target_logT = logT + np.cos(angle)*sigma_logT
				target_logL = logL + np.sin(angle)*sigma_logL
				guess_ellipse_logage_mass = self.logage_mass_interp.evaluate(target_logT, target_logL)
				if np.any(np.isnan(guess_ellipse_logage_mass)):
						guess_ellipse_logage_mass = self.nearest_logage_mass_interp.evaluate(target_logT, target_logL)
				res = scipy.optimize.minimize(self.min_helper(self.logT_logL_interp.evaluate, np.array([target_logT, target_logL])), guess_ellipse_logage_mass, options={"maxiter": 10, "disp": False})
				guess_ellipse_logage_mass = res.x
				if res.fun < convergence_tol:
					list_sigma_logage[index_angle] = np.abs(guess_ellipse_logage_mass[0]-guess_logage_mass[0])
					list_sigma_mass[index_angle] = np.abs(guess_ellipse_logage_mass[1]-guess_logage_mass[1])
		print "list_sigma_logage", (list_sigma_logage)
		print "list_sigma_mass", (list_sigma_mass)

		if len(list_sigma_logage) > 0:
			sigma_logage = np.nanmax([np.nanmin(list_sigma_logage), min_sigma])
		else: sigma_logage = 1.

		if np.isnan(sigma_logage):
				sigma_logage = 1.

		if len(list_sigma_mass) > 0:
			sigma_mass = np.nanmax([np.nanmin(list_sigma_mass), min_sigma])
		else: sigma_mass = 2.

		if np.isnan(sigma_mass):
				sigma_mass = 2.

		#### Clip ranges to the box of the grid (can go over)
		max_logage =\
		np.nanmin([guess_logage_mass[0] + initial_sigma_lim*sigma_logage,
		self.range_logage[1]])
		min_logage =\
		np.nanmax([guess_logage_mass[0] - initial_sigma_lim*sigma_logage,
		self.range_logage[0]])

		max_mass =\
		np.nanmin([guess_logage_mass[1] + initial_sigma_lim*sigma_logage,
		self.range_mass[1]])
		min_mass =\
		np.nanmax([guess_logage_mass[1] - initial_sigma_lim*sigma_logage,
		self.range_mass[0]])

		### Iteration 1:

		# Calculate the range between -sig_lim and +sig_lim
		logage_var = np.linspace(min_logage, max_logage, num_bins)
		mass_var = np.linspace(min_mass, max_mass, num_bins)

		logage_grid, mass_grid = np.meshgrid(logage_var, mass_var)
		logT_grid, logL_grid =\
		self.logT_logL_interp.\
		evaluate(logage_grid.flatten(), mass_grid.flatten())

		chisq =\
		np.square((logT - logT_grid)/sigma_logT) +\
		np.square((logL - logL_grid)/sigma_logL)

		ln_likelihood = -0.5*chisq
		ln_posterior =\
		np.log(age_prior(logage_grid.flatten()))+\
		np.log(mass_prior(mass_grid.flatten()))+\
		ln_likelihood ###unnormalized

		# Arbitrarily normalize it by setting the ln peak to 0 to prevent
		# overflow issues.
		ln_posterior -= np.nanmax(ln_posterior)

		#Calculate the posterior:
		posterior = np.exp(ln_posterior)
		#Condition for being finite:
		finite = np.isfinite(posterior)

		#Normalize the posterior:
		logage_width = logage_var[-1] - logage_var[0]
		mass_width = mass_var[-1] - mass_var[0]
		posterior = posterior/(\
		np.nansum(posterior[finite])*(logage_width * mass_width))

		### Calculate confidence intervals for logage and mass, marginalized:
		### We do this by computing the cdf, and then finding the smallest
		### interval over 68% and 95%, then sort by how much they go over.
		logage_width = logage_var[1] - logage_var[0]
		hist_logage = np.nansum(np.reshape(posterior, (num_bins, num_bins)), axis=0)/(np.nansum(posterior)*logage_width)

		mass_width = mass_var[1] - mass_var[0]
		hist_mass = np.nansum(np.reshape(posterior, (num_bins, num_bins)), axis=1)/(np.nansum(posterior)*mass_width)

		logage_interv_1sig = self.confidence(logage_var, hist_logage, 0.68)
		logage_interv_2sig = self.confidence(logage_var, hist_logage, 0.95)
		mass_interv_1sig = self.confidence(mass_var, hist_mass, 0.68)
		mass_interv_2sig = self.confidence(mass_var, hist_mass, 0.95)

		### Iteration 2 (refined):
		#### Clip ranges to the box of the grid (can go over)
		max_logage =\
		np.nanmin([logage_interv_1sig[1] + sigma_limit*(logage_interv_1sig[2] - logage_interv_1sig[1]),
		self.range_logage[1]])
		min_logage =\
		np.nanmax([logage_interv_1sig[1] - sigma_limit*(logage_interv_1sig[1] - logage_interv_1sig[0]),
		self.range_logage[0]])

		max_mass =\
		np.nanmin([mass_interv_1sig[1] + sigma_limit*(mass_interv_1sig[2] - mass_interv_1sig[1]),
		self.range_mass[1]])
		min_mass =\
		np.nanmax([mass_interv_1sig[1] - sigma_limit*(mass_interv_1sig[1] - mass_interv_1sig[0]),
		self.range_mass[0]])

		logage_var = np.linspace(min_logage, max_logage, num_bins)
		mass_var = np.linspace(min_mass, max_mass, num_bins)

		logage_grid, mass_grid = np.meshgrid(logage_var, mass_var)
		logT_grid, logL_grid =\
		self.logT_logL_interp.\
		evaluate(logage_grid.flatten(), mass_grid.flatten())

		chisq =\
		np.square((logT - logT_grid)/sigma_logT) +\
		np.square((logL - logL_grid)/sigma_logL)

		ln_likelihood = -0.5*chisq
		ln_posterior =\
		np.log(age_prior(logage_grid.flatten()))+\
		np.log(mass_prior(mass_grid.flatten()))+\
		ln_likelihood ###unnormalized

		# Arbitrarily normalize it by setting the ln peak to 0 to prevent
		# overflow issues.
		ln_posterior -= np.nanmax(ln_posterior)

		#Calculate the posterior:
		posterior = np.exp(ln_posterior)
		#Condition for being finite:
		finite = np.isfinite(posterior)

		#Normalize the posterior:
		logage_width = logage_var[-1] - logage_var[0]
		mass_width = mass_var[-1] - mass_var[0]
		posterior = posterior/(\
		np.nansum(posterior[finite])*(logage_width * mass_width))

		### Calculate confidence intervals for logage and mass, marginalized:
		### We do this by computing the cdf, and then finding the smallest
		### interval over 68% and 95%, then sort by how much they go over.
		logage_width = logage_var[1] - logage_var[0]
		hist_logage = np.nansum(np.reshape(posterior, (num_bins, num_bins)), axis=0)/(np.nansum(posterior)*logage_width)

		mass_width = mass_var[1] - mass_var[0]
		hist_mass = np.nansum(np.reshape(posterior, (num_bins, num_bins)), axis=1)/(np.nansum(posterior)*mass_width)

		#Create a flag to assess whether the 1sigma interval falls outside of the grid.
		flag_1sig_interv_outside = False

		logage_interv_1sig = self.confidence(logage_var, hist_logage, 0.68)
		logage_interv_2sig = self.confidence(logage_var, hist_logage, 0.95)
		mass_interv_1sig = self.confidence(mass_var, hist_mass, 0.68)
		mass_interv_2sig = self.confidence(mass_var, hist_mass, 0.95)

		### If the ellipse has NaN elements, then the 1sigma intervals should be set to NaN.
		if np.any(np.isnan(list_sigma_logage)) or np.any(np.isnan(list_sigma_mass)):
			flag_1sig_interv_outside = True

		   ### Plot the best values for logage and mass on the central Bayesian plot!
		### Take an weighted average of the logages and masses:
		#best_logage =\
		#np.nansum(logage_f*posterior_f*posterior_f)/np.nansum(posterior_f*posterior_f)
		#best_mass =\
		#np.nansum(mass_f*posterior_f*posterior_f)/np.nansum(posterior_f*posterior_f)

		### Do plotting routines:

		nullfmt = NullFormatter()
		# definitions for the axes
		left, width = 0.1, 0.65
		bottom, height = 0.1, 0.65
		bottom_h = left_h = left + width + 0.02

		rect_bayesian = [left, bottom, width, height]
		rect_histx = [left, bottom_h, width, 0.2]
		rect_histy = [left_h, bottom, 0.2, height]
		rect_hrd = [left_h, bottom_h, 0.2, 0.2]
		rect_colorbar = [left_h + 0.22, bottom, 0.01, 0.87]
		# start with a rectangular Figure
		fig = plt.figure(1, figsize=(12, 12))
		axBayesian = plt.axes(rect_bayesian)
		axHistx = plt.axes(rect_histx)
		axHisty = plt.axes(rect_histy)
		axHistx.xaxis.set_major_formatter(nullfmt)
		axHisty.yaxis.set_major_formatter(nullfmt)
		axHRD = plt.axes(rect_hrd)
		axHRD.tick_params(axis="y",direction="in", pad=-22)
		axHRD.tick_params(axis="x",direction="in", pad=-15)
		#axHRD.xaxis.set_major_formatter(nullfmt)
		#axHRD.yaxis.set_major_formatter(nullfmt)

		axBayesian.set_xlim((logage_var[0], logage_var[-1]))
		axBayesian.set_ylim((mass_var[0], mass_var[-1]))
		axHistx.set_xlim(axBayesian.get_xlim())
		axHisty.set_ylim(axBayesian.get_ylim())
		axHRD.invert_xaxis()

		isochrones = [["$0.1 \\, Myr$", 5.], ["$1 \\, Myr$", 6.], ["$10 \\, Myr$", 7.], ["$0.1 \\, Gyr$", 8.]]
		mass_tracks = [["$0.1 \\, M_\\odot$", 0.104], ["$0.3 \\, M_\\odot$", 0.3], ["$1 \\, M_\\odot$", 1.], ["$3 \\, M_\\odot$", 3.], ["$10 \\, M_\\odot$", 10.], ["$30 \\, M_\\odot$", 30.]]
		### Plot isochrones and mass tracks:
		#logT_coords = np.linspace(self.range_logT[0], self.range_logT[1])
		#logL_coords = np.linspace(self.range_logL[0], self.range_logL[1])
		sample_points = 1000
		### Set the isochronal and mass track limits:
		min_mass = 0.03
		max_mass = 30.
		min_logage = 0.
		max_logage = 9.
		mass_coords = np.logspace(np.log10(min_mass), np.log10(max_mass), sample_points)
		logage_coords = np.linspace(min_logage, max_logage, sample_points)
		param_array = np.zeros(len(mass_coords))
		for isochrone_txt, isochrone_val in isochrones:
			param_array.fill(isochrone_val)
			#values = self.isochrone_interp(logT_coords, param_array)[1]
			logT_coords, logL_coords =\
			self.logT_logL_interp.evaluate(param_array, mass_coords)
			cond_plot = np.logical_and(np.isfinite(logT_coords),
									   np.isfinite(logL_coords))
			if np.sum(cond_plot) > 1:
				axHRD.plot(logT_coords[cond_plot], logL_coords[cond_plot],
						   label=isochrone_txt, zorder=2)
				#axHRD.plot(logT_coords[cond_plot], values[cond_plot], label=isochrone_txt)
		for mass_txt, mass_val in mass_tracks:
			param_array.fill(mass_val)
			#values = self.masstrack_interp(logL_coords, param_array)[0]
			logT_coords, logL_coords =\
			self.logT_logL_interp.evaluate(logage_coords, param_array)
			cond_plot = np.logical_and(np.isfinite(logT_coords),
									   np.isfinite(logL_coords))
			if np.sum(cond_plot) > 1:
				axHRD.plot(logT_coords[cond_plot], logL_coords[cond_plot],
						   linestyle = "dashed", label=mass_txt, zorder=2)
				#axHRD.plot(values[cond_plot], logL_coords[cond_plot], linestyle = "dashed", label=mass_txt)
			#if mass_txt == "$1 \\, M_\\odot$":
				#return logage_coords[np.nanargmax(logT_coords)]
		#labelLines(axHRD.get_lines())
		axHRD.legend(framealpha=0.)
		### Plot the HRD convex hull and the location of the star on this region.
		for simplex in self.hull_hrd.simplices:
			axHRD.plot(self.points_hrd[simplex, 0], self.points_hrd[simplex, 1], 'k-', zorder=1)
		### Plot HRD:
		axHRD.errorbar(logT, logL,
					 xerr = sigma_logT,
					 yerr = sigma_logL,
					 ecolor='b',
					 fmt=",",
					 color='b',
					 zorder=3)

		### Find a location to put the self.track_label on the HRD:
		#self.track_label_x = np.min(axBayesian.get_xlim())
		#self.track_label_y = np.max(axHRD.get_ylim())
		plt.text(0.98, 0.98, self.track_label, horizontalalignment='right',
				 verticalalignment='top', transform=axHRD.transAxes)

		### Plot the main Bayesian posterior on axBayesian.
		extent = (logage_var[0], logage_var[-1], mass_var[0], mass_var[-1])
		# Reshape the posterior
		posterior = posterior.reshape(logage_grid.shape)
		Bayes = axBayesian.imshow(posterior, extent = extent,
						  origin="lower", aspect="auto")

		### Plot the marginalized histograms on the x and y axes, normalized.
		# Because the interpolation is on a regular grid, the width is simply
		# the distance of a single point to another.
		axHistx.bar(logage_var, hist_logage, width = logage_width)
		axHisty.barh(mass_var, hist_mass, height = mass_width)

		### Calculate the level for contour calculations.
		### We get internally-consistent best fit from the models here for free as well.
		posterior_f, logage_f, mass_f =\
		zip(*sorted(zip(posterior.flatten(), logage_grid.flatten(), mass_grid.flatten()), key = lambda x: -x[0]))
		sum_posterior_f = np.nansum(posterior_f)
		posterior_f = np.nan_to_num(posterior_f)
		posterior_sum = 0.
		index_posterior = 0
		while posterior_sum < 0.68:
			posterior_sum += posterior_f[index_posterior]/sum_posterior_f
			index_posterior += 1
		level_1sig = posterior_f[index_posterior]
		while posterior_sum < 0.95:
			posterior_sum += posterior_f[index_posterior]/sum_posterior_f
			index_posterior += 1
		level_2sig = posterior_f[index_posterior]
		if np.abs(level_2sig-level_1sig) < 1e-16:
			level_2sig -= 1e-8
			pass #print level_2sig, "same"
		else:
			pass #print "not the same"
		levels = sorted([level_1sig, level_2sig],key = lambda x:x)
		print levels
		fmt = {level_1sig: "$68 \\%$", level_2sig: "$95 \\%$"}
		contour = axBayesian.contour(logage_grid, mass_grid,
					 posterior.reshape(logage_grid.shape), levels = levels,
					 colors = "white")
		axBayesian.clabel(contour, contour.levels, inline=True, fmt=fmt, fontsize=10)
		axBayesian.set_xlabel("$log_{10} (Age \\, [yr])$")
		axBayesian.set_ylabel("Mass $[M_\\odot]$")

		### Overplot confidence intervals for logage and mass.
		ci_linewidth = 2
		one_sig_linestyle = "dashed"
		two_sig_linestyle = "dotted"

		axHistx.axvline(logage_interv_1sig[1],
		linewidth=ci_linewidth,
		linestyle="solid",
		label = "Marginal Best")

		axHistx.axvline(logage_interv_1sig[0],
		linewidth=ci_linewidth,
		linestyle=one_sig_linestyle,
		label = "$1 \\, \\sigma$")
		axHistx.axvline(logage_interv_1sig[2],
		linewidth=ci_linewidth,
		linestyle=one_sig_linestyle)

		axHistx.axvline(logage_interv_2sig[0],
		linewidth=ci_linewidth,
		linestyle=two_sig_linestyle,
		label = "$2 \\, \\sigma$")
		axHistx.axvline(logage_interv_2sig[2],
		linewidth=ci_linewidth,
		linestyle=two_sig_linestyle)

		axHisty.axhline(mass_interv_1sig[1],
		linewidth=ci_linewidth,
		linestyle="solid")

		axHisty.axhline(mass_interv_1sig[0],
		linewidth=ci_linewidth,
		linestyle=one_sig_linestyle)
		axHisty.axhline(mass_interv_1sig[2],
		linewidth=ci_linewidth,
		linestyle=one_sig_linestyle)

		axHisty.axhline(mass_interv_2sig[0],
		linewidth=ci_linewidth,
		linestyle=two_sig_linestyle)
		axHisty.axhline(mass_interv_2sig[2],
		linewidth=ci_linewidth,
		linestyle=two_sig_linestyle)

		### Prepare legend for 1 sig, 2 sig on axHistx:
		axHistx.legend()

		if flag_1sig_interv_outside:
			axBayesian.plot([],[], label = "$1 \\sigma$ off-grid")
		else:
			#axBayesian.plot(best_logage, best_mass,
			axBayesian.plot(logage_interv_1sig[1], mass_interv_1sig[1],
				marker="*",
				markersize=10,
				color="white",
				alpha = 1.0,
				markeredgecolor="black",
				markeredgewidth=1.0,
				label = "Peak, $log_{10}(Age \\, [yr]) = %.3f$, $M = %.3f \\, M_\\odot$" %\
			(logage_interv_1sig[1], mass_interv_1sig[1]))
			#(best_logage, best_mass))

		leg = axBayesian.legend(\
		fancybox = True, numpoints = 1, fontsize=14, bbox_to_anchor = (1.47, -.05),)
		cb = plt.colorbar(Bayes, cax = fig.add_axes(rect_colorbar))

		### Prepare the title
		print_title =\
		"$%.2f ^ {+%.2f} _ {-%.2f} M_\\odot$, $A = %.2f ^ {+%.2f} _ {-%.2f} Myr$" %\
		(mass_interv_1sig[1],
		 mass_interv_1sig[2] - mass_interv_1sig[1],
		 mass_interv_1sig[1] - mass_interv_1sig[0],
		 np.power(10.,logage_interv_1sig[1]-6),
		 np.power(10.,logage_interv_1sig[2]-6) - np.power(10.,logage_interv_1sig[1]-6),
		 np.power(10.,logage_interv_1sig[1]-6) - np.power(10.,logage_interv_1sig[0]-6))
		if self.title is not None:
			if len(self.title) > 0:
				print_title = self.title + " " + print_title
		ttl = plt.suptitle(print_title,y=1.01)

		if filename is not None:
			figure = plt.gcf() # get current figure
			figure.set_size_inches(12, 12)
			plt.savefig(filename,bbox_extra_artists=(ttl,leg,), bbox_inches='tight', dpi = 300)
		else:
			plt.show()
		plt.clf()

		if flag_1sig_interv_outside:
			logage_interv_1sig = [np.nan, np.nan, np.nan]
			logage_interv_2sig = [np.nan, np.nan, np.nan]
			mass_interv_1sig = [np.nan, np.nan, np.nan]
			mass_interv_2sig = [np.nan, np.nan, np.nan]

		print "logage_interv_1sig", logage_interv_1sig
		print "mass_interv_1sig", mass_interv_1sig

		return ([logage_interv_1sig[0], logage_interv_1sig[1], logage_interv_1sig[2]], [mass_interv_1sig[0], mass_interv_1sig[1], mass_interv_1sig[2]])

class SingleStarTripleTupleAgeMassCalculator(SingleStarAgeMassCalculator):
	title = ""
	track_label = "Somers & Pinsonneault"
	@staticmethod
	def wrapper(interp, spot_alpha):
		"""Wraps an interpolation function, allowing pre-specification of
		spot_alpha."""
		class helper_wrapper(object):
			def __init__(self):
				pass
			@staticmethod
			def evaluate(*args):
				if hasattr(args[0], "__len__"):
					return interp.evaluate(args[0],
					args[1],
					np.zeros(len(args[0])) + spot_alpha)
				return interp.evaluate(args[0], args[1], spot_alpha)
		#def helper_wrapper(*args):
			#return interp(args, spot_alpha)
		return helper_wrapper
	def __init__(self, IsochroneInterpolator =\
				 SomersIsochroneInterpolator, scheme="linear"):
		"""A variant of SingleStarAgeMassCalculator, adapted for
		interpolators which use a tuple with three elements to specify
		an input value."""
		self.IsochroneInterpolator = IsochroneInterpolator
		### For spotted models, create the underlying interpolators as
		### hidden (underscore) values.

		### Forward facing interpolators are created in the evaluate method.
		extern_loc = os.path.dirname(os.path.abspath(__file__))\
                 + "/../../"

		self._logage_mass_interp =\
		self.IsochroneInterpolator(("logT", "logL", "spot_alpha"),
								   ("logage", "mass"),
								   scheme=scheme,
								   filename = extern_loc +\
								   "Somers_logage_mass_interp_res100.npz")

		self._logT_logL_interp =\
		self.IsochroneInterpolator(("logage", "mass", "spot_alpha"),
								   ("logT", "logL"),
								   scheme=scheme,
								   filename = extern_loc +\
								   "Somers_logT_logL_interp_res100.npz")
		#Create the nearest-neighbor interpolators
		#in the case of going off the grid.
		self._nearest_logage_mass_interp =\
		self.IsochroneInterpolator(("logT", "logL", "spot_alpha"),
								   ("logage", "mass"),
								   scheme="nearest",
								   filename = extern_loc +\
								   "Somers_logage_mass_interp_res100_nearest.npz")

		self._nearest_logT_logL_interp =\
		self.IsochroneInterpolator(("logage", "mass", "spot_alpha"),
								   ("logT", "logL"),
								   scheme="nearest",
								   filename = extern_loc +\
								   "Somers_logT_logL_interp_res100_nearest.npz")

		#Save indices of parameters.
		index_logage = self._logage_mass_interp.column_headers.index("logage")
		index_mass = self._logage_mass_interp.column_headers.index("mass")
		index_logT = self._logage_mass_interp.column_headers.index("logT")
		index_logL = self._logage_mass_interp.column_headers.index("logL")
		#Create convex hull of the tracks for HRD plotting.
		self.points_hrd =\
		np.array([[point[index_logT], point[index_logL]] for point in\
				  self._logage_mass_interp.points])
		self.hull_hrd = ConvexHull(self.points_hrd)
		#Create max ranges of the isochronal points.
		mins = np.nanmin(self._logage_mass_interp.points, axis = 0)
		maxes = np.nanmax(self._logage_mass_interp.points, axis = 0)
		self.range_mass = (mins[index_mass], maxes[index_mass])
		self.range_logage = (mins[index_logage], maxes[index_logage])
		self.range_logT = (mins[index_logT], maxes[index_logT])
		self.range_logL = (mins[index_logL], maxes[index_logL])
	def _evaluate(self, spot_alpha, logT, logL, sigma_logT = None, sigma_logL = None,
				  title = None,
				  sigma_limit = 3,
				  initial_sigma_lim = 2,
				  filename = None,
				  num_bins = 101,
				  n_samples = 1000,
				  mass_prior = priors.chabrier_mass_prior,
				  age_prior = priors.uniform_age_prior,
				  track_label = "Somers & Pinsonneault"):

		### Generate the spotted interpolators with wrappers:
		self.logage_mass_interp =\
		self.wrapper(self._logage_mass_interp, spot_alpha)
		self.logT_logL_interp =\
		self.wrapper(self._logT_logL_interp, spot_alpha)
		self.nearest_logage_mass_interp =\
		self.wrapper(self._nearest_logage_mass_interp, spot_alpha)
		self.nearest_logT_logL_interp =\
		self.wrapper(self._nearest_logT_logL_interp, spot_alpha)

		return super(SingleStarTripleTupleAgeMassCalculator, self)._evaluate(\
		logT, logL, sigma_logT = sigma_logT, sigma_logL = sigma_logL,
		title = "Spot $\\alpha = %.2f$ %s" % (spot_alpha, title),
		sigma_limit = sigma_limit,
		initial_sigma_lim = initial_sigma_lim,
		filename = filename,
		num_bins = num_bins,
		n_samples = n_samples,
		mass_prior = mass_prior,
		age_prior = age_prior,
		track_label = track_label)

class MultipleStarsAgeMassCalculator(SingleStarAgeMassCalculator):
	track_label = "MIST"
	def _evaluate(self, logTs, logLs, sigma_logTs = None, sigma_logLs = None,
				  title = None,
				  filename = None,
				  min_logage = 5.,
				  max_logage = 8.,
				  min_mass = 0.1,
				  max_mass = 30.,
				  num_bins = 101,
				  n_samples = 1000,
				  mass_prior = priors.chabrier_mass_prior,
				  age_prior = priors.uniform_age_prior,
				  track_label = "MIST",
				  sum_chisq = False,
				  show_colorbar = False,
				  log_mass = False,
				  output_HRD = True):
		"""Returns a tuple with the confidence intervals of the logage
		and then mass if sigma_logTs and sigma_logLs are specified.
		Otherwise, returns a best value with
		the attached IsochroneInterpolator.
		If `filename` is specified, do a plot and save it to the directory
		requested. If it is not specified, show it to the screen.

		n_samples determines the number of samples used to calculate the
		age and mass sigma values.

		sum_chisq is True for summed posteriors,
		False for multiplied posteriors.

		show_colorbar shows the colorbar on the right-hand side of the plot.

		log_mass, when True, forces the plot to be performed on
		a logarithmic scale."""

		### Set the title.
		if title is not None:
			self.title += title

		if track_label is not None:
			self.track_label = track_label

		logage_var = np.linspace(min_logage, max_logage, num_bins)
		if log_mass:
			mass_var = np.logspace(np.log10(min_mass), np.log10(max_mass), num_bins)
		else:
			mass_var = np.linspace(min_mass, max_mass, num_bins)

		logage_grid, mass_grid = np.meshgrid(logage_var, mass_var)
		print logage_grid
		logT_grid, logL_grid =\
		self.logT_logL_interp.\
		evaluate(logage_grid.flatten(), mass_grid.flatten())

		#Assuming conditional independence, chisq is multiplied.
		sum_chisq = sum_chisq #This should be false

		if sum_chisq:
			#DEBUG: sum the posteriors instead.
			posterior = np.zeros(logT_grid.shape)
			for idx in range(len(logTs)):
				chisq =\
				np.square((logTs[idx] - logT_grid)/sigma_logTs[idx]) +\
				np.square((logLs[idx] - logL_grid)/sigma_logLs[idx])

				ln_likelihood = -0.5*chisq
				ln_posterior =\
				np.log(age_prior(logage_grid.flatten()))+\
				np.log(mass_prior(mass_grid.flatten()))+\
				ln_likelihood ###unnormalized

				# Arbitrarily normalize it by setting the ln peak to 0 to prevent
				# overflow issues.
				ln_posterior -= np.nanmax(ln_posterior)

				#Calculate the posterior:
				posterior += np.exp(ln_posterior)
		else:
			chisq = np.zeros(logT_grid.shape)
			for idx in range(len(logTs)):
				chisq +=\
				np.square((logTs[idx] - logT_grid)/sigma_logTs[idx]) +\
				np.square((logLs[idx] - logL_grid)/sigma_logLs[idx])

			ln_likelihood = -0.5*chisq
			ln_posterior =\
			np.log(age_prior(logage_grid.flatten()))+\
			np.log(mass_prior(mass_grid.flatten()))+\
			ln_likelihood ###unnormalized

			# Arbitrarily normalize it by setting the ln peak to 0 to prevent
			# overflow issues.
			ln_posterior -= np.nanmax(ln_posterior)

			#Calculate the posterior:
			posterior = np.exp(ln_posterior)
		#Condition for being finite:
		finite = np.isfinite(posterior)

		#Normalize the posterior:
		total_logage_width = logage_var[-1] - logage_var[0]
		total_mass_width = mass_var[-1] - mass_var[0]
		posterior = posterior/(\
		np.nansum(posterior[finite])*(total_logage_width * total_mass_width))

		### Convert NaN's to 0's:
		posterior = np.nan_to_num(posterior)

		### Calculate confidence intervals for logage and mass, marginalized:
		### We do this by computing the cdf, and then finding the smallest
		### interval over 68% and 95%, then sort by how much they go over.
		logage_width_right = np.zeros(len(mass_var))
		logage_width_right[0:-1] = logage_var[1:] - logage_var[0:-1]
		logage_width_right[-1] = logage_width_right[-2]

		logage_width_left = np.zeros(len(mass_var))
		logage_width_left[1:] = logage_var[1:] - logage_var[0:-1]
		logage_width_left[0] = logage_width_left[1]

		logage_width = 0.5*(logage_width_left + logage_width_right)

		hist_logage = \
		np.dot(np.reshape(posterior, (num_bins, num_bins)).T, logage_width)\
		/(np.nansum(np.dot(np.reshape(posterior, (num_bins, num_bins)).T, logage_width)))

		mass_width_right = np.zeros(len(mass_var))
		mass_width_right[0:-1] = mass_var[1:] - mass_var[0:-1]
		mass_width_right[-1] = mass_width_right[-2]

		mass_width_left = np.zeros(len(mass_var))
		mass_width_left[1:] = mass_var[1:] - mass_var[0:-1]
		mass_width_left[0] = mass_width_left[1]

		mass_width = 0.5*(mass_width_left + mass_width_right)

		# print "mass width", mass_width
		#
		# print "mass", np.dot(logage_grid, logage_width)
		# print "shape mass", np.dot(logage_grid, logage_width).shape
		#
		# print "logage", np.dot(mass_grid.T, mass_width)
		# print "shape logage", np.dot(mass_grid.T, mass_width).shape

		hist_mass =\
		np.dot(np.reshape(posterior, (num_bins, num_bins)), mass_width)\
		/(np.nansum(np.dot(np.reshape(posterior, (num_bins, num_bins)), mass_width)))

		logage_interv_1sig = self.confidence(logage_var, hist_logage, 0.68)
		logage_interv_2sig = self.confidence(logage_var, hist_logage, 0.95)
		mass_interv_1sig = self.confidence(mass_var, hist_mass, 0.68)
		mass_interv_2sig = self.confidence(mass_var, hist_mass, 0.95)

		### Plot the best values for logage and mass on the central Bayesian plot!
		### Take an weighted average of the logages and masses:
		#best_logage =\
		#np.nansum(logage_f*posterior_f*posterior_f)/np.nansum(posterior_f*posterior_f)
		#best_mass =\
		#np.nansum(mass_f*posterior_f*posterior_f)/np.nansum(posterior_f*posterior_f)

		### Do plotting routines:

		nullfmt = NullFormatter()
		# definitions for the axes
		left, width = 0.1, 0.65
		bottom, height = 0.1, 0.65
		bottom_h = left_h = left + width + 0.02

		rect_bayesian = [left, bottom, width, height]
		rect_histx = [left, bottom_h, width, 0.2]
		rect_histy = [left_h, bottom, 0.2, height]
		# if show_colorbar:
		# 	rect_histx = [left, bottom_h, width, 0.2]
		# 	rect_histy = [left_h, bottom, 0.2, height]
		# else:
		# 	### Allow the histograms to fill through the side plots of the HRD
		# 	rect_histx = [left, bottom_h, width+0., 0.3]
		# 	rect_histy = [left_h, bottom, 0.3, height]
		rect_hrd = [left_h, bottom_h, 0.2, 0.2]
		rect_colorbar = [left_h + 0.22, bottom, 0.01, height]
		### Create age/teff plot on top
		rect_age_teff = [left_h, bottom_h + 0.2, 0.2, 0.1]
		### Create logl/mass plot on bottom
		rect_logl_mass = [left_h + 0.2, bottom_h, 0.1, 0.2]
		### Create age/mass plot on very top right
		rect_age_mass = [left_h + 0.2, bottom_h + 0.2, 0.1, 0.1]
		# start with a rectangular Figure
		fig = plt.figure(1, figsize=(12, 12))
		axBayesian = plt.axes(rect_bayesian)
		axHistx = plt.axes(rect_histx)
		axHisty = plt.axes(rect_histy)
		axHistx.xaxis.set_major_formatter(nullfmt)
		axHisty.yaxis.set_major_formatter(nullfmt)
		axHRD = plt.axes(rect_hrd)
		axHRD.tick_params(axis="y",direction="in", pad=-22)
		axHRD.tick_params(axis="x",direction="in", pad=-15)
		#axHRD.xaxis.set_major_formatter(nullfmt)
		#axHRD.yaxis.set_major_formatter(nullfmt)

		axBayesian.set_xlim((logage_var[0], logage_var[-1]))
		axBayesian.set_ylim((mass_var[0], mass_var[-1]))
		axHistx.set_xlim(axBayesian.get_xlim())
		axHisty.set_ylim(axBayesian.get_ylim())
		axHRD.invert_xaxis()

		isochrones = [["$0.1 \\, Myr$", 5.], ["$1 \\, Myr$", 6.], ["$10 \\, Myr$", 7.], ["$0.1 \\, Gyr$", 8.]]
		mass_tracks = [["$0.03 \\, M_\\odot$", 0.03], ["$0.1 \\, M_\\odot$", 0.1], ["$0.3 \\, M_\\odot$", 0.3], ["$1 \\, M_\\odot$", 1.], ["$3 \\, M_\\odot$", 3.], ["$10 \\, M_\\odot$", 10.]]
		### Plot isochrones and mass tracks:
		#logT_coords = np.linspace(self.range_logT[0], self.range_logT[1])
		#logL_coords = np.linspace(self.range_logL[0], self.range_logL[1])
		sample_points = 1000
		### Set the isochronal and mass track limits:
		min_mass = 0.03
		max_mass = 10.
		min_logage = 0.
		max_logage = 8.
		mass_coords = np.logspace(np.log10(min_mass), np.log10(max_mass), sample_points)
		logage_coords = np.linspace(min_logage, max_logage, sample_points)
		param_array = np.zeros(len(mass_coords))
		for isochrone_txt, isochrone_val in isochrones:
			param_array.fill(isochrone_val)
			#values = self.isochrone_interp(logT_coords, param_array)[1]
			logT_coords, logL_coords =\
			self.logT_logL_interp.evaluate(param_array, mass_coords)
			cond_plot = np.logical_and(np.isfinite(logT_coords),
									   np.isfinite(logL_coords))
			if np.sum(cond_plot) > 1:
				axHRD.plot(logT_coords[cond_plot], logL_coords[cond_plot],
						   label=isochrone_txt, zorder=2)
				#axHRD.plot(logT_coords[cond_plot], values[cond_plot], label=isochrone_txt)
		for mass_txt, mass_val in mass_tracks:
			param_array.fill(mass_val)
			#values = self.masstrack_interp(logL_coords, param_array)[0]
			logT_coords, logL_coords =\
			self.logT_logL_interp.evaluate(logage_coords, param_array)
			cond_plot = np.logical_and(np.isfinite(logT_coords),
									   np.isfinite(logL_coords))
			if np.sum(cond_plot) > 1:
				axHRD.plot(logT_coords[cond_plot], logL_coords[cond_plot],
						   linestyle = "dashed", label=mass_txt, zorder=2)
				#axHRD.plot(values[cond_plot], logL_coords[cond_plot], linestyle = "dashed", label=mass_txt)
			#if mass_txt == "$1 \\, M_\\odot$":
				#return logage_coords[np.nanargmax(logT_coords)]
		#labelLines(axHRD.get_lines())
		axHRD_leg = axHRD.legend(framealpha=0.)
		### Plot the HRD convex hull and the location of the star on this region.
		for simplex in self.hull_hrd.simplices:
			axHRD.plot(self.points_hrd[simplex, 0], self.points_hrd[simplex, 1], 'k-', zorder=1)
		### Plot HRD:
		axHRD.errorbar(logTs, logLs,
					 xerr = sigma_logTs,
					 yerr = sigma_logLs,
					 ecolor='b',
					 fmt=",",
					 color='b',
					 zorder=3)

		### Find a location to put the self.track_label on the HRD:
		#self.track_label_x = np.min(axBayesian.get_xlim())
		#self.track_label_y = np.max(axHRD.get_ylim())
		plt.text(0.98, 0.98, self.track_label, horizontalalignment='right',
				 verticalalignment='top', transform=axHRD.transAxes)

		### Plot the main Bayesian posterior on axBayesian.
		extent = (logage_var[0], logage_var[-1], mass_var[0], mass_var[-1])
		# Reshape the posterior
		posterior = posterior.reshape(logage_grid.shape)
		#Bayes = axBayesian.imshow(posterior, extent = extent,
		#				  origin="lower", aspect="auto")
		X,Y=logage_grid, mass_grid
		Z=posterior
		Bayes = axBayesian.pcolormesh(X,Y,Z, vmin=np.nanmin(Z),
		vmax=np.nanmax(Z))

		### Apply artistic modifications:
		axBayesian.yaxis.set_tick_params(which = "both", length=5)

		if log_mass:
			axBayesian.semilogy()
			axBayesian.tick_params(axis='y', which='minor')
			axBayesian.yaxis.\
			set_minor_formatter(ticker.FormatStrFormatter("%g"))
			axBayesian.yaxis.\
			set_major_formatter(ticker.FormatStrFormatter("%g"))

		### Plot the marginalized histograms on the x and y axes, normalized.
		# Because the interpolation is on a regular grid, the width is simply
		# the distance of a single point to another.
		axHistx.bar(logage_var, hist_logage, width = logage_width)

		axHisty.barh(mass_var, hist_mass, height = mass_width)
		if log_mass:
			axHisty.semilogy()

		### Calculate the level for contour calculations.
		### We get internally-consistent best fit from the models here for free as well.
		posterior_f, logage_f, mass_f =\
		zip(*sorted(zip(posterior.flatten(), logage_grid.flatten(), mass_grid.flatten()), key = lambda x: -x[0]))
		sum_posterior_f = np.nansum(posterior_f)
		posterior_f = np.nan_to_num(posterior_f)
		posterior_sum = 0.
		index_posterior = 0
		while posterior_sum < 0.68:
			posterior_sum += posterior_f[index_posterior]/sum_posterior_f
			index_posterior += 1
		level_1sig = posterior_f[index_posterior]
		while posterior_sum < 0.95:
			posterior_sum += posterior_f[index_posterior]/sum_posterior_f
			index_posterior += 1
		level_2sig = posterior_f[index_posterior]-1e-16
		levels = sorted([level_1sig, level_2sig],key = lambda x:x)
		fmt = {level_1sig: "$68 \\%$", level_2sig: "$95 \\%$"}
		contour = axBayesian.contour(logage_grid, mass_grid,
					 posterior.reshape(logage_grid.shape), levels = levels,
					 colors = "white")
		axBayesian.clabel(contour, contour.levels, inline=True, fmt=fmt, fontsize=10)
		axBayesian.set_xlabel("$log_{10} (Age \\, [yr])$")
		axBayesian.set_ylabel("Mass $[M_\\odot]$")

		### Overplot confidence intervals for logage and mass.
		ci_linewidth = 2
		one_sig_linestyle = "dashed"
		two_sig_linestyle = "dotted"

		axHistx.axvline(logage_interv_1sig[1],
		linewidth=ci_linewidth,
		linestyle="solid",
		label = "Marginal Best")

		axHistx.axvline(logage_interv_1sig[0],
		linewidth=ci_linewidth,
		linestyle=one_sig_linestyle,
		label = "$1 \\, \\sigma$")
		axHistx.axvline(logage_interv_1sig[2],
		linewidth=ci_linewidth,
		linestyle=one_sig_linestyle)

		axHistx.axvline(logage_interv_2sig[0],
		linewidth=ci_linewidth,
		linestyle=two_sig_linestyle,
		label = "$2 \\, \\sigma$")
		axHistx.axvline(logage_interv_2sig[2],
		linewidth=ci_linewidth,
		linestyle=two_sig_linestyle)

		axHisty.axhline(mass_interv_1sig[1],
		linewidth=ci_linewidth,
		linestyle="solid")

		axHisty.axhline(mass_interv_1sig[0],
		linewidth=ci_linewidth,
		linestyle=one_sig_linestyle)
		axHisty.axhline(mass_interv_1sig[2],
		linewidth=ci_linewidth,
		linestyle=one_sig_linestyle)

		axHisty.axhline(mass_interv_2sig[0],
		linewidth=ci_linewidth,
		linestyle=two_sig_linestyle)
		axHisty.axhline(mass_interv_2sig[2],
		linewidth=ci_linewidth,
		linestyle=two_sig_linestyle)

		axHisty.yaxis.tick_right()
		axHisty.set_xticks(axHisty.get_xticks()[1:])

		### Prepare legend for 1 sig, 2 sig on axHistx:
		axHistx.legend()

		#axBayesian.plot(best_logage, best_mass,
		axBayesian.plot(logage_interv_1sig[1], mass_interv_1sig[1],
			marker="*",
			markersize=10,
			color="white",
			alpha = 1.0,
			markeredgecolor="black",
			markeredgewidth=1.0,
			label = "Peak, $log_{10}(Age \\, [yr]) = %.3f$, $M = %.3f \\, M_\\odot$" %\
		(logage_interv_1sig[1], mass_interv_1sig[1]))
		#(best_logage, best_mass))

		leg = axBayesian.legend(\
		fancybox = True, numpoints = 1, fontsize=14, bbox_to_anchor = (1.47, -.05),)
		if show_colorbar:
			cb = plt.colorbar(Bayes, cax = fig.add_axes(rect_colorbar))

		### Prepare the title

		# First, find the required number of decimals for the +/- signs.

		if np.isfinite(np.log10(mass_interv_1sig[2] - mass_interv_1sig[1])):
			mass_decimals_req_p = int(np.ceil(np.nanmax([2, -np.log10(mass_interv_1sig[2] - mass_interv_1sig[1])])))
		else:
			mass_decimals_req_p = int(2)

		if np.isfinite(np.log10(mass_interv_1sig[1] - mass_interv_1sig[0])):
			mass_decimals_req_n = int(np.ceil(np.nanmax([2, -np.log10(mass_interv_1sig[1] - mass_interv_1sig[0])])))
		else:
			mass_decimals_req_n = int(2)

		mass_decimals_req = np.max([mass_decimals_req_p, mass_decimals_req_n])

		mass_interv_string_p = ("%."+str(mass_decimals_req)+"f") % (mass_interv_1sig[2] - mass_interv_1sig[1])
		mass_interv_string_n = ("%."+str(mass_decimals_req)+"f") % (mass_interv_1sig[1] - mass_interv_1sig[0])

		if np.isfinite(np.log10(np.power(10.,logage_interv_1sig[2]-6) - np.power(10.,logage_interv_1sig[1]-6))):
			logage_decimals_req_p = int(np.ceil(np.nanmax([2, -np.log10(np.power(10.,logage_interv_1sig[2]-6) - np.power(10.,logage_interv_1sig[1]-6))])))
		else:
			logage_decimals_req_p = int(2)

		if np.isfinite(np.log10(np.power(10.,logage_interv_1sig[1]-6) - np.power(10.,logage_interv_1sig[0]-6))):
			logage_decimals_req_n = int(np.ceil(np.nanmax([2, -np.log10(np.power(10.,logage_interv_1sig[1]-6) - np.power(10.,logage_interv_1sig[0]-6))])))
		else:
			logage_decimals_req_n = int(2)

		logage_decimals_req = np.max([logage_decimals_req_p, logage_decimals_req_n])

		logage_interv_string_p = ("%."+str(logage_decimals_req)+"f") % (np.power(10.,logage_interv_1sig[2]-6) - np.power(10.,logage_interv_1sig[1]-6))
		logage_interv_string_n = ("%."+str(logage_decimals_req)+"f") % (np.power(10.,logage_interv_1sig[1]-6) - np.power(10.,logage_interv_1sig[0]-6))

		print_title =\
		("$%."+str(mass_decimals_req)+"f ^ {+%s} _ {-%s} M_\\odot$, $A = %."+str(logage_decimals_req)+"f ^ {+%s} _ {-%s} Myr$") %\
		(mass_interv_1sig[1],
		 mass_interv_string_p,
		 mass_interv_string_n,
		 np.power(10.,logage_interv_1sig[1]-6),
		 logage_interv_string_p,
		 logage_interv_string_n)
		if self.title is not None:
			if len(self.title) > 0:
				print_title = self.title + " " + print_title
		ttl = plt.suptitle(print_title,y=1.01)

		### Get individual ages and masses for each star:
		num_samples = 33

		logTs_random = RandomNumber(logTs, sigma_logTs, num_samples = num_samples).output.flatten()
		logLs_random = RandomNumber(logLs, sigma_logLs, num_samples = num_samples).output.flatten()

		logages_random, masses_random =\
		self.logage_mass_interp.evaluate(logTs_random, logLs_random)

		logages_random = np.reshape(logages_random, (len(logTs), num_samples))
		masses_random = np.reshape(masses_random, (len(logTs), num_samples))

		individual_logages = np.take(logages_random, 0, axis=1)
		individual_logages_e = np.std(logages_random, axis=1)

		individual_masses = np.take(masses_random, 0, axis=1)
		individual_masses_e = np.std(masses_random, axis=1)
		#print individual_logages, individual_masses

		### Create additional plots on the top right.
		axHRD.set_xticklabels([])
		axHRD.set_yticklabels([])

		ax_age_teff = plt.axes(rect_age_teff)
		ax_age_teff.invert_xaxis()
		ax_age_teff.set_xlim(axHRD.get_xlim())
		ax_age_teff.yaxis.set_minor_locator(ticker.MultipleLocator(0.5))
		ax_age_teff.tick_params(axis="y",which="both",direction="in", pad=-22)
		#ax_age_teff.tick_params(axis="x",direction="in", pad=-15)
		ax_age_teff.xaxis.set_label_position("top")
		ax_age_teff.xaxis.tick_top()
		ax_age_teff.set_yticklabels([])
		ax_age_teff.set_xlabel("$log_{10} \\, (T_{eff} \\, [K])$")
		# ax_age_teff.scatter(logTs, individual_logages, marker = ".",
		# s=3)
		ax_age_teff.errorbar(logTs, individual_logages,
		xerr = sigma_logTs, yerr = individual_logages_e,
		fmt = 'none')

		ax_logl_mass = plt.axes(rect_logl_mass)
		ax_logl_mass.set_ylim(axHRD.get_ylim())
		#ax_logl_mass.tick_params(axis="y",direction="in", pad=-22)
		ax_logl_mass.xaxis.set_minor_locator(ticker.MultipleLocator(0.25))
		ax_logl_mass.tick_params(axis="x",which="both",direction="in", pad=-15)
		ax_logl_mass.yaxis.set_label_position("right")
		ax_logl_mass.yaxis.tick_right()
		ax_logl_mass.set_ylabel("$log_{10} \\, L/L_\\odot$")
		ax_logl_mass.set_xticklabels([])
		# ax_logl_mass.scatter(individual_masses, logLs, marker = ".",
		# s=3)
		ax_logl_mass.errorbar(individual_masses, logLs,
		xerr = individual_masses_e, yerr = sigma_logLs,
		fmt = 'none')

		ax_age_mass = plt.axes(rect_age_mass)
		ax_age_mass.set_ylim(ax_age_teff.get_ylim())
		ax_age_mass.set_xlim(ax_logl_mass.get_xlim())
		ax_age_mass.xaxis.set_minor_locator(ticker.MultipleLocator(0.25))
		ax_age_mass.yaxis.set_label_position("right")
		ax_age_mass.yaxis.tick_right()
		ax_age_mass.yaxis.set_minor_locator(ticker.MultipleLocator(0.5))
		ax_age_mass.xaxis.set_label_position("top")
		ax_age_mass.xaxis.tick_top()
		ax_age_mass.set_xlabel("Mass $[M_\\odot]$")
		ax_age_mass.set_ylabel("$log_{10} \\, (Age \\, [yr])$")
		# ax_age_mass.scatter(individual_masses, individual_logages,
		# marker = ".", s=3)
		ax_age_mass.errorbar(individual_masses, individual_logages,
		xerr = individual_masses_e, yerr = individual_logages_e,
		fmt = 'none')

		if filename is not None:
			figure = plt.gcf() # get current figure
			figure.set_size_inches(12, 12)
			plt.savefig(filename,bbox_extra_artists=(ttl,leg,), bbox_inches='tight', dpi = 300)
			figure = plt.gcf() # get current figure
			figure.set_size_inches(6, 6)
			leg.set_visible(False)
			leg = axBayesian.legend(\
			fancybox = True, numpoints = 1, fontsize=8, bbox_to_anchor = (1.47, -.05),)
			if show_colorbar:
				cb = plt.colorbar(Bayes, cax = fig.add_axes(rect_colorbar))
			axHRD_leg.set_visible(False)
			ttl.set_visible(False)
			ttl = plt.suptitle(print_title,x=0.1,y=1.01,horizontalalignment='left')
			axBayesian.yaxis.set_tick_params(labelsize=6., which = "both")

			ax_age_teff.set_xlabel("$log_{10} \\, (T_{eff} \\, [K])$", fontsize=8)
			ax_logl_mass.set_ylabel("$log_{10} \\, L/L_\\odot$", fontsize=8)
			ax_age_mass.set_xlabel("Mass $[M_\\odot]$", fontsize=8)
			ax_age_mass.set_ylabel("$log_{10} \\, (Age \\, [yr])$", fontsize=8)

			plt.savefig("six_in_" + filename,bbox_extra_artists=(ttl,leg,), bbox_inches='tight', dpi = 300)
		else:
			plt.show()
		plt.clf()
		if output_HRD:
			### Do the errorbar HRD plot.
			plt.gca().invert_xaxis()

			isochrones = [["$0.1 \\, Myr$", 5.], ["$1 \\, Myr$", 6.], ["$10 \\, Myr$", 7.], ["$0.1 \\, Gyr$", 8.]]
			mass_tracks = [["$0.03 \\, M_\\odot$", 0.03], ["$0.1 \\, M_\\odot$", 0.1], ["$0.3 \\, M_\\odot$", 0.3], ["$1 \\, M_\\odot$", 1.], ["$3 \\, M_\\odot$", 3.], ["$10 \\, M_\\odot$", 10.]]
			### Plot isochrones and mass tracks:
			#logT_coords = np.linspace(self.range_logT[0], self.range_logT[1])
			#logL_coords = np.linspace(self.range_logL[0], self.range_logL[1])
			sample_points = 1000
			### Set the isochronal and mass track limits:
			min_mass = 0.03
			max_mass = 10.
			min_logage = 0.
			max_logage = 8.
			mass_coords = np.logspace(np.log10(min_mass), np.log10(max_mass), sample_points)
			logage_coords = np.linspace(min_logage, max_logage, sample_points)
			param_array = np.zeros(len(mass_coords))
			for isochrone_txt, isochrone_val in isochrones:
				param_array.fill(isochrone_val)
				#values = self.isochrone_interp(logT_coords, param_array)[1]
				logT_coords, logL_coords =\
				self.logT_logL_interp.evaluate(param_array, mass_coords)
				cond_plot = np.logical_and(np.isfinite(logT_coords),
										   np.isfinite(logL_coords))
				if np.sum(cond_plot) > 1:
					plt.plot(logT_coords[cond_plot], logL_coords[cond_plot],
							   label=isochrone_txt, zorder=2)
			for mass_txt, mass_val in mass_tracks:
				param_array.fill(mass_val)
				#values = self.masstrack_interp(logL_coords, param_array)[0]
				logT_coords, logL_coords =\
				self.logT_logL_interp.evaluate(logage_coords, param_array)
				cond_plot = np.logical_and(np.isfinite(logT_coords),
										   np.isfinite(logL_coords))
				if np.sum(cond_plot) > 1:
					plt.plot(logT_coords[cond_plot], logL_coords[cond_plot],
							   linestyle = "dashed", label=mass_txt, zorder=2)

			### Plot HRD:
			plt.errorbar(logTs, logLs,
						 xerr = sigma_logTs,
						 yerr = sigma_logLs,
						 ecolor='b',
						 fmt=",",
						 color='b',
						 zorder=3)
			### Plot the 1 sigma solution in age:
			# Get the left logage:
			param_array.fill(logage_interv_1sig[0])
			logT_coords_left, logL_coords_left =\
			self.logT_logL_interp.evaluate(param_array, mass_coords)
			cond_plot_left = np.logical_and(np.isfinite(logT_coords_left),
									   np.isfinite(logL_coords_left))

			logT_coords_left = logT_coords_left[cond_plot_left]
			logL_coords_left = logL_coords_left[cond_plot_left]

			# Sort:
			logL_coords_left = [x[1] for x in sorted(zip(logT_coords_left, logL_coords_left), key = lambda y: y[0])]
			logT_coords_left = sorted(logT_coords_left)

			# Get the mean logage:
			param_array.fill(logage_interv_1sig[1])
			logT_coords_mean, logL_coords_mean =\
			self.logT_logL_interp.evaluate(param_array, mass_coords)
			cond_plot_mean = np.logical_and(np.isfinite(logT_coords_mean),
									   np.isfinite(logL_coords_mean))

			logT_coords_mean = logT_coords_mean[cond_plot_mean]
			logL_coords_mean = logL_coords_mean[cond_plot_mean]

			# Sort:
			logL_coords_mean = [x[1] for x in sorted(zip(logT_coords_mean, logL_coords_mean), key = lambda y: y[0])]
			logT_coords_mean = sorted(logT_coords_mean)

			# Get the right logage:
			param_array.fill(logage_interv_1sig[2])
			logT_coords_right, logL_coords_right =\
			self.logT_logL_interp.evaluate(param_array, mass_coords)
			cond_plot_right = np.logical_and(np.isfinite(logT_coords_right),
									   np.isfinite(logL_coords_right))

			logT_coords_right = logT_coords_right[cond_plot_right]
			logL_coords_right = logL_coords_right[cond_plot_right]

			# Sort:
			logL_coords_right = [x[1] for x in sorted(zip(logT_coords_right, logL_coords_right), key = lambda y: y[0])]
			logT_coords_right = sorted(logT_coords_right)

			### Resample to universal logT coords, which is a subset of the region.
			logT_low = np.nanmax([logT_coords_left[0], logT_coords_right[0], np.nanmin(logT_coords_mean)])
			logT_hi = np.nanmin([logT_coords_left[-1], logT_coords_right[-1], np.nanmax(logT_coords_mean)])

			### Create a subsample on logT:
			logT_new_sample = np.linspace(logT_low, logT_hi, sample_points)

			# Interpolate!
			interp_logL_left = scipy.interpolate.interp1d(logT_coords_left, logL_coords_left)(logT_new_sample)
			interp_logL_mid = scipy.interpolate.interp1d(sorted(logT_coords_mean),
			[y[1] for y in sorted(zip(logT_coords_mean, logL_coords_mean),
			key = lambda x: x[0])])(logT_new_sample)
			interp_logL_right = scipy.interpolate.interp1d(logT_coords_right, logL_coords_right)(logT_new_sample)

			ttl = plt.title(print_title)

			plt.xlabel("$log _{10} (T_{eff})$ $[K]$")
			plt.ylabel("$log _{10} (L/L_\\odot)$")

			plt.plot(logT_coords_mean, logL_coords_mean, zorder = 5, alpha = 0.5, lw = 3, label = "Mean Age isochrone", linestyle = "dotted", color = "black")

			plt.fill_between(logT_new_sample, interp_logL_left, interp_logL_mid,
			zorder = 4, alpha = 0.25, label = "$\\pm 1 \\sigma$", facecolor = "orange")
			plt.fill_between(logT_new_sample, interp_logL_mid, interp_logL_right,
			zorder = 4, alpha = 0.25, facecolor = "orange")

			leg = plt.legend(framealpha=0.)
			if filename is not None:
				fn_array = filename.split("/")
				fn_array[-1] = "HRD_" + fn_array[-1]
				figure = plt.gcf() # get current figure
				figure.set_size_inches(12, 12)
				plt.savefig("/".join(fn_array),bbox_extra_artists=(ttl,leg,), bbox_inches='tight', dpi = 300)
				figure = plt.gcf() # get current figure
				fn_array = filename.split("/")
				fn_array[-1] = "six_in_HRD_" + fn_array[-1]
				figure.set_size_inches(6, 6)
				plt.savefig("/".join(fn_array),bbox_extra_artists=(ttl,leg,), bbox_inches='tight', dpi = 300)
			else:
				plt.show()
			plt.clf()

			#### Do the plot without errorbars
			isochrones = [["$0.1 \\, Myr$", 5.], ["$1 \\, Myr$", 6.], ["$10 \\, Myr$", 7.], ["$0.1 \\, Gyr$", 8.]]
			mass_tracks = [["$0.03 \\, M_\\odot$", 0.03], ["$0.1 \\, M_\\odot$", 0.1], ["$0.3 \\, M_\\odot$", 0.3], ["$1 \\, M_\\odot$", 1.], ["$3 \\, M_\\odot$", 3.], ["$10 \\, M_\\odot$", 10.]]
			### Plot isochrones and mass tracks:
			#logT_coords = np.linspace(self.range_logT[0], self.range_logT[1])
			#logL_coords = np.linspace(self.range_logL[0], self.range_logL[1])
			sample_points = 1000
			### Set the isochronal and mass track limits:
			min_mass = 0.03
			max_mass = 10.
			min_logage = 0.
			max_logage = 8.
			mass_coords = np.logspace(np.log10(min_mass), np.log10(max_mass), sample_points)
			logage_coords = np.linspace(min_logage, max_logage, sample_points)
			param_array = np.zeros(len(mass_coords))
			for isochrone_txt, isochrone_val in isochrones:
				param_array.fill(isochrone_val)
				#values = self.isochrone_interp(logT_coords, param_array)[1]
				logT_coords, logL_coords =\
				self.logT_logL_interp.evaluate(param_array, mass_coords)
				cond_plot = np.logical_and(np.isfinite(logT_coords),
										   np.isfinite(logL_coords))
				if np.sum(cond_plot) > 1:
					plt.plot(logT_coords[cond_plot], logL_coords[cond_plot],
							   label=isochrone_txt, zorder=2)
			for mass_txt, mass_val in mass_tracks:
				param_array.fill(mass_val)
				#values = self.masstrack_interp(logL_coords, param_array)[0]
				logT_coords, logL_coords =\
				self.logT_logL_interp.evaluate(logage_coords, param_array)
				cond_plot = np.logical_and(np.isfinite(logT_coords),
										   np.isfinite(logL_coords))
				if np.sum(cond_plot) > 1:
					plt.plot(logT_coords[cond_plot], logL_coords[cond_plot],
							   linestyle = "dashed", label=mass_txt, zorder=2)

			### Plot HRD:
			min_logT = np.nanmin(logTs)
			max_logT = np.nanmax(logTs)
			min_logL = np.nanmin(logLs)
			max_logL = np.nanmax(logLs)

			logT_padding = 0.1
			logL_padding = 0.5

			plt.scatter(logTs, logLs, zorder=3, s = 3.)
			### Plot the 1 sigma solution in age:
			# Get the left logage:
			param_array.fill(logage_interv_1sig[0])
			logT_coords_left, logL_coords_left =\
			self.logT_logL_interp.evaluate(param_array, mass_coords)
			cond_plot_left = np.logical_and(np.isfinite(logT_coords_left),
									   np.isfinite(logL_coords_left))

			logT_coords_left = logT_coords_left[cond_plot_left]
			logL_coords_left = logL_coords_left[cond_plot_left]

			# Sort:
			logL_coords_left = [x[1] for x in sorted(zip(logT_coords_left, logL_coords_left), key = lambda y: y[0])]
			logT_coords_left = sorted(logT_coords_left)

			# Get the mean logage:
			param_array.fill(logage_interv_1sig[1])
			logT_coords_mean, logL_coords_mean =\
			self.logT_logL_interp.evaluate(param_array, mass_coords)
			cond_plot_mean = np.logical_and(np.isfinite(logT_coords_mean),
									   np.isfinite(logL_coords_mean))

			logT_coords_mean = logT_coords_mean[cond_plot_mean]
			logL_coords_mean = logL_coords_mean[cond_plot_mean]

			# Sort:
			logL_coords_mean = [x[1] for x in sorted(zip(logT_coords_mean, logL_coords_mean), key = lambda y: y[0])]
			logT_coords_mean = sorted(logT_coords_mean)

			# Get the right logage:
			param_array.fill(logage_interv_1sig[2])
			logT_coords_right, logL_coords_right =\
			self.logT_logL_interp.evaluate(param_array, mass_coords)
			cond_plot_right = np.logical_and(np.isfinite(logT_coords_right),
									   np.isfinite(logL_coords_right))

			logT_coords_right = logT_coords_right[cond_plot_right]
			logL_coords_right = logL_coords_right[cond_plot_right]

			# Sort:
			logL_coords_right = [x[1] for x in sorted(zip(logT_coords_right, logL_coords_right), key = lambda y: y[0])]
			logT_coords_right = sorted(logT_coords_right)

			### Resample to universal logT coords, which is a subset of the region.
			logT_low = np.nanmax([logT_coords_left[0], logT_coords_right[0], np.nanmin(logT_coords_mean)])
			logT_hi = np.nanmin([logT_coords_left[-1], logT_coords_right[-1], np.nanmax(logT_coords_mean)])

			### Create a subsample on logT:
			logT_new_sample = np.linspace(logT_low, logT_hi, sample_points)

			# Interpolate!
			interp_logL_left = scipy.interpolate.interp1d(logT_coords_left, logL_coords_left)(logT_new_sample)
			interp_logL_mid = scipy.interpolate.interp1d(sorted(logT_coords_mean),
			[y[1] for y in sorted(zip(logT_coords_mean, logL_coords_mean),
			key = lambda x: x[0])])(logT_new_sample)
			interp_logL_right = scipy.interpolate.interp1d(logT_coords_right, logL_coords_right)(logT_new_sample)

			ttl = plt.title(print_title)

			plt.xlabel("$log _{10} (T_{eff})$ $[K]$")
			plt.ylabel("$log _{10} (L/L_\\odot)$")

			plt.plot(logT_coords_mean, logL_coords_mean, zorder = 5, alpha = 0.5, lw = 3, label = "Mean Age isochrone", linestyle = "dotted", color = "black")

			plt.fill_between(logT_new_sample, interp_logL_left, interp_logL_mid,
			zorder = 4, alpha = 0.25, label = "$\\pm 1 \\sigma$", facecolor = "orange")
			plt.fill_between(logT_new_sample, interp_logL_mid, interp_logL_right,
			zorder = 4, alpha = 0.25, facecolor = "orange")

			plt.xlim(min_logT-logT_padding, max_logT+logT_padding)
			plt.ylim(min_logL-logL_padding, max_logL+logL_padding)

			leg = plt.legend(framealpha=0.)
			plt.gca().invert_xaxis()
			if filename is not None:
				fn_array = filename.split("/")
				fn_array[-1] = "pt_HRD_" + fn_array[-1]
				figure = plt.gcf() # get current figure
				figure.set_size_inches(12, 12)
				plt.savefig("/".join(fn_array),bbox_extra_artists=(ttl,leg,), bbox_inches='tight', dpi = 300)
				figure = plt.gcf() # get current figure
				fn_array = filename.split("/")
				fn_array[-1] = "six_in_pt_HRD_" + fn_array[-1]
				figure.set_size_inches(6, 6)
				plt.savefig("/".join(fn_array),bbox_extra_artists=(ttl,leg,), bbox_inches='tight', dpi = 300)
			else:
				plt.show()
			plt.clf()

			### Plot Hess:
			min_logT = np.nanmin(logTs)
			max_logT = np.nanmax(logTs)
			min_logL = np.nanmin(logLs)
			max_logL = np.nanmax(logLs)

			logT_padding = 0.1
			logL_padding = 0.5

			sample_points = 1000
			### Set the isochronal and mass track limits:

			logT_coords = np.linspace(min_logT, max_logT, num_bins)
			logL_coords = np.linspace(min_logL, max_logL, num_bins)

			logT_grid, logL_grid = np.meshgrid(logT_coords, logL_coords)

			posterior = np.zeros(len(logT_grid.flatten()))

			for grid_idx in range(len(logTs)):
				if np.all(np.logical_not(np.isnan([logTs[grid_idx], logLs[grid_idx], sigma_logTs[grid_idx], sigma_logLs[grid_idx]]))):
					posterior +=\
					np.exp(-.5*(\
					np.square((logT_grid.flatten()-logTs[grid_idx])/sigma_logTs[grid_idx]) +\
					np.square((logL_grid.flatten()-logLs[grid_idx])/sigma_logLs[grid_idx]) ))/\
					(2.*np.pi*sigma_logTs[grid_idx]*sigma_logLs[grid_idx])

			left, width = 0.1, 0.75
			bottom, height = 0.1, 0.75
			bottom_h = left_h = left + width + 0.02

			rect_bayesian = [left, bottom, width, height]
			rect_histx = [left, bottom_h, width, 0.1]
			rect_histy = [left_h, bottom, 0.1, height]

			axBayesian = plt.axes(rect_bayesian)
			axHistx = plt.axes(rect_histx)
			axHisty = plt.axes(rect_histy)
			axHistx.xaxis.set_major_formatter(nullfmt)
			axHisty.yaxis.set_major_formatter(nullfmt)

			# Reshape the posterior
			posterior = posterior.reshape(logT_grid.shape)
			print posterior
			#Bayes = axBayesian.imshow(posterior, extent = extent,
			#				  origin="lower", aspect="auto")
			X,Y=logT_grid, logL_grid
			Z=posterior
			Bayes = axBayesian.pcolormesh(X,Y,Z,
			vmin=np.nanmin(Z), vmax=np.nanmax(Z))

			# ### Apply artistic modifications:
			# axBayesian.yaxis.set_tick_params(which = "both", length=5)
			#
			# if log_mass:
			# 	axBayesian.semilogy()
			# 	axBayesian.tick_params(axis='y', which='minor')
			# 	axBayesian.yaxis.\
			# 	set_minor_formatter(ticker.FormatStrFormatter("%s"))
			# 	axBayesian.yaxis.\
			# 	set_major_formatter(ticker.FormatStrFormatter("%s"))

			### Plot the marginalized histograms on the x and y axes, normalized.
			# Because the interpolation is on a regular grid, the width is simply
			# the distance of a single point to another.

			logT_width_right = np.zeros(len(logT_coords))
			logT_width_right[0:-1] = logT_coords[1:] - logT_coords[0:-1]
			logT_width_right[-1] = logT_width_right[-2]

			logT_width_left = np.zeros(len(logT_coords))
			logT_width_left[1:] = logT_coords[1:] - logT_coords[0:-1]
			logT_width_left[0] = logT_width_left[1]

			logT_width = 0.5*(logT_width_left + logT_width_right)

			logL_width_right = np.zeros(len(logL_coords))
			logL_width_right[0:-1] = logL_coords[1:] - logL_coords[0:-1]
			logL_width_right[-1] = logL_width_right[-2]

			logL_width_left = np.zeros(len(logL_coords))
			logL_width_left[1:] = logL_coords[1:] - logL_coords[0:-1]
			logL_width_left[0] = logL_width_left[1]

			logL_width = 0.5*(logL_width_left + logL_width_right)

			hist_logT = \
			np.dot(np.reshape(posterior, (num_bins, num_bins)).T, logT_width)\
			/(np.nansum(np.dot(np.reshape(posterior, (num_bins, num_bins)).T, logT_width)))

			hist_logL = \
			np.dot(np.reshape(posterior, (num_bins, num_bins)), logL_width)\
			/(np.nansum(np.dot(np.reshape(posterior, (num_bins, num_bins)), logL_width)))

			axHistx.bar(logT_coords, hist_logT, width = logT_width)

			axHisty.barh(logL_coords, hist_logL, height = logL_width)

			### Calculate the level for contour calculations.
			### We get internally-consistent best fit from the models here for free as well.
			# posterior_f, logage_f, mass_f =\
			# zip(*sorted(zip(posterior.flatten(), logage_grid.flatten(), mass_grid.flatten()), key = lambda x: -x[0]))
			# sum_posterior_f = np.nansum(posterior_f)
			# posterior_f = np.nan_to_num(posterior_f)
			# posterior_sum = 0.
			# index_posterior = 0
			# while posterior_sum < 0.68:
			# 	posterior_sum += posterior_f[index_posterior]/sum_posterior_f
			# 	index_posterior += 1
			# level_1sig = posterior_f[index_posterior]
			# while posterior_sum < 0.95:
			# 	posterior_sum += posterior_f[index_posterior]/sum_posterior_f
			# 	index_posterior += 1
			# level_2sig = posterior_f[index_posterior]-1e-16
			# levels = sorted([level_1sig, level_2sig],key = lambda x:x)
			# fmt = {level_1sig: "$68 \\%$", level_2sig: "$95 \\%$"}
			# contour = axBayesian.contour(logage_grid, mass_grid,
			# 			 posterior.reshape(logage_grid.shape), levels = levels,
			# 			 colors = "white")
			# axBayesian.clabel(contour, contour.levels, inline=True, fmt=fmt, fontsize=10)
			# axBayesian.set_xlabel("$log_{10} (Age \\, [yr])$")
			# axBayesian.set_ylabel("Mass $[M_\\odot]$")
			#
			# param_array = np.zeros(len(logT_coords))
			#
			# ## Plot the 1 sigma solution in age:
			# # Get the left logage:
			# param_array.fill(logage_interv_1sig[0])
			# logT_coords_left, logL_coords_left =\
			# self.logT_logL_interp.evaluate(param_array, mass_coords)
			# cond_plot_left = np.logical_and(np.isfinite(logT_coords_left),
			# 						   np.isfinite(logL_coords_left))
			#
			# logT_coords_left = logT_coords_left[cond_plot_left]
			# logL_coords_left = logL_coords_left[cond_plot_left]
			#
			# # Sort:
			# logL_coords_left = [x[1] for x in sorted(zip(logT_coords_left, logL_coords_left), key = lambda y: y[0])]
			# logT_coords_left = sorted(logT_coords_left)
			#
			# # Get the mean logage:
			# param_array.fill(logage_interv_1sig[1])
			# logT_coords_mean, logL_coords_mean =\
			# self.logT_logL_interp.evaluate(param_array, mass_coords)
			# cond_plot_mean = np.logical_and(np.isfinite(logT_coords_mean),
			# 						   np.isfinite(logL_coords_mean))
			#
			# logT_coords_mean = logT_coords_mean[cond_plot_mean]
			# logL_coords_mean = logL_coords_mean[cond_plot_mean]
			#
			# # Sort:
			# logL_coords_mean = [x[1] for x in sorted(zip(logT_coords_mean, logL_coords_mean), key = lambda y: y[0])]
			# logT_coords_mean = sorted(logT_coords_mean)
			#
			# # Get the right logage:
			# param_array.fill(logage_interv_1sig[2])
			# logT_coords_right, logL_coords_right =\
			# self.logT_logL_interp.evaluate(param_array, mass_coords)
			# cond_plot_right = np.logical_and(np.isfinite(logT_coords_right),
			# 						   np.isfinite(logL_coords_right))
			#
			# logT_coords_right = logT_coords_right[cond_plot_right]
			# logL_coords_right = logL_coords_right[cond_plot_right]
			#
			# # Sort:
			# logL_coords_right = [x[1] for x in sorted(zip(logT_coords_right, logL_coords_right), key = lambda y: y[0])]
			# logT_coords_right = sorted(logT_coords_right)
			#
			# ### Resample to universal logT coords, which is a subset of the region.
			# logT_low = np.nanmax([logT_coords_left[0], logT_coords_right[0], np.nanmin(logT_coords_mean)])
			# logT_hi = np.nanmin([logT_coords_left[-1], logT_coords_right[-1], np.nanmax(logT_coords_mean)])
			#
			# ### Create a subsample on logT:
			# logT_new_sample = np.linspace(logT_low, logT_hi, sample_points)
			#
			# # Interpolate!
			# interp_logL_left = scipy.interpolate.interp1d(logT_coords_left, logL_coords_left)(logT_new_sample)
			# interp_logL_mid = scipy.interpolate.interp1d(sorted(logT_coords_mean),
			# [y[1] for y in sorted(zip(logT_coords_mean, logL_coords_mean),
			# key = lambda x: x[0])])(logT_new_sample)
			# interp_logL_right = scipy.interpolate.interp1d(logT_coords_right, logL_coords_right)(logT_new_sample)

			ttl = plt.suptitle(print_title,y=1.01)

			axBayesian.set_xlabel("$log _{10} (T_{eff})$ $[K]$")
			axBayesian.set_ylabel("$log _{10} (L/L_\\odot)$")

			# plt.plot(logT_coords_mean, logL_coords_mean, zorder = 5, alpha = 0.5, lw = 3, label = "Mean Age isochrone", linestyle = "dotted", color = "black")

			# plt.fill_between(logT_new_sample, interp_logL_left, interp_logL_mid,
			# zorder = 4, alpha = 0.5, label = "$\\pm 1 \\sigma$", facecolor = "orange")
			# plt.fill_between(logT_new_sample, interp_logL_mid, interp_logL_right,
			# zorder = 4, alpha = 0.5, facecolor = "orange")

			axBayesian.set_xlim(min_logT, max_logT)
			axBayesian.set_ylim(min_logL, max_logL)
			axHistx.set_xlim(axBayesian.get_xlim())
			axHisty.set_ylim(axBayesian.get_ylim())
			axBayesian.invert_xaxis()
			axHistx.invert_xaxis()

			# axBayesian.colorbar()

			# leg = plt.legend(framealpha=0.)
			if filename is not None:
				fn_array = filename.split("/")
				fn_array[-1] = "Hess_" + fn_array[-1]
				figure = plt.gcf() # get current figure
				figure.set_size_inches(12, 12)
				plt.savefig("/".join(fn_array),bbox_extra_artists=(ttl,), bbox_inches='tight', dpi = 300)
				figure = plt.gcf() # get current figure
				fn_array = filename.split("/")
				fn_array[-1] = "six_in_Hess_" + fn_array[-1]
				figure.set_size_inches(6, 6)
				plt.savefig("/".join(fn_array),bbox_extra_artists=(ttl,), bbox_inches='tight', dpi = 300)
			else:
				plt.show()
			plt.clf()
		return ([logage_interv_1sig[0], logage_interv_1sig[1], logage_interv_1sig[2]], [mass_interv_1sig[0], mass_interv_1sig[1], mass_interv_1sig[2]])
	def _flip_eval(self, logTs, logLs, sigma_logTs = None, sigma_logLs = None,
				  title = None,
				  filename = None,
				  min_logage = 5.,
				  max_logage = 8.,
				  min_mass = 0.1,
				  max_mass = 30.,
				  num_bins = 101,
				  n_samples = 1000,
				  mass_prior = priors.chabrier_mass_prior,
				  age_prior = priors.uniform_age_prior,
				  track_label = "MIST",
				  sum_chisq = False,
				  show_colorbar = False,
				  log_mass = False,
				  output_HRD = True):
		"""Returns a tuple with the confidence intervals of the logage
		and then mass if sigma_logTs and sigma_logLs are specified.
		Otherwise, returns a best value with
		the attached IsochroneInterpolator.
		If `filename` is specified, do a plot and save it to the directory
		requested. If it is not specified, show it to the screen.

		n_samples determines the number of samples used to calculate the
		age and mass sigma values.

		sum_chisq is True for summed posteriors,
		False for multiplied posteriors.

		show_colorbar shows the colorbar on the right-hand side of the plot.

		log_mass, when True, forces the plot to be performed on
		a logarithmic scale."""

		### Set the title.
		# if title is not None:
		# 	self.title += title

		if track_label is not None:
			self.track_label = track_label

		logage_var = np.linspace(min_logage, max_logage, num_bins)
		if log_mass:
			mass_var = np.logspace(np.log10(min_mass), np.log10(max_mass), num_bins)
		else:
			mass_var = np.linspace(min_mass, max_mass, num_bins)

		logage_grid, mass_grid = np.meshgrid(logage_var, mass_var)
		print logage_grid
		logT_grid, logL_grid =\
		self.logT_logL_interp.\
		evaluate(logage_grid.flatten(), mass_grid.flatten())

		#Assuming conditional independence, chisq is multiplied.
		sum_chisq = sum_chisq #This should be false

		if sum_chisq:
			#DEBUG: sum the posteriors instead.
			posterior = np.zeros(logT_grid.shape)
			for idx in range(len(logTs)):
				chisq =\
				np.square((logTs[idx] - logT_grid)/sigma_logTs[idx]) +\
				np.square((logLs[idx] - logL_grid)/sigma_logLs[idx])

				ln_likelihood = -0.5*chisq
				ln_posterior =\
				np.log(age_prior(logage_grid.flatten()))+\
				np.log(mass_prior(mass_grid.flatten()))+\
				ln_likelihood ###unnormalized

				# Arbitrarily normalize it by setting the ln peak to 0 to prevent
				# overflow issues.
				ln_posterior -= np.nanmax(ln_posterior)

				#Calculate the posterior:
				posterior += np.exp(ln_posterior)
		else:
			chisq = np.zeros(logT_grid.shape)
			for idx in range(len(logTs)):
				chisq +=\
				np.square((logTs[idx] - logT_grid)/sigma_logTs[idx]) +\
				np.square((logLs[idx] - logL_grid)/sigma_logLs[idx])

			ln_likelihood = -0.5*chisq
			ln_posterior =\
			np.log(age_prior(logage_grid.flatten()))+\
			np.log(mass_prior(mass_grid.flatten()))+\
			ln_likelihood ###unnormalized

			# Arbitrarily normalize it by setting the ln peak to 0 to prevent
			# overflow issues.
			ln_posterior -= np.nanmax(ln_posterior)

			#Calculate the posterior:
			posterior = np.exp(ln_posterior)
		#Condition for being finite:
		finite = np.isfinite(posterior)

		#Normalize the posterior:
		total_logage_width = logage_var[-1] - logage_var[0]
		total_mass_width = mass_var[-1] - mass_var[0]
		posterior = posterior/(\
		np.nansum(posterior[finite])*(total_logage_width * total_mass_width))

		### Convert NaN's to 0's:
		posterior = np.nan_to_num(posterior)

		### Calculate confidence intervals for logage and mass, marginalized:
		### We do this by computing the cdf, and then finding the smallest
		### interval over 68% and 95%, then sort by how much they go over.
		logage_width_right = np.zeros(len(mass_var))
		logage_width_right[0:-1] = logage_var[1:] - logage_var[0:-1]
		logage_width_right[-1] = logage_width_right[-2]

		logage_width_left = np.zeros(len(mass_var))
		logage_width_left[1:] = logage_var[1:] - logage_var[0:-1]
		logage_width_left[0] = logage_width_left[1]

		logage_width = 0.5*(logage_width_left + logage_width_right)

		hist_logage = \
		np.dot(np.reshape(posterior, (num_bins, num_bins)).T, logage_width)\
		/(np.nansum(np.dot(np.reshape(posterior, (num_bins, num_bins)).T, logage_width)))

		mass_width_right = np.zeros(len(mass_var))
		mass_width_right[0:-1] = mass_var[1:] - mass_var[0:-1]
		mass_width_right[-1] = mass_width_right[-2]

		mass_width_left = np.zeros(len(mass_var))
		mass_width_left[1:] = mass_var[1:] - mass_var[0:-1]
		mass_width_left[0] = mass_width_left[1]

		mass_width = 0.5*(mass_width_left + mass_width_right)

		# print "mass width", mass_width
		#
		# print "mass", np.dot(logage_grid, logage_width)
		# print "shape mass", np.dot(logage_grid, logage_width).shape
		#
		# print "logage", np.dot(mass_grid.T, mass_width)
		# print "shape logage", np.dot(mass_grid.T, mass_width).shape

		hist_mass =\
		np.dot(np.reshape(posterior, (num_bins, num_bins)), mass_width)\
		/(np.nansum(np.dot(np.reshape(posterior, (num_bins, num_bins)), mass_width)))

		logage_interv_1sig = self.confidence(logage_var, hist_logage, 0.68)
		logage_interv_2sig = self.confidence(logage_var, hist_logage, 0.95)
		mass_interv_1sig = self.confidence(mass_var, hist_mass, 0.68)
		mass_interv_2sig = self.confidence(mass_var, hist_mass, 0.95)

		### Plot the best values for logage and mass on the central Bayesian plot!
		### Take an weighted average of the logages and masses:
		#best_logage =\
		#np.nansum(logage_f*posterior_f*posterior_f)/np.nansum(posterior_f*posterior_f)
		#best_mass =\
		#np.nansum(mass_f*posterior_f*posterior_f)/np.nansum(posterior_f*posterior_f)

		### Do plotting routines:

		nullfmt = NullFormatter()
		# definitions for the axes
		left, width = 0.1, 0.65
		bottom, height = 0.1, 0.65
		bottom_h = left_h = left + width + 0.02

		rect_bayesian = [left, bottom, width, height]
		rect_histx = [left, bottom_h, width, 0.2]
		rect_histy = [left_h, bottom, 0.2, height]
		# if show_colorbar:
		# 	rect_histx = [left, bottom_h, width, 0.2]
		# 	rect_histy = [left_h, bottom, 0.2, height]
		# else:
		# 	### Allow the histograms to fill through the side plots of the HRD
		# 	rect_histx = [left, bottom_h, width+0., 0.3]
		# 	rect_histy = [left_h, bottom, 0.3, height]
		rect_hrd = [left_h, bottom_h, 0.2, 0.2]
		rect_colorbar = [left_h + 0.22, bottom, 0.01, height]
		### Create age/teff plot on top
		rect_age_teff = [left_h, bottom_h + 0.2, 0.2, 0.1]
		### Create logl/mass plot on bottom
		rect_logl_mass = [left_h + 0.2, bottom_h, 0.1, 0.2]
		### Create age/mass plot on very top right
		rect_age_mass = [left_h + 0.2, bottom_h + 0.2, 0.1, 0.1]
		# start with a rectangular Figure
		fig = plt.figure(1, figsize=(12, 12))
		axBayesian = plt.axes(rect_bayesian)
		axHistx = plt.axes(rect_histx)
		axHisty = plt.axes(rect_histy)
		axHistx.xaxis.set_major_formatter(nullfmt)
		axHisty.yaxis.set_major_formatter(nullfmt)
		axHRD = plt.axes(rect_hrd)
		axHRD.tick_params(axis="y",direction="in", pad=-22)
		axHRD.tick_params(axis="x",direction="in", pad=-15)
		#axHRD.xaxis.set_major_formatter(nullfmt)
		#axHRD.yaxis.set_major_formatter(nullfmt)

		axBayesian.set_ylim((logage_var[0], logage_var[-1]))
		axBayesian.set_xlim((mass_var[0], mass_var[-1]))
		axHistx.set_xlim(axBayesian.get_xlim())
		axHisty.set_ylim(axBayesian.get_ylim())
		axHRD.invert_xaxis()

		isochrones = [["$0.1 \\, Myr$", 5.], ["$1 \\, Myr$", 6.], ["$10 \\, Myr$", 7.], ["$0.1 \\, Gyr$", 8.]]
		mass_tracks = [["$0.03 \\, M_\\odot$", 0.03], ["$0.1 \\, M_\\odot$", 0.1], ["$0.3 \\, M_\\odot$", 0.3], ["$1 \\, M_\\odot$", 1.], ["$3 \\, M_\\odot$", 3.], ["$10 \\, M_\\odot$", 10.]]
		### Plot isochrones and mass tracks:
		#logT_coords = np.linspace(self.range_logT[0], self.range_logT[1])
		#logL_coords = np.linspace(self.range_logL[0], self.range_logL[1])
		sample_points = 1000
		### Set the isochronal and mass track limits:
		min_mass = 0.03
		max_mass = 10.
		min_logage = 0.
		max_logage = 8.
		mass_coords = np.logspace(np.log10(min_mass), np.log10(max_mass), sample_points)
		logage_coords = np.linspace(min_logage, max_logage, sample_points)
		param_array = np.zeros(len(mass_coords))
		for isochrone_txt, isochrone_val in isochrones:
			param_array.fill(isochrone_val)
			#values = self.isochrone_interp(logT_coords, param_array)[1]
			logT_coords, logL_coords =\
			self.logT_logL_interp.evaluate(param_array, mass_coords)
			cond_plot = np.logical_and(np.isfinite(logT_coords),
									   np.isfinite(logL_coords))
			if np.sum(cond_plot) > 1:
				axHRD.plot(logT_coords[cond_plot], logL_coords[cond_plot],
						   label=isochrone_txt, zorder=2)
				#axHRD.plot(logT_coords[cond_plot], values[cond_plot], label=isochrone_txt)
		for mass_txt, mass_val in mass_tracks:
			param_array.fill(mass_val)
			#values = self.masstrack_interp(logL_coords, param_array)[0]
			logT_coords, logL_coords =\
			self.logT_logL_interp.evaluate(logage_coords, param_array)
			cond_plot = np.logical_and(np.isfinite(logT_coords),
									   np.isfinite(logL_coords))
			if np.sum(cond_plot) > 1:
				axHRD.plot(logT_coords[cond_plot], logL_coords[cond_plot],
						   linestyle = "dashed", label=mass_txt, zorder=2)
				#axHRD.plot(values[cond_plot], logL_coords[cond_plot], linestyle = "dashed", label=mass_txt)
			#if mass_txt == "$1 \\, M_\\odot$":
				#return logage_coords[np.nanargmax(logT_coords)]
		#labelLines(axHRD.get_lines())
		axHRD_leg = axHRD.legend(framealpha=0.)
		### Plot the HRD convex hull and the location of the star on this region.
		for simplex in self.hull_hrd.simplices:
			axHRD.plot(self.points_hrd[simplex, 0], self.points_hrd[simplex, 1], 'k-', zorder=1)
		### Plot HRD:
		axHRD.errorbar(logTs, logLs,
					 xerr = sigma_logTs,
					 yerr = sigma_logLs,
					 ecolor='b',
					 fmt=",",
					 color='b',
					 zorder=3)

		### Find a location to put the self.track_label on the HRD:
		#self.track_label_x = np.min(axBayesian.get_xlim())
		#self.track_label_y = np.max(axHRD.get_ylim())
		plt.text(0.98, 0.98, self.track_label, horizontalalignment='right',
				 verticalalignment='top', transform=axHRD.transAxes)

		### Plot the main Bayesian posterior on axBayesian.
		extent = (mass_var[0], mass_var[-1], logage_var[0], logage_var[-1])
		# Reshape the posterior
		posterior = posterior.reshape(logage_grid.shape)
		#Bayes = axBayesian.imshow(posterior, extent = extent,
		#				  origin="lower", aspect="auto")
		X,Y=mass_grid, logage_grid
		Z=posterior
		Bayes = axBayesian.pcolormesh(X,Y,Z, vmin=np.nanmin(Z),
		vmax=np.nanmax(Z))

		### Apply artistic modifications:
		axBayesian.yaxis.set_tick_params(which = "both", length=5)

		if log_mass:
			axBayesian.semilogx()
			axBayesian.tick_params(axis='x', which='minor')
			axBayesian.xaxis.\
			set_minor_formatter(ticker.FormatStrFormatter("%g"))
			axBayesian.xaxis.\
			set_major_formatter(ticker.FormatStrFormatter("%g"))

		### Plot the marginalized histograms on the x and y axes, normalized.
		# Because the interpolation is on a regular grid, the width is simply
		# the distance of a single point to another.
		axHisty.barh(logage_var, hist_logage, height = logage_width)

		axHistx.bar(mass_var, hist_mass, width = mass_width)
		if log_mass:
			axHistx.semilogx()

		### Calculate the level for contour calculations.
		### We get internally-consistent best fit from the models here for free as well.
		posterior_f, logage_f, mass_f =\
		zip(*sorted(zip(posterior.flatten(), logage_grid.flatten(), mass_grid.flatten()), key = lambda x: -x[0]))
		sum_posterior_f = np.nansum(posterior_f)
		posterior_f = np.nan_to_num(posterior_f)
		posterior_sum = 0.
		index_posterior = 0
		while posterior_sum < 0.68:
			posterior_sum += posterior_f[index_posterior]/sum_posterior_f
			index_posterior += 1
		level_1sig = posterior_f[index_posterior]
		while posterior_sum < 0.95:
			posterior_sum += posterior_f[index_posterior]/sum_posterior_f
			index_posterior += 1
		level_2sig = posterior_f[index_posterior]-1e-16
		levels = sorted([level_1sig, level_2sig],key = lambda x:x)
		fmt = {level_1sig: "$68 \\%$", level_2sig: "$95 \\%$"}
		contour = axBayesian.contour(mass_grid, logage_grid,
					 posterior.reshape(logage_grid.shape), levels = levels,
					 colors = "white")
		axBayesian.clabel(contour, contour.levels, inline=True, fmt=fmt, fontsize=10)
		axBayesian.set_ylabel("$log_{10} (Age \\, [yr])$")
		axBayesian.set_xlabel("Mass $[M_\\odot]$")

		### Overplot confidence intervals for logage and mass.
		ci_linewidth = 2
		one_sig_linestyle = "dashed"
		two_sig_linestyle = "dotted"

		axHistx.axvline(mass_interv_1sig[1],
		linewidth=ci_linewidth,
		linestyle="solid",
		label = "Marginal Best")

		axHistx.axvline(mass_interv_1sig[0],
		linewidth=ci_linewidth,
		linestyle=one_sig_linestyle,
		label = "$1 \\, \\sigma$")
		axHistx.axvline(mass_interv_1sig[2],
		linewidth=ci_linewidth,
		linestyle=one_sig_linestyle)

		axHistx.axvline(mass_interv_2sig[0],
		linewidth=ci_linewidth,
		linestyle=two_sig_linestyle,
		label = "$2 \\, \\sigma$")
		axHistx.axvline(mass_interv_2sig[2],
		linewidth=ci_linewidth,
		linestyle=two_sig_linestyle)

		axHisty.axhline(logage_interv_1sig[1],
		linewidth=ci_linewidth,
		linestyle="solid")

		axHisty.axhline(logage_interv_1sig[0],
		linewidth=ci_linewidth,
		linestyle=one_sig_linestyle)
		axHisty.axhline(logage_interv_1sig[2],
		linewidth=ci_linewidth,
		linestyle=one_sig_linestyle)

		axHisty.axhline(logage_interv_2sig[0],
		linewidth=ci_linewidth,
		linestyle=two_sig_linestyle)
		axHisty.axhline(logage_interv_2sig[2],
		linewidth=ci_linewidth,
		linestyle=two_sig_linestyle)

		axHisty.yaxis.tick_right()
		axHisty.set_xticks(axHisty.get_xticks()[1:])

		### Prepare legend for 1 sig, 2 sig on axHistx:
		axHistx.legend()

		#axBayesian.plot(best_logage, best_mass,
		axBayesian.plot(mass_interv_1sig[1], logage_interv_1sig[1],
			marker="*",
			markersize=10,
			color="white",
			alpha = 1.0,
			markeredgecolor="black",
			markeredgewidth=1.0,
			label = "$log_{10}(Age \\, [yr]) = %.3f \\pm %.2f$, $M = %.3f  \\pm %.2f \\, M_\\odot$" %\
		(logage_interv_1sig[1], 0.5*(logage_interv_1sig[2] - logage_interv_1sig[0]),
		mass_interv_1sig[1], 0.5*(mass_interv_1sig[2] - mass_interv_1sig[0])))
		#(best_logage, best_mass))

		leg = axBayesian.legend(\
		fancybox = True, numpoints = 1, fontsize=14, bbox_to_anchor = (1.47, -.05),)
		if show_colorbar:
			cb = plt.colorbar(Bayes, cax = fig.add_axes(rect_colorbar))

		### Prepare the title

		# First, find the required number of decimals for the +/- signs.

		if np.isfinite(np.log10(mass_interv_1sig[2] - mass_interv_1sig[1])):
			mass_decimals_req_p = int(np.ceil(np.nanmax([2, -np.log10(mass_interv_1sig[2] - mass_interv_1sig[1])])))
		else:
			mass_decimals_req_p = int(2)

		if np.isfinite(np.log10(mass_interv_1sig[1] - mass_interv_1sig[0])):
			mass_decimals_req_n = int(np.ceil(np.nanmax([2, -np.log10(mass_interv_1sig[1] - mass_interv_1sig[0])])))
		else:
			mass_decimals_req_n = int(2)

		mass_decimals_req = np.max([mass_decimals_req_p, mass_decimals_req_n])

		mass_interv_string_p = ("%."+str(mass_decimals_req)+"f") % (mass_interv_1sig[2] - mass_interv_1sig[1])
		mass_interv_string_n = ("%."+str(mass_decimals_req)+"f") % (mass_interv_1sig[1] - mass_interv_1sig[0])

		if np.isfinite(np.log10(np.power(10.,logage_interv_1sig[2]-6) - np.power(10.,logage_interv_1sig[1]-6))):
			logage_decimals_req_p = int(np.ceil(np.nanmax([2, -np.log10(np.power(10.,logage_interv_1sig[2]-6) - np.power(10.,logage_interv_1sig[1]-6))])))
		else:
			logage_decimals_req_p = int(2)

		if np.isfinite(np.log10(np.power(10.,logage_interv_1sig[1]-6) - np.power(10.,logage_interv_1sig[0]-6))):
			logage_decimals_req_n = int(np.ceil(np.nanmax([2, -np.log10(np.power(10.,logage_interv_1sig[1]-6) - np.power(10.,logage_interv_1sig[0]-6))])))
		else:
			logage_decimals_req_n = int(2)

		logage_decimals_req = np.max([logage_decimals_req_p, logage_decimals_req_n])

		logage_interv_string_p = ("%."+str(logage_decimals_req)+"f") % (np.power(10.,logage_interv_1sig[2]-6) - np.power(10.,logage_interv_1sig[1]-6))
		logage_interv_string_n = ("%."+str(logage_decimals_req)+"f") % (np.power(10.,logage_interv_1sig[1]-6) - np.power(10.,logage_interv_1sig[0]-6))

		print_title =\
		("$%."+str(mass_decimals_req)+"f ^ {+%s} _ {-%s} M_\\odot$, $A = %."+str(logage_decimals_req)+"f ^ {+%s} _ {-%s} Myr$") %\
		(mass_interv_1sig[1],
		 mass_interv_string_p,
		 mass_interv_string_n,
		 np.power(10.,logage_interv_1sig[1]-6),
		 logage_interv_string_p,
		 logage_interv_string_n)
		if self.title is not None:
			if len(self.title) > 0:
				print_title = self.title + " " + print_title
		ttl = plt.suptitle(print_title,y=1.01)

		### Get individual ages and masses for each star:
		num_samples = 33

		logTs_random = RandomNumber(logTs, sigma_logTs, num_samples = num_samples).output.flatten()
		logLs_random = RandomNumber(logLs, sigma_logLs, num_samples = num_samples).output.flatten()

		logages_random, masses_random =\
		self.logage_mass_interp.evaluate(logTs_random, logLs_random)

		logages_random = np.reshape(logages_random, (len(logTs), num_samples))
		masses_random = np.reshape(masses_random, (len(logTs), num_samples))

		individual_logages = np.take(logages_random, 0, axis=1)
		individual_logages_e = np.std(logages_random, axis=1)

		individual_masses = np.take(masses_random, 0, axis=1)
		individual_masses_e = np.std(masses_random, axis=1)
		#print individual_logages, individual_masses

		### Create additional plots on the top right.
		axHRD.set_xticklabels([])
		axHRD.set_yticklabels([])

		ax_age_teff = plt.axes(rect_age_teff)
		ax_age_teff.invert_xaxis()
		ax_age_teff.set_xlim(axHRD.get_xlim())
		ax_age_teff.yaxis.set_minor_locator(ticker.MultipleLocator(0.5))
		ax_age_teff.tick_params(axis="y",which="both",direction="in", pad=-22)
		#ax_age_teff.tick_params(axis="x",direction="in", pad=-15)
		ax_age_teff.xaxis.set_label_position("top")
		ax_age_teff.xaxis.tick_top()
		ax_age_teff.set_yticklabels([])
		ax_age_teff.set_xlabel("$log_{10} \\, (T_{eff} \\, [K])$")
		# ax_age_teff.scatter(logTs, individual_logages, marker = ".",
		# s=3)
		ax_age_teff.errorbar(logTs, individual_logages,
		xerr = sigma_logTs, yerr = individual_logages_e,
		fmt = 'none')

		ax_logl_mass = plt.axes(rect_logl_mass)
		ax_logl_mass.set_ylim(axHRD.get_ylim())
		#ax_logl_mass.tick_params(axis="y",direction="in", pad=-22)
		ax_logl_mass.xaxis.set_minor_locator(ticker.MultipleLocator(0.25))
		ax_logl_mass.tick_params(axis="x",which="both",direction="in", pad=-15)
		ax_logl_mass.yaxis.set_label_position("right")
		ax_logl_mass.yaxis.tick_right()
		ax_logl_mass.set_ylabel("$log_{10} \\, L/L_\\odot$")
		ax_logl_mass.set_xticklabels([])
		# ax_logl_mass.scatter(individual_masses, logLs, marker = ".",
		# s=3)
		ax_logl_mass.errorbar(individual_masses, logLs,
		xerr = individual_masses_e, yerr = sigma_logLs,
		fmt = 'none')

		ax_age_mass = plt.axes(rect_age_mass)
		ax_age_mass.set_ylim(ax_age_teff.get_ylim())
		ax_age_mass.set_xlim(ax_logl_mass.get_xlim())
		ax_age_mass.xaxis.set_minor_locator(ticker.MultipleLocator(0.25))
		ax_age_mass.yaxis.set_label_position("right")
		ax_age_mass.yaxis.tick_right()
		ax_age_mass.yaxis.set_minor_locator(ticker.MultipleLocator(0.5))
		ax_age_mass.xaxis.set_label_position("top")
		ax_age_mass.xaxis.tick_top()
		ax_age_mass.set_xlabel("Mass $[M_\\odot]$")
		ax_age_mass.set_ylabel("$log_{10} \\, (Age \\, [yr])$")
		# ax_age_mass.scatter(individual_masses, individual_logages,
		# marker = ".", s=3)
		ax_age_mass.errorbar(individual_masses, individual_logages,
		xerr = individual_masses_e, yerr = individual_logages_e,
		fmt = 'none')
		axBayesian.xaxis.set_tick_params(rotation=60, which = "both")

		if filename is not None:
			figure = plt.gcf() # get current figure
			figure.set_size_inches(12, 12)
			plt.savefig("flip_" + filename,bbox_extra_artists=(ttl,leg,), bbox_inches='tight', dpi = 300)
			figure = plt.gcf() # get current figure
			figure.set_size_inches(6, 6)
			leg.set_visible(False)
			leg = axBayesian.legend(\
			fancybox = True, numpoints = 1, fontsize=8, bbox_to_anchor = (1.47, -.05),)
			if show_colorbar:
				cb = plt.colorbar(Bayes, cax = fig.add_axes(rect_colorbar))
			axHRD_leg.set_visible(False)
			ttl.set_visible(False)
			ttl = plt.suptitle(print_title,x=0.1,y=1.01,horizontalalignment='left')
			axBayesian.xaxis.set_tick_params(labelsize=6., which = "both")

			ax_age_teff.set_xlabel("$log_{10} \\, (T_{eff} \\, [K])$", fontsize=8)
			ax_logl_mass.set_ylabel("$log_{10} \\, L/L_\\odot$", fontsize=8)
			ax_age_mass.set_xlabel("Mass $[M_\\odot]$", fontsize=8)
			ax_age_mass.set_ylabel("$log_{10} \\, (Age \\, [yr])$", fontsize=8)

			plt.savefig("six_in_flip_" + filename,bbox_extra_artists=(ttl,leg,), bbox_inches='tight', dpi = 300)
		else:
			plt.show()
		plt.clf()
		return ([logage_interv_1sig[0], logage_interv_1sig[1], logage_interv_1sig[2]], [mass_interv_1sig[0], mass_interv_1sig[1], mass_interv_1sig[2]])

class MultipleStarsTripleAgeMassCalculator(SingleStarTripleTupleAgeMassCalculator):
	def _evaluate(self, spot_alphas, logTs, logLs, sigma_logTs = None, sigma_logLs = None,
				  title = None,
				  filename = None,
				  min_logage = 5.,
				  max_logage = 8.,
				  min_mass = 0.1,
				  max_mass = 30.,
				  num_bins = 101,
				  n_samples = 1000,
				  mass_prior = priors.chabrier_mass_prior,
				  age_prior = priors.uniform_age_prior,
				  track_label = "MIST",
				  sum_chisq = False,
				  show_colorbar = False,
				  log_mass = False,
				  output_HRD = True,
				  HRD_alpha = 1.):
		"""Returns a tuple with the confidence intervals of the logage
		and then mass if sigma_logTs and sigma_logLs are specified.
		Otherwise, returns a best value with
		the attached IsochroneInterpolator.
		If `filename` is specified, do a plot and save it to the directory
		requested. If it is not specified, show it to the screen.

		n_samples determines the number of samples used to calculate the
		age and mass sigma values.

		sum_chisq is True for summed posteriors,
		False for multiplied posteriors.

		show_colorbar shows the colorbar on the right-hand side of the plot.

		log_mass, when True, forces the plot to be performed on
		a logarithmic scale."""

		### Set the title.
		if title is not None:
			self.title += title

		if track_label is not None:
			self.track_label = track_label

		logage_var = np.linspace(min_logage, max_logage, num_bins)
		if log_mass:
			mass_var = np.logspace(np.log10(min_mass), np.log10(max_mass), num_bins)
		else:
			mass_var = np.linspace(min_mass, max_mass, num_bins)

		logage_grid, mass_grid = np.meshgrid(logage_var, mass_var)
		alpha_grid = np.zeros((num_bins, num_bins)) + np.NaN

		if sum_chisq:
			#DEBUG: sum the posteriors instead.
			posterior = np.zeros(logage_grid.shape)
			for idx in range(len(logTs)):
				alpha_grid.fill(spot_alphas[idx])

				logT_grid, logL_grid =\
				self._logT_logL_interp.\
				evaluate(logage_grid.flatten(), mass_grid.flatten(), alpha_grid.flatten())

				chisq =\
				np.square((logTs[idx] - logT_grid.reshape((num_bins, num_bins)))/sigma_logTs[idx]) +\
				np.square((logLs[idx] - logL_grid.reshape((num_bins, num_bins)))/sigma_logLs[idx])

				ln_likelihood = -0.5*chisq
				ln_posterior =\
				np.log(age_prior(logage_grid))+\
				np.log(mass_prior(mass_grid))+\
				ln_likelihood ###unnormalized

				# Arbitrarily normalize it by setting the ln peak to 0 to prevent
				# overflow issues.
				ln_posterior -= np.nanmax(ln_posterior)

				#Calculate the posterior:
				posterior += np.exp(ln_posterior)
		else:
			chisq = np.zeros((num_bins, num_bins))
			for idx in range(len(logTs)):
				alpha_grid.fill(spot_alphas[idx])

				logT_grid, logL_grid =\
				self._logT_logL_interp.\
				evaluate(logage_grid.flatten(), mass_grid.flatten(), alpha_grid.flatten())

				chisq +=\
				np.square((logTs[idx] - logT_grid.reshape((num_bins, num_bins)))/sigma_logTs[idx]) +\
				np.square((logLs[idx] - logL_grid.reshape((num_bins, num_bins)))/sigma_logLs[idx])

				alpha_grid.fill(np.NaN)

			ln_likelihood = -0.5*chisq
			ln_posterior =\
			np.log(age_prior(logage_grid.flatten()))+\
			np.log(mass_prior(mass_grid.flatten()))+\
			ln_likelihood.flatten() ###unnormalized

			ln_posterior = ln_posterior.reshape((num_bins, num_bins))

			# Arbitrarily normalize it by setting the ln peak to 0 to prevent
			# overflow issues.
			ln_posterior -= np.nanmax(ln_posterior)

			#Calculate the posterior:
			posterior = np.exp(ln_posterior)
		#Condition for being finite:
		finite = np.isfinite(posterior)

		#Normalize the posterior:
		total_logage_width = logage_var[-1] - logage_var[0]
		total_mass_width = mass_var[-1] - mass_var[0]
		posterior = posterior/(\
		np.nansum(posterior[finite])*(total_logage_width * total_mass_width))

		### Convert NaN's to 0's:
		posterior = np.nan_to_num(posterior)

		### Calculate confidence intervals for logage and mass, marginalized:
		### We do this by computing the cdf, and then finding the smallest
		### interval over 68% and 95%, then sort by how much they go over.
		logage_width_right = np.zeros(len(mass_var))
		logage_width_right[0:-1] = logage_var[1:] - logage_var[0:-1]
		logage_width_right[-1] = logage_width_right[-2]

		logage_width_left = np.zeros(len(mass_var))
		logage_width_left[1:] = logage_var[1:] - logage_var[0:-1]
		logage_width_left[0] = logage_width_left[1]

		logage_width = 0.5*(logage_width_left + logage_width_right)

		hist_logage = \
		np.dot(np.reshape(posterior, (num_bins, num_bins)).T, logage_width)\
		/(np.nansum(np.dot(np.reshape(posterior, (num_bins, num_bins)).T, logage_width)))

		mass_width_right = np.zeros(len(mass_var))
		mass_width_right[0:-1] = mass_var[1:] - mass_var[0:-1]
		mass_width_right[-1] = mass_width_right[-2]

		mass_width_left = np.zeros(len(mass_var))
		mass_width_left[1:] = mass_var[1:] - mass_var[0:-1]
		mass_width_left[0] = mass_width_left[1]

		mass_width = 0.5*(mass_width_left + mass_width_right)

		# print "mass width", mass_width
		#
		# print "mass", np.dot(logage_grid, logage_width)
		# print "shape mass", np.dot(logage_grid, logage_width).shape
		#
		# print "logage", np.dot(mass_grid.T, mass_width)
		# print "shape logage", np.dot(mass_grid.T, mass_width).shape

		hist_mass =\
		np.dot(np.reshape(posterior, (num_bins, num_bins)), mass_width)\
		/(np.nansum(np.dot(np.reshape(posterior, (num_bins, num_bins)), mass_width)))

		logage_interv_1sig = self.confidence(logage_var, hist_logage, 0.68)
		logage_interv_2sig = self.confidence(logage_var, hist_logage, 0.95)
		mass_interv_1sig = self.confidence(mass_var, hist_mass, 0.68)
		mass_interv_2sig = self.confidence(mass_var, hist_mass, 0.95)

		### Plot the best values for logage and mass on the central Bayesian plot!
		### Take an weighted average of the logages and masses:
		#best_logage =\
		#np.nansum(logage_f*posterior_f*posterior_f)/np.nansum(posterior_f*posterior_f)
		#best_mass =\
		#np.nansum(mass_f*posterior_f*posterior_f)/np.nansum(posterior_f*posterior_f)

		### Do plotting routines:

		nullfmt = NullFormatter()
		# definitions for the axes
		left, width = 0.1, 0.65
		bottom, height = 0.1, 0.65
		bottom_h = left_h = left + width + 0.02

		rect_bayesian = [left, bottom, width, height]
		rect_histx = [left, bottom_h, width, 0.2]
		rect_histy = [left_h, bottom, 0.2, height]
		# if show_colorbar:
		# 	rect_histx = [left, bottom_h, width, 0.2]
		# 	rect_histy = [left_h, bottom, 0.2, height]
		# else:
		# 	### Allow the histograms to fill through the side plots of the HRD
		# 	rect_histx = [left, bottom_h, width+0., 0.3]
		# 	rect_histy = [left_h, bottom, 0.3, height]
		rect_hrd = [left_h, bottom_h, 0.2, 0.2]
		rect_colorbar = [left_h + 0.22, bottom, 0.01, height]
		### Create age/teff plot on top
		rect_age_teff = [left_h, bottom_h + 0.2, 0.2, 0.1]
		### Create logl/mass plot on bottom
		rect_logl_mass = [left_h + 0.2, bottom_h, 0.1, 0.2]
		### Create age/mass plot on very top right
		rect_age_mass = [left_h + 0.2, bottom_h + 0.2, 0.1, 0.1]
		# start with a rectangular Figure
		fig = plt.figure(1, figsize=(12, 12))
		axBayesian = plt.axes(rect_bayesian)
		axHistx = plt.axes(rect_histx)
		axHisty = plt.axes(rect_histy)
		axHistx.xaxis.set_major_formatter(nullfmt)
		axHisty.yaxis.set_major_formatter(nullfmt)
		axHRD = plt.axes(rect_hrd)
		axHRD.tick_params(axis="y",direction="in", pad=-22)
		axHRD.tick_params(axis="x",direction="in", pad=-15)
		#axHRD.xaxis.set_major_formatter(nullfmt)
		#axHRD.yaxis.set_major_formatter(nullfmt)

		axBayesian.set_xlim((logage_var[0], logage_var[-1]))
		axBayesian.set_ylim((mass_var[0], mass_var[-1]))
		axHistx.set_xlim(axBayesian.get_xlim())
		axHisty.set_ylim(axBayesian.get_ylim())
		axHRD.invert_xaxis()

		isochrones = [["$0.1 \\, Myr$", 5.], ["$1 \\, Myr$", 6.], ["$10 \\, Myr$", 7.], ["$0.1 \\, Gyr$", 8.]]
		mass_tracks = [["$0.1 \\, M_\\odot$", 0.104], ["$0.3 \\, M_\\odot$", 0.3], ["$1 \\, M_\\odot$", 1.], ["$3 \\, M_\\odot$", 3.], ["$10 \\, M_\\odot$", 10.], ["$30 \\, M_\\odot$", 30.]]
		### Plot isochrones and mass tracks:
		#logT_coords = np.linspace(self.range_logT[0], self.range_logT[1])
		#logL_coords = np.linspace(self.range_logL[0], self.range_logL[1])
		sample_points = 1000
		### Set the isochronal and mass track limits:
		min_mass = 0.03
		max_mass = 10.
		min_logage = 0.
		max_logage = 8.
		mass_coords = np.logspace(np.log10(min_mass), np.log10(max_mass), sample_points)
		logage_coords = np.linspace(min_logage, max_logage, sample_points)
		param_array = np.zeros(len(mass_coords))
		for isochrone_txt, isochrone_val in isochrones:
			param_array.fill(isochrone_val)
			#values = self.isochrone_interp(logT_coords, param_array)[1]
			logT_coords, logL_coords =\
			self._logT_logL_interp.evaluate(param_array, mass_coords, np.zeros(len(param_array)) + HRD_alpha)
			cond_plot = np.logical_and(np.isfinite(logT_coords),
									   np.isfinite(logL_coords))
			if np.sum(cond_plot) > 1:
				axHRD.plot(logT_coords[cond_plot], logL_coords[cond_plot],
						   label=isochrone_txt, zorder=2)
				#axHRD.plot(logT_coords[cond_plot], values[cond_plot], label=isochrone_txt)
		for mass_txt, mass_val in mass_tracks:
			param_array.fill(mass_val)
			#values = self.masstrack_interp(logL_coords, param_array)[0]
			logT_coords, logL_coords =\
			self._logT_logL_interp.evaluate(logage_coords, param_array, np.zeros(len(param_array)) + HRD_alpha)
			cond_plot = np.logical_and(np.isfinite(logT_coords),
									   np.isfinite(logL_coords))
			if np.sum(cond_plot) > 1:
				axHRD.plot(logT_coords[cond_plot], logL_coords[cond_plot],
						   linestyle = "dashed", label=mass_txt, zorder=2)
				#axHRD.plot(values[cond_plot], logL_coords[cond_plot], linestyle = "dashed", label=mass_txt)
			#if mass_txt == "$1 \\, M_\\odot$":
				#return logage_coords[np.nanargmax(logT_coords)]
		#labelLines(axHRD.get_lines())
		axHRD.legend(framealpha=0.)
		### Plot the HRD convex hull and the location of the star on this region.
		for simplex in self.hull_hrd.simplices:
			axHRD.plot(self.points_hrd[simplex, 0], self.points_hrd[simplex, 1], 'k-', zorder=1)
		### Plot HRD:
		axHRD.errorbar(logTs, logLs,
					 xerr = sigma_logTs,
					 yerr = sigma_logLs,
					 ecolor='b',
					 fmt=",",
					 color='b',
					 zorder=3)

		### Find a location to put the self.track_label on the HRD:
		#self.track_label_x = np.min(axBayesian.get_xlim())
		#self.track_label_y = np.max(axHRD.get_ylim())
		plt.text(0.98, 0.98, self.track_label, horizontalalignment='right',
				 verticalalignment='top', transform=axHRD.transAxes)

		### Plot the main Bayesian posterior on axBayesian.
		extent = (logage_var[0], logage_var[-1], mass_var[0], mass_var[-1])
		# Reshape the posterior
		posterior = posterior.reshape(logage_grid.shape)
		#Bayes = axBayesian.imshow(posterior, extent = extent,
		#				  origin="lower", aspect="auto")
		X,Y=logage_grid, mass_grid
		Z=posterior
		Bayes = axBayesian.pcolormesh(X,Y,Z, vmin=np.nanmin(Z),
		vmax=np.nanmax(Z))

		### Apply artistic modifications:
		axBayesian.yaxis.set_tick_params(which = "both", length=5)

		if log_mass:
			axBayesian.semilogy()
			axBayesian.tick_params(axis='y', which='minor')
			axBayesian.yaxis.\
			set_minor_formatter(ticker.FormatStrFormatter("%s"))
			axBayesian.yaxis.\
			set_major_formatter(ticker.FormatStrFormatter("%s"))

		### Plot the marginalized histograms on the x and y axes, normalized.
		# Because the interpolation is on a regular grid, the width is simply
		# the distance of a single point to another.
		axHistx.bar(logage_var, hist_logage, width = logage_width)

		axHisty.barh(mass_var, hist_mass, height = mass_width)
		if log_mass:
			axHisty.semilogy()

		### Calculate the level for contour calculations.
		### We get internally-consistent best fit from the models here for free as well.
		posterior_f, logage_f, mass_f =\
		zip(*sorted(zip(posterior.flatten(), logage_grid.flatten(), mass_grid.flatten()), key = lambda x: -x[0]))
		sum_posterior_f = np.nansum(posterior_f)
		posterior_f = np.nan_to_num(posterior_f)
		posterior_sum = 0.
		index_posterior = 0
		while posterior_sum < 0.68:
			posterior_sum += posterior_f[index_posterior]/sum_posterior_f
			index_posterior += 1
		level_1sig = posterior_f[index_posterior]
		while posterior_sum < 0.95:
			posterior_sum += posterior_f[index_posterior]/sum_posterior_f
			index_posterior += 1
		level_2sig = posterior_f[index_posterior]-1e-16
		levels = sorted([level_1sig, level_2sig],key = lambda x:x)
		fmt = {level_1sig: "$68 \\%$", level_2sig: "$95 \\%$"}
		contour = axBayesian.contour(logage_grid, mass_grid,
					 posterior.reshape(logage_grid.shape), levels = levels,
					 colors = "white")
		axBayesian.clabel(contour, contour.levels, inline=True, fmt=fmt, fontsize=10)
		axBayesian.set_xlabel("$log_{10} (Age \\, [yr])$")
		axBayesian.set_ylabel("Mass $[M_\\odot]$")

		### Overplot confidence intervals for logage and mass.
		ci_linewidth = 2
		one_sig_linestyle = "dashed"
		two_sig_linestyle = "dotted"

		axHistx.axvline(logage_interv_1sig[1],
		linewidth=ci_linewidth,
		linestyle="solid",
		label = "Marginal Best")

		axHistx.axvline(logage_interv_1sig[0],
		linewidth=ci_linewidth,
		linestyle=one_sig_linestyle,
		label = "$1 \\, \\sigma$")
		axHistx.axvline(logage_interv_1sig[2],
		linewidth=ci_linewidth,
		linestyle=one_sig_linestyle)

		axHistx.axvline(logage_interv_2sig[0],
		linewidth=ci_linewidth,
		linestyle=two_sig_linestyle,
		label = "$2 \\, \\sigma$")
		axHistx.axvline(logage_interv_2sig[2],
		linewidth=ci_linewidth,
		linestyle=two_sig_linestyle)

		axHisty.axhline(mass_interv_1sig[1],
		linewidth=ci_linewidth,
		linestyle="solid")

		axHisty.axhline(mass_interv_1sig[0],
		linewidth=ci_linewidth,
		linestyle=one_sig_linestyle)
		axHisty.axhline(mass_interv_1sig[2],
		linewidth=ci_linewidth,
		linestyle=one_sig_linestyle)

		axHisty.axhline(mass_interv_2sig[0],
		linewidth=ci_linewidth,
		linestyle=two_sig_linestyle)
		axHisty.axhline(mass_interv_2sig[2],
		linewidth=ci_linewidth,
		linestyle=two_sig_linestyle)

		### Prepare legend for 1 sig, 2 sig on axHistx:
		axHistx.legend()

		#axBayesian.plot(best_logage, best_mass,
		axBayesian.plot(logage_interv_1sig[1], mass_interv_1sig[1],
			marker="*",
			markersize=10,
			color="white",
			alpha = 1.0,
			markeredgecolor="black",
			markeredgewidth=1.0,
			label = "Peak, $log_{10}(Age \\, [yr]) = %.3f$, $M = %.3f \\, M_\\odot$" %\
		(logage_interv_1sig[1], mass_interv_1sig[1]))
		#(best_logage, best_mass))

		leg = axBayesian.legend(\
		fancybox = True, numpoints = 1, fontsize=14, bbox_to_anchor = (1.47, -.05),)
		if show_colorbar:
			cb = plt.colorbar(Bayes, cax = fig.add_axes(rect_colorbar))

		### Prepare the title

		# First, find the required number of decimals for the +/- signs.

		if np.isfinite(np.log10(mass_interv_1sig[2] - mass_interv_1sig[1])):
			mass_decimals_req_p = int(np.ceil(np.nanmax([2, -np.log10(mass_interv_1sig[2] - mass_interv_1sig[1])])))
		else:
			mass_decimals_req_p = int(2)

		if np.isfinite(np.log10(mass_interv_1sig[1] - mass_interv_1sig[0])):
			mass_decimals_req_n = int(np.ceil(np.nanmax([2, -np.log10(mass_interv_1sig[1] - mass_interv_1sig[0])])))
		else:
			mass_decimals_req_n = int(2)

		mass_decimals_req = np.max([mass_decimals_req_p, mass_decimals_req_n])

		mass_interv_string_p = ("%."+str(mass_decimals_req)+"f") % (mass_interv_1sig[2] - mass_interv_1sig[1])
		mass_interv_string_n = ("%."+str(mass_decimals_req)+"f") % (mass_interv_1sig[1] - mass_interv_1sig[0])

		if np.isfinite(np.log10(np.power(10.,logage_interv_1sig[2]-6) - np.power(10.,logage_interv_1sig[1]-6))):
			logage_decimals_req_p = int(np.ceil(np.nanmax([2, -np.log10(np.power(10.,logage_interv_1sig[2]-6) - np.power(10.,logage_interv_1sig[1]-6))])))
		else:
			logage_decimals_req_p = int(2)

		if np.isfinite(np.log10(np.power(10.,logage_interv_1sig[1]-6) - np.power(10.,logage_interv_1sig[0]-6))):
			logage_decimals_req_n = int(np.ceil(np.nanmax([2, -np.log10(np.power(10.,logage_interv_1sig[1]-6) - np.power(10.,logage_interv_1sig[0]-6))])))
		else:
			logage_decimals_req_n = int(2)

		logage_decimals_req = np.max([logage_decimals_req_p, logage_decimals_req_n])

		logage_interv_string_p = ("%."+str(logage_decimals_req)+"f") % (np.power(10.,logage_interv_1sig[2]-6) - np.power(10.,logage_interv_1sig[1]-6))
		logage_interv_string_n = ("%."+str(logage_decimals_req)+"f") % (np.power(10.,logage_interv_1sig[1]-6) - np.power(10.,logage_interv_1sig[0]-6))

		print_title =\
		("$%."+str(mass_decimals_req)+"f ^ {+%s} _ {-%s} M_\\odot$, $A = %."+str(logage_decimals_req)+"f ^ {+%s} _ {-%s} Myr$") %\
		(mass_interv_1sig[1],
		 mass_interv_string_p,
		 mass_interv_string_n,
		 np.power(10.,logage_interv_1sig[1]-6),
		 logage_interv_string_p,
		 logage_interv_string_n)
		if self.title is not None:
			if len(self.title) > 0:
				print_title = self.title + " " + print_title
		ttl = plt.suptitle(print_title,y=1.01,ha='left')

		### Get individual ages and masses for each star:
		num_samples = 33

		logTs_random = RandomNumber(logTs, sigma_logTs, num_samples = num_samples).output.flatten()
		logLs_random = RandomNumber(logLs, sigma_logLs, num_samples = num_samples).output.flatten()

		logages_random, masses_random =\
		self._logage_mass_interp.evaluate(logTs_random, logLs_random, np.zeros(len(logLs_random)) + 1.)

		logages_random = np.reshape(logages_random, (len(logTs), num_samples))
		masses_random = np.reshape(masses_random, (len(logTs), num_samples))

		individual_logages = np.take(logages_random, 0, axis=1)
		individual_logages_e = np.std(logages_random, axis=1)

		individual_masses = np.take(masses_random, 0, axis=1)
		individual_masses_e = np.std(masses_random, axis=1)
		#print individual_logages, individual_masses

		### Create additional plots on the top right.

		ax_age_teff = plt.axes(rect_age_teff)
		ax_age_teff.invert_xaxis()
		ax_age_teff.set_xlim(axHRD.get_xlim())
		ax_age_teff.yaxis.set_minor_locator(ticker.MultipleLocator(0.5))
		ax_age_teff.tick_params(axis="y",direction="in", pad=-22)
		#ax_age_teff.tick_params(axis="x",direction="in", pad=-15)
		ax_age_teff.xaxis.set_label_position("top")
		ax_age_teff.xaxis.tick_top()
		ax_age_teff.set_xlabel("$log_{10} \\, (T_{eff} \\, [K])$")
		ax_age_teff.set_ylabel("$log_{10} \\, (Age \\, [yr])$")
		# ax_age_teff.scatter(logTs, individual_logages, marker = ".",
		# s=3)
		ax_age_teff.errorbar(logTs, individual_logages,
		xerr = sigma_logTs, yerr = individual_logages_e,
		fmt = 'none')

		ax_logl_mass = plt.axes(rect_logl_mass)
		ax_logl_mass.set_ylim(axHRD.get_ylim())
		#ax_logl_mass.tick_params(axis="y",direction="in", pad=-22)
		ax_logl_mass.xaxis.set_minor_locator(ticker.MultipleLocator(0.25))
		ax_logl_mass.tick_params(axis="x",direction="in", pad=-15)
		ax_logl_mass.yaxis.set_label_position("right")
		ax_logl_mass.yaxis.tick_right()
		ax_logl_mass.set_ylabel("$log_{10} \\, L/L_\\odot$")
		ax_logl_mass.set_xlabel("Mass $[M_\\odot]$")
		# ax_logl_mass.scatter(individual_masses, logLs, marker = ".",
		# s=3)
		ax_logl_mass.errorbar(individual_masses, logLs,
		xerr = individual_masses_e, yerr = sigma_logLs,
		fmt = 'none')

		ax_age_mass = plt.axes(rect_age_mass)
		ax_age_mass.set_ylim(ax_age_teff.get_ylim())
		ax_age_mass.set_xlim(ax_logl_mass.get_xlim())
		ax_age_mass.xaxis.set_minor_locator(ticker.MultipleLocator(0.25))
		ax_age_mass.yaxis.set_label_position("right")
		ax_age_mass.yaxis.tick_right()
		ax_age_mass.yaxis.set_minor_locator(ticker.MultipleLocator(0.5))
		ax_age_mass.tick_params(axis="x",direction="in", pad=-15)
		# ax_age_mass.scatter(individual_masses, individual_logages,
		# marker = ".", s=3)
		ax_age_mass.errorbar(individual_masses, individual_logages,
		xerr = individual_masses_e, yerr = individual_logages_e,
		fmt = 'none')

		if filename is not None:
			figure = plt.gcf() # get current figure
			figure.set_size_inches(12, 12)
			plt.savefig(filename,bbox_extra_artists=(ttl,leg,), bbox_inches='tight', dpi = 300)
		else:
			plt.show()
		plt.clf()
		if output_HRD:
			plt.gca().invert_xaxis()

			isochrones = [["$0.1 \\, Myr$", 5.], ["$1 \\, Myr$", 6.], ["$10 \\, Myr$", 7.], ["$0.1 \\, Gyr$", 8.]]
			mass_tracks = [["$0.1 \\, M_\\odot$", 0.104], ["$0.3 \\, M_\\odot$", 0.3], ["$1 \\, M_\\odot$", 1.], ["$3 \\, M_\\odot$", 3.], ["$10 \\, M_\\odot$", 10.], ["$30 \\, M_\\odot$", 30.]]
			### Plot isochrones and mass tracks:
			#logT_coords = np.linspace(self.range_logT[0], self.range_logT[1])
			#logL_coords = np.linspace(self.range_logL[0], self.range_logL[1])
			sample_points = 1000
			### Set the isochronal and mass track limits:
			min_mass = 0.03
			max_mass = 10.
			min_logage = 0.
			max_logage = 8.
			mass_coords = np.logspace(np.log10(min_mass), np.log10(max_mass), sample_points)
			logage_coords = np.linspace(min_logage, max_logage, sample_points)
			param_array = np.zeros(len(mass_coords))
			for isochrone_txt, isochrone_val in isochrones:
				param_array.fill(isochrone_val)
				#values = self.isochrone_interp(logT_coords, param_array)[1]
				logT_coords, logL_coords =\
				self._logT_logL_interp.evaluate(param_array, mass_coords, np.zeros(len(param_array)) + HRD_alpha)
				cond_plot = np.logical_and(np.isfinite(logT_coords),
										   np.isfinite(logL_coords))
				if np.sum(cond_plot) > 1:
					plt.plot(logT_coords[cond_plot], logL_coords[cond_plot],
							   label=isochrone_txt, zorder=2)
			for mass_txt, mass_val in mass_tracks:
				param_array.fill(mass_val)
				#values = self.masstrack_interp(logL_coords, param_array)[0]
				logT_coords, logL_coords =\
				self._logT_logL_interp.evaluate(logage_coords, param_array, np.zeros(len(param_array)) + HRD_alpha)
				cond_plot = np.logical_and(np.isfinite(logT_coords),
										   np.isfinite(logL_coords))
				if np.sum(cond_plot) > 1:
					plt.plot(logT_coords[cond_plot], logL_coords[cond_plot],
							   linestyle = "dashed", label=mass_txt, zorder=2)

			### Plot HRD:
			plt.errorbar(logTs, logLs,
						 xerr = sigma_logTs,
						 yerr = sigma_logLs,
						 ecolor='b',
						 fmt=",",
						 color='b',
						 zorder=3)
			### Plot the 1 sigma solution in age:
			# Get the left logage:
			param_array.fill(logage_interv_1sig[0])
			logT_coords_left, logL_coords_left =\
			self._logT_logL_interp.evaluate(param_array, mass_coords, np.zeros(len(param_array)) + HRD_alpha)
			cond_plot_left = np.logical_and(np.isfinite(logT_coords_left),
									   np.isfinite(logL_coords_left))

			logT_coords_left = logT_coords_left[cond_plot_left]
			logL_coords_left = logL_coords_left[cond_plot_left]

			# Sort:
			logL_coords_left = [x[1] for x in sorted(zip(logT_coords_left, logL_coords_left), key = lambda y: y[0])]
			logT_coords_left = sorted(logT_coords_left)

			# Get the mean logage:
			param_array.fill(logage_interv_1sig[1])
			logT_coords_mean, logL_coords_mean =\
			self._logT_logL_interp.evaluate(param_array, mass_coords, np.zeros(len(param_array)) + HRD_alpha)
			cond_plot_mean = np.logical_and(np.isfinite(logT_coords_mean),
									   np.isfinite(logL_coords_mean))

			logT_coords_mean = logT_coords_mean[cond_plot_mean]
			logL_coords_mean = logL_coords_mean[cond_plot_mean]

			# Sort:
			logL_coords_mean = [x[1] for x in sorted(zip(logT_coords_mean, logL_coords_mean), key = lambda y: y[0])]
			logT_coords_mean = sorted(logT_coords_mean)

			# Get the right logage:
			param_array.fill(logage_interv_1sig[2])
			logT_coords_right, logL_coords_right =\
			self._logT_logL_interp.evaluate(param_array, mass_coords, np.zeros(len(param_array)) + HRD_alpha)
			cond_plot_right = np.logical_and(np.isfinite(logT_coords_right),
									   np.isfinite(logL_coords_right))

			logT_coords_right = logT_coords_right[cond_plot_right]
			logL_coords_right = logL_coords_right[cond_plot_right]

			# Sort:
			logL_coords_right = [x[1] for x in sorted(zip(logT_coords_right, logL_coords_right), key = lambda y: y[0])]
			logT_coords_right = sorted(logT_coords_right)

			### Resample to universal logT coords, which is a subset of the region.
			logT_low = np.nanmax([logT_coords_left[0], logT_coords_right[0], np.nanmin(logT_coords_mean)])
			logT_hi = np.nanmin([logT_coords_left[-1], logT_coords_right[-1], np.nanmax(logT_coords_mean)])

			### Create a subsample on logT:
			logT_new_sample = np.linspace(logT_low, logT_hi, sample_points)

			# Interpolate!
			interp_logL_left = scipy.interpolate.interp1d(logT_coords_left, logL_coords_left)(logT_new_sample)
			interp_logL_mid = scipy.interpolate.interp1d(sorted(logT_coords_mean),
			[y[1] for y in sorted(zip(logT_coords_mean, logL_coords_mean),
			key = lambda x: x[0])])(logT_new_sample)
			interp_logL_right = scipy.interpolate.interp1d(logT_coords_right, logL_coords_right)(logT_new_sample)

			ttl = plt.title(print_title)

			plt.xlabel("$log _{10} (T_{eff})$ $[K]$")
			plt.ylabel("$log _{10} (L/L_\\odot)$")

			plt.plot(logT_coords_mean, logL_coords_mean, zorder = 5, alpha = 0.5, lw = 3, label = "Mean Age isochrone", linestyle = "dotted", color = "black")

			plt.fill_between(logT_new_sample, interp_logL_left, interp_logL_mid,
			zorder = 4, alpha = 0.5, label = "$\\pm 1 \\sigma$", facecolor = "orange")
			plt.fill_between(logT_new_sample, interp_logL_mid, interp_logL_right,
			zorder = 4, alpha = 0.5, facecolor = "orange")

			leg = plt.legend(framealpha=0.)
			if filename is not None:
				fn_array = filename.split("/")
				fn_array[-1] = "HRD_" + fn_array[-1]
				figure = plt.gcf() # get current figure
				figure.set_size_inches(12, 12)
				plt.savefig("/".join(fn_array),bbox_extra_artists=(ttl,leg,), bbox_inches='tight', dpi = 300)
			else:
				plt.show()
			plt.clf()
		return ([logage_interv_1sig[0], logage_interv_1sig[1], logage_interv_1sig[2]], [mass_interv_1sig[0], mass_interv_1sig[1], mass_interv_1sig[2]])
